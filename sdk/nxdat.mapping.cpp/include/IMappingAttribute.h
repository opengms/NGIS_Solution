#ifndef __I_NGISMODEL_MAPPING_ATTRIBUTE_H__
#define __I_NGISMODEL_MAPPING_ATTRIBUTE_H__

#include <string>
#include <vector>

namespace NGIS
{
	namespace DataMapping
	{
		enum ELocalizationType
		{
			ELT_ZH_CN, //中文
			ELT_EN_US, //英文
			ELT_DE_DE, //法文
			ELT_RU_RU, //俄文
			ELT_AR, //阿拉伯文
			ELT_ES, //西班牙文
			ELT_COUNT
		};

		struct LocalAttribute
		{
			LocalAttribute()
			{
				localType = ELT_COUNT;
				localName = "Name";
				abstractInfo = "Abstract";
				wikiUrl = "";
				keywords.clear();
			}

			bool compareOther(LocalAttribute pLocal)
			{
				if (localType != pLocal.localType) return false;
				if (localName != pLocal.localName) return false;
				if (abstractInfo != pLocal.abstractInfo) return false;
				if (wikiUrl != pLocal.wikiUrl) return false;
				if (keywords.size() != pLocal.keywords.size()) return false;
				for (int iKey=0; iKey<keywords.size(); iKey++)
				{
					if (keywords[iKey] != pLocal.keywords[iKey])
					{
						return false;
					}
				}
				return true;
			}

			ELocalizationType localType;
			std::string localName;
			std::string abstractInfo;
			std::string wikiUrl;
			std::vector<std::string> keywords;
		};
	}
}


#endif