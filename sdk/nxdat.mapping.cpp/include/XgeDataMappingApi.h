//!文件属于 NGIS 工程的一部分
//!作者:乐松山
//!创建时间:2017年7月17日

#ifndef __NGIS_DATA_MAPPING_API_H__
#define __NGIS_DATA_MAPPING_API_H__

#include "IMappingMethod.h"

// 在windows上的api开放设置
#ifdef _WINDOWS

#ifndef _NXDATA_MAPPING_STATIC_LIB_
#ifdef NXDATA_MAPPING_EXPORTS
#define NXDATA_MAPPING_API __declspec(dllexport)
#else
#define NXDATA_MAPPING_API __declspec(dllimport)
#endif // NXDATA_MAPPING_API
#else
#define NXDATA_MAPPING_API
#endif // _NXDATA_MAPPING_STATIC_LIB_

// Declare the calling convention.
#if defined(_STDCALL_SUPPORTED)
#define NXDATA_MAPPING_CALLCONV __stdcall
#else
#define NXDATA_MAPPING_CALLCONV __cdecl
#endif // STDCALL_SUPPORTED

#else // _IMP_WINDOWS_API_

// Force symbol export in shared libraries built with gcc.
#if (__GNUC__ >= 4) && !defined(_NXDATA_MAPPING_STATIC_LIB_) && defined(NXDATA_MAPPING_EXPORTS)
#define NXDATA_MAPPING_API __attribute__ ((visibility("default")))
#else
#define NXDATA_MAPPING_API
#endif
#define NXDATA_MAPPING_CALLCONV

#endif

//////////////////////////////////

// 以下用于导出对外开放的API接口
namespace NGIS
{
	namespace DataMapping
	{
		extern "C" NXDATA_MAPPING_API const char* NXDATA_MAPPING_CALLCONV getVersion();

		extern "C" NXDATA_MAPPING_API IModelClass* NXDATA_MAPPING_CALLCONV createModelClass();
	}

}
#endif

