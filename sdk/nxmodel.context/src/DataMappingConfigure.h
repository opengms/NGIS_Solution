#ifndef __C_DATA_MAPPING_CONFIGURE_H__
#define __C_DATA_MAPPING_CONFIGURE_H__

#include <string>
#include <vector>
#include "tinyxml2.h"
#include "IModelDataHandler.h"

namespace NGIS
{
	namespace Model
	{
		class CDataMappingConfigure : public INxUnknown
		{
		private:
			int file_count;
			std::vector<std::string> file_name_collection;
			std::string typical_extension;

		public:
			bool LoadConfigureFile(std::string file_name)
			{
				tinyxml2::XMLDocument doc;
				doc.LoadFile( file_name.c_str() );
				tinyxml2::XMLElement* rootEle = doc.RootElement();
				tinyxml2::XMLElement* nameEle = rootEle->FirstChildElement();
				tinyxml2::XMLElement* formatEle = nameEle->NextSiblingElement();
				tinyxml2::XMLElement* udxSchemaEle = formatEle->NextSiblingElement();

				int file_count = nameEle->IntAttribute("count");
			}

			bool FormatConfigureFile(std::string save_file_name)
			{


			}

			bool AddFileName(std::string file_name)
			{
				for (int i=0; i<file_name_collection.size(); i++)
				{
					std::string temp = file_name_collection[i];
					if (temp == file_name)
					{
						return false;
					}
				}
				file_name_collection.push_back(file_name);
				return false;
			}



		};
	}
}

#endif