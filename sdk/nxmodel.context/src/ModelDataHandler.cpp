#include "ModelDataHandler.h"
#include "XgeModelServiceApi.h"

#include "UdxDataApi.h"
#include "UdxDataSchemaApi.h"

#include "tinyxml2.h"

#include "ModelServiceContext.h"
#include <Windows.h>
#include <io.h>
#include <vector>

using namespace NGIS::Data;
using namespace NGIS::Data::Schema;

namespace NGIS
{
	namespace Model
	{
		extern "C" NGISMODELSERVICE_API IModelDataHandler* NGISMODELSERVICE_CALLCONV createNativeRequestDataHandler(IModelServiceContext* pContext)
		{
			return new CModelDataHandler(pContext);
		}

		std::string& trim(std::string &s)   
		{
			if (s.empty())   
			{  
				return s;  
			}  
			s.erase(0,s.find_first_not_of(" "));  
			s.erase(s.find_last_not_of(" ") + 1);  
			return s;  
		}

		//! 链接数据映射方法，使用相对路径
		void CModelDataHandler::connectDataMappingMethod(const char* pExecutionName)
		{
			mExecutionName = pExecutionName;
			CModelServiceContext* pRealContext = (CModelServiceContext*)mModelServiceContext;

			mExecutionPath = pRealContext->getMappingLibraryDirectory();
			if (mExecutionPath.find_last_of('\\') != mExecutionPath.size()-1)
			{
				mExecutionPath += "\\";
			}
			
			mSavePath = "";
			mSaveName = "";
			mReturnFileFullName = "";
			//configureWorkingDirectory("");
		}

		//! 配置数据映射方法的工作空间，数据映射结果的保存路径
		void CModelDataHandler::configureWorkingDirectory(const char* pSavePath)
		{
			std::string pSavePathStr = pSavePath;
			if (pSavePathStr == "")
			{
				CModelServiceContext* pRealContext = (CModelServiceContext*)mModelServiceContext;
				mSavePath = pRealContext->getModelInstanceDirectory();
				if (mSavePath.find_last_of('\\') != mSavePath.size()-1)
				{
					mSavePath += "\\";
				}
			}
			else
			{
				mSavePath = pSavePath;
				if (mSavePath.find_last_of('\\') != mSavePath.size()-1)
				{
					mSavePath += "\\";
				}
			}
		}

		//! 启用UDX映射原始数据方法，结果保存相对路径
		bool CModelDataHandler::conductUDXMapping(const char* pResultSaveName)
		{
			if (mModelServiceContext->getRequestDataFlag() != ERDF_OK)
			{
				mSavePath = "";
				mSaveName = "";
				mReturnFileFullName = "";
				return false;
			}

			mSavePath = mModelServiceContext->getCurrentDataDirectory();
			mSaveName = trim(std::string(pResultSaveName));

		 	ERequestResponseDataMIME pMIME = mModelServiceContext->getRequestDataMIME();
			if (pMIME== ERDM_XML_FILE)
			{
				const char* dataBody = mModelServiceContext->getRequestDataBody();
				std::string xml_file = dataBody;
				if (mSaveName == "")
				{
					CModelServiceContext* pRealContext = (CModelServiceContext*)mModelServiceContext;
					std::string eventName = pRealContext->getCurrentRunningEvent();
					//! 怎么处理后缀名的问题？？
					mSaveName = eventName + ".raw";
				}

				std::string cmd = mExecutionPath + mExecutionName + " -w -u ";
				cmd += xml_file;
				cmd += " -f ";
				cmd += mSavePath + mSaveName;

				bool retVal = mModelServiceContext->startANewProcess(cmd.c_str(), mSavePath.c_str());

				mReturnFileFullName = mSavePath + mSaveName;
				return retVal;
			}
			else if (pMIME == ERDM_ZIP_FILE)
			{
				std::string dataBody = mModelServiceContext->getRequestDataBody();
				int idx = dataBody.find_last_of('\\');
				std::string dataPath = dataBody.substr(0, idx+1);

				CModelServiceContext* realContext = (CModelServiceContext*)mModelServiceContext;
				std::string runningDir = realContext->getMappingLibraryDirectory();
				
				std::string cmd = runningDir + "miniunz.exe -e -o ";
				cmd += dataBody;
				cmd += " -d ";
				cmd += mSavePath;
				
				bool ret = mModelServiceContext->startANewProcess(cmd.c_str(), mSavePath.c_str());
				if (ret == false)
				{
					return false;
				}

				std::string configureName = "configure.udxcfg";
				std::string configureFileName = mSavePath;
				std::string configureFullName = configureFileName + configureName;
				tinyxml2::XMLDocument doc;
				if (doc.LoadFile(configureFullName.c_str()) != 0)
					return false;

				tinyxml2::XMLElement* rootEle = doc.RootElement();
				tinyxml2::XMLElement* nameEle = rootEle->FirstChildElement();

				std::string fileName="";
				int file_count = nameEle->IntAttribute("count");
				if (file_count == 1)
				{
					tinyxml2::XMLElement* ele=nameEle->FirstChildElement();
					fileName = ele->Attribute("value");

					if (mSaveName=="")
					{
						mSaveName = fileName;
						mReturnFileFullName = mSavePath + mSaveName;
						return true;
					}
					//////////////////////////////////////////////////////////////////////////
					if (mSaveName!="" && mSaveName !=fileName)
					{
						if (mSaveName.find_last_of('.')==mSaveName.size()-1 ||
							mSaveName.find_last_of('.')<0)
						{
							int idx = fileName.find_last_of('.');
							std::string ext = fileName.substr(idx, fileName.size()-idx);
							mSaveName += ext;
						}
						std::string oldName = mSavePath+fileName;
						std::string newName = mSavePath+mSaveName;
						int ret = rename((LPSTR)oldName.c_str(), (LPSTR)newName.c_str());
						if (ret != 0)
						{
							mSaveName = fileName;
						}
						mReturnFileFullName = mSavePath + mSaveName;
					}
				}
				else if (mSaveName!="")
				{
					int last_dot = mSaveName.find_last_of('.');
					if (last_dot >0 && last_dot != mSaveName.size())
					{
						mSaveName = mSaveName.substr(0, last_dot); //去掉后缀名
					}

					for (tinyxml2::XMLElement* ele=nameEle->FirstChildElement(); ele; ele=ele->NextSiblingElement())
					{
						fileName = ele->Attribute("value");
						int idx = fileName.find_last_of('.');
						std::string ext = fileName.substr(idx, fileName.size()-idx);

						std::string oldName = mSavePath+fileName;
						std::string newName = mSavePath+mSaveName+ext;
						int ret = rename((LPSTR)oldName.c_str(), (LPSTR)newName.c_str());
						if (ret == 0)
						{
							mSaveName = fileName;
							break;
						}
					}
					mReturnFileFullName = mSavePath + mSaveName;
				}
				return true;
			}
			else if (pMIME == ERDM_RAW_FILE)
			{
				std::string dataBody = mModelServiceContext->getRequestDataBody();
				int idx = dataBody.find_last_of('\\');
				std::string dataPath = dataBody.substr(0, idx+1);
				std::string dataName = dataBody.substr(idx+1);

				mSavePath = dataPath;
				mSaveName = dataName;
				mReturnFileFullName = mSavePath + mSaveName;
				return true;
			}
			else if (pMIME == ERDM_XML_STREAM)
			{
				//! 暂不支持以数据流的形式进行传递
				//! TODO
			}
			else if (pMIME == ERDM_ZIP_STREAM)
			{
				//! 暂不支持以数据流的形式进行传递
				//! TODO
			}
			else if (pMIME == ERDM_RAW_STREAM)
			{
				//! 暂不支持以数据流的形式进行传递
				//! TODO
			}
			mReturnFileFullName = "";
			return false;
		}

		//! 启用原始数据映射UDX方法，结果保存相对路径
		bool CModelDataHandler::conductFileMapping(const char* pResultSaveName, std::string* rawFiles, int rawFileCount)
		{
			if (mModelServiceContext->getResponseDataFlag() != ERDF_OK)
			{
				mSavePath = "";
				mSaveName = "";
				mReturnFileFullName = "";
				return false;
			}

			mSavePath = mModelServiceContext->getCurrentDataDirectory();
			mSaveName = trim(std::string(pResultSaveName));

			if (mModelServiceContext->getResponseDataFlag() == ERequestResponseDataFlag::ERDF_OK)
			{
				switch (mModelServiceContext->getResponseDataMIME())
				{
				case ERequestResponseDataMIME::ERDM_XML_FILE:
					{
						std::string format_file = rawFiles[0];

						std::string cmd = mExecutionPath + mExecutionName + " -r -f ";
						cmd += format_file;
						cmd += " -u ";
						cmd += mSavePath + mSaveName;

						//! 利用数据映射方法，将原始文件转换成UDX的XML格式
						bool ret = mModelServiceContext->startANewProcess(cmd.c_str(), mSavePath.c_str());
						if (ret)
						{
							FILE* fp = fopen((mSavePath + mSaveName).c_str(), "r");
							if (fp == NULL)
							{
								mSavePath = "";
								mSaveName = "";
								mReturnFileFullName = "";
								return false;
							}
							mReturnFileFullName = mSavePath + mSaveName;
							return true;
						}
						else
						{
							mSavePath = "";
							mSaveName = "";
							mReturnFileFullName = "";
							return false;
						}
					}
					break;
				case ERequestResponseDataMIME::ERDM_ZIP_FILE:
					{
						tinyxml2::XMLDocument doc;
						tinyxml2::XMLElement* rootEle = doc.NewElement("UDXZip");
						doc.LinkEndChild(rootEle);

						tinyxml2::XMLElement* fileNameEle = doc.NewElement("FileName");
						rootEle->LinkEndChild(fileNameEle);
						fileNameEle->SetAttribute("count", rawFileCount);
						for (int iFile=0; iFile<rawFileCount; iFile++)
						{
							tinyxml2::XMLElement* fileItemEle = doc.NewElement("Add");
							fileNameEle->LinkEndChild(fileItemEle);

							std::string temp_raw_file = rawFiles[iFile];
							int idx = temp_raw_file.find_last_of('\\');
							temp_raw_file = temp_raw_file.substr(idx);
							fileItemEle->SetAttribute("value", temp_raw_file.c_str());
						}
						//! TODO Add UDX Schema	

						std::string cfgFile = mSavePath + "configure.udxcfg";
						doc.SaveFile(cfgFile.c_str());

						CModelServiceContext* realContext = (CModelServiceContext*)mModelServiceContext;
						std::string runningDir = realContext->getMappingLibraryDirectory();

						std::string cmd = runningDir + "minizip.exe -o -5 -j "; //如果加上访问码的话 -p 123
						cmd += mSavePath+mSaveName;
						for (int iFile=0; iFile<rawFileCount; iFile++)
						{
							cmd += " " + rawFiles[iFile];	
						}
						cmd += " " + cfgFile;

						bool ret = mModelServiceContext->startANewProcess(cmd.c_str(), mSavePath.c_str());
						if (ret==false)
						{
							mSavePath = "";
							mSaveName = "";
							mReturnFileFullName = "";
							return false;
						}
						mReturnFileFullName = mSavePath + mSaveName;
						return true;
					}
					break;
				case ERequestResponseDataMIME::ERDM_RAW_FILE:
					{
						for (int iFile=0; iFile<rawFileCount; iFile++)
						{
							std::string temp_raw_file = rawFiles[iFile];
							mReturnFileFullName += temp_raw_file;
							if (iFile!=rawFileCount-1)
							{
								mReturnFileFullName += ";";
							}
						}
						return true;
					}
					break;
				case ERequestResponseDataMIME::ERDM_XML_STREAM:
					{
						//! 暂不支持以数据流的形式进行传递
						//! TODO
					}
					break;
				case ERequestResponseDataMIME::ERDM_ZIP_STREAM:
					{
						//! 暂不支持以数据流的形式进行传递
						//! TODO
					}
					break;
				case ERequestResponseDataMIME::ERDM_RAW_STREAM:
					{
						//! 暂不支持以数据流的形式进行传递
						//! TODO
					}
					break;
				}
			}
			return false;
		}

		//! 启用原始数据映射UDX方法，结果保存相对路径
		bool CModelDataHandler::conductFileMapping(const char* pResultSaveName, std::string rawFile)
		{
			std::vector<std::string> rawFiles;
			rawFiles.push_back(rawFile);
			return conductFileMapping(pResultSaveName, rawFiles.data(), 1);
		}

		//! 获取真实的数据名
		const char* CModelDataHandler::getRealResultSaveName()
		{
			return mReturnFileFullName.c_str();
		}


	}
}