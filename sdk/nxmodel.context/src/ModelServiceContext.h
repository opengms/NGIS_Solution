#ifndef __C_MODEL_SERVICE_CONTEXTI_H__
#define __C_MODEL_SERVICE_CONTEXTI_H__

#include "IModelServiceContext.h"
#include <stdio.h>
#include <string>
#include "pthread.h"

#ifdef _WINDOWS
#include <WinSock2.h>
#include <io.h>
#include <direct.h>
#pragma comment(lib,"ws2_32.lib")
#endif

#ifdef linux   
#include <unistd.h>
#include <sys/types.h>  
#include <sys/stat.h>
#endif


namespace NGIS
{
	namespace Model
	{
		class CModelServiceContext : public IModelServiceContext
		{
		public:
			CModelServiceContext()
			{
				mContextStatus = EMCS_UNKOWN;
			}

			~CModelServiceContext()
			{

			}

		public:
			//! 触发-初始化
			virtual nxUID onInitialize(const char* pHost, const char* pPort, const char* pInstanceID);

			//! 触发-进入状态
			virtual int onEnterState(nxUID pInstanceId, const char* pStateId);

			//! 触发-引发事件
			virtual int onFireEvent(nxUID pInstanceId, const char* pStateId, const char* pEventId);

			//! 触发-向外请求数据
			virtual int onRequestData(nxUID pInstanceId, const char* pStateId, const char* pEventId);

			//! 触发-向外提供数据
			virtual int onResponseData(nxUID pInstanceId, const char* pStateId, const char* pEventId);

			//! 触发-提交错误信息
			virtual int onPostErrorInfo(nxUID pInstanceId, const char* pErrorInfo);

			//! 触发-离开状态
			virtual int onLeaveState(nxUID pInstanceId, const char* pStateId);

			//! 触发-完成
			virtual int onFinalize(nxUID pInstanceId);

			//////////////////////////////////////////////////////////////////////////
			//! 获取请求到的数据Flag
			virtual ERequestResponseDataFlag getRequestDataFlag()
			{
				return mRequestDataFlag;
			}

			//! 获取请求到的数据类型
			virtual ERequestResponseDataMIME getRequestDataMIME()
			{
				return mRequestDataMIME;
			}

			//! 获取请求到的数据内容
			virtual const char* getRequestDataBody()
			{
				return mRequestDataBody.c_str();
			}

			//! 设置发送的数据标记Flag
			virtual void setResponseDataFlag(ERequestResponseDataFlag pFlag)
			{
				mResponseDataFlag = pFlag;
			}

			//! 设置发送的数据MIME类型
			virtual void setResponseDataMIME(ERequestResponseDataMIME pMIME)
			{
				mResponseDataMIME = pMIME;
			}

			//! 设置发送的数据内容
			virtual void setResponseDataBody(const char* pDataBody)
			{
				mResponseDataBody = pDataBody;
			}

			//! 获取发送的数据Flag
			virtual ERequestResponseDataFlag getResponseDataFlag()
			{
				return mResponseDataFlag;
			}

			//! 获取发送的数据类型
			virtual ERequestResponseDataMIME getResponseDataMIME()
			{
				return mResponseDataMIME;
			}

			//! 获取发送的数据内容
			virtual const char* getResponseDataBody()
			{
				return mResponseDataBody.c_str();
			}

			//! 获取当前与外部通信的状态
			virtual EModelContextStatus getCurrentStatus()
			{
				return mContextStatus;
			}

			//////////////////////////////////////////////////////////////////////////
			//! 启动一个外部进程实例
			virtual bool startANewProcess(const char* cmd, const char* workingPath);

			//////////////////////////////////////////////////////////////////////////
			//! 获取当前运行过程中的数据目录
			virtual std::string getCurrentDataDirectory() 
			{
				std::string instanceDir = getModelInstanceDirectory();
				if (createDirectory(instanceDir) == "")
					return "";
				
				std::string stateDir = getCurrentRunningState() + "\\";
				std::string eventDir = getCurrentRunningEvent() + "\\";
			
				std::string statePath = instanceDir + stateDir;
				if (createDirectory(statePath) == "")
					return "";

				std::string eventPath = statePath + eventDir;
				if (createDirectory(eventPath) == "")
					return "";

				return eventPath;
			}
			
			std::string createDirectory(std::string path)
			{
				if (access(path.c_str(), 0)== 0)
				{
					return path;
				}
				else
				{
#ifdef _WINDOWS  
					int flag=mkdir(path.c_str());
					if (flag == 0)
						return path;
					return "";
#endif  
#ifdef linux   
					int flag=mkdir(dir.c_str(), 0777);  
					if (flag == 0)
						return path;
					return "";
#endif  
				}
			}

		public:
			//! 绑定Socket通信
			int bindSocket();

			//! 发送消息
			int sendMessage(const std::string &pMessage);

			//! 接收消息
			int receiveMessage(std::string &pMessage);

			//! 关闭当前连接
			void closeSocket();

			//! 等待消息接收，直到消息接收后改变当前的通信状态
			void wait(int seconds, EModelContextStatus pStatus);

			//! 设置当前的通信状态
			void setCurrentContextStatus(EModelContextStatus pStatus);

			//! 设置当前接收到的信息
			void setCurrentReceivedInfo(std::string pInfo);

			//! 获取数据映射方法目录
			std::string getMappingLibraryDirectory();

			//! 获取模型运行实例的数据目录
			std::string getModelInstanceDirectory();

			//! 获取模型运行实例当前的State
			std::string getCurrentRunningState() { return mCurrentState; }

			//! 获取模型运行实例当前的Event
			std::string getCurrentRunningEvent() { return mCurrentEvent; }

		private:
			pthread_t										mThreadId; //! 异步的消息接收线程
			EModelContextStatus					mContextStatus; //! 异步消息接收线程控制的当前消息通信状态

			int												mPort;  //! 服务容器的IP地址
			std::string										mHost; //! 服务容器的IP端口
			nxUID											mInstaceId; //! 当前模型运行实例的唯一ID
			std::string										mMappingLibDir; //! 数据映射方法统一安装目录
			std::string										mInstanceDir; //! 当前模型运行实例的数据目录（以State文件夹组织）

			SOCKET										mClientSocket; //! 模型运行实例和服务容器之间的通信接口
			char												mSendBuf[200]; //! 发送消息的缓存
			char												mReceiveBuf[1024]; //! 接收消息的缓存

			std::string										mReceivedString; //! 接收到的字符串
			
			std::string										mCurrentState; //! 模型实例运行过程中当前的State
			std::string										mCurrentEvent; //! 模型实例运行过程中当前的Event
			
			ERequestResponseDataFlag			mRequestDataFlag; //! 模型运行过程中请求到的数据的Flag（OK或者其他）
			ERequestResponseDataMIME		mRequestDataMIME; //! 模型运行过程中请求到的数据的MIME（UDX-XML, UDX-ZIP, RAW-DATA）
			std::string										mRequestDataBody; //! 模型运行过程中请求到的数据的内容的主体（UDX的XML流，或者文件路径）

			ERequestResponseDataFlag			mResponseDataFlag; //! 模型运行过程中分发出去的数据的Flag（OK或者其他）
			ERequestResponseDataMIME		mResponseDataMIME; //! 模型运行过程中分发出去的数据的MIME（UDX-XML, UDX-ZIP, RAW-DATA）
			std::string										mResponseDataBody; //! 模型运行过程中分发出去的数据的主体（UDX的XML流，或者文件路径）
		};
	}
}
#endif