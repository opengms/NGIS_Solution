#ifndef __C_REQUEST_DATA_HANDLER_H__
#define __C_REQUEST_DATA_HANDLER_H__

#include <string>
#include "IModelDataHandler.h"
#include "IModelServiceContext.h"

namespace NGIS
{
	namespace Model
	{
		class CModelDataHandler : public IModelDataHandler
		{
		public:
			CModelDataHandler(IModelServiceContext* pContext)
			{
				mModelServiceContext = pContext;
				if (mModelServiceContext)
					mModelServiceContext->addRef();
			}

			~CModelDataHandler()
			{
				if (mModelServiceContext)
					mModelServiceContext->release();
			}

		public:
			//! 链接数据映射方法，使用相对路径
			virtual void connectDataMappingMethod(const char* pExecutionName);

			//! 配置数据映射方法的工作空间，数据映射结果的保存路径
			virtual void configureWorkingDirectory(const char* pSavePath);

			//! 启用UDX映射原始数据方法，结果保存相对路径
			virtual bool conductUDXMapping(const char* pResultSaveName);

			//! 启用原始数据映射UDX方法，结果保存相对路径
			virtual bool conductFileMapping(const char* pResultSaveName, std::string* rawFiles, int rawFileCount);

			//! 启用原始数据映射UDX方法，结果保存相对路径
			virtual bool conductFileMapping(const char* pResultSaveName, std::string rawFile);

			//! 获取真实的数据名
			virtual const char* getRealResultSaveName();

		private:
			std::string								mExecutionPath;
			std::string								mExecutionName;

			std::string								mSavePath;
			std::string								mSaveName;

			std::string								mReturnFileFullName;
			
			IModelServiceContext*			mModelServiceContext;
		};

	}
}

#endif