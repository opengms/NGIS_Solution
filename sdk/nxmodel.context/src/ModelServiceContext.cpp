#include "ModelServiceContext.h"
#include "XgeModelServiceApi.h"

namespace NGIS
{
	namespace Model
	{
		extern "C" NGISMODELSERVICE_API const char* NGISMODELSERVICE_CALLCONV getVersion()
		{
			return "2.0";
		}

		extern "C" NGISMODELSERVICE_API IModelServiceContext* NGISMODELSERVICE_CALLCONV createModelServiceContext()
		{
			return new CModelServiceContext();
		}

		int CModelServiceContext::bindSocket()
		{
			WSADATA wsaData = { 0 };//存放套接字信息
			mClientSocket = INVALID_SOCKET;//客户端套接字
			SOCKADDR_IN ServerAddr = { 0 };//服务端地址
			USHORT uPort = mPort;//服务端端口
			//初始化套接字
			if (WSAStartup(MAKEWORD(2, 2), &wsaData))
			{
				return -1;
			}
			//判断套接字版本
			if (LOBYTE(wsaData.wVersion) != 2 || HIBYTE(wsaData.wVersion) != 2)
			{
				return -1;
			}
			//创建套接字
			mClientSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
			if (mClientSocket == INVALID_SOCKET)
			{
				return -1;
			}
			//设置服务器地址
			ServerAddr.sin_family = AF_INET;
			ServerAddr.sin_port = htons(uPort);//服务器端口
			ServerAddr.sin_addr.S_un.S_addr = inet_addr(mHost.c_str());//服务器地址

			//连接服务器
			if (SOCKET_ERROR == connect(mClientSocket, (SOCKADDR*)&ServerAddr, sizeof(ServerAddr)))
			{
				closesocket(mClientSocket);
				WSACleanup();
				return -1;
			}
			return 0;
		}

		int CModelServiceContext::sendMessage(const std::string &pMessage)
		{
			if (pMessage.size() == 0)
				return -1;
			int size = pMessage.size()+1;
			send(mClientSocket, pMessage.c_str(), size, 0);
			return 0;
		}

		int CModelServiceContext::receiveMessage(std::string &pMessage)
		{
			memset(mReceiveBuf, 0, 1024);
			recv(mClientSocket, mReceiveBuf, 1024 , 0);
			pMessage = mReceiveBuf;
			if (pMessage.size() <= 0)
				return -1;
			return 0;
		}

		void CModelServiceContext::closeSocket()
		{
			closesocket(mClientSocket);
			WSACleanup();
		}

		void CModelServiceContext::wait(int seconds, EModelContextStatus pStatus)
		{
			while(true)
			{
				if(this->mContextStatus == pStatus)
				{
					break;
				}
				Sleep(seconds);
			}
		}

		void CModelServiceContext::setCurrentContextStatus(EModelContextStatus pStatus)
		{
			mContextStatus = pStatus;
		}

		void CModelServiceContext::setCurrentReceivedInfo( std::string pInfo )
		{
			mReceivedString = pInfo;
		}

		std::string CModelServiceContext::getMappingLibraryDirectory()
		{
			if (mMappingLibDir.find_last_of('\\') != mMappingLibDir.size()-1)
			{
				mMappingLibDir += "\\";
			}
			return mMappingLibDir;
		}

		std::string CModelServiceContext::getModelInstanceDirectory()
		{
			if (mInstanceDir.find_last_of('\\') != mInstanceDir.size()-1)
			{
				mInstanceDir += "\\";
			}
			return mInstanceDir;
		}

		void* pthread_function(void* pContext)
		{
			CModelServiceContext* realContext = (CModelServiceContext*)pContext;

			while(1) 
			{
				std::string receiveBuf="";
				int i = realContext->receiveMessage(receiveBuf);
				if (i<0) continue;

				printf("********");printf(receiveBuf.c_str()); printf("\n");

				realContext->setCurrentReceivedInfo(receiveBuf);
				int idx = receiveBuf.find_first_of('}');
				std::string header = receiveBuf.substr(0, idx+1);
				if (header == "{Initialized}")
				{
					realContext->setCurrentContextStatus(EMCS_INIT_END);
				}
				else if (header == "{Enter State Notified}")
				{
					realContext->setCurrentContextStatus(EMCS_STATE_ENTER_END);
				}
				else if (header == "{Fire Event Notified}")
				{
					realContext->setCurrentContextStatus(EMCS_EVENT_END);
				}
				else if (header == "{Request Data Notified}")
				{
					realContext->setCurrentContextStatus(EMCS_REQUEST_END);
				}
				else if (header == "{Response Data Notified}")
				{
					realContext->setCurrentContextStatus(EMCS_RESPONSE_END);
				}
				else if (header == "{Response Data Received}")
				{
					realContext->setCurrentContextStatus(EMCS_RESPONSE_END);
				}
				else if (header == "{Post Error Info Notified}")
				{
					realContext->setCurrentContextStatus(EMCS_POST_END);
				}
				else if (header == "{Leave State Notified}")
				{
					realContext->setCurrentContextStatus(EMCS_STATE_LEAVE_END);
				}
				else if (header == "{Finalize Notified}")
				{
					realContext->setCurrentContextStatus(EMCS_FINALIZE_END);
					break;
				}

				if (header == "{kill}")
					exit(0);

				Sleep(1000);
			}
			return NULL;
		} 

		nxUID CModelServiceContext::onInitialize(const char* pHost, const char* pPort, const char* pInstanceID)
		{
			mHost = pHost;
			mPort = atoi(pPort);
			mInstaceId = pInstanceID;

			int retVal = bindSocket();
			if (retVal !=0) return "";

			//! 启动监听线程
			if(pthread_create(&mThreadId, NULL, pthread_function, this)!=0)
			{
				printf("create thread error\n");
			}

			this->mContextStatus = EMCS_INIT_BEGIN;
			this->mContextStatus = EMCS_INIT;
			memset(mSendBuf, 0, 200);
			sprintf(mSendBuf, "{init}%s", mInstaceId);
			sendMessage(mSendBuf);

			this->wait(1000, EMCS_INIT_END);

			int idx = mReceivedString.find_first_of('}');
			std::string header = mReceivedString.substr(0, idx+1);
			std::string body = mReceivedString.substr(idx+1, mReceivedString.size()-idx-1);

			int idx1 = body.find_first_of('[');
			int idx2 = body.find_first_of(']');
			mMappingLibDir = body.substr(idx1+1, idx2-idx1-1);
			
			idx1 = body.find_last_of('[');
			idx2 = body.find_last_of(']');
			mInstanceDir = body.substr(idx1+1, idx2-idx1-1);

			return mInstaceId;
		}

		int CModelServiceContext::onEnterState(nxUID pInstanceId, const char* pStateId)
		{
			mCurrentState = pStateId;
			this->mContextStatus = EMCS_STATE_ENTER_BEGIN;
			memset(mSendBuf, 0, 200);
			sprintf(mSendBuf, "{onEnterState}%s&%s",pInstanceId, pStateId);
			this->mContextStatus = EMCS_STATE_ENTER;
			sendMessage(mSendBuf);

			this->wait(1000, EMCS_STATE_ENTER_END);

			int idx = mReceivedString.find_first_of('}');
			std::string header = mReceivedString.substr(0, idx+1);
			std::string body = mReceivedString.substr(idx+1, mReceivedString.size()-idx-1);

			header += "\n"; printf(header.c_str());
			return 0;
		}

		int CModelServiceContext::onFireEvent(nxUID pInstanceId, const char* pStateId, const char* pEventId)
		{
			mCurrentEvent = pEventId;
			this->mContextStatus = EMCS_EVENT_BEGIN;
			memset(mSendBuf, 0, 200);
			sprintf(mSendBuf, "{onFireEvent}%s&%s&%s",pInstanceId, pStateId,pEventId);
			this->mContextStatus = EMCS_EVENT;
			sendMessage(mSendBuf);

			this->wait(1000, EMCS_EVENT_END);

			int idx = mReceivedString.find_first_of('}');
			std::string header = mReceivedString.substr(0, idx+1);
			std::string body = mReceivedString.substr(idx+1, mReceivedString.size()-idx-1);

			header += "\n"; printf(header.c_str());
			return 0;
		}

		int CModelServiceContext::onRequestData(nxUID pInstanceId, const char* pStateId, const char* pEventId)
		{
			this->mContextStatus = EMCS_REQUEST_BEGIN;
			memset(mSendBuf, 0, 200);
			sprintf(mSendBuf, "{onRequestData}%s&%s&%s",pInstanceId, pStateId,pEventId);
			this->mContextStatus = EMCS_REQUEST;
			sendMessage(mSendBuf);

			this->wait(1000, EMCS_REQUEST_END);

			int idx = mReceivedString.find_first_of('}');
			std::string header = mReceivedString.substr(0, idx+1);
			idx = mReceivedString.find_last_of(']');
			std::string body = mReceivedString.substr(idx+1, mReceivedString.size()-idx-1);

			header += "\n"; printf(header.c_str());

			//////////////////////////////////////////////////////////////////////////
			int idx1 = mReceivedString.find_first_of('[');
			int idx2 = mReceivedString.find_first_of(']');
			std::string data_flag = mReceivedString.substr(idx1+1, idx2-idx1-1);
			
			if (data_flag == "OK")
			{
				mRequestDataFlag = ERequestResponseDataFlag::ERDF_OK;
			}
			else
			{
				mRequestDataMIME = ERequestResponseDataMIME::ERDM_UNKNOW;
				mRequestDataBody = "";
				return 0;
			}

			idx1 = mReceivedString.find_last_of('[');
			idx2 = mReceivedString.find_last_of(']');
			std::string data_mime = mReceivedString.substr(idx1+1, idx2-idx1-1);

			if (data_mime == "XML|STREAM")
			{
				mRequestDataMIME = ERequestResponseDataMIME::ERDM_XML_STREAM;
			}
			else if (data_mime == "ZIP|STREAM")
			{
				mRequestDataMIME = ERequestResponseDataMIME::ERDM_ZIP_STREAM;
			}
			else if (data_mime == "RAW|STREAM")
			{
				mRequestDataMIME = ERequestResponseDataMIME::ERDM_RAW_STREAM;
			}
			else if (data_mime == "XML|FILE")
			{
				mRequestDataMIME = ERequestResponseDataMIME::ERDM_XML_FILE;
			}
			else if (data_mime == "ZIP|FILE")
			{
				mRequestDataMIME = ERequestResponseDataMIME::ERDM_ZIP_FILE;
			}
			else if (data_mime == "RAW|FILE")
			{
				mRequestDataMIME = ERequestResponseDataMIME::ERDM_RAW_FILE;
			}

			mRequestDataBody = body;

			return 0;
		}

		int CModelServiceContext::onResponseData(nxUID pInstanceId, const char* pStateId, const char* pEventId)
		{
			if (mResponseDataFlag == ERDF_OK)
			{
				this->mContextStatus = EMCS_RESPONSE_BEGIN;
				int size = mResponseDataBody.size();
				memset(mSendBuf, 0, 200);
				sprintf(mSendBuf, "{onResponseData}%s&%s&%s&%d[OK]",pInstanceId, pStateId,pEventId,size);
				std::string sendBufStr = mSendBuf;

				if (mResponseDataMIME == ERDM_XML_STREAM)
				{
					sendBufStr += "[XML|STREAM]";
				}
				else if (mResponseDataMIME == ERDM_ZIP_STREAM)
				{
					sendBufStr += "[ZIP|STREAM]";
				}
				else if (mResponseDataMIME == ERDM_RAW_STREAM)
				{
					sendBufStr += "[RAW|STREAM]";
				}
				else if (mResponseDataMIME == ERDM_XML_FILE)
				{
					sendBufStr += "[XML|FILE]";
				}
				else if (mResponseDataMIME == ERDM_ZIP_FILE)
				{
					sendBufStr += "[ZIP|FILE]";
				}
				else if (mResponseDataMIME == ERDM_RAW_FILE)
				{
					sendBufStr += "[RAW|FILE]";
				}
				sendBufStr += mResponseDataBody;
				this->mContextStatus = EMCS_RESPONSE;
				sendMessage(sendBufStr);

				this->wait(1000, EMCS_RESPONSE_END);

				mReceivedString += "\n"; printf(mReceivedString.c_str());
			}
			else if (mResponseDataFlag == ERDF_ERROR)
			{
				this->mContextStatus = EMCS_RESPONSE_BEGIN;
				memset(mSendBuf, 0, 200);
				sprintf(mSendBuf, "{onResponseData}%s&%s&%s&%d[ERROR]",pInstanceId, pStateId,pEventId,0);
				this->mContextStatus = EMCS_RESPONSE;
				sendMessage(mSendBuf);

				this->wait(1000, EMCS_RESPONSE_END);

				int idx = mReceivedString.find_first_of('}');
				std::string header = mReceivedString.substr(0, idx+1);
				std::string body = mReceivedString.substr(idx+1, mReceivedString.size()-idx-1);

				header += "\n"; printf(header.c_str());
			}
			else if (mResponseDataFlag == ERDF_NOTREADY)
			{
				this->mContextStatus = EMCS_RESPONSE_BEGIN;
				int size = mResponseDataBody.size();
				memset(mSendBuf, 0, 200);
				sprintf(mSendBuf, "{onResponseData}%s&%s&%s&%d[NOTREADY]",pInstanceId, pStateId,pEventId,size);
				this->mContextStatus = EMCS_RESPONSE;
				sendMessage(mSendBuf);

				this->wait(1000, EMCS_RESPONSE_END);

				int idx = mReceivedString.find_first_of('}');
				std::string header = mReceivedString.substr(0, idx+1);
				std::string body = mReceivedString.substr(idx+1, mReceivedString.size()-idx-1);

				header += "\n"; printf(header.c_str());
			}
			
			return 0;
		}

		int CModelServiceContext::onPostErrorInfo(nxUID pInstanceId, const char* pErrorInfo)
		{
			this->mContextStatus = EMCS_POST_BEGIN;
			memset(mSendBuf, 0, 200);
			sprintf(mSendBuf, "{onPostErrorInfo}%s&%s",pInstanceId,pErrorInfo);
			this->mContextStatus = EMCS_POST;
			sendMessage(mSendBuf);

			this->wait(1000, EMCS_POST_END);

			int idx = mReceivedString.find_first_of('}');
			std::string header = mReceivedString.substr(0, idx+1);
			std::string body = mReceivedString.substr(idx+1, mReceivedString.size()-idx-1);

			header += "\n"; printf(header.c_str());

			return 0;
		}

		int CModelServiceContext::onLeaveState(nxUID pInstanceId, const char* pStateId)
		{
			this->mContextStatus = EMCS_STATE_LEAVE_BEGIN;
			memset(mSendBuf, 0, 200);
			sprintf(mSendBuf, "{onLeaveState}%s&%s",pInstanceId, pStateId);
			this->mContextStatus = EMCS_STATE_LEAVE;
			sendMessage(mSendBuf);

			this->wait(1000, EMCS_STATE_LEAVE_END);

			int idx = mReceivedString.find_first_of('}');
			std::string header = mReceivedString.substr(0, idx+1);
			std::string body = mReceivedString.substr(idx+1, mReceivedString.size()-idx-1);

			header += "\n"; printf(header.c_str());

			return 0;
		}

		int CModelServiceContext::onFinalize(nxUID pInstanceId)
		{
			this->mContextStatus = EMCS_FINALIZE_BEGIN;
			memset(mSendBuf, 0, 200);
			sprintf(mSendBuf, "{onFinalize}%s",pInstanceId);
			this->mContextStatus = EMCS_FINALIZE;
			sendMessage(mSendBuf);

			this->wait(1000, EMCS_FINALIZE_END);

			int idx = mReceivedString.find_first_of('}');
			std::string header = mReceivedString.substr(0, idx+1);
			std::string body = mReceivedString.substr(idx+1, mReceivedString.size()-idx-1);

			header += "\n"; printf(header.c_str());

			pthread_cancel(mThreadId);
			closeSocket();

			return 0;
		}

		bool CModelServiceContext::startANewProcess(const char* cmd_str, const char* workingPath_str)
		{
#ifdef _WINDOWS_
			std::string cmd = cmd_str;
			std::string workingPath = workingPath_str;

			char sSysDir[MAX_PATH] = {0};  
			GetSystemDirectory(sSysDir, MAX_PATH);  
			std::string strFullPath = sSysDir;  
			strFullPath += ":\\cmd.exe";  
			std::string strCmdLine = " /C "; 
			strCmdLine += cmd;

			STARTUPINFO StartInfo; 
			PROCESS_INFORMATION pinfo; 

			memset(&StartInfo,0,sizeof(STARTUPINFO)); //对程序的启动信息不作任何设定，全部清0 
			StartInfo.cb = sizeof(STARTUPINFO);//设定结构的大小 
			StartInfo.dwFlags = STARTF_USESHOWWINDOW;  
			StartInfo.wShowWindow = SW_SHOW;

			BOOL ret=CreateProcess(NULL, (LPSTR)cmd.c_str(), NULL,NULL,false,
				NORMAL_PRIORITY_CLASS,NULL, 
				(LPSTR)workingPath.c_str(),
				&StartInfo,&pinfo);

			DWORD dwExitCode;  
			if(ret)  
			{
				CloseHandle(pinfo.hThread);  
				WaitForSingleObject(pinfo.hProcess,INFINITE);  
				GetExitCodeProcess(pinfo.hProcess, &dwExitCode);  
				CloseHandle(pinfo.hProcess);  
				return true;
			}
			else
			{
				return false;
			}
#else

			return false;
#endif
		}

	}
}