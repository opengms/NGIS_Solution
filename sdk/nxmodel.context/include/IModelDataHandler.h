#ifndef __I_REQUEST_DATA_HANDLER_H__
#define __I_REQUEST_DATA_HANDLER_H__

#include "INxUnknown.h"
#include <string>

namespace NGIS
{
	namespace Model
	{
		enum ERequestResponseDataFlag
		{
			ERDF_OK,
			ERDF_NOTREADY,
			ERDF_ERROR,
			ERDF_UNKNOW
		};

		enum ERequestResponseDataMIME
		{
			ERDM_XML_STREAM,
			ERDM_ZIP_STREAM,
			ERDM_RAW_STREAM,
			ERDM_XML_FILE,
			ERDM_ZIP_FILE,
			ERDM_RAW_FILE,
			ERDM_UNKNOW
		};
		class IModelServiceContext;
		class IModelDataHandler;
		class IDataMappingMethod;

		class IModelDataHandler : public INxUnknown
		{
		public:
			//! 链接数据映射方法，使用相对路径
			virtual void connectDataMappingMethod(const char* pExecutionName) = 0;

			//! 配置数据映射方法的工作空间，数据映射结果的保存路径
			virtual void configureWorkingDirectory(const char* pSavePath) = 0;
		
			//! 启用UDX映射原始数据方法，结果保存相对路径
			virtual bool conductUDXMapping(const char* pResultSaveName) = 0;

			//! 启用原始数据映射UDX方法，结果保存相对路径
			virtual bool conductFileMapping(const char* pResultSaveName, std::string* rawFiles, int rawFileCount) = 0;

			//! 启用原始数据映射UDX方法，结果保存相对路径
			virtual bool conductFileMapping(const char* pResultSaveName, std::string rawFile) = 0;

			//! 获取真实的数据名
			virtual const char* getRealResultSaveName() = 0;
		};

	}
}

#endif