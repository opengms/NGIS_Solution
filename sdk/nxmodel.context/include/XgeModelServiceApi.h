//!文件属于 NGIS 工程的一部分
//!作者:乐松山
//!创建时间:2017年6月16日

#ifndef __NGIS_MODELING_SERVICE_API_H__
#define __NGIS_MODELING_SERVICE_API_H__

#include "IModelServiceContext.h"

// 在windows上的api开放设置
#ifdef _WINDOWS

#ifndef _NXMODEL_SERVICE_STATIC_LIB_
#ifdef NXMODELSERVICE_EXPORTS
#define NGISMODELSERVICE_API __declspec(dllexport)
#else
#define NGISMODELSERVICE_API __declspec(dllimport)
#endif // NGISMODEL_API
#else
#define NGISMODELSERVICE_API
#endif // _NXMODEL_STATIC_LIB_

// Declare the calling convention.
#if defined(_STDCALL_SUPPORTED)
#define NGISMODELSERVICE_CALLCONV __stdcall
#else
#define NGISMODELSERVICE_CALLCONV __cdecl
#endif // STDCALL_SUPPORTED

#else // _IMP_WINDOWS_API_

// Force symbol export in shared libraries built with gcc.
#if (__GNUC__ >= 4) && !defined(_NXMODEL_SERVICE_STATIC_LIB_) && defined(NXMODELSERVICE_EXPORTS)
#define NGISMODELSERVICE_API __attribute__ ((visibility("default")))
#else
#define NGISMODELSERVICE_API
#endif
#define NGISMODELSERVICE_CALLCONV

#endif

//////////////////////////////////

// 以下用于导出对外开放的API接口
namespace NGIS
{
	namespace Model
	{
		extern "C" NGISMODELSERVICE_API const char* NGISMODELSERVICE_CALLCONV getVersion();

		extern "C" NGISMODELSERVICE_API IModelServiceContext* NGISMODELSERVICE_CALLCONV createModelServiceContext();

		extern "C" NGISMODELSERVICE_API IModelDataHandler* NGISMODELSERVICE_CALLCONV createNativeRequestDataHandler(IModelServiceContext* pContext);

	}
}
#endif
