#ifndef __I_MODEL_SERVICE_CONTEXTI_H__
#define __I_MODEL_SERVICE_CONTEXTI_H__

#include "INxUnknown.h"
#include "IModelDataHandler.h"
#include <string>

typedef const char* nxUID;

namespace NGIS
{
	namespace Model
	{
		class IModelServiceContext;
		enum EModelContextStatus
		{
			EMCS_INIT_BEGIN,
			EMCS_INIT,
			EMCS_INIT_END,
			
			EMCS_STATE_ENTER_BEGIN,
			EMCS_STATE_ENTER,
			EMCS_STATE_ENTER_END,
			
			EMCS_EVENT_BEGIN,
			EMCS_EVENT,
			EMCS_EVENT_END,
			
			EMCS_REQUEST_BEGIN,
			EMCS_REQUEST,
			EMCS_REQUEST_END,
			
			EMCS_RESPONSE_BEGIN,
			EMCS_RESPONSE,
			EMCS_RESPONSE_END,
			
			EMCS_POST_BEGIN,
			EMCS_POST,
			EMCS_POST_END,
			
			EMCS_STATE_LEAVE_BEGIN,
			EMCS_STATE_LEAVE,
			EMCS_STATE_LEAVE_END,

			EMCS_FINALIZE_BEGIN,
			EMCS_FINALIZE,
			EMCS_FINALIZE_END,

			EMCS_UNKOWN
		};

		class IModelServiceContext : public INxUnknown
		{
		public:
			//! 触发-初始化
			virtual nxUID onInitialize(const char* pHost, const char* pPort, const char* pInstanceID) = 0;

			//! 触发-进入状态
			virtual int onEnterState(nxUID pInstanceId, const char* pStateId) = 0;

			//! 触发-引发事件
			virtual int onFireEvent(nxUID pInstanceId, const char* pStateId, const char* pEventId) = 0;

			//! 触发-向外请求数据
			virtual int onRequestData(nxUID pInstanceId, const char* pStateId, const char* pEventId) = 0;

			//! 触发-向外提供数据
			virtual int onResponseData(nxUID pInstanceId, const char* pStateId, const char* pEventId) = 0;

			//! 触发-提交错误信息
			virtual int onPostErrorInfo(nxUID pInstanceId, const char* pErrorInfo) = 0;

			//! 触发-离开状态
			virtual int onLeaveState(nxUID pInstanceId, const char* pStateId) = 0;

			//! 触发-完成
			virtual int onFinalize(nxUID pInstanceId) = 0;

			//////////////////////////////////////////////////////////////////////////
			//! 获取请求到的数据Flag
			virtual ERequestResponseDataFlag getRequestDataFlag() = 0;

			//! 获取请求到的数据类型
			virtual ERequestResponseDataMIME getRequestDataMIME() = 0;

			//! 获取请求到的数据内容
			virtual const char* getRequestDataBody() = 0;

			//! 设置发送的数据标记Flag
			virtual void setResponseDataFlag(ERequestResponseDataFlag pFlag) = 0;

			//! 设置发送的数据MIME类型
			virtual void setResponseDataMIME(ERequestResponseDataMIME pMIME) = 0;

			//! 设置发送的数据内容
			virtual void setResponseDataBody(const char* pDataBody) = 0;

			//! 获取发送的数据Flag
			virtual ERequestResponseDataFlag getResponseDataFlag() = 0;

			//! 获取发送的数据类型
			virtual ERequestResponseDataMIME getResponseDataMIME() = 0;

			//! 获取发送的数据内容
			virtual const char* getResponseDataBody() = 0;

			//! 获取当前与外部通信的状态
			virtual EModelContextStatus getCurrentStatus() = 0;

			//////////////////////////////////////////////////////////////////////////
			//! 启动一个新的外部进程
			virtual bool startANewProcess(const char* cmd, const char* workingPath) = 0;

			//////////////////////////////////////////////////////////////////////////
			//! 获取当前运行过程中的数据目录
			virtual std::string getCurrentDataDirectory() = 0;
		};
	}
}

#endif