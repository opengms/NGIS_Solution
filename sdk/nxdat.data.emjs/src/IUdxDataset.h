#ifndef __I_NGISDATA_UDXDATASET_H__
#define __I_NGISDATA_UDXDATASET_H__

#include "INxUnknown.h"
#include "IUdxNode.h"

namespace NGIS
{
	namespace Data
	{
		class IUdxDataset : public virtual IUdxNode
		{
		public:
			virtual bool LoadFromXmlFile(const char* fileName) = 0;

			virtual bool FormatToXmlFile(const char* fileName) = 0;

			virtual bool LoadFromXmlStream(const char* xmlStr) = 0;

			virtual bool FormatToXmlStream(std::string& xmlStr) = 0;

			//////////////////////////////////////////////////////////////////////////

			virtual bool LoadFromJsonFile(const char* fileName) = 0;

			virtual bool FormatToJsonFile(const char* fileName) = 0;

			virtual bool LoadFromJsonStream(const char* jsonStr) = 0;

			virtual bool FormatToJsonStream(std::string& jsonStr) = 0;

			//////////////////////////////////////////////////////////////////////////

			virtual bool constructDataFromSchema(Schema::IUdxNodeSchema* pSchema) = 0;
		};
	}
} 

#endif