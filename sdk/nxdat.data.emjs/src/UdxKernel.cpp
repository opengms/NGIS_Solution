#include "UdxKernel.h"
#include "UdxNode.h"

namespace NGIS
{
	namespace Data
	{
		CUdxKernel::CUdxKernel(EKernelType pType, const CUdxNode* pNode)
		{
			mType = pType;
			mNode = pNode;
		}

		CUdxKernel::~CUdxKernel()
		{

		}

		const IUdxNode* CUdxKernel::getNode()
		{
			return mNode;
		}

		NGIS::Data::EKernelType CUdxKernel::getType()
		{
			return mType;
		}

		//////////////////////////////////////////////////////////////////////////
		//
		//
		//
		//////////////////////////////////////////////////////////////////////////

		bool CUdxKernelIntValue::setTypedValue(int pValue)
		{
			if (pValue == mValue) return false;
			mValue = pValue;
			return true;
		}

		int CUdxKernelIntValue::getTypedValue()
		{
			return mValue;
		}


		bool CUdxKernelRealValue::setTypedValue(double pValue)
		{
			if (pValue == mValue) return false;
			mValue = pValue;
			return true;
		}

		double CUdxKernelRealValue::getTypedValue()
		{
			return mValue;
		}


		bool CUdxKernelStringValue::setTypedValue(std::string pValue)
		{
			if (pValue == mValue) return false;
			mValue = pValue;
			return true;
		}

		std::string CUdxKernelStringValue::getTypedValue()
		{
			return mValue;
		}


		bool CUdxKernelVector2dValue::setTypedValue(double pX, double pY)
		{
			mValue.x = pX;
			mValue.y = pY;
			return true;
		}

		Vector2d CUdxKernelVector2dValue::getTypedValue()
		{
			return mValue;
		}

		void CUdxKernelVector2dValue::getTypedValue( double& pX, double& pY )
		{
			pX = mValue.x;
			pY = mValue.y;
		}


		bool CUdxKernelVector3dValue::setTypedValue(double pX, double pY, double pZ)
		{
			mValue.x = pX;
			mValue.y = pX;
			mValue.z	= pZ;
			return true;
		}

		Vector3d CUdxKernelVector3dValue::getTypedValue()
		{
			return mValue;
		}

		void CUdxKernelVector3dValue::getTypedValue( double& pX, double& pY, double& pZ )
		{
			pX = mValue.x;
			pY = mValue.y;
			pZ = mValue.z;
		}


		bool CUdxKernelVector4dValue::setTypedValue(double pX, double pY, double pZ, double pM)
		{
			mValue.x = pX;
			mValue.y = pX;
			mValue.z	= pZ;
			mValue.m	= pM;
			return true;
		}

		Vector4d CUdxKernelVector4dValue::getTypedValue()
		{
			return mValue;
		}

		void CUdxKernelVector4dValue::getTypedValue( double& pX, double& pY, double& pZ, double& pM )
		{
			pX = mValue.x;
			pY = mValue.y;
			pZ = mValue.z;
			pM = mValue.m;
		}

		//////////////////////////////////////////////////////////////////////////
		//
		//
		//
		//////////////////////////////////////////////////////////////////////////

		bool CUdxKernelIntArray::getTypedValueByIndex(int idx, int& val)
		{
			if (idx<0 || idx>=mValueList.size()) 
				return false;
			val = mValueList[idx];
			return true;
		}

		bool CUdxKernelIntArray::addTypedValue(int pValue)
		{
			mValueList.push_back(pValue);
			return true;
		}

		bool CUdxKernelIntArray::setTypedValue( int pValue, int idx )
		{
			if (idx<0 || idx >=mValueList.size())
				return false;
			mValueList[idx] = pValue;
			return true;
		}


		bool CUdxKernelRealArray::getTypedValueByIndex(int idx, double& val)
		{
			if (idx<0 || idx>=mValueList.size()) 
				return false;
			val = mValueList[idx];
			return true;
		}

		bool CUdxKernelRealArray::addTypedValue(double pValue)
		{
			mValueList.push_back(pValue);
			return true;
		}

		bool CUdxKernelRealArray::setTypedValue( double pValue, int idx )
		{
			if (idx<0 || idx >=mValueList.size())
				return false;
			mValueList[idx] = pValue;
			return true;
		}


		bool CUdxKernelStringArray::getTypedValueByIndex(int idx, std::string& val)
		{
			if (idx<0 || idx>=mValueList.size()) 
				return false;
			val = mValueList[idx];
			return true;
		}

		bool CUdxKernelStringArray::addTypedValue(std::string pValue)
		{
			mValueList.push_back(pValue);
			return true;
		}

		bool CUdxKernelStringArray::setTypedValue( std::string pValue, int idx )
		{
			if (idx<0 || idx >=mValueList.size())
				return false;
			mValueList[idx] = pValue;
			return true;
		}


		bool CUdxKernelVector2dArray::getTypedValueByIndex(int idx, Vector2d& val)
		{
			if (idx<0 || idx>=mValueList.size()) 
				return false;
			val = mValueList[idx];
			return true;
		}

		bool CUdxKernelVector2dArray::getTypedValueByIndex( int idx, double& pX, double& pY )
		{
			if (idx<0 || idx>=mValueList.size()) 
				return false;
			Vector2d val = mValueList[idx];
			pX = val.x;
			pY = val.y;
			return true;
		}

		bool CUdxKernelVector2dArray::addTypedValue(double pX, double pY)
		{
			Vector2d pValue;
			pValue.x = pX;
			pValue.y = pY;
			mValueList.push_back(pValue);
			return true;
		}

		bool CUdxKernelVector2dArray::setTypedValue( Vector2d pValue, int idx )
		{
			if (idx<0 || idx >=mValueList.size())
				return false;
			mValueList[idx].x = pValue.x;
			mValueList[idx].y = pValue.y;
			return true;
		}


		bool CUdxKernelVector3dArray::getTypedValueByIndex(int idx, Vector3d& val)
		{
			if (idx<0 || idx>=mValueList.size()) 
				return false;
			val = mValueList[idx];
			return true;
		}

		bool CUdxKernelVector3dArray::getTypedValueByIndex( int idx, double& pX, double& pY, double& pZ )
		{
			if (idx<0 || idx>=mValueList.size()) 
				return false;
			Vector3d val = mValueList[idx];
			pX = val.x;
			pY = val.y;
			pZ = val.z;
			return true;
		}

		bool CUdxKernelVector3dArray::addTypedValue(double pX, double pY, double pZ)
		{
			Vector3d pValue;
			pValue.x = pX;
			pValue.y = pY;
			pValue.z = pZ;
			mValueList.push_back(pValue);
			return true;
		}

		bool CUdxKernelVector3dArray::setTypedValue( Vector3d pValue, int idx )
		{
			if (idx<0 || idx >=mValueList.size())
				return false;
			mValueList[idx] = pValue;
			return true;
		}


		bool CUdxKernelVector4dArray::getTypedValueByIndex(int idx, Vector4d& val)
		{
			if (idx<0 || idx>=mValueList.size())
				return false;
			val = mValueList[idx];
			return true;
		}

		bool CUdxKernelVector4dArray::getTypedValueByIndex( int idx, double& pX, double& pY, double& pZ, double& pM )
		{
			if (idx<0 || idx>=mValueList.size()) 
				return false;
			Vector4d val = mValueList[idx];
			pX = val.x;
			pY = val.y;
			pZ = val.z;
			pM = val.m;
			return true;
		}

		bool CUdxKernelVector4dArray::addTypedValue(double pX, double pY, double pZ, double pM)
		{
			Vector4d pValue;
			pValue.x = pX;
			pValue.y = pY;
			pValue.z = pZ;
			pValue.m = pM;
			mValueList.push_back(pValue);
			return true;
		}

		bool CUdxKernelVector4dArray::setTypedValue( Vector4d pValue, int idx )
		{
			if (idx<0 || idx >=mValueList.size())
				return false;
			mValueList[idx] = pValue;
			return true;
		}

	}
}
