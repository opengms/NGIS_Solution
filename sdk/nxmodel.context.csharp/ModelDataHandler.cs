﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using NGIS.Data;
using System.IO;
using System.Diagnostics;

namespace NGIS
{
    namespace Model
    {
        public enum ERequestResponseDataFlag
        {
            ERDF_OK,
            ERDF_NOTREADY,
            ERDF_ERROR,
            ERDF_UNKNOW
        };

        public enum ERequestResponseDataMIME
        {
            ERDM_XML_STREAM,
            ERDM_ZIP_STREAM,
            ERDM_RAW_STREAM,
            ERDM_XML_FILE,
            ERDM_ZIP_FILE,
            ERDM_RAW_FILE,
            ERDM_UNKNOW
        };

        public class ModelDataHandler
        {
            private string mExecutionPath;
            private string mZipExecutionPath;
            private string mExecutionName;

            private string mSavePath;
            private string mSaveName;

            private string mReturnFileFullName;

            private ModelServiceContext mModelServiceContext;

            public ModelDataHandler(ModelServiceContext pContext)
            {
                mModelServiceContext = pContext;

                string process_name = Process.GetCurrentProcess().ProcessName;
                if (process_name.Contains("."))
                {
                    process_name = process_name.Substring(0, process_name.LastIndexOf('.'));
                    if (process_name.Contains("."))
                    {
                        process_name = process_name.Substring(process_name.LastIndexOf('.')+1);
                    }
                }
                mModelServiceContext.setModelInstanceDebugScript(Directory.GetCurrentDirectory() + "\\" + process_name + "_Debug.xml");
            }

            //! 链接数据映射方法，使用相对路径
            public void connectDataMappingMethod(string pExecutionName)
            {
                mExecutionName = pExecutionName;

                mZipExecutionPath = mModelServiceContext.getMappingLibraryDirectory();
                if (mZipExecutionPath.LastIndexOf('\\') != mZipExecutionPath.Length - 1)
                {
                    mZipExecutionPath += "\\";
                }

                //////////////////////////////////////////////////////////////////////////////
                //////////////////////////////////////////////////////////////////////////////

                mExecutionPath = mModelServiceContext.onGetModelAssembly(pExecutionName);
                mModelServiceContext.onPostMessageInfo(mExecutionPath);
                if (mExecutionPath.LastIndexOf('\\') != mExecutionPath.Length - 1)
                {
                    mExecutionPath += "\\";
                }
                mSavePath = "";
                mSaveName = "";
                mReturnFileFullName = "";
            }

            //! 配置数据映射方法的工作空间，数据映射结果的保存路径
            public void configureWorkingDirectory(string pSavePath)
            {
                string pSavePathStr = pSavePath;
                if (pSavePathStr == "")
                {
                    mSavePath = mModelServiceContext.getModelInstanceDirectory();
                    if (mSavePath.LastIndexOf('\\') != mSavePath.Length - 1)
                    {
                        mSavePath += "\\";
                    }
                }
                else
                {
                    mSavePath = pSavePath;
                    if (mSavePath.LastIndexOf('\\') != mSavePath.Length - 1)
                    {
                        mSavePath += "\\";
                    }
                }
            }

            //! 启用UDX映射原始数据方法，结果保存相对路径
            public bool conductUDXMapping(string pResultSaveName)
            {
                if (mModelServiceContext.getRequestDataFlag() != ERequestResponseDataFlag.ERDF_OK)
                {
                    mSavePath = "";
                    mSaveName = "";
                    mReturnFileFullName = "";
                    return false;
                }

                mSavePath = mModelServiceContext.getCurrentDataDirectory();
                mSaveName = pResultSaveName.Trim();

                ERequestResponseDataMIME pMIME = mModelServiceContext.getRequestDataMIME();
                if (pMIME == ERequestResponseDataMIME.ERDM_XML_FILE)
                {
                    mModelServiceContext.onPostMessageInfo("pMIME: ERDM_XML_FILE");

                    string dataBody = mModelServiceContext.getRequestDataBody();
                    mModelServiceContext.onPostMessageInfo("dataBody: " + dataBody);

                    string xml_file = dataBody;
                    if (mSaveName == "")
                    {
                        string eventName = mModelServiceContext.getCurrentRunningEvent();
                        //! 怎么处理后缀名的问题？？
                        string ext_name = ".raw";
                        if (mExecutionName.Contains("GDAL"))
                            ext_name = ".tif";
                        else if (mExecutionName.Contains("OGR"))
                            ext_name = ".shp";

                        mSaveName = eventName + ext_name;
                    }

                    string cmd = mExecutionPath + mExecutionName + " -w -u ";
                    cmd += xml_file;
                    cmd += " -f ";
                    cmd += mSavePath + mSaveName;

                    bool retVal = mModelServiceContext.startANewProcess(cmd, mSavePath);

                    mReturnFileFullName = mSavePath + mSaveName;
                    return retVal;
                }
                else if (pMIME == ERequestResponseDataMIME.ERDM_ZIP_FILE)
                {
                    mModelServiceContext.onPostMessageInfo("pMIME: ERDM_ZIP_FILE");

                    string dataBody = mModelServiceContext.getRequestDataBody();
                    mModelServiceContext.onPostMessageInfo("dataBody: " + dataBody);

                    int idx = dataBody.LastIndexOf('\\');
                    string dataPath = dataBody.Substring(0, idx + 1);

                    if (File.Exists(dataBody) == false)
                        return false;

                    string cmd = mZipExecutionPath + "miniunz.exe -e -o ";
                    cmd += dataBody;
                    cmd += " -d ";
                    cmd += mSavePath;

                    mModelServiceContext.onPostMessageInfo(cmd);

                    bool ret = mModelServiceContext.startANewProcess(cmd, mSavePath);
                    if (ret == false)
                    {
                        return false;
                    }

                    string configureFileName = mSavePath + "configure.udxcfg";
                    if (File.Exists(configureFileName) == false)
                        return false;
                    XmlDocument doc = new XmlDocument();
                    try
                    {
                        doc.Load(configureFileName);
                    }
                    catch (System.Exception ex)
                    {
                        return false;
                    }
                    if (doc == null)
                        return false;

                    XmlElement rootEle = doc.DocumentElement;
                    XmlElement nameEle = (XmlElement)rootEle.FirstChild;
                    string fileName = "";
                    int file_count = Convert.ToInt32(nameEle.GetAttribute("count"));
                    if (file_count == 1)
                    {
                        XmlElement ele = (XmlElement)nameEle.FirstChild;
                        fileName = ele.GetAttribute("value");

                        if (mSaveName == "")
                        {
                            mSaveName = fileName;
                            mReturnFileFullName = mSavePath + mSaveName;
                            return true;
                        }
                        //////////////////////////////////////////////////////////////////////////
                        if (mSaveName != "" && mSaveName != fileName)
                        {
                            if (mSaveName.LastIndexOf('.') == mSaveName.Length - 1 ||
                                mSaveName.LastIndexOf('.') < 0)
                            {
                                int idx1 = fileName.LastIndexOf('.');
                                string ext = fileName.Substring(idx1, fileName.Length - idx1);
                                mSaveName += ext;
                            }
                            string oldName = mSavePath + fileName;
                            string newName = mSavePath + mSaveName;
                            System.IO.FileInfo fileInfo = new System.IO.FileInfo(oldName);
                            try
                            {
                                fileInfo.MoveTo(newName);
                            }
                            catch (System.Exception ex)
                            {
                                mSaveName = fileName;
                                mReturnFileFullName = mSavePath + mSaveName;
                                return false;
                            }
                            if (System.IO.File.Exists(newName) == false)
                            {
                                mSaveName = fileName;
                                mReturnFileFullName = mSavePath + mSaveName;
                                return false;
                            }
                            mReturnFileFullName = mSavePath + mSaveName;
                        }
                    }
                    else if (mSaveName != "")
                    {
                        int last_dot = mSaveName.LastIndexOf('.');
                        if (last_dot > 0 && last_dot != mSaveName.Length)
                        {
                            mSaveName = mSaveName.Substring(0, last_dot); //去掉后缀名
                        }

                        for (int iChild = 0; iChild < nameEle.ChildNodes.Count; iChild++)
                        {
                            XmlElement ele = (XmlElement)nameEle.ChildNodes[iChild];
                            fileName = ele.GetAttribute("value");
                            string ext = fileName.Substring(idx, fileName.Length - idx);

                            string oldName = mSavePath + fileName;
                            string newName = mSavePath + mSaveName + ext;
                            System.IO.FileInfo fileInfo = new System.IO.FileInfo(oldName);
                            fileInfo.MoveTo(newName);
                            if (System.IO.File.Exists(newName) == false)
                            {
                                mSaveName = fileName;
                                break;
                            }
                        }
                        mReturnFileFullName = mSavePath + mSaveName;
                    }
                    else
                    {
                        for (int iChild = 0; iChild < nameEle.ChildNodes.Count; iChild++)
                        {
                            XmlElement ele = (XmlElement)nameEle.ChildNodes[iChild];
                            fileName = ele.GetAttribute("value");
                            if (fileName.Contains("shp"))
                            {
                                mSaveName = fileName;
                                break;
                            }
                            else if (fileName.Contains("tif"))
                            {
                                mSaveName = fileName;
                                break;
                            }
                            else if (fileName.Contains("img"))
                            {
                                mSaveName = fileName;
                                break;
                            }
                        }
                        mReturnFileFullName = mSavePath + mSaveName;
                    }
                    return true;
                }
                else if (pMIME == ERequestResponseDataMIME.ERDM_RAW_FILE)
                {
                    string dataBody = mModelServiceContext.getRequestDataBody();
                    int idx = dataBody.LastIndexOf('\\');
                    string dataPath = dataBody.Substring(0, idx + 1);
                    string dataName = dataBody.Substring(idx + 1);

                    mSavePath = dataPath;
                    mSaveName = dataName;
                    mReturnFileFullName = mSavePath + mSaveName;
                    return true;
                }
                else if (pMIME == ERequestResponseDataMIME.ERDM_XML_STREAM)
                {
                    //! TODO 数据流的暂时还不支持，没时间写啊
                }
                else if (pMIME == ERequestResponseDataMIME.ERDM_ZIP_STREAM)
                {
                    //! TODO 数据流的暂时还不支持，没时间写啊
                }
                else if (pMIME == ERequestResponseDataMIME.ERDM_RAW_STREAM)
                {
                    //! TODO 数据流的暂时还不支持，没时间写啊
                }
                mReturnFileFullName = "";
                return false;
            }

            //! 启用原始数据映射UDX方法，结果保存相对路径
            public bool conductFileMapping(string pResultSaveName, List<string> rawFiles, int rawFileCount)
            {
                if (mModelServiceContext.getResponseDataFlag() != ERequestResponseDataFlag.ERDF_OK)
                {
                    mSavePath = "";
                    mSaveName = "";
                    mReturnFileFullName = "";
                    return false;
                }

                mSavePath = mModelServiceContext.getCurrentDataDirectory();
                mSaveName = pResultSaveName.Trim();

                if (this.mModelServiceContext.getResponseDataFlag() == ERequestResponseDataFlag.ERDF_OK)
                {
                    switch (this.mModelServiceContext.getResponseDataMIME())
                    {
                        case ERequestResponseDataMIME.ERDM_XML_FILE:
                            {
                                string format_file = rawFiles[0];
                                if (File.Exists(format_file) == false)
                                {
                                    mSavePath = "";
                                    mSaveName = "";
                                    mReturnFileFullName = "";
                                    return false;
                                }

                                string cmd = mExecutionPath + mExecutionName + " -r -f ";
                                cmd += format_file;
                                cmd += " -u ";
                                cmd += mSavePath + mSaveName;

                                //! 利用数据映射方法，将原始文件转换成UDX的XML格式
                                bool ret = mModelServiceContext.startANewProcess(cmd, mSavePath);
                                if (ret)
                                {
                                    if (System.IO.File.Exists(mSavePath + mSaveName) == false)
                                    {
                                        mSavePath = "";
                                        mSaveName = "";
                                        mReturnFileFullName = "";
                                        return false;
                                    }
                                    mReturnFileFullName = mSavePath + mSaveName;
                                    return true;
                                }
                                else
                                {
                                    mSavePath = "";
                                    mSaveName = "";
                                    mReturnFileFullName = "";
                                    return false;
                                }
                            }
                            break;
                        case ERequestResponseDataMIME.ERDM_ZIP_FILE:
                            {
                                XmlDocument doc = new XmlDocument();
                                XmlElement rootEle = doc.CreateElement("UDXZip");
                                doc.AppendChild(rootEle);

                                XmlElement fileNameEle = doc.CreateElement("FileName");
                                rootEle.AppendChild(fileNameEle);
                                fileNameEle.SetAttribute("count", rawFileCount.ToString());
                                for (int iFile = 0; iFile < rawFileCount; iFile++)
                                {
                                    string temp_raw_file = rawFiles[iFile];
                                    if (File.Exists(temp_raw_file) == false)
                                        continue;

                                    XmlElement fileItemEle = doc.CreateElement("Add");
                                    fileNameEle.AppendChild(fileItemEle);

                                    temp_raw_file = Path.GetFileName(temp_raw_file);
                                    fileItemEle.SetAttribute("value", temp_raw_file);
                                }
                                //! TODO Add UDX Schema	
                                if (fileNameEle.ChildNodes.Count <= 0)
                                {
                                    mSavePath = "";
                                    mSaveName = "";
                                    mReturnFileFullName = "";
                                    return false;
                                }

                                string cfgFile = mSavePath + "configure.udxcfg";
                                doc.Save(cfgFile);

                                bool zip_ret = false;
                                if (rawFileCount > 1)
                                {
                                    string cmd = mZipExecutionPath + "minizip.exe -o -5 -j "; //如果加上访问码的话 -p 123
                                    cmd += mSavePath + mSaveName;
                                    cmd += " " + cfgFile;
                                    zip_ret = mModelServiceContext.startANewProcess(cmd, mSavePath);

                                    for (int iFile = 0; iFile < rawFileCount; iFile++)
                                    {
                                        cmd = mZipExecutionPath + "minizip.exe -a -5 -j ";
                                        cmd += mSavePath + mSaveName;
                                        if (File.Exists(rawFiles[iFile]) == false)
                                            continue;
                                        cmd += " " + rawFiles[iFile];
                                        zip_ret = mModelServiceContext.startANewProcess(cmd, mSavePath);
                                        if (zip_ret == false)
                                        {
                                            mSavePath = "";
                                            mSaveName = "";
                                            mReturnFileFullName = "";
                                            return false;
                                        }
                                    }
                                }
                                else
                                {
                                    string cmd = mZipExecutionPath + "minizip.exe -o -5 -j "; //如果加上访问码的话 -p 123
                                    cmd += mSavePath + mSaveName;
                                    for (int iFile = 0; iFile < rawFileCount; iFile++)
                                    {
                                        if (File.Exists(rawFiles[iFile]) == false)
                                            continue;
                                        cmd += " " + rawFiles[iFile];
                                    }
                                    cmd += " " + cfgFile;

                                    zip_ret = mModelServiceContext.startANewProcess(cmd, mSavePath);
                                }

                                if (zip_ret == false)
                                {
                                    mSavePath = "";
                                    mSaveName = "";
                                    mReturnFileFullName = "";
                                    return false;
                                }
                                mReturnFileFullName = mSavePath + mSaveName;
                                return true;
                            }
                            break;
                        case ERequestResponseDataMIME.ERDM_RAW_FILE:
                            {
                                for (int iFile = 0; iFile < rawFileCount; iFile++)
                                {
                                    string temp_raw_file = rawFiles[iFile];
                                    if (File.Exists(temp_raw_file) == false)
                                        continue;
                                    mReturnFileFullName += temp_raw_file;
                                    if (iFile != rawFileCount - 1)
                                    {
                                        mReturnFileFullName += ";";
                                    }
                                }
                                if (mReturnFileFullName == "")
                                    return false;
                                else
                                    return true;
                            }
                            break;
                        case ERequestResponseDataMIME.ERDM_XML_STREAM:
                            {
                                //! TODO 数据流的暂时还不支持，没时间写啊
                            }
                            break;
                        case ERequestResponseDataMIME.ERDM_ZIP_STREAM:
                            {
                                //! TODO 数据流的暂时还不支持，没时间写啊
                            }
                            break;
                        case ERequestResponseDataMIME.ERDM_RAW_STREAM:
                            {
                                //! TODO 数据流的暂时还不支持，没时间写啊
                            }
                            break;
                    }
                }

                return false;
            }

            //! 启用原始数据映射UDX方法，结果保存相对路径
            public bool conductFileMapping(string pResultSaveName, string rawFile)
            {
                List<string> rawFiles = new List<string>();
                rawFiles.Add(rawFile);
                return conductFileMapping(pResultSaveName, rawFiles, 1);
            }

            //! 获取真实的数据名
            public string getRealResultSaveName()
            {
                if (File.Exists(mReturnFileFullName) == false)
                    mReturnFileFullName = "";
                return mReturnFileFullName;
            }

            /////////////////////////////////////////////////////////////////////////
            //
            //
            //
            /////////////////////////////////////////////////////////////////////////
            public bool tryGetUdxIntValue(ref int val)
            {
                try
                {
                    if (mModelServiceContext.getRequestDataFlag() == ERequestResponseDataFlag.ERDF_OK)
                    {
                        if (mModelServiceContext.getRequestDataMIME() == ERequestResponseDataMIME.ERDM_XML_FILE)
                        {
                            UdxDataset dataset = new UdxDataset();
                            dataset.LoadFromXmlFile(mModelServiceContext.getRequestDataBody());
                            UdxKernelIntValue realKernel = dataset.getChildNode(0).getKernel() as UdxKernelIntValue;
                            val = realKernel.getTypedValue();
                            return true;
                        }
                        else if (mModelServiceContext.getRequestDataMIME() == ERequestResponseDataMIME.ERDM_XML_STREAM)
                        {
                            UdxDataset dataset = new UdxDataset();
                            dataset.LoadFromXmlStream(mModelServiceContext.getRequestDataBody());
                            UdxKernelIntValue realKernel = dataset.getChildNode(0).getKernel() as UdxKernelIntValue;
                            val = realKernel.getTypedValue();
                            return true;
                        }
                    }
                }
                catch (System.Exception ex)
                {

                }
                return false;
            }

            public bool tryGetUdxRealValue(ref double val)
            {
                try
                {
                    if (mModelServiceContext.getRequestDataFlag() == ERequestResponseDataFlag.ERDF_OK)
                    {
                        if (mModelServiceContext.getRequestDataMIME() == ERequestResponseDataMIME.ERDM_XML_FILE)
                        {
                            UdxDataset dataset = new UdxDataset();
                            dataset.LoadFromXmlFile(mModelServiceContext.getRequestDataBody());
                            UdxKernelRealValue realKernel = dataset.getChildNode(0).getKernel() as UdxKernelRealValue;
                            val = realKernel.getTypedValue();
                            return true;
                        }
                        else if (mModelServiceContext.getRequestDataMIME() == ERequestResponseDataMIME.ERDM_XML_STREAM)
                        {
                            UdxDataset dataset = new UdxDataset();
                            dataset.LoadFromXmlStream(mModelServiceContext.getRequestDataBody());
                            UdxKernelRealValue realKernel = dataset.getChildNode(0).getKernel() as UdxKernelRealValue;
                            val = realKernel.getTypedValue();
                            return true;
                        }
                    }
                }
                catch (System.Exception ex)
                {

                }
                return false;
            }

            public bool tryGetUdxStringValue(ref string val)
            {
                try
                {
                    if (mModelServiceContext.getRequestDataFlag() == ERequestResponseDataFlag.ERDF_OK)
                    {
                        if (mModelServiceContext.getRequestDataMIME() == ERequestResponseDataMIME.ERDM_XML_FILE)
                        {
                            UdxDataset dataset = new UdxDataset();
                            dataset.LoadFromXmlFile(mModelServiceContext.getRequestDataBody());
                            UdxKernelStringValue realKernel = dataset.getChildNode(0).getKernel() as UdxKernelStringValue;
                            val = realKernel.getTypedValue();
                            return true;
                        }
                        else if (mModelServiceContext.getRequestDataMIME() == ERequestResponseDataMIME.ERDM_XML_STREAM)
                        {
                            UdxDataset dataset = new UdxDataset();
                            dataset.LoadFromXmlStream(mModelServiceContext.getRequestDataBody());
                            UdxKernelStringValue realKernel = dataset.getChildNode(0).getKernel() as UdxKernelStringValue;
                            val = realKernel.getTypedValue();
                            return true;
                        }
                    }
                }
                catch (System.Exception ex)
                {

                }
                return false;
            }

            public bool tryGetUdxVector2dValue(ref Vector2d val)
            {
                try
                {
                    if (mModelServiceContext.getRequestDataFlag() == ERequestResponseDataFlag.ERDF_OK)
                    {
                        if (mModelServiceContext.getRequestDataMIME() == ERequestResponseDataMIME.ERDM_XML_FILE)
                        {
                            UdxDataset dataset = new UdxDataset();
                            dataset.LoadFromXmlFile(mModelServiceContext.getRequestDataBody());
                            UdxKernelVector2dValue realKernel = dataset.getChildNode(0).getKernel() as UdxKernelVector2dValue;
                            val = realKernel.getTypedValue();
                            return true;
                        }
                        else if (mModelServiceContext.getRequestDataMIME() == ERequestResponseDataMIME.ERDM_XML_STREAM)
                        {
                            UdxDataset dataset = new UdxDataset();
                            dataset.LoadFromXmlStream(mModelServiceContext.getRequestDataBody());
                            UdxKernelVector2dValue realKernel = dataset.getChildNode(0).getKernel() as UdxKernelVector2dValue;
                            val = realKernel.getTypedValue();
                            return true;
                        }
                    }
                }
                catch (System.Exception ex)
                {

                }
                return false;
            }

            public bool tryGetUdxVector3dValue(ref Vector3d val)
            {
                try
                {
                    if (mModelServiceContext.getRequestDataFlag() == ERequestResponseDataFlag.ERDF_OK)
                    {
                        if (mModelServiceContext.getRequestDataMIME() == ERequestResponseDataMIME.ERDM_XML_FILE)
                        {
                            UdxDataset dataset = new UdxDataset();
                            dataset.LoadFromXmlFile(mModelServiceContext.getRequestDataBody());
                            UdxKernelVector3dValue realKernel = dataset.getChildNode(0).getKernel() as UdxKernelVector3dValue;
                            val = realKernel.getTypedValue();
                            return true;
                        }
                        else if (mModelServiceContext.getRequestDataMIME() == ERequestResponseDataMIME.ERDM_XML_STREAM)
                        {
                            UdxDataset dataset = new UdxDataset();
                            dataset.LoadFromXmlStream(mModelServiceContext.getRequestDataBody());
                            UdxKernelVector3dValue realKernel = dataset.getChildNode(0).getKernel() as UdxKernelVector3dValue;
                            val = realKernel.getTypedValue();
                            return true;
                        }
                    }
                }
                catch (System.Exception ex)
                {

                }
                return false;
            }

            public bool tryGetUdxVector4dValue(ref Vector4d val)
            {
                try
                {
                    if (mModelServiceContext.getRequestDataFlag() == ERequestResponseDataFlag.ERDF_OK)
                    {
                        if (mModelServiceContext.getRequestDataMIME() == ERequestResponseDataMIME.ERDM_XML_FILE)
                        {
                            UdxDataset dataset = new UdxDataset();
                            dataset.LoadFromXmlFile(mModelServiceContext.getRequestDataBody());
                            UdxKernelVector4dValue realKernel = dataset.getChildNode(0).getKernel() as UdxKernelVector4dValue;
                            val = realKernel.getTypedValue();
                            return true;
                        }
                        else if (mModelServiceContext.getRequestDataMIME() == ERequestResponseDataMIME.ERDM_XML_STREAM)
                        {
                            UdxDataset dataset = new UdxDataset();
                            dataset.LoadFromXmlStream(mModelServiceContext.getRequestDataBody());
                            UdxKernelVector4dValue realKernel = dataset.getChildNode(0).getKernel() as UdxKernelVector4dValue;
                            val = realKernel.getTypedValue();
                            return true;
                        }
                    }
                }
                catch (System.Exception ex)
                {

                }
                return false;
            }

            public bool tryGetUdxIntListValue(ref List<int> val)
            {
                try
                {
                    if (mModelServiceContext.getRequestDataFlag() == ERequestResponseDataFlag.ERDF_OK)
                    {
                        if (mModelServiceContext.getRequestDataMIME() == ERequestResponseDataMIME.ERDM_XML_FILE)
                        {
                            UdxDataset dataset = new UdxDataset();
                            dataset.LoadFromXmlFile(mModelServiceContext.getRequestDataBody());
                            UdxKernelIntArray realKernel = dataset.getChildNode(0).getKernel() as UdxKernelIntArray;
                            int count = realKernel.getCount();
                            for (int i = 0; i < count; i++)
                            {
                                val.Add(realKernel.getTypedValueByIndex(i));
                            }
                            return true;               
                        }
                        else if (mModelServiceContext.getRequestDataMIME() == ERequestResponseDataMIME.ERDM_XML_STREAM)
                        {
                            UdxDataset dataset = new UdxDataset();
                            dataset.LoadFromXmlStream(mModelServiceContext.getRequestDataBody());
                            UdxKernelIntArray realKernel = dataset.getChildNode(0).getKernel() as UdxKernelIntArray;
                            int count = realKernel.getCount();
                            for (int i = 0; i < count; i++)
                            {
                                val.Add(realKernel.getTypedValueByIndex(i));
                            }
                            return true;
                        }
                    }
                }
                catch (System.Exception ex)
                {

                }
                return false;
            }

            public bool tryGetUdxRealListValue(ref List<double> val)
            {
                try
                {
                    if (mModelServiceContext.getRequestDataFlag() == ERequestResponseDataFlag.ERDF_OK)
                    {
                        if (mModelServiceContext.getRequestDataMIME() == ERequestResponseDataMIME.ERDM_XML_FILE)
                        {
                            UdxDataset dataset = new UdxDataset();
                            dataset.LoadFromXmlFile(mModelServiceContext.getRequestDataBody());
                            UdxKernelRealArray realKernel = dataset.getChildNode(0).getKernel() as UdxKernelRealArray;
                            int count = realKernel.getCount();
                            for (int i = 0; i < count; i++)
                            {
                                val.Add(realKernel.getTypedValueByIndex(i));
                            }
                            return true;
                        }
                        else if (mModelServiceContext.getRequestDataMIME() == ERequestResponseDataMIME.ERDM_XML_STREAM)
                        {
                            UdxDataset dataset = new UdxDataset();
                            dataset.LoadFromXmlStream(mModelServiceContext.getRequestDataBody());
                            UdxKernelRealArray realKernel = dataset.getChildNode(0).getKernel() as UdxKernelRealArray;
                            int count = realKernel.getCount();
                            for (int i = 0; i < count; i++)
                            {
                                val.Add(realKernel.getTypedValueByIndex(i));
                            }
                            return true;
                        }
                    }
                }
                catch (System.Exception ex)
                {

                }
                return false;
            }

            public bool tryGetUdxStringListValue(ref List<string> val)
            {
                try
                {
                    if (mModelServiceContext.getRequestDataFlag() == ERequestResponseDataFlag.ERDF_OK)
                    {
                        if (mModelServiceContext.getRequestDataMIME() == ERequestResponseDataMIME.ERDM_XML_FILE)
                        {
                            UdxDataset dataset = new UdxDataset();
                            dataset.LoadFromXmlFile(mModelServiceContext.getRequestDataBody());
                            UdxKernelStringArray realKernel = dataset.getChildNode(0).getKernel() as UdxKernelStringArray;
                            int count = realKernel.getCount();
                            for (int i = 0; i < count; i++)
                            {
                                val.Add(realKernel.getTypedValueByIndex(i));
                            }
                            return true;
                        }
                        else if (mModelServiceContext.getRequestDataMIME() == ERequestResponseDataMIME.ERDM_XML_STREAM)
                        {
                            UdxDataset dataset = new UdxDataset();
                            dataset.LoadFromXmlStream(mModelServiceContext.getRequestDataBody());
                            UdxKernelStringArray realKernel = dataset.getChildNode(0).getKernel() as UdxKernelStringArray;
                            int count = realKernel.getCount();
                            for (int i = 0; i < count; i++)
                            {
                                val.Add(realKernel.getTypedValueByIndex(i));
                            }
                            return true;
                        }
                    }
                }
                catch (System.Exception ex)
                {

                }
                return false;
            }

            public bool tryGetUdxVector2dListValue(ref List<Vector2d> val)
            {
                try
                {
                    if (mModelServiceContext.getRequestDataFlag() == ERequestResponseDataFlag.ERDF_OK)
                    {
                        if (mModelServiceContext.getRequestDataMIME() == ERequestResponseDataMIME.ERDM_XML_FILE)
                        {
                            UdxDataset dataset = new UdxDataset();
                            dataset.LoadFromXmlFile(mModelServiceContext.getRequestDataBody());
                            UdxKernelVector2dArray realKernel = dataset.getChildNode(0).getKernel() as UdxKernelVector2dArray;
                            int count = realKernel.getCount();
                            for (int i = 0; i < count; i++)
                            {
                                val.Add(realKernel.getTypedValueByIndex(i));
                            }
                            return true;
                        }
                        else if (mModelServiceContext.getRequestDataMIME() == ERequestResponseDataMIME.ERDM_XML_STREAM)
                        {
                            UdxDataset dataset = new UdxDataset();
                            dataset.LoadFromXmlStream(mModelServiceContext.getRequestDataBody());
                            UdxKernelVector2dArray realKernel = dataset.getChildNode(0).getKernel() as UdxKernelVector2dArray;
                            int count = realKernel.getCount();
                            for (int i = 0; i < count; i++)
                            {
                                val.Add(realKernel.getTypedValueByIndex(i));
                            }
                            return true;
                        }
                    }
                }
                catch (System.Exception ex)
                {

                }
                return false;
            }

            public bool tryGetUdxVector3dListValue(ref List<Vector3d> val)
            {
                try
                {
                    if (mModelServiceContext.getRequestDataFlag() == ERequestResponseDataFlag.ERDF_OK)
                    {
                        if (mModelServiceContext.getRequestDataMIME() == ERequestResponseDataMIME.ERDM_XML_FILE)
                        {
                            UdxDataset dataset = new UdxDataset();
                            dataset.LoadFromXmlFile(mModelServiceContext.getRequestDataBody());
                            UdxKernelVector3dArray realKernel = dataset.getChildNode(0).getKernel() as UdxKernelVector3dArray;
                            int count = realKernel.getCount();
                            for (int i = 0; i < count; i++)
                            {
                                val.Add(realKernel.getTypedValueByIndex(i));
                            }
                            return true;
                        }
                        else if (mModelServiceContext.getRequestDataMIME() == ERequestResponseDataMIME.ERDM_XML_STREAM)
                        {
                            UdxDataset dataset = new UdxDataset();
                            dataset.LoadFromXmlStream(mModelServiceContext.getRequestDataBody());
                            UdxKernelVector3dArray realKernel = dataset.getChildNode(0).getKernel() as UdxKernelVector3dArray;
                            int count = realKernel.getCount();
                            for (int i = 0; i < count; i++)
                            {
                                val.Add(realKernel.getTypedValueByIndex(i));
                            }
                            return true;
                        }
                    }
                }
                catch (System.Exception ex)
                {

                }
                return false;
            }

            public bool tryGetUdxVector4dListValue(ref List<Vector4d> val)
            {
                try
                {
                    if (mModelServiceContext.getRequestDataFlag() == ERequestResponseDataFlag.ERDF_OK)
                    {
                        if (mModelServiceContext.getRequestDataMIME() == ERequestResponseDataMIME.ERDM_XML_FILE)
                        {
                            UdxDataset dataset = new UdxDataset();
                            dataset.LoadFromXmlFile(mModelServiceContext.getRequestDataBody());
                            UdxKernelVector4dArray realKernel = dataset.getChildNode(0).getKernel() as UdxKernelVector4dArray;
                            int count = realKernel.getCount();
                            for (int i = 0; i < count; i++)
                            {
                                val.Add(realKernel.getTypedValueByIndex(i));
                            }
                            return true;
                        }
                        else if (mModelServiceContext.getRequestDataMIME() == ERequestResponseDataMIME.ERDM_XML_STREAM)
                        {
                            UdxDataset dataset = new UdxDataset();
                            dataset.LoadFromXmlStream(mModelServiceContext.getRequestDataBody());
                            UdxKernelVector4dArray realKernel = dataset.getChildNode(0).getKernel() as UdxKernelVector4dArray;
                            int count = realKernel.getCount();
                            for (int i = 0; i < count; i++)
                            {
                                val.Add(realKernel.getTypedValueByIndex(i));
                            }
                            return true;
                        }
                    }
                }
                catch (System.Exception ex)
                {

                }
                return false;
            }

            public UdxDataset tryGetUdxDataset()
            {
                try
                {
                    if (mModelServiceContext.getRequestDataFlag() == ERequestResponseDataFlag.ERDF_OK)
                    {
                        if (mModelServiceContext.getRequestDataMIME() == ERequestResponseDataMIME.ERDM_XML_FILE)
                        {
                            UdxDataset dataset = new UdxDataset();
                            bool ret = dataset.LoadFromXmlFile(mModelServiceContext.getRequestDataBody());
                            if (ret == false)
                                dataset = null;
                            return dataset;
                        }
                        else if (mModelServiceContext.getRequestDataMIME() == ERequestResponseDataMIME.ERDM_XML_STREAM)
                        {
                            UdxDataset dataset = new UdxDataset();
                            bool ret = dataset.LoadFromXmlStream(mModelServiceContext.getRequestDataBody());
                            if (ret == false)
                                dataset = null;
                            return dataset;
                        }
                    }
                }
                catch (System.Exception ex)
                {

                }
                return null;
            }

            /////////////////////////////////////////////////////////////////////////
            //
            //
            //
            /////////////////////////////////////////////////////////////////////////
            public bool conductSimpleFileMapping(string pResultSaveName, string name, object value, EKernelType pType)
            {
                mSavePath = mModelServiceContext.getCurrentDataDirectory();
                mSaveName = pResultSaveName.Trim();
                pResultSaveName = mSavePath + mSaveName;

                if (pType == EKernelType.EKT_INT)
                {
                    UdxDataset res = new UdxDataset();
                    UdxNode resNode = res.addChildNode(name, pType);
                    ((UdxKernelIntValue)resNode.getKernel()).setTypedValue(Convert.ToInt32(value));
                    res.FormatToXmlFile(pResultSaveName);
                    return true;
                }
                else if (pType == EKernelType.EKT_REAL)
                {
                    UdxDataset res = new UdxDataset();
                    UdxNode resNode = res.addChildNode(name, pType);
                    ((UdxKernelRealValue)resNode.getKernel()).setTypedValue(Convert.ToDouble(value));
                    res.FormatToXmlFile(pResultSaveName);
                    return true;
                }
                else if (pType == EKernelType.EKT_STRING)
                {
                    UdxDataset res = new UdxDataset();
                    UdxNode resNode = res.addChildNode(name, pType);
                    ((UdxKernelStringValue)resNode.getKernel()).setTypedValue(Convert.ToString(value));
                    res.FormatToXmlFile(pResultSaveName);
                    return true;
                }
                else if (pType == EKernelType.EKT_VECTOR2)
                {
                    UdxDataset res = new UdxDataset();
                    UdxNode resNode = res.addChildNode(name, pType);
                    Vector2d val = (Vector2d)value;
                    ((UdxKernelVector2dValue)resNode.getKernel()).setTypedValue(val.x, val.y);
                    res.FormatToXmlFile(pResultSaveName);
                    return true;
                }
                else if (pType == EKernelType.EKT_VECTOR3)
                {
                    UdxDataset res = new UdxDataset();
                    UdxNode resNode = res.addChildNode(name, pType);
                    Vector3d val = (Vector3d)value;
                    ((UdxKernelVector3dValue)resNode.getKernel()).setTypedValue(val.x, val.y, val.z);
                    res.FormatToXmlFile(pResultSaveName);
                    return true;
                }
                else if (pType == EKernelType.EKT_VECTOR4)
                {
                    UdxDataset res = new UdxDataset();
                    UdxNode resNode = res.addChildNode(name, pType);
                    Vector4d val = (Vector4d)value;
                    ((UdxKernelVector4dValue)resNode.getKernel()).setTypedValue(val.x, val.y, val.z, val.m);
                    res.FormatToXmlFile(pResultSaveName);
                    return true;
                }
                return false;
            }

            public bool conductSimpleFileMapping(string pResultSaveName, string name, object[] value, EKernelType pType)
            {
                mSavePath = mModelServiceContext.getCurrentDataDirectory();
                mSaveName = pResultSaveName.Trim();
                pResultSaveName = mSavePath + mSaveName;

                if (pType == EKernelType.EKT_INT_LIST)
                {
                    int count = value.Length;
                    UdxDataset res = new UdxDataset();
                    UdxNode resNode = res.addChildNode(name, pType);
                    UdxKernelIntArray realKernel = (UdxKernelIntArray)resNode.getKernel();
                    for (int i = 0; i < count; i++)
                    {
                        realKernel.addTypedValue(Convert.ToInt32(value[i]));
                    }
                    res.FormatToXmlFile(pResultSaveName);
                    return true;
                }
                else if (pType == EKernelType.EKT_REAL_LIST)
                {
                    int count = value.Length;
                    UdxDataset res = new UdxDataset();
                    UdxNode resNode = res.addChildNode(name, pType);
                    UdxKernelRealArray realKernel = (UdxKernelRealArray)resNode.getKernel();
                    for (int i = 0; i < count; i++)
                    {
                        realKernel.addTypedValue(Convert.ToDouble(value[i]));
                    }
                    res.FormatToXmlFile(pResultSaveName);
                    return true;
                }
                else if (pType == EKernelType.EKT_STRING_LIST)
                {
                    int count = value.Length;
                    UdxDataset res = new UdxDataset();
                    UdxNode resNode = res.addChildNode(name, pType);
                    UdxKernelStringArray realKernel = (UdxKernelStringArray)resNode.getKernel();
                    for (int i = 0; i < count; i++)
                    {
                        realKernel.addTypedValue(Convert.ToString(value[i]));
                    }
                    res.FormatToXmlFile(pResultSaveName);
                    return true;
                }
                else if (pType == EKernelType.EKT_VECTOR2_LIST)
                {
                    int count = value.Length;
                    UdxDataset res = new UdxDataset();
                    UdxNode resNode = res.addChildNode(name, pType);
                    UdxKernelVector2dArray realKernel = (UdxKernelVector2dArray)resNode.getKernel();
                    for (int i = 0; i < count; i++)
                    {
                        Vector2d val = (Vector2d)value[i]; 
                        realKernel.addTypedValue(val.x, val.y);
                    }
                    res.FormatToXmlFile(pResultSaveName);
                    return true;
                }
                else if (pType == EKernelType.EKT_VECTOR3_LIST)
                {
                    int count = value.Length;
                    UdxDataset res = new UdxDataset();
                    UdxNode resNode = res.addChildNode(name, pType);
                    UdxKernelVector3dArray realKernel = (UdxKernelVector3dArray)resNode.getKernel();
                    for (int i = 0; i < count; i++)
                    {
                        Vector3d val = (Vector3d)value[i];
                        realKernel.addTypedValue(val.x, val.y, val.z);
                    }
                    res.FormatToXmlFile(pResultSaveName);
                    return true;
                }
                else if (pType == EKernelType.EKT_VECTOR4_LIST)
                {
                    int count = value.Length;
                    UdxDataset res = new UdxDataset();
                    UdxNode resNode = res.addChildNode(name, pType);
                    UdxKernelVector4dArray realKernel = (UdxKernelVector4dArray)resNode.getKernel();
                    for (int i = 0; i < count; i++)
                    {
                        Vector4d val = (Vector4d)value[i];
                        realKernel.addTypedValue(val.x, val.y, val.z, val.m);
                    }
                    res.FormatToXmlFile(pResultSaveName);
                    return true;
                }

                return false;
            }

            public bool conductSimpleFileMapping(string pResultSaveName, string name, List<object> value, EKernelType pType)
            {
                mSavePath = mModelServiceContext.getCurrentDataDirectory();
                mSaveName = pResultSaveName.Trim();
                pResultSaveName = mSavePath + mSaveName;

                if (pType == EKernelType.EKT_INT_LIST)
                {
                    int count = value.Count;
                    UdxDataset res = new UdxDataset();
                    UdxNode resNode = res.addChildNode(name, pType);
                    UdxKernelIntArray realKernel = (UdxKernelIntArray)resNode.getKernel();
                    for (int i = 0; i < count; i++)
                    {
                        realKernel.addTypedValue(Convert.ToInt32(value[i]));
                    }
                    res.FormatToXmlFile(pResultSaveName);
                    return true;
                }
                else if (pType == EKernelType.EKT_REAL_LIST)
                {
                    int count = value.Count;
                    UdxDataset res = new UdxDataset();
                    UdxNode resNode = res.addChildNode(name, pType);
                    UdxKernelRealArray realKernel = (UdxKernelRealArray)resNode.getKernel();
                    for (int i = 0; i < count; i++)
                    {
                        realKernel.addTypedValue(Convert.ToDouble(value[i]));
                    }
                    res.FormatToXmlFile(pResultSaveName);
                    return true;
                }
                else if (pType == EKernelType.EKT_STRING_LIST)
                {
                    int count = value.Count;
                    UdxDataset res = new UdxDataset();
                    UdxNode resNode = res.addChildNode(name, pType);
                    UdxKernelStringArray realKernel = (UdxKernelStringArray)resNode.getKernel();
                    for (int i = 0; i < count; i++)
                    {
                        realKernel.addTypedValue(Convert.ToString(value[i]));
                    }
                    res.FormatToXmlFile(pResultSaveName);
                    return true;
                }
                else if (pType == EKernelType.EKT_VECTOR2_LIST)
                {
                    int count = value.Count;
                    UdxDataset res = new UdxDataset();
                    UdxNode resNode = res.addChildNode(name, pType);
                    UdxKernelVector2dArray realKernel = (UdxKernelVector2dArray)resNode.getKernel();
                    for (int i = 0; i < count; i++)
                    {
                        Vector2d val = (Vector2d)value[i];
                        realKernel.addTypedValue(val.x, val.y);
                    }
                    res.FormatToXmlFile(pResultSaveName);
                    return true;
                }
                else if (pType == EKernelType.EKT_VECTOR3_LIST)
                {
                    int count = value.Count;
                    UdxDataset res = new UdxDataset();
                    UdxNode resNode = res.addChildNode(name, pType);
                    UdxKernelVector3dArray realKernel = (UdxKernelVector3dArray)resNode.getKernel();
                    for (int i = 0; i < count; i++)
                    {
                        Vector3d val = (Vector3d)value[i];
                        realKernel.addTypedValue(val.x, val.y, val.z);
                    }
                    res.FormatToXmlFile(pResultSaveName);
                    return true;
                }
                else if (pType == EKernelType.EKT_VECTOR4_LIST)
                {
                    int count = value.Count;
                    UdxDataset res = new UdxDataset();
                    UdxNode resNode = res.addChildNode(name, pType);
                    UdxKernelVector4dArray realKernel = (UdxKernelVector4dArray)resNode.getKernel();
                    for (int i = 0; i < count; i++)
                    {
                        Vector4d val = (Vector4d)value[i];
                        realKernel.addTypedValue(val.x, val.y, val.z, val.m);
                    }
                    res.FormatToXmlFile(pResultSaveName);
                    return true;
                }

                return false;
            }

            public UdxDataset conductMemoryUDXFileMapping(string name, object value, EKernelType pType)
            {
                if (pType == EKernelType.EKT_INT)
                {
                    UdxDataset res = new UdxDataset();
                    UdxNode resNode = res.addChildNode(name, pType);
                    ((UdxKernelIntValue)resNode.getKernel()).setTypedValue(Convert.ToInt32(value));
                    return res;
                }
                else if (pType == EKernelType.EKT_REAL)
                {
                    UdxDataset res = new UdxDataset();
                    UdxNode resNode = res.addChildNode(name, pType);
                    ((UdxKernelRealValue)resNode.getKernel()).setTypedValue(Convert.ToDouble(value));
                    return res;
                }
                else if (pType == EKernelType.EKT_STRING)
                {
                    UdxDataset res = new UdxDataset();
                    UdxNode resNode = res.addChildNode(name, pType);
                    ((UdxKernelStringValue)resNode.getKernel()).setTypedValue(Convert.ToString(value));
                    return res;
                }
                else if (pType == EKernelType.EKT_VECTOR2)
                {
                    UdxDataset res = new UdxDataset();
                    UdxNode resNode = res.addChildNode(name, pType);
                    Vector2d val = (Vector2d)value;
                    ((UdxKernelVector2dValue)resNode.getKernel()).setTypedValue(val.x, val.y);
                    return res;
                }
                else if (pType == EKernelType.EKT_VECTOR3)
                {
                    UdxDataset res = new UdxDataset();
                    UdxNode resNode = res.addChildNode(name, pType);
                    Vector3d val = (Vector3d)value;
                    ((UdxKernelVector3dValue)resNode.getKernel()).setTypedValue(val.x, val.y, val.z);
                    return res;
                }
                else if (pType == EKernelType.EKT_VECTOR4)
                {
                    UdxDataset res = new UdxDataset();
                    UdxNode resNode = res.addChildNode(name, pType);
                    Vector4d val = (Vector4d)value;
                    ((UdxKernelVector4dValue)resNode.getKernel()).setTypedValue(val.x, val.y, val.z, val.m);
                    return res;
                }
                return null;
            }

            public UdxDataset conductMemoryUDXFileMapping(string name, object[] value, EKernelType pType)
            {
                if (pType == EKernelType.EKT_INT_LIST)
                {
                    int count = value.Length;
                    UdxDataset res = new UdxDataset();
                    UdxNode resNode = res.addChildNode(name, pType);
                    UdxKernelIntArray realKernel = (UdxKernelIntArray)resNode.getKernel();
                    for (int i = 0; i < count; i++)
                    {
                        realKernel.addTypedValue(Convert.ToInt32(value[i]));
                    }
                    return res;
                }
                else if (pType == EKernelType.EKT_REAL_LIST)
                {
                    int count = value.Length;
                    UdxDataset res = new UdxDataset();
                    UdxNode resNode = res.addChildNode(name, pType);
                    UdxKernelRealArray realKernel = (UdxKernelRealArray)resNode.getKernel();
                    for (int i = 0; i < count; i++)
                    {
                        realKernel.addTypedValue(Convert.ToDouble(value[i]));
                    }
                    return res;
                }
                else if (pType == EKernelType.EKT_STRING_LIST)
                {
                    int count = value.Length;
                    UdxDataset res = new UdxDataset();
                    UdxNode resNode = res.addChildNode(name, pType);
                    UdxKernelStringArray realKernel = (UdxKernelStringArray)resNode.getKernel();
                    for (int i = 0; i < count; i++)
                    {
                        realKernel.addTypedValue(Convert.ToString(value[i]));
                    }
                    return res;
                }
                else if (pType == EKernelType.EKT_VECTOR2_LIST)
                {
                    int count = value.Length;
                    UdxDataset res = new UdxDataset();
                    UdxNode resNode = res.addChildNode(name, pType);
                    UdxKernelVector2dArray realKernel = (UdxKernelVector2dArray)resNode.getKernel();
                    for (int i = 0; i < count; i++)
                    {
                        Vector2d val = (Vector2d)value[i];
                        realKernel.addTypedValue(val.x, val.y);
                    }
                    return res;
                }
                else if (pType == EKernelType.EKT_VECTOR3_LIST)
                {
                    int count = value.Length;
                    UdxDataset res = new UdxDataset();
                    UdxNode resNode = res.addChildNode(name, pType);
                    UdxKernelVector3dArray realKernel = (UdxKernelVector3dArray)resNode.getKernel();
                    for (int i = 0; i < count; i++)
                    {
                        Vector3d val = (Vector3d)value[i];
                        realKernel.addTypedValue(val.x, val.y, val.z);
                    }
                    return res;
                }
                else if (pType == EKernelType.EKT_VECTOR4_LIST)
                {
                    int count = value.Length;
                    UdxDataset res = new UdxDataset();
                    UdxNode resNode = res.addChildNode(name, pType);
                    UdxKernelVector4dArray realKernel = (UdxKernelVector4dArray)resNode.getKernel();
                    for (int i = 0; i < count; i++)
                    {
                        Vector4d val = (Vector4d)value[i];
                        realKernel.addTypedValue(val.x, val.y, val.z, val.m);
                    }
                    return res;
                }
                return null;
            }

            public UdxDataset conductMemoryUDXFileMapping(string name, List<object> value, EKernelType pType)
            {
                if (pType == EKernelType.EKT_INT_LIST)
                {
                    int count = value.Count;
                    UdxDataset res = new UdxDataset();
                    UdxNode resNode = res.addChildNode(name, pType);
                    UdxKernelIntArray realKernel = (UdxKernelIntArray)resNode.getKernel();
                    for (int i = 0; i < count; i++)
                    {
                        realKernel.addTypedValue(Convert.ToInt32(value[i]));
                    }
                    return res;
                }
                else if (pType == EKernelType.EKT_REAL_LIST)
                {
                    int count = value.Count;
                    UdxDataset res = new UdxDataset();
                    UdxNode resNode = res.addChildNode(name, pType);
                    UdxKernelRealArray realKernel = (UdxKernelRealArray)resNode.getKernel();
                    for (int i = 0; i < count; i++)
                    {
                        realKernel.addTypedValue(Convert.ToDouble(value[i]));
                    }
                    return res;
                }
                else if (pType == EKernelType.EKT_STRING_LIST)
                {
                    int count = value.Count;
                    UdxDataset res = new UdxDataset();
                    UdxNode resNode = res.addChildNode(name, pType);
                    UdxKernelStringArray realKernel = (UdxKernelStringArray)resNode.getKernel();
                    for (int i = 0; i < count; i++)
                    {
                        realKernel.addTypedValue(Convert.ToString(value[i]));
                    }
                    return res;
                }
                else if (pType == EKernelType.EKT_VECTOR2_LIST)
                {
                    int count = value.Count;
                    UdxDataset res = new UdxDataset();
                    UdxNode resNode = res.addChildNode(name, pType);
                    UdxKernelVector2dArray realKernel = (UdxKernelVector2dArray)resNode.getKernel();
                    for (int i = 0; i < count; i++)
                    {
                        Vector2d val = (Vector2d)value[i];
                        realKernel.addTypedValue(val.x, val.y);
                    }
                    return res;
                }
                else if (pType == EKernelType.EKT_VECTOR3_LIST)
                {
                    int count = value.Count;
                    UdxDataset res = new UdxDataset();
                    UdxNode resNode = res.addChildNode(name, pType);
                    UdxKernelVector3dArray realKernel = (UdxKernelVector3dArray)resNode.getKernel();
                    for (int i = 0; i < count; i++)
                    {
                        Vector3d val = (Vector3d)value[i];
                        realKernel.addTypedValue(val.x, val.y, val.z);
                    }
                    return res;
                }
                else if (pType == EKernelType.EKT_VECTOR4_LIST)
                {
                    int count = value.Count;
                    UdxDataset res = new UdxDataset();
                    UdxNode resNode = res.addChildNode(name, pType);
                    UdxKernelVector4dArray realKernel = (UdxKernelVector4dArray)resNode.getKernel();
                    for (int i = 0; i < count; i++)
                    {
                        Vector4d val = (Vector4d)value[i];
                        realKernel.addTypedValue(val.x, val.y, val.z, val.m);
                    }
                    return res;
                }
                return null;
            }

            public string conductMemoryUDXStringMapping(string name, object value, EKernelType pType)
            {
                string udx_xml_str = "";
                if (pType == EKernelType.EKT_INT)
                {
                    UdxDataset res = new UdxDataset();
                    UdxNode resNode = res.addChildNode(name, pType);
                    ((UdxKernelIntValue)resNode.getKernel()).setTypedValue(Convert.ToInt32(value));
                    udx_xml_str = res.FormatToXmlStream();
                }
                else if (pType == EKernelType.EKT_REAL)
                {
                    UdxDataset res = new UdxDataset();
                    UdxNode resNode = res.addChildNode(name, pType);
                    ((UdxKernelRealValue)resNode.getKernel()).setTypedValue(Convert.ToDouble(value));
                    udx_xml_str = res.FormatToXmlStream();
                }
                else if (pType == EKernelType.EKT_STRING)
                {
                    UdxDataset res = new UdxDataset();
                    UdxNode resNode = res.addChildNode(name, pType);
                    ((UdxKernelStringValue)resNode.getKernel()).setTypedValue(Convert.ToString(value));
                    udx_xml_str = res.FormatToXmlStream();
                }
                else if (pType == EKernelType.EKT_VECTOR2)
                {
                    UdxDataset res = new UdxDataset();
                    UdxNode resNode = res.addChildNode(name, pType);
                    Vector2d val = (Vector2d)value;
                    ((UdxKernelVector2dValue)resNode.getKernel()).setTypedValue(val.x, val.y);
                    udx_xml_str = res.FormatToXmlStream();
                }
                else if (pType == EKernelType.EKT_VECTOR3)
                {
                    UdxDataset res = new UdxDataset();
                    UdxNode resNode = res.addChildNode(name, pType);
                    Vector3d val = (Vector3d)value;
                    ((UdxKernelVector3dValue)resNode.getKernel()).setTypedValue(val.x, val.y, val.z);
                    udx_xml_str = res.FormatToXmlStream();
                }
                else if (pType == EKernelType.EKT_VECTOR4)
                {
                    UdxDataset res = new UdxDataset();
                    UdxNode resNode = res.addChildNode(name, pType);
                    Vector4d val = (Vector4d)value;
                    ((UdxKernelVector4dValue)resNode.getKernel()).setTypedValue(val.x, val.y, val.z, val.m);
                    udx_xml_str = res.FormatToXmlStream();
                }
                return udx_xml_str;
            }

            public string conductMemoryUDXStringMapping(string name, object[] value, EKernelType pType)
            {
                string udx_xml_str = "";
                if (pType == EKernelType.EKT_INT_LIST)
                {
                    int count = value.Length;
                    UdxDataset res = new UdxDataset();
                    UdxNode resNode = res.addChildNode(name, pType);
                    UdxKernelIntArray realKernel = (UdxKernelIntArray)resNode.getKernel();
                    for (int i = 0; i < count; i++)
                    {
                        realKernel.addTypedValue(Convert.ToInt32(value[i]));
                    }
                    udx_xml_str = res.FormatToXmlStream();
                }
                else if (pType == EKernelType.EKT_REAL_LIST)
                {
                    int count = value.Length;
                    UdxDataset res = new UdxDataset();
                    UdxNode resNode = res.addChildNode(name, pType);
                    UdxKernelRealArray realKernel = (UdxKernelRealArray)resNode.getKernel();
                    for (int i = 0; i < count; i++)
                    {
                        realKernel.addTypedValue(Convert.ToDouble(value[i]));
                    }
                    udx_xml_str = res.FormatToXmlStream();
                }
                else if (pType == EKernelType.EKT_STRING_LIST)
                {
                    int count = value.Length;
                    UdxDataset res = new UdxDataset();
                    UdxNode resNode = res.addChildNode(name, pType);
                    UdxKernelStringArray realKernel = (UdxKernelStringArray)resNode.getKernel();
                    for (int i = 0; i < count; i++)
                    {
                        realKernel.addTypedValue(Convert.ToString(value[i]));
                    }
                    udx_xml_str = res.FormatToXmlStream();
                }
                else if (pType == EKernelType.EKT_VECTOR2_LIST)
                {
                    int count = value.Length;
                    UdxDataset res = new UdxDataset();
                    UdxNode resNode = res.addChildNode(name, pType);
                    UdxKernelVector2dArray realKernel = (UdxKernelVector2dArray)resNode.getKernel();
                    for (int i = 0; i < count; i++)
                    {
                        Vector2d val = (Vector2d)value[i];
                        realKernel.addTypedValue(val.x, val.y);
                    }
                    udx_xml_str = res.FormatToXmlStream();
                }
                else if (pType == EKernelType.EKT_VECTOR3_LIST)
                {
                    int count = value.Length;
                    UdxDataset res = new UdxDataset();
                    UdxNode resNode = res.addChildNode(name, pType);
                    UdxKernelVector3dArray realKernel = (UdxKernelVector3dArray)resNode.getKernel();
                    for (int i = 0; i < count; i++)
                    {
                        Vector3d val = (Vector3d)value[i];
                        realKernel.addTypedValue(val.x, val.y, val.z);
                    }
                    udx_xml_str = res.FormatToXmlStream();
                }
                else if (pType == EKernelType.EKT_VECTOR4_LIST)
                {
                    int count = value.Length;
                    UdxDataset res = new UdxDataset();
                    UdxNode resNode = res.addChildNode(name, pType);
                    UdxKernelVector4dArray realKernel = (UdxKernelVector4dArray)resNode.getKernel();
                    for (int i = 0; i < count; i++)
                    {
                        Vector4d val = (Vector4d)value[i];
                        realKernel.addTypedValue(val.x, val.y, val.z, val.m);
                    }
                    udx_xml_str = res.FormatToXmlStream();
                }
                return udx_xml_str;
            }

            public string conductMemoryUDXStringMapping(string name, List<object> value, EKernelType pType)
            {
                string udx_xml_str = "";
                if (pType == EKernelType.EKT_INT_LIST)
                {
                    int count = value.Count;
                    UdxDataset res = new UdxDataset();
                    UdxNode resNode = res.addChildNode(name, pType);
                    UdxKernelIntArray realKernel = (UdxKernelIntArray)resNode.getKernel();
                    for (int i = 0; i < count; i++)
                    {
                        realKernel.addTypedValue(Convert.ToInt32(value[i]));
                    }
                    udx_xml_str = res.FormatToXmlStream();
                }
                else if (pType == EKernelType.EKT_REAL_LIST)
                {
                    int count = value.Count;
                    UdxDataset res = new UdxDataset();
                    UdxNode resNode = res.addChildNode(name, pType);
                    UdxKernelRealArray realKernel = (UdxKernelRealArray)resNode.getKernel();
                    for (int i = 0; i < count; i++)
                    {
                        realKernel.addTypedValue(Convert.ToDouble(value[i]));
                    }
                    udx_xml_str = res.FormatToXmlStream();
                }
                else if (pType == EKernelType.EKT_STRING_LIST)
                {
                    int count = value.Count;
                    UdxDataset res = new UdxDataset();
                    UdxNode resNode = res.addChildNode(name, pType);
                    UdxKernelStringArray realKernel = (UdxKernelStringArray)resNode.getKernel();
                    for (int i = 0; i < count; i++)
                    {
                        realKernel.addTypedValue(Convert.ToString(value[i]));
                    }
                    udx_xml_str = res.FormatToXmlStream();
                }
                else if (pType == EKernelType.EKT_VECTOR2_LIST)
                {
                    int count = value.Count;
                    UdxDataset res = new UdxDataset();
                    UdxNode resNode = res.addChildNode(name, pType);
                    UdxKernelVector2dArray realKernel = (UdxKernelVector2dArray)resNode.getKernel();
                    for (int i = 0; i < count; i++)
                    {
                        Vector2d val = (Vector2d)value[i];
                        realKernel.addTypedValue(val.x, val.y);
                    }
                    udx_xml_str = res.FormatToXmlStream();
                }
                else if (pType == EKernelType.EKT_VECTOR3_LIST)
                {
                    int count = value.Count;
                    UdxDataset res = new UdxDataset();
                    UdxNode resNode = res.addChildNode(name, pType);
                    UdxKernelVector3dArray realKernel = (UdxKernelVector3dArray)resNode.getKernel();
                    for (int i = 0; i < count; i++)
                    {
                        Vector3d val = (Vector3d)value[i];
                        realKernel.addTypedValue(val.x, val.y, val.z);
                    }
                    udx_xml_str = res.FormatToXmlStream();
                }
                else if (pType == EKernelType.EKT_VECTOR4_LIST)
                {
                    int count = value.Count;
                    UdxDataset res = new UdxDataset();
                    UdxNode resNode = res.addChildNode(name, pType);
                    UdxKernelVector4dArray realKernel = (UdxKernelVector4dArray)resNode.getKernel();
                    for (int i = 0; i < count; i++)
                    {
                        Vector4d val = (Vector4d)value[i];
                        realKernel.addTypedValue(val.x, val.y, val.z, val.m);
                    }
                    udx_xml_str = res.FormatToXmlStream();
                }
                return udx_xml_str;
            }
            
            //////////////////////////////////////////////////////////////////////////
            //
            //
            //
            //////////////////////////////////////////////////////////////////////////
            public string doRequestEvent_MappingData(string pStateId1, string pEventId1, string mappingMethod)
            {
                mModelServiceContext.onFireEvent(pStateId1, pEventId1);
                mModelServiceContext.onRequestData(pStateId1, pEventId1);

                string data_path = "";
                if (mModelServiceContext.getRequestDataFlag() == ERequestResponseDataFlag.ERDF_OK)
                {
                    this.connectDataMappingMethod(mappingMethod);
                    this.conductUDXMapping("");
                    data_path = this.getRealResultSaveName();
                }
                else
                {
                    mModelServiceContext.onFinalize();
                }

                mModelServiceContext.onPostMessageInfo("data_path" + data_path);

                return data_path;
            }

            public UdxDataset doRequestEvent_GetUDX(string pStateId1, string pEventId1)
            {
                mModelServiceContext.onFireEvent(pStateId1, pEventId1);
                mModelServiceContext.onRequestData(pStateId1, pEventId1);

                if (mModelServiceContext.getRequestDataFlag() == ERequestResponseDataFlag.ERDF_OK)
                {
                    UdxDataset dataset = this.tryGetUdxDataset();
                    return dataset;
                }
                else
                {
                    mModelServiceContext.onFinalize();
                }
                return null;
            }

            //////////////////////////////////////////////////////////////////////////
            //
            //
            //
            //////////////////////////////////////////////////////////////////////////

            public int getResponseStyle()
            {
                string exe_name = Process.GetCurrentProcess().MainModule.FileName;
                string exe_path = Directory.GetParent(exe_name).FullName;
                string data_style_path = exe_path + "\\data_style.txt";
                
                if (File.Exists(data_style_path) == false)
                    return 0;
                else
                {
                    StreamReader sr = new StreamReader(data_style_path);
                    string style_str = sr.ReadLine();
                    int style_value = Int32.Parse(style_str);
                    return style_value;
                }
            }

            public void doResponseData_MappingData(string pStateId1, string pEventId1, string file_name, string mappingMethod)
            {
                int response_style = getResponseStyle();
                mModelServiceContext.onPostMessageInfo("response_style: " + response_style);
                if (response_style == 1)
                {
                    mModelServiceContext.setResponseDataFlag(ERequestResponseDataFlag.ERDF_OK);
                    mModelServiceContext.setResponseDataMIME(ERequestResponseDataMIME.ERDM_XML_FILE);

                    string file_pure_name = Path.GetFileNameWithoutExtension(file_name);
                    string save_name = "udx_xml_" + file_pure_name + ".xml";
                    this.connectDataMappingMethod(mappingMethod);
                    bool mapping_ret = this.conductFileMapping(save_name, file_name);
                    
                    if (mapping_ret == false)
                    {
                        mModelServiceContext.setResponseDataFlag(ERequestResponseDataFlag.ERDF_OK);
                        mModelServiceContext.setResponseDataMIME(ERequestResponseDataMIME.ERDM_RAW_FILE);
                        mModelServiceContext.setResponseDataBody(file_name);
                    }
                    else
                    {
                        string name1 = this.getRealResultSaveName();
                        mModelServiceContext.setResponseDataBody(name1);
                    }
                }
                else if (response_style == 2)
                {
                    if (File.Exists(file_name))
                    {
                        mModelServiceContext.setResponseDataFlag(ERequestResponseDataFlag.ERDF_OK);
                        mModelServiceContext.setResponseDataMIME(ERequestResponseDataMIME.ERDM_RAW_FILE);
                        mModelServiceContext.setResponseDataBody(file_name);
                    }
                    else
                    {
                        mModelServiceContext.setResponseDataFlag(ERequestResponseDataFlag.ERDF_ERROR);
                        mModelServiceContext.setResponseDataMIME(ERequestResponseDataMIME.ERDM_RAW_FILE);
                        mModelServiceContext.setResponseDataBody("");
                    }
                }
                else
                {
                    mModelServiceContext.setResponseDataFlag(ERequestResponseDataFlag.ERDF_OK);
                    mModelServiceContext.setResponseDataMIME(ERequestResponseDataMIME.ERDM_ZIP_FILE);

                    string file_pure_name = Path.GetFileNameWithoutExtension(file_name);
                    string save_name = "udx_zip_" + file_pure_name + ".zip";

                    if (mappingMethod == "")
                    {
                        mModelServiceContext.setResponseDataMIME(ERequestResponseDataMIME.ERDM_RAW_FILE);
                    }

                    List<string> rawFiles = new List<string>();
                    string file_dir = Directory.GetParent(file_name).FullName;
                    string file_name_without_exe = Path.GetFileNameWithoutExtension(file_name);
                    string[] file_list = Directory.GetFiles(file_dir, file_name_without_exe+".*");
                    for (int iFile = 0; iFile < file_list.Length; iFile++)
                    {
                        string temp_file_name = file_list[iFile];
                        rawFiles.Add(temp_file_name);
                    }
                    this.connectDataMappingMethod(mappingMethod);
                    bool mapping_ret = this.conductFileMapping(save_name, rawFiles, file_list.Length);
                    if (mapping_ret == false)
                    {
                        mModelServiceContext.setResponseDataFlag(ERequestResponseDataFlag.ERDF_ERROR);
                        mModelServiceContext.setResponseDataMIME(ERequestResponseDataMIME.ERDM_XML_FILE);
                        mModelServiceContext.setResponseDataBody("");
                    }
                    else
                    {
                        string name1 = this.getRealResultSaveName();
                        mModelServiceContext.setResponseDataBody(name1);
                    }
                }

                mModelServiceContext.onResponseData(pStateId1, pEventId1);
            }

            public void doResponseData_SimpleUDX(string pStateId1, string pEventId1, object value, string nodeName, EKernelType pKernelType)
            {
                int response_style = getResponseStyle();
                if (response_style == 1)
                {
                    mModelServiceContext.setResponseDataFlag(ERequestResponseDataFlag.ERDF_OK);
                    mModelServiceContext.setResponseDataMIME(ERequestResponseDataMIME.ERDM_XML_STREAM);

                    string result = this.conductMemoryUDXStringMapping(nodeName, value, pKernelType);
                    if (result != "")
                    {
                        mModelServiceContext.setResponseDataBody(result);
                    }
                    else
                    {
                        mModelServiceContext.setResponseDataFlag(ERequestResponseDataFlag.ERDF_ERROR);
                        mModelServiceContext.setResponseDataMIME(ERequestResponseDataMIME.ERDM_XML_STREAM);
                        mModelServiceContext.setResponseDataBody(result);
                    }
                }
                else
                {
                    mModelServiceContext.setResponseDataFlag(ERequestResponseDataFlag.ERDF_OK);
                    mModelServiceContext.setResponseDataMIME(ERequestResponseDataMIME.ERDM_XML_FILE);

                    bool ret = this.conductSimpleFileMapping("udx_xml_"+nodeName, nodeName, value, pKernelType);
                    if (ret != false)
                    {
                        mModelServiceContext.setResponseDataBody(this.getRealResultSaveName());
                    }
                    else
                    {
                        mModelServiceContext.setResponseDataFlag(ERequestResponseDataFlag.ERDF_ERROR);
                        mModelServiceContext.setResponseDataMIME(ERequestResponseDataMIME.ERDM_XML_FILE);
                        mModelServiceContext.setResponseDataBody("");
                    }
                }

                mModelServiceContext.onResponseData(pStateId1, pEventId1);
            }

            public void doResponseData_UdxDataset(string pStateId1, string pEventId1, UdxDataset dataset)
            {
                int response_style = getResponseStyle();
                if (response_style == 1)
                {
                    mModelServiceContext.setResponseDataFlag(ERequestResponseDataFlag.ERDF_OK);
                    mModelServiceContext.setResponseDataMIME(ERequestResponseDataMIME.ERDM_XML_STREAM);
                    string result = "";
                    try
                    {
                        result = dataset.FormatToXmlStream();
                        mModelServiceContext.setResponseDataBody(result);	
                    }
                    catch (System.Exception ex)
                    {
                        mModelServiceContext.setResponseDataFlag(ERequestResponseDataFlag.ERDF_ERROR);
                        mModelServiceContext.setResponseDataMIME(ERequestResponseDataMIME.ERDM_XML_STREAM);
                        mModelServiceContext.setResponseDataBody(result);	
                    }
                }
                else
                {
                    mModelServiceContext.setResponseDataFlag(ERequestResponseDataFlag.ERDF_OK);
                    mModelServiceContext.setResponseDataMIME(ERequestResponseDataMIME.ERDM_XML_FILE);

                    mSavePath = mModelServiceContext.getCurrentDataDirectory();
                    mSaveName = "udx_xml_"+ pEventId1 +".xml";
                    string pResultSaveName = mSavePath + mSaveName;

                    try
                    {
                        dataset.FormatToXmlFile(pResultSaveName);
                        mModelServiceContext.setResponseDataBody(this.getRealResultSaveName());
                    }
                    catch (System.Exception ex)
                    {
                        mModelServiceContext.setResponseDataFlag(ERequestResponseDataFlag.ERDF_ERROR);
                        mModelServiceContext.setResponseDataMIME(ERequestResponseDataMIME.ERDM_XML_FILE);
                        mModelServiceContext.setResponseDataBody("");	
                    }
                }

                mModelServiceContext.onResponseData(pStateId1, pEventId1);
            }

        }
    }
}
