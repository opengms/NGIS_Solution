//!文件属于 NGIS 工程的一部分
//!作者:乐松山
//!创建时间:2016年8月5日

#ifndef __I_NGISDATA_UNKNOWN_H__
#define __I_NGISDATA_UNKNOWN_H__

namespace NGIS
{
	//!基类，支持引用计数
	class INxUnknown
	{
	public:
		//!构造函数
		INxUnknown(): mReferenceCounter(1)
		{

		}

		//!析构函数
		virtual ~INxUnknown(){};

		//!添加引用计数
		virtual int addRef()
		{
			return ++mReferenceCounter;
		}

		//!减少引用计数
		virtual int release()
		{
			mReferenceCounter--;
			if(!mReferenceCounter){
				delete this;return 0;
			}
			else{
				return mReferenceCounter;
			}
		}
		//!获得计数个数
		virtual int getReferenceCounter(){return mReferenceCounter;}

	protected:
		int mReferenceCounter;
	};
}

#endif