//!文件属于 NGIS 工程的一部分
//!作者:乐松山
//!创建时间:2016年8月5日

#ifndef __NGIS_UDXSCHEMA_API_H__
#define __NGIS_UDXSCHEMA_API_H__

#include "IUdxSchemaDataset.h"

// 在windows上的api开放设置
#ifdef _WINDOWS

#ifndef _NGISDATASCHEMA_STATIC_LIB_
#ifdef NGISDATASCHEMA_EXPORTS
#define NGISDATASCHEMA_API __declspec(dllexport)
#else
#define NGISDATASCHEMA_API __declspec(dllimport)
#endif // NGISDATASCHEMA_API
#else
#define NGISDATASCHEMA_API
#endif // _NGISDATASCHEMA_STATIC_LIB_

// Declare the calling convention.
#if defined(_STDCALL_SUPPORTED)
#define NGISDATASCHEMA_CALLCONV __stdcall
#else
#define NGISDATASCHEMA_CALLCONV __cdecl
#endif // STDCALL_SUPPORTED

#else // _IMP_WINDOWS_API_

// Force symbol export in shared libraries built with gcc.
#if (__GNUC__ >= 4) && !defined(_NGISDATASCHEMA_STATIC_LIB_) && defined(NGISDATASCHEMA_EXPORTS)
#define NGISDATASCHEMA_API __attribute__ ((visibility("default")))
#else
#define NGISDATASCHEMA_API
#endif
#define NGISDATASCHEMA_CALLCONV

#endif

//////////////////////////////////

// 以下用于导出对外开放的API接口
namespace NGIS
{
	namespace Data
	{
		namespace Schema
		{
			extern "C" NGISDATASCHEMA_API IUdxDatasetSchema* NGISDATASCHEMA_CALLCONV createUdxDatasetSchema(const char* pName);

			extern "C" NGISDATASCHEMA_API INodeDescription* NGISDATASCHEMA_CALLCONV createUdxNodeDescription(ESchemaNodeType pKernelType, const char* pNodeInfo="");
		}
	}

}
#endif

