#include "UdxDataSchemaApi.h"
#include "UdxDatasetSchema.h"
#include "UdxSchemaDescription.h"

namespace NGIS
{
	namespace Data
	{
		namespace Schema
		{

			//////////////////////////////////////////////////////////////////////////
			extern "C" NGISDATASCHEMA_API IUdxDatasetSchema* NGISDATASCHEMA_CALLCONV createUdxDatasetSchema(const char* pName)
			{
				CUdxDatasetSchema* pUdxDatasetSchema = new CUdxDatasetSchema(pName);
				return pUdxDatasetSchema;
			}

			extern "C" NGISDATASCHEMA_API INodeDescription* NGISDATASCHEMA_CALLCONV createUdxNodeDescription(ESchemaNodeType pNodeType, const char* pNodeInfo)
			{
				CUdxSchemaDescription* pNodeDescription = new CUdxSchemaDescription(pNodeType, pNodeInfo);
				return pNodeDescription;
			}

		}
	}
}
