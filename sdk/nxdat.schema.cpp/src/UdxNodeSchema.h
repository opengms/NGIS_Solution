#ifndef __C_NGISDATA_UDXSCHEMA_NODE_H__
#define __C_NGISDATA_UDXSCHEMA_NODE_H__

#include "IUdxSchemaNode.h"
#include "UdxSchemaDescription.h"
#include <vector>

namespace NGIS
{
	namespace Data
	{
		namespace Schema
		{
			class CUdxNodeSchema : public IUdxNodeSchema
			{
			public:
				CUdxNodeSchema(IUdxNodeSchema* pParent, const char* pName, INodeDescription* pDescription);

				~CUdxNodeSchema();

				//////////////////////////////////////////////////////////////////////////
				virtual const char* getName();

				virtual INodeDescription* getDescription();

				virtual bool modifyName(const char* pName);

				virtual IUdxNodeSchema* getParentNode();

				virtual int getChildNodeCount();

				virtual IUdxNodeSchema* getChildNode(int idx);

				virtual IUdxNodeSchema* addChildNode(const char* pName, INodeDescription* pDescription);

				virtual IUdxNodeSchema* addChildNode(const char* pName, int pNodeType, const char* pNodeInfo="");

				virtual bool removeChildNode(IUdxNodeSchema* pNode);

				virtual bool removeChildNode(int idx);

				virtual void setExtension(int flag);

				virtual int getExtension();

				virtual bool compareOther(IUdxNodeSchema* pNode);

			private:
				bool compareNodeInfo(IUdxNodeSchema* pNode1, IUdxNodeSchema* pNode2);

			protected:
				std::string											mName;
				INodeDescription*							mDescription;
				std::vector<IUdxNodeSchema*>		mChildNodes;
				IUdxNodeSchema*							mParentNode;
				int													mExtension;
			};
		}
	}
}


#endif