﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NGIS.Data;
using NGIS.Data.Schema;

namespace DataMappingTemplateDotNet
{
    public class UDXUtility
    {
        public static EKernelType getKernelTypeBySchema(ESchemaNodeType pSType)
		{
			if (pSType == ESchemaNodeType.EDTKT_INT)
			{
                return EKernelType.EKT_INT;
			}
			else if (pSType == ESchemaNodeType.EDTKT_REAL)
			{
                return EKernelType.EKT_REAL;
			}
			else if (pSType == ESchemaNodeType.EDTKT_VECTOR2)
			{
                return EKernelType.EKT_VECTOR2;
			}
			else if (pSType == ESchemaNodeType.EDTKT_VECTOR3)
			{
                return EKernelType.EKT_VECTOR3;
			}
			else if (pSType == ESchemaNodeType.EDTKT_VECTOR4)
			{
                return EKernelType.EKT_VECTOR4;
			}
			else if (pSType == ESchemaNodeType.EDTKT_STRING)
			{
                return EKernelType.EKT_STRING;
			}
			else if (pSType == (ESchemaNodeType.EDTKT_INT | ESchemaNodeType.EDTKT_LIST))
			{
                return EKernelType.EKT_INT | EKernelType.EKT_LIST;
			}
			else if (pSType == (ESchemaNodeType.EDTKT_REAL | ESchemaNodeType.EDTKT_LIST))
            {
                return EKernelType.EKT_REAL | EKernelType.EKT_LIST;
			}
			else if (pSType == (ESchemaNodeType.EDTKT_VECTOR2 | ESchemaNodeType.EDTKT_LIST))
            {
                return EKernelType.EKT_VECTOR2 | EKernelType.EKT_LIST;
			}
			else if (pSType == (ESchemaNodeType.EDTKT_VECTOR3 | ESchemaNodeType.EDTKT_LIST))
            {
                return EKernelType.EKT_VECTOR3 | EKernelType.EKT_LIST;
			}
			else if (pSType == (ESchemaNodeType.EDTKT_VECTOR4 | ESchemaNodeType.EDTKT_LIST))
            {
                return EKernelType.EKT_VECTOR4 | EKernelType.EKT_LIST;
			}
			else if (pSType == (ESchemaNodeType.EDTKT_STRING | ESchemaNodeType.EDTKT_LIST))
            {
                return EKernelType.EKT_STRING | EKernelType.EKT_LIST;
			}
			else if (pSType == ESchemaNodeType.EDTKT_NODE)
                return EKernelType.EKT_NODE;
			else if (pSType == ESchemaNodeType.EDTKT_LIST)
                return EKernelType.EKT_LIST;
			else if (pSType == ESchemaNodeType.EDTKT_MAP)
                return EKernelType.EKT_MAP;
			else if (pSType == ESchemaNodeType.EDTKT_TABLE)
                return EKernelType.EKT_TABLE;
            return EKernelType.EKT_COUNT;
		}

        public static void IterateChildNodes(UdxNodeSchema pSNode, UdxNode pDNode)
        {
            if (pSNode == null) return;
            int count = pSNode.getChildNodeCount();
            if (count == 0) return;
            for (int i = 0; i < count; i++)
            {
                UdxNodeSchema tempSNode = pSNode.getChildNode(i);
                string name = tempSNode.getName();
                ESchemaNodeType pSType = tempSNode.getDescription().getKernelType();
                EKernelType pDType = getKernelTypeBySchema(pSType);
                UdxNode tempDNode = pDNode.addChildNode(name, pDType);
                IterateChildNodes(tempSNode, tempDNode);
            }
        }

        public static bool constructNode(UdxNodeSchema pSNode, UdxNode pDNode)
        {
            for (int i = 0; i < pSNode.getChildNodeCount(); i++)
            {
                UdxNodeSchema tempSNode = pSNode.getChildNode(i);
                string name = tempSNode.getName();
                ESchemaNodeType pSType = tempSNode.getDescription().getKernelType();
                EKernelType pDType = getKernelTypeBySchema(pSType);
                UdxNode tempDNode = pDNode.addChildNode(name, pDType);
                IterateChildNodes(tempSNode, tempDNode);
            }
            return true;
        }
    }
}
