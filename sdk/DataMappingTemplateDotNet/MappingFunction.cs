﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataMappingTemplateDotNet
{
    public delegate string  fn_get_mapping_version();
    public delegate string  fn_get_mapping_name();
    public delegate string  fn_get_supported_schema();
    public delegate int       fn_get_capability_count();
    public delegate string  fn_get_capability_by_idx(int idx);
    public delegate bool    fn_set_node_read_index(int node_idx, int read_idx);
    public delegate bool    fn_read_to_node(string format_filename, string node_name, string save_filename);
    public delegate bool    fn_write_from_node(string udx_filename, string save_filename);

    public class MappingFunction
    {
        public event fn_get_mapping_version GetMethodVersion_Event;
        public event fn_get_mapping_name GetMethodName_Event;
        public event fn_get_supported_schema GetSupportSchema_Event;
        public event fn_get_capability_count GetCapabilityCount_Event;
        public event fn_get_capability_by_idx GetCapabilityByIdx_Event;
        public event fn_set_node_read_index SetNodeReadIndex_Event;
        public event fn_read_to_node ReadToNode_Event;
        public event fn_write_from_node WriteFromNode_Event;

        public string getVersion()
        {
            if (GetMethodVersion_Event != null)
            {
                return GetMethodVersion_Event.Invoke();
            }
            return "unknow";
        }

        public string getName()
        {
            if (GetMethodName_Event != null)
            {
                return GetMethodName_Event.Invoke();
            }
            return "unknow";
        }

        public string getSchema()
        {
            if (GetSupportSchema_Event != null)
            {
                return GetSupportSchema_Event.Invoke();
            }
            return "unknow";
        }

        public int getCapabilityCount()
        {
            if (GetCapabilityCount_Event != null)
            {
                return GetCapabilityCount_Event.Invoke();
            }
            return 0;
        }
        
        public string getCapabilityByIndex(int idx)
        {
            int count = getCapabilityCount();
            if (idx < 0 || idx >= count)
            {
                return "";
            }

            if (GetCapabilityCount_Event != null)
            {
                return GetCapabilityByIdx_Event.Invoke(idx);
            }
            return "";
        }

        public bool setReadNodeIndex(int node_idx, int read_idx)
        {
            if (SetNodeReadIndex_Event != null)
            {
                return SetNodeReadIndex_Event(node_idx, read_idx);
            }
            return false;
        }

        public bool readToNode(string format_filename, string node_name, string save_filename)
        {
            if (System.IO.File.Exists(format_filename) == false)
            {
                Console.Write("Input file does not exist\n");
                return false;
            }

            if (ReadToNode_Event != null)
            {
                return ReadToNode_Event(format_filename, node_name, save_filename);
            }
            return false;
        }

        public bool writeFromNode(string udx_filename, string save_filename)
        {
            if (System.IO.File.Exists(udx_filename) == false)
            {
                Console.Write("Input file does not exist\n");
                return false;
            }

            if (WriteFromNode_Event != null)
            {
                return WriteFromNode_Event(udx_filename, save_filename);
            }
            return false;
        }
    }

}
