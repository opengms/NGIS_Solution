﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataMappingTemplateDotNet
{
    class Program
    {
        static MappingFunction global_mapping_function = null;

        static void Main(string[] args)
        {
            int i = 0;
            string udx_filename = "";
            string format_filename = "";
            string node_idx_array = "";
            string read_node_name = "";

            int read_write_flag = -1; //0表示读UDX，1表示写原始数据

            if (global_mapping_function == null)
            {
                global_mapping_function = new MappingFunction();
                global_mapping_function.GetMethodVersion_Event += new fn_get_mapping_version(MethodImp.getVersion_imp);
                global_mapping_function.GetMethodName_Event += new fn_get_mapping_name(MethodImp.getName_imp);
                global_mapping_function.GetSupportSchema_Event += new fn_get_supported_schema(MethodImp.getSchema_imp);
                global_mapping_function.GetCapabilityCount_Event += new fn_get_capability_count(MethodImp.getCapabilityCount_imp);
                global_mapping_function.GetCapabilityByIdx_Event += new fn_get_capability_by_idx(MethodImp.getCapabilityName_imp);
                global_mapping_function.SetNodeReadIndex_Event += new fn_set_node_read_index(MethodImp.setNodeReadIndex_imp);
                global_mapping_function.ReadToNode_Event += new fn_read_to_node(MethodImp.readToNode_imp);
                global_mapping_function.WriteFromNode_Event += new fn_write_from_node(MethodImp.writeFromNode);
            }

            if (args.Length == 0)
            {
                Console.Write("**************Data Mapping Method**************\n");
                Console.Write("version: {0} \n", global_mapping_function.getVersion());                
            }
            else
            {
                for (i=0; i<args.Length; i++)
		        {
                    if (args[i][0] == '-')
                    {
                        string cmd = args[i].Substring(1).ToUpper();
                        if (cmd == "H")
                        {
                            do_help();
                        }
                        else if (cmd == "V")
                        {
                            Console.Write("version: {0} \n", global_mapping_function.getVersion());    
                        }
                        else if (cmd == "S")
                        {
                            Console.Write(global_mapping_function.getSchema());
                        }
                        else if (cmd == "C")
                        {
                            int capablity_count = global_mapping_function.getCapabilityCount();
                            Console.Write("capability count: {0} \n", capablity_count);
                            for (int cap = 0; cap < capablity_count; cap++)
                            {
                                string cap_name = global_mapping_function.getCapabilityByIndex(cap);
                                Console.Write("capability name: {0} \n", cap_name);
                            }
                        }
                        else if (cmd == "W")
                        {
                            read_write_flag = 1;
                        }
                        else if (cmd == "R")
                        {
                            read_write_flag = 0;
                        }
                        else if (cmd == "U")
                        {
                            if (args.Length > i+1)
                                udx_filename = args[i + 1];
                            else
                                udx_filename = "";
                        }
                        else if (cmd == "F")
                        {
                            if (args.Length > i + 1)
                                format_filename = args[i + 1];
                            else
                                format_filename = "";
                        }
                        else if (cmd == "I")
                        {
						    node_idx_array=args[i+1];
                            int idx1 = node_idx_array.IndexOf('[');
                            int idx2 = node_idx_array.IndexOf(']');
                            node_idx_array = node_idx_array.Substring(idx1 + 1, idx2 - idx1 - 1);
                            string[] node_idx_array_str = node_idx_array.Split('&');
                            for (int idx = 0; idx < node_idx_array_str.Length; idx++ )
                            {
                                global_mapping_function.setReadNodeIndex(idx, Convert.ToInt32(node_idx_array_str[idx]));
                            }
                        }
                        else if (cmd == "N")
                        {
                            read_node_name = args[i + 1];
                        }
                        else
                        {
                            do_help();
                            return;
                        }
                    }
                    else
                    {
                    }
		        } //! End for
            } // End if (args.Length == 0)

            Console.Write("Parse End\n");
            if (read_write_flag == 0 && format_filename != "")
            {
                Console.Write("executing read udx node from formated file\n");
                global_mapping_function.readToNode(format_filename, read_node_name, udx_filename);
            }
            if (read_write_flag == 1 && udx_filename != "" && format_filename != "")
            {
                Console.Write("executing write formated file from udx node\n");
                global_mapping_function.writeFromNode(udx_filename, format_filename);
            }

            Console.ReadKey();
        }

        static void do_help()
        {
            Console.Write("Usage : DataMappingTemplate.exe [-h] [-v] [-c] [-u] udx.xml [-f] format.* [-i] [1,1,1]\n\n" +
                "  -h\tShow help information\n" +
                "  -v\tShow version\n" +
                "  -c\tList Capabilitier\n" +
                "  -u\tUDX file path\n" +
		        "  -r\tRead UDX node from raw data\n" +
		        "  -w\tWrite raw data from UDX node\n" +
                "  -f\tOriginal format file path\n" +
                "  -n\tName of try read node\n" +
                "  -i\tSet read index for each related node\n\n");
        }

    }
}
