#include "nxid.h"
#include <windows.h>
#include <set>
#include <string>
using namespace std;
namespace mp{
	class Cnxid_tInstance
	{
	public:
		Cnxid_tInstance()
		{
			InitializeCriticalSection(&m_crtital_section);
		}
		~Cnxid_tInstance()
		{
			DeleteCriticalSection(&m_crtital_section);
		}
		nxid_t id(const char* n)
		{
			std::set<string>::iterator it;
			EnterCriticalSection(&m_crtital_section);
				string _n(n);
				m_set.insert(_n); 
				it=m_set.find(_n);
			LeaveCriticalSection(&m_crtital_section);
			return nxid_t((*it).c_str());
			//return u;
		}
	private:
		CRITICAL_SECTION 	m_crtital_section;
		std::set<string> 	m_set;
	};

	Cnxid_tInstance gInstance;

	
}

extern"C"{
	__NXID_API__	 nxid_t  nxid(const char* n)
	{
		if(n==NULL)
			return NULL;
		return mp::gInstance.id(n);
	}
}