﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NGIS.Data.Schema
{
    /// <summary>
    /// 国际化标示
    /// </summary>
    public enum LocalizationEnum
    {
        ZH_CN = 1,
        EN_US = 2
    }

    /// <summary>
    /// 国际化描述
    /// </summary>
    public class LocalizationClass
    {
        public LocalizationClass(LocalizationEnum localtype)
        {
            local = localtype;
        }

        public LocalizationClass()
        {

        }

        private LocalizationEnum local;
        public LocalizationEnum Local
        {
            get { return local; }
            set { local = value; }
        }

        private string typeString;
        public string TypeString
        {
            get { return typeString; }
            set { typeString = value; }
        }


        private string name;
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        private string description;
        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        private string symbol;
        public string Symbol
        {
            get { return symbol; }
            set { symbol = value; }
        }

    }


}
