﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NGIS.Data.Schema
{
    /// <summary>
    /// 概念词条定义
    /// </summary>
    public class ConceptClass
    {
        public ConceptClass()
        {

        }

        /// <summary>
        /// 唯一标识符
        /// </summary>
        internal string id;
        public string ID
        {
            get { return id; }
            set { id = value; }
        }

        /// <summary>
        /// 标识的名称
        /// </summary>
        internal string name;
        public string NAME
        {
            get { return name; }
            set { name = value; }
        }

        /// <summary>
        /// 指向的地址
        /// </summary>
        internal string urn;
        public string URN
        {
            get { return urn; }
            set { urn = value; }
        }

        /// <summary>
        /// 本地化描述
        /// </summary>
        internal List<LocalizationClass> localizations = new List<LocalizationClass>();
        public List<LocalizationClass> Localizations
        {
            get { return localizations; }
        }

        /// <summary>
        /// 分类的描述
        /// </summary>
        internal List<ClassiferClass> classifers = new List<ClassiferClass>();
        public List<ClassiferClass> Classifers
        {
            get { return classifers; }
        }

        #region 本地化描述的事件

        /// <summary>
        /// 根据本地化种类返回某条本地化描述
        /// </summary>
        /// <param name="lenum"></param>
        /// <returns></returns>
        public LocalizationClass ReturnLocalization(LocalizationEnum lenum)
        {
            try
            {
                if (localizations.Count < 1) return null;
                foreach (LocalizationClass item in localizations)
                {
                    if (item.Local == lenum)
                    {
                        return item;
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// 根据本地化种类删除词条描述
        /// </summary>
        /// <param name="lenum"></param>
        /// <returns></returns>
        public bool RemoveLocalization(LocalizationEnum lenum)
        {
            try
            {
                if (localizations.Count < 1) return false;
                foreach (LocalizationClass item in localizations)
                {
                    if (item.Local == lenum)
                    {
                        return localizations.Remove(item);
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// 添加一条本地化描述
        /// </summary>
        /// <param name="item"></param>
        public void AddLocalization(LocalizationClass item)
        {
            if (item == null) return;
            localizations.Add(item);
        }


        #endregion

        #region 分类描述的事件
      
        /// <summary>
        /// 添加分类对象
        /// </summary>
        /// <param name="item"></param>
        public void AddClassifer(ClassiferClass item)
        {
            if (classifers==null) return;
            classifers.Add(item);
        }

        /// <summary>
        /// 删除分类对象
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool RemoveClassifer(string m_id)
        {
            try
            {
                if (classifers.Count < 1) return false;
                foreach(ClassiferClass item in classifers)
                {
                    if (item.ID == m_id)
                    {
                      return classifers.Remove(item);
                    }
                }
                return false;
            }
            catch(Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// 根据分类标识，返回分类对象
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ClassiferClass ReturnClassifer(string id)
        {
            try
            {
                if (classifers.Count < 1) return null;
                foreach (ClassiferClass item in classifers)
                {
                    if (item.ID == id)
                    {
                        return item;
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        #endregion
    }

    /// <summary>
    /// 概念分类定义
    /// </summary>
    public class ClassiferClass
    {
        public ClassiferClass(string m_id)
        {
            id = m_id;
        }

        internal string id;
        public string ID
        {
            get { return id; }
            
        }

        internal List<LocalizationClass> localizations = new List<LocalizationClass>();
        public List<LocalizationClass> Localizations
        {
            get { return localizations; }
        }

        public LocalizationClass ReturnLocalization(LocalizationEnum lenum)
        {
            try
            {
                if (localizations.Count < 1) return null;
                foreach(LocalizationClass item in localizations)
                {
                    if(item.Local==lenum)
                    {
                        return item;
                    }
                }
                return null;
            }
            catch(Exception ex)
            {
                return null;
            }
        }

        public bool RemoveLocalization(LocalizationEnum lenum)
        {
            try
            {
                if (localizations.Count < 1) return false;
                foreach (LocalizationClass item in localizations)
                {
                    if (item.Local == lenum)
                    {
                       return  localizations.Remove(item); 
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public void AddLocalization(LocalizationClass item)
        {
            if(item==null)return ;
            localizations.Add(item);
        }

    }

    /// <summary>
    /// 概念词条命名空间的定义
    /// </summary>
    public class ConceptNS
    {
        public ConceptNS()
        {
        }

        //唯一标识符
        internal string id;
        public string ID
        {
            get { return id; }
            set { id = value; }
        }

        //显示名称
        internal string wkname;
        public string WKName
        {
            get { return wkname; }
            set { wkname = value; }
        }

        //描述信息
        internal string description;
        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        internal List<ConceptClass> concepts = new List<ConceptClass>();
        public List<ConceptClass> Concepts
        {
            get { return concepts; }
        }

        #region 概念保存

        /// <summary>
        /// 向命名空间中添加概念词条
        /// </summary>
        /// <param name="item"></param>
        public void AddConcept(ConceptClass item)
        {
            if (item == null) return;
            concepts.Add(item);
        }

        /// <summary>
        /// 根据ID移除空间中的概念词条
        /// </summary>
        /// <param name="m_id"></param>
        /// <returns></returns>
        public bool RemoveConcept(string m_id)
        {
            try
            {
                if (concepts.Count < 1) return false;
                foreach(ConceptClass item in concepts)
                {
                    if (item.ID == m_id) return concepts.Remove(item);
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// 根据ID返回空间中的概念词条
        /// </summary>
        /// <param name="m_id"></param>
        /// <returns></returns>
        public ConceptClass ReturnConcept(string m_id)
        {
            try
            {
                if (concepts.Count < 1) return null;
                foreach (ConceptClass item in concepts)
                {
                    if (item.ID == m_id) return item;
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        #endregion

    }
}
