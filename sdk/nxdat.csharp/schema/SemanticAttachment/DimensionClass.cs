﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NGIS.Data.Schema
{
    public class DimensionClass
    {
        /// <summary>
        /// 创建量纲对象
        /// </summary>
        /// <param name="Dtype">量纲类别</param>
        public DimensionClass(DimensionEnum Dtype)
        {
            type = Dtype;
        }

        //量纲的标识
        private string id;
        public string ID
        {
            get { return id; }
            set { id = value; }
        }

        //量纲的类别
        private DimensionEnum type;
        public DimensionEnum TYPE
        {
            get { return type; }

        }

        //量纲的表达式
        private string expression;
        public string EXPRESSION
        {
            get { return expression; }
            set { expression = value; }
        }

        //量纲的本地化描述
        private List<LocalizationClass> localization = new List<LocalizationClass>();
        public List<LocalizationClass> Localization
        {
            get { return localization; }
        }

        /// <summary>
        /// 添加本地化对象
        /// </summary>
        /// <param name="local"></param>
        public void Add_Localization(LocalizationClass local)
        {
            if (IsLocalExistence(local) == true) return;
            localization.Add(local);
        }

        /// <summary>
        /// 判断在本地化列表中是否存在此描述的对象
        /// </summary>
        /// <param name="local">本地化描述的对象</param>
        /// <returns></returns>
        public bool IsLocalExistence(LocalizationClass local)
        {
            try
            {
                foreach(LocalizationClass item in localization)
                {
                    if(item.Local==local.Local)
                    {
                        return true;
                    }
                }
                return false;
            }
            catch(Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// 判断在本地化列表中是否存在某类描述
        /// </summary>
        /// <param name="local"></param>
        /// <returns></returns>
        public bool IsLocalExistence(LocalizationEnum local)
        {
            try
            {
                foreach (LocalizationClass item in localization)
                {
                    if (item.Local == local)
                    {
                        return true;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// 删除本地化列表中的对象
        /// </summary>
        /// <param name="local">本地化的某个对象</param>
        /// <returns>删除成功与否</returns>
        public bool Remove_Localization(LocalizationClass local)
        {
            if (IsLocalExistence(local) == true) 
            {
                return localization.Remove(local);
            }
            return false;
        }

        /// <summary>
        /// 删除本地化列表中的对象
        /// </summary>
        /// <param name="local">本地化的种类</param>
        /// <returns>删除成功与否</returns>
        public bool Remove_Localization(LocalizationEnum local)
        {
            if (IsLocalExistence(local) == true)
            {
                localization.Remove(Return_Localization(local));
            }
            return false;
        }

        /// <summary>
        /// 根据本地化的类型获取本地化对象
        /// </summary>
        /// <param name="local">本地化类型</param>
        /// <returns>本地化对象</returns>
        public LocalizationClass Return_Localization(LocalizationEnum local)
        {
            try
            {
                foreach (LocalizationClass item in localization)
                {
                    if (item.Local == local)
                    {
                        return item;
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }

    /// <summary>
    /// 量纲类别
    /// </summary>
    public enum DimensionEnum
    {
        basic=1,//基本量纲
        compound=2//复合量纲
    }
}
