﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NGIS.Data
{
    //!节点类型
    public class UdxNode
    {
        internal String mName;
        internal UdxNode mParentNode;
        internal UdxKernel mKernel;

        private List<UdxNode> mChildNodes;

        public UdxNode(UdxNode parentNode, String pName, EKernelType pType)
        {
            mChildNodes = new List<UdxNode>();
            if (parentNode != null)
            {
                mName = pName;
                mParentNode = parentNode;
                createKernel(pType);
            }
        }

        private void createKernel(EKernelType pType)
        {
            switch (pType)
            {
                case EKernelType.EKT_NULL:
                case EKernelType.EKT_COUNT:
                    break;

                case EKernelType.EKT_INT:
                    {
                        mKernel = new UdxKernelIntValue(pType, this);
                    }
                    break;
                case EKernelType.EKT_REAL:
                    {
                        mKernel = new UdxKernelRealValue(pType, this);
                    }
                    break;
                case EKernelType.EKT_STRING:
                    {
                        mKernel = new UdxKernelStringValue(pType, this);
                    }
                    break;
                case EKernelType.EKT_VECTOR2:
                    {
                        mKernel = new UdxKernelVector2dValue(pType, this);
                    }
                    break;
                case EKernelType.EKT_VECTOR3:
                    {
                        mKernel = new UdxKernelVector3dValue(pType, this);
                    }
                    break;
                case EKernelType.EKT_VECTOR4:
                    {
                        mKernel = new UdxKernelVector4dValue(pType, this);
                    }
                    break;

                case EKernelType.EKT_INT | EKernelType.EKT_LIST:
                    {
                        mKernel = new UdxKernelIntArray(pType, this);
                    }
                    break;
                case EKernelType.EKT_REAL | EKernelType.EKT_LIST:
                    {
                        mKernel = new UdxKernelRealArray(pType, this);
                    }
                    break;
                case EKernelType.EKT_STRING | EKernelType.EKT_LIST:
                    {
                        mKernel = new UdxKernelStringArray(pType, this);
                    }
                    break;
                case EKernelType.EKT_VECTOR2 | EKernelType.EKT_LIST:
                    {
                        mKernel = new UdxKernelVector2dArray(pType, this);
                    }
                    break;
                case EKernelType.EKT_VECTOR3 | EKernelType.EKT_LIST:
                    {
                        mKernel = new UdxKernelVector3dArray(pType, this);
                    }
                    break;
                case EKernelType.EKT_VECTOR4 | EKernelType.EKT_LIST:
                    {
                        mKernel = new UdxKernelVector4dArray(pType, this);
                    }
                    break;

                case EKernelType.EKT_NODE:
                case EKernelType.EKT_LIST:
                case EKernelType.EKT_MAP:
                case EKernelType.EKT_TABLE:
                    {
                        mKernel = new UdxKernel(pType, this);
                    }
                    break;
            }
        }

        public String getName()
        {
            return mName;
        }

        public UdxKernel getKernel()
        {
            return mKernel;
        }

        public int getChildNodeCount()
        {
            return mChildNodes.Count();
        }

        public UdxNode getChildNode(int idx)
        {
            if (idx < 0 || idx >= mChildNodes.Count())
                return null;
            return mChildNodes[idx];
        }

        public UdxNode addChildNode(String pName, EKernelType pType)
        {
            UdxNode node = new UdxNode(this, pName, pType);
            mChildNodes.Add(node);
            return node;
        }

        public bool removeChildNode(UdxNode pNode)
        {
            if (mChildNodes.Contains(pNode))
            {
                mChildNodes.Remove(pNode);
                return true;
            }
            return false;
        }

        public bool removeChildNode(int idx)
        {
            if (idx<0 || idx>=mChildNodes.Count)                
                return false;
            mChildNodes.RemoveAt(idx);
            return true;
        }

    }
}
