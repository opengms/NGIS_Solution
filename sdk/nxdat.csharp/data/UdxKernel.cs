﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NGIS.Data
{
    public enum EKernelType
    {
        EKT_NULL = 0,

        EKT_INT = 2,	
        EKT_REAL = 4,
        EKT_STRING = 8,	
        EKT_VECTOR2 = 16,	
        EKT_VECTOR3 = 32,	
        EKT_VECTOR4 = 64,	

        EKT_NODE = 128,	
        EKT_LIST = 256,	
        EKT_MAP = 512,	
        EKT_TABLE = 1024,

        EKT_INT_LIST = 2 | 256,
        EKT_REAL_LIST = 4 | 256,
        EKT_STRING_LIST = 8 | 256,
        EKT_VECTOR2_LIST = 16 | 256,
        EKT_VECTOR3_LIST = 32 | 256,
        EKT_VECTOR4_LIST = 64 | 256,

        EKT_COUNT = 17
    }

    public class UdxKernel
    {
        protected UdxNode mNode;
        protected EKernelType mType;

        public UdxKernel(EKernelType pType, UdxNode pNode)
        {
            mNode = pNode;
            mType = pType;
        }

        public UdxNode getNode()
        {
            return mNode;
        }

        public EKernelType getType()
        {
            return mType;
        }

        public static string KernelType2String(EKernelType pType)
        {
            if (pType == EKernelType.EKT_NULL) return "";
            else if (pType == EKernelType.EKT_INT) return "int";
            else if (pType == EKernelType.EKT_REAL) return "real";
            else if (pType == EKernelType.EKT_STRING) return "string";
            else if (pType == EKernelType.EKT_VECTOR2) return "vector2d";
            else if (pType == EKernelType.EKT_VECTOR3) return "vector3d";
            else if (pType == EKernelType.EKT_VECTOR4) return "vector4d";
            else if (pType == (EKernelType.EKT_INT | EKernelType.EKT_LIST)) return "int_array";
            else if (pType == (EKernelType.EKT_REAL | EKernelType.EKT_LIST)) return "real_array";
            else if (pType == (EKernelType.EKT_STRING | EKernelType.EKT_LIST)) return "string_array";
            else if (pType == (EKernelType.EKT_VECTOR2 | EKernelType.EKT_LIST)) return "vector2d_array";
            else if (pType == (EKernelType.EKT_VECTOR3 | EKernelType.EKT_LIST)) return "vector3d_array";
            else if (pType == (EKernelType.EKT_VECTOR4 | EKernelType.EKT_LIST)) return "vector4d_array";
            else if (pType == EKernelType.EKT_NODE) return "any";
            else if (pType == EKernelType.EKT_LIST) return "list";
            else if (pType == EKernelType.EKT_MAP) return "map";
            else if (pType == EKernelType.EKT_TABLE) return "table";
            else if (pType == EKernelType.EKT_COUNT) return "";
            return "";
        }

        public static EKernelType String2KernelType(string pType)
        {
            if (pType == "int") return EKernelType.EKT_INT;
            else if (pType == "real") return EKernelType.EKT_REAL;
            else if (pType == "string") return EKernelType.EKT_STRING;
            else if (pType == "vector2d") return EKernelType.EKT_VECTOR2;
            else if (pType == "vector3d") return EKernelType.EKT_VECTOR3;
            else if (pType == "vector4d") return EKernelType.EKT_VECTOR4;
            else if (pType == "int_array") return EKernelType.EKT_INT | EKernelType.EKT_LIST;
            else if (pType == "real_array") return EKernelType.EKT_REAL | EKernelType.EKT_LIST;
            else if (pType == "string_array") return EKernelType.EKT_STRING | EKernelType.EKT_LIST;
            else if (pType == "vector2d_array") return EKernelType.EKT_VECTOR2 | EKernelType.EKT_LIST;
            else if (pType == "vector3d_array") return EKernelType.EKT_VECTOR3 | EKernelType.EKT_LIST;
            else if (pType == "vector4d_array") return EKernelType.EKT_VECTOR4 | EKernelType.EKT_LIST;
            else if (pType == "any") return EKernelType.EKT_NODE;
            else if (pType == "list") return EKernelType.EKT_LIST;
            else if (pType == "map") return EKernelType.EKT_MAP;
            else if (pType == "table") return EKernelType.EKT_TABLE;
            return EKernelType.EKT_NULL;
        }
    }
}
