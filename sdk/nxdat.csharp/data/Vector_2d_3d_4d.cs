﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NGIS.Data
{
    public class Vector2d
    {
        public Vector2d() { x = 0.0; y = 0.0; }
        public Vector2d(double _x, double _y) { x = _x; y = _y; }
        public double x;
        public double y;

        public bool Equal(Vector2d v1)
        {
            if (v1 != null && this != null)
            {
                if (Math.Abs(v1.x - this.x) < 0.0000001 && Math.Abs(v1.y - this.y) < 0.0000001)
                {
                    return true;
                }
            }
            return false;
        }
    }

    public class Vector3d
    {
        public Vector3d() { x = 0.0; y = 0.0; z = 0.0; }
        public Vector3d(double _x, double _y, double _z) { x = _x; y = _y; z = _z; }
        public double x;
        public double y;
        public double z;

        public bool Equal(Vector3d v1)
        {
            if (v1 != null && this != null)
            {
                if (Math.Abs(v1.x - this.x) < 0.0000001 && Math.Abs(v1.y - this.y) < 0.0000001 &&
                    Math.Abs(v1.z - this.z) < 0.0000001)
                {
                    return true;
                }
            }
            return false;
        }
    }

    public class Vector4d
    {
        public Vector4d() { x = 0.0; y = 0.0; z = 0.0; m = 0.0; }
        public Vector4d(double _x, double _y, double _z, double _m) { x = _x; y = _y; z = _z; m = _m; }
        public double x;
        public double y;
        public double z;
        public double m;

        public bool Equal(Vector4d v1)
        {
            if (v1 != null && this != null)
            {
                if (Math.Abs(v1.x - this.x) < 0.0000001 && Math.Abs(v1.y - this.y) < 0.0000001 &&
                    Math.Abs(v1.z - this.z) < 0.0000001 && Math.Abs(v1.m - this.m) < 0.0000001)
                {
                    return true;
                }
            }
            return false;
        }
    }

}
