﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace NGIS.Data
{
    /// <summary>
    /// 
    /// </summary>
    public class UdxKernelIntValue : UdxKernel
    {
        private int mValue = 0;
        public UdxKernelIntValue(EKernelType pType, UdxNode pNode)
            : base(pType, pNode)
        {
        }

        public bool setTypedValue(int pValue)
        {
            if (pValue == mValue) return false;
            mValue = pValue;
            return true;
        }

        public int getTypedValue()
        {
            return mValue;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class UdxKernelRealValue : UdxKernel
    {
        private double mValue = 0.0;
        public UdxKernelRealValue(EKernelType pType, UdxNode pNode)
            : base(pType, pNode)
        {
        }


        public bool setTypedValue(double pValue)
        {
            if (pValue == mValue) return false;
            mValue = pValue;
            return true;
        }

        public double getTypedValue()
        {
            return mValue;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class UdxKernelStringValue : UdxKernel
    {
        private string mValue = "";
        public UdxKernelStringValue(EKernelType pType, UdxNode pNode)
            : base(pType, pNode)
        {
        }

        public bool setTypedValue(String pValue)
        {
            if (pValue == mValue) return false;
            mValue = pValue;
            return true;
        }

        public string getTypedValue()
        {
            return mValue;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class UdxKernelVector2dValue : UdxKernel
    {
        private Vector2d mValue = new Vector2d();
        public UdxKernelVector2dValue(EKernelType pType, UdxNode pNode)
            : base(pType, pNode)
        {
        }

        public bool setTypedValue(double pX, double pY)
        {
            Vector2d pValue = new Vector2d(pX, pY);
            if (pValue.Equal(mValue)) return false;
            mValue.x = pX;
            mValue.y = pY;
            return true;
        }

        public Vector2d getTypedValue()
        {
            return mValue;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class UdxKernelVector3dValue : UdxKernel
    {
        private Vector3d mValue = new Vector3d();
        public UdxKernelVector3dValue(EKernelType pType, UdxNode pNode)
            : base(pType, pNode)
        {
        }

        public bool setTypedValue(double pX, double pY, double pZ)
        {
            Vector3d pValue = new Vector3d(pX, pY, pZ);
            if (pValue.Equal(mValue)) return false;
            mValue.x = pX;
            mValue.y = pY;
            mValue.z = pZ;
            return true;
        }

        public Vector3d getTypedValue()
        {
            return mValue;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class UdxKernelVector4dValue : UdxKernel
    {
        private Vector4d mValue = new Vector4d();
        public UdxKernelVector4dValue(EKernelType pType, UdxNode pNode)
            : base(pType, pNode)
        {
        }

        public bool setTypedValue(double pX, double pY, double pZ, double pM)
        {
            Vector4d pValue = new Vector4d(pX, pY, pZ, pM);
            if (pValue.Equal(mValue)) return false;
            mValue.x = pX;
            mValue.y = pY;
            mValue.z = pZ;
            mValue.m = pM;
            return true;
        }

        public Vector4d getTypedValue()
        {
            return mValue;
        }
    }




}
