﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace NGIS.Data
{
    /// <summary>
    /// 
    /// </summary>
    public class UdxKernelIntArray : UdxKernel
    {
        private List<int> mValueList = new List<int>();
        public UdxKernelIntArray(EKernelType pType, UdxNode pNode)
            : base(pType, pNode)
        {
        }

        public bool addTypedValue(int pValue)
        {
            mValueList.Add(pValue);
            return true;
        }

        public int getTypedValueByIndex(int idx)
        {
            if (idx < 0 || idx >= mValueList.Count)
                return 0;
            return mValueList[idx];
        }

        public int getCount()
        {
            return mValueList.Count;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class UdxKernelRealArray : UdxKernel
    {
        private List<double> mValueList = new List<double>();
        public UdxKernelRealArray(EKernelType pType, UdxNode pNode)
            : base(pType, pNode)
        {
        }

        public bool addTypedValue(double pValue)
        {
            mValueList.Add(pValue);
            return true;
        }

        public double getTypedValueByIndex(int idx)
        {
            if (idx < 0 || idx >= mValueList.Count)
                return 0.0;
            return mValueList[idx];
        }

        public int getCount()
        {
            return mValueList.Count;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class UdxKernelStringArray : UdxKernel
    {
        private List<string> mValueList = new List<string>();
        public UdxKernelStringArray(EKernelType pType, UdxNode pNode)
            : base(pType, pNode)
        {
        }

        public bool addTypedValue(String pValue)
        {
            mValueList.Add(pValue);
            return true;
        }

        public string getTypedValueByIndex(int idx)
        {
            if (idx < 0 || idx >= mValueList.Count)
                return "";
            return mValueList[idx];
        }

        public int getCount()
        {
            return mValueList.Count;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class UdxKernelVector2dArray : UdxKernel
    {
        private List<Vector2d> mValueList = new List<Vector2d>();
        public UdxKernelVector2dArray(EKernelType pType, UdxNode pNode)
            : base(pType, pNode)
        {
        }

        public bool addTypedValue(double pX, double pY)
        {
            mValueList.Add(new Vector2d(pX, pY));
            return true;
        }

        public Vector2d getTypedValueByIndex(int idx)
        {
            if (idx < 0 || idx >= mValueList.Count)
                return null;
            return mValueList[idx];
        }

        public int getCount()
        {
            return mValueList.Count;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class UdxKernelVector3dArray : UdxKernel
    {
        private List<Vector3d> mValueList = new List<Vector3d>();
        public UdxKernelVector3dArray(EKernelType pType, UdxNode pNode)
            : base(pType, pNode)
        {
        }

        public bool addTypedValue(double pX, double pY, double pZ)
        {
            mValueList.Add(new Vector3d(pX, pY, pZ));
            return true;
        }

        public Vector3d getTypedValueByIndex(int idx)
        {
            if (idx < 0 || idx >= mValueList.Count)
                return null;
            return mValueList[idx];
        }

        public int getCount()
        {
            return mValueList.Count;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class UdxKernelVector4dArray : UdxKernel
    {
        private List<Vector4d> mValueList = new List<Vector4d>();
        public UdxKernelVector4dArray(EKernelType pType, UdxNode pNode)
            : base(pType, pNode)
        {
        }

        public bool addTypedValue(double pX, double pY, double pZ, double pM)
        {
            mValueList.Add(new Vector4d(pX, pY, pZ, pM));
            return true;
        }

        public Vector4d getTypedValueByIndex(int idx)
        {
            if (idx < 0 || idx >= mValueList.Count)
                return null;
            return mValueList[idx];
        }

        public int getCount()
        {
            return mValueList.Count;
        }
    }

}
