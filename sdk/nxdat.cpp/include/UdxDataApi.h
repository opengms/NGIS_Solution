//!文件属于 NGIS 工程的一部分
//!作者:乐松山
//!创建时间:2016年8月5日

#ifndef __NGIS_UDX_API_H__
#define __NGIS_UDX_API_H__

#include "IUdxDataset.h"

// 在windows上的api开放设置
#ifdef _WINDOWS

#ifndef _NGISDATA_STATIC_LIB_
#ifdef NGISDATA_EXPORTS
#define NGISDATA_API __declspec(dllexport)
#else
#define NGISDATA_API __declspec(dllimport)
#endif // NGISDATA_API
#else
#define NGISDATA_API
#endif // _NGISDATA_STATIC_LIB_

// Declare the calling convention.
#if defined(_STDCALL_SUPPORTED)
#define NGISDATA_CALLCONV __stdcall
#else
#define NGISDATA_CALLCONV __cdecl
#endif // STDCALL_SUPPORTED

#else // _IMP_WINDOWS_API_

// Force symbol export in shared libraries built with gcc.
#if (__GNUC__ >= 4) && !defined(_NGISDATA_STATIC_LIB_) && defined(NGISDATA_EXPORTS)
#define NGISDATA_API __attribute__ ((visibility("default")))
#else
#define NGISDATA_API
#endif
#define NGISDATA_CALLCONV

#endif

//////////////////////////////////

// 以下用于导出对外开放的API接口
namespace NGIS
{
	namespace Data
	{
		extern "C" NGISDATA_API const char* NGISDATA_CALLCONV getVersion();

		extern "C" NGISDATA_API IUdxDataset* NGISDATA_CALLCONV createUdxDataset(const char* pName);
	}

}
#endif

