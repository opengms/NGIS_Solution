#ifndef __C_NGISDATA_UDXDATASET_H__
#define __C_NGISDATA_UDXDATASET_H__

#include "IUdxDataset.h"
#include "UdxNode.h"
#include "tinyxml2.h"
//#include "json.h"

namespace NGIS
{
	namespace Data
	{
		class CUdxDataset : public virtual IUdxDataset, public virtual CUdxNode
		{
		public:
			CUdxDataset(const char* pName) : CUdxNode(NULL, pName, EKernelType::EKT_NODE)
			{
				mParentDxNode = NULL;
			}

			~CUdxDataset() 
			{
			}

			void ParseXDO(IUdxNode* containerNode, tinyxml2::XMLElement* element);

			void FormatXDO(IUdxNode* pNode, tinyxml2::XMLElement* element);

			void ParseXDO(IUdxNode* containerNode);

		public:
			virtual bool LoadFromXmlFile(const char* fileName);

			virtual bool FormatToXmlFile(const char* fileName);

			virtual bool LoadFromXmlStream(const char* xmlStr);

			virtual bool FormatToXmlStream(std::string& xmlStr);

			//////////////////////////////////////////////////////////////////////////
			
			virtual bool LoadFromJsonFile(const char* fileName);

			virtual bool FormatToJsonFile(const char* fileName);

			virtual bool LoadFromJsonStream(const char* jsonStr);

			virtual bool FormatToJsonStream(std::string& jsonStr);

			//////////////////////////////////////////////////////////////////////////

			virtual bool constructDataFromSchema(Schema::IUdxNodeSchema* pSchema);


			static void IterateChildNodes(Schema::IUdxNodeSchema* pSNode, IUdxNode* pDNode)
			{
				if (pSNode == NULL) return;
				int count = pSNode->getChildNodeCount();
				if (count == 0) return;
				for (int i = 0; i < count; i++)
				{
					Schema::IUdxNodeSchema* tempSNode = pSNode->getChildNode(i);
					std::string name = tempSNode->getName();
					Schema::ESchemaNodeType pSType = tempSNode->getDescription()->getKernelType();
					EKernelType pDType = getKernelTypeBySchema(pSType);
					IUdxNode* tempDNode = pDNode->addChildNode(name.c_str(), pDType);
					IterateChildNodes(tempSNode, tempDNode);
				}
			}


		public:
			virtual const char* getName() { return CUdxNode::getName(); }

			virtual void setName(const char* pName) { return CUdxNode::setName(pName); }

			virtual IUdxKernel* getKernel() { return CUdxNode::getKernel(); }

			virtual int getChildNodeCount() { return CUdxNode::getChildNodeCount(); }

			virtual IUdxNode* getChildNode(int idx) { return CUdxNode::getChildNode(idx); }

			virtual IUdxNode* addChildNode(const char* pName, EKernelType pType) 
			{ return CUdxNode::addChildNode(pName, pType); }

			virtual bool removeChildNode(IUdxNode* pNode) { return CUdxNode::removeChildNode(pNode); }

			virtual bool removeChildNode(int idx) { return CUdxNode::removeChildNode(idx); }

			virtual bool validateSchema(Schema::IUdxNodeSchema* pSchema) { return CUdxNode::validateSchema(pSchema); }
		};
	}
}

#endif