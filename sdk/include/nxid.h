#ifndef	__NXID_H__
#define __NXID_H__

#ifdef NXID_EXPORT
	#define __NXID_API__ __declspec(dllexport)
#else
	#define __NXID_API__ __declspec(dllimport)
	//#pragma  comment(lib,"nxid.lib") 
#endif 


#ifdef __cplusplus	//C++
#include <string>
	extern "C" 
	{
#endif
	typedef const char*		nxid_t;
	__NXID_API__  
	nxid_t	nxid(const char* n);
#ifdef __cplusplus //C++	
	}
   // inline nxid_t  nxid(const std::string& n) {return nxid(n.c_str());}
#endif

#define QID(v,id) \
	static nxid_t v=nxid(id);
	
#endif