#ifndef __NX_MODULE_EXEC_STUB_H__
#define __NX_MODULE_EXEC_STUB_H__
#include "nxid.h"
#include "nxdat.h"
#ifdef __cplusplus
	extern "C"{
#endif
	typedef enum e_mex_io_token{ //模型输入输入类型标记
		mx_io_token_initialize=1,//初始化
		mx_io_token_state_shift, //状态切换
		mx_io_token_request,  //数据请求
		mx_io_token_post,	  //数据返回
		mx_io_token_finalize, //模型终止	
		mx_io_token_next	  //继续下一步运行
	}e_mex_io_token_t;
	
	typedef struct mex_io_data{			//模型io数据
		e_mex_io_token_t 	token;		//控制码
		dxftable_t*			dxft;		//外部传入的数据交互函数表
		nxid_t				id;			//操作码
		dx_object_t*		dxobject;	//数据交换对象
	}mex_io_data_t;
	
	//!将所有的模型运行模式简化为同步，且不需要回调的模式
	//!同时和我们对我们的三类划分保持兼容
	//!将模型的运行看成一个迭代器模型
	//!1.简单模型只需要一次迭代，输入参数是 mex_io_data_t.token=mx_io_token_initialize
	//!2.时间推进模式 第一次输入参数mex_io_data_t.token=mx_io_token_initialize
	//!其余都是mex_io_data_t.token=mx_token_next
	//!返回值是	类型是 mex_io_data_t.token=mx_io_token_next||mx_io_token_finalize
	//!3.状态机模式
	//!初始输入 token=mx_io_token_initialize
	//!返回值包括 token=mx_io_token_state_shift || mx_io_token_io_request ||mx_io_token_post||mx_io_token_finalize
	//!当是 mx_io_token_state_shift时，需要输入	mx_io_token_next，
	//!当是 mx_io_token_request 时，需要回应一个	mx_io_token_request输入
	mex_io_data_t  (__stdcall *nx_module_exec_func_t)(mex_io_data input);
#ifdef __cplusplus
}
#endif
#endif