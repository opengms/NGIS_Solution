#ifndef 	__UX_DAT_H___
#define 	__UX_DAT_H___

#include "nxid.h"

#ifdef __cplusplus
extern "C"{  //遵守C命名规则
#endif

//!定义ux data 中支持的cell数据类型
typedef enum{
	dxnull,
	dxint,			//!<integer
	dxreal,		//!<float with 64bit precise
	dxvector2,		//!<2 dimensions vector with x and y
	dxvector3,		//!<3 dimensions vector with x, y and z
	dxvector4,		//!<4 dimensions vector with x, y, z and w
	dxstring,		//!<a string with NULL terminated
	dxwstring,		//!<a wide byte(16 bits) string
	dxnode,		//!<a no-constraints node container
	dxlist,		//!<a container with same structure node
	dxmap,			//!<a k-v map container
	dxtable,		//!<a data table container
	dxcount
}e_dx_type_t;

typedef int 			dx_int_t;
typedef double 			dx_real_t;
typedef const char* 	dx_string_t;
typedef const wchar_t* 	dx_wstring_t;

	
typedef struct{ //!2 dimensions vector with x,y
	dx_real_t x;	//!<x
	dx_real_t y;	//!<y
}dx_vector2_t;

typedef struct{	//!3 dimensions vector with x,y and z
	dx_real_t x;	//!<x
	dx_real_t y;	//!<y
	dx_real_t z;	//!<z
}dx_vector3_t;
	
typedef	struct{	//!<4 dimensions vector with x,y,z and w
	dx_real_t x;	//!<x
	dx_real_t y;	//!<y
	dx_real_t z;	//!<z
	dx_real_t w;	//!<w
}dx_vector4_t;

struct dx_node;

//!data cell for access node container
typedef  struct dx_cell{
	struct 	dx_node*		node;	//!<node to retrive data
	int 					length;	//!<length indicating the count of data element or the count of the sub nodes
	e_dx_type_t				type;	//!<node data type
	union	//!a union to get the specificated data reference
	{
		void*				pdata;	
		dx_int_t* 			iVal;	
		dx_real_t*			rVal;	
		dx_vector2_t*		vec2;	
		dx_vector3_t*		vec3;	
		dx_vector4_t*		vec4;	
		dx_string_t			str;	
		dx_wstring_t		wstr;	
		dx_string_t*		pstr;
		dx_wstring_t*		pwstr;
	};
}dx_cell_t;

struct 	dx_datset;
typedef struct dx_node{
	struct dx_object* dxobject;	// a pointer to owner dataset
}dx_node_t;

typedef struct dx_node_iterator{//a iterator to for visit each every child of the node
	dx_node_t* node;
}dx_node_iterator_t;	

typedef struct dx_node_meta{	//!meta data
	nxid_t 				name;	//!<name
	e_dx_type_t			type;	//!type
	nxid_t*				tags;   //!semantic tags 
}dx_node_meta_t;

struct 	dx_object;
//!dx节点函数表
typedef struct dx_node_function_table
{
	nxid_t 					(*fn_node_get_name)			(dx_node_t* node);//!<<node name
	e_dx_type_t				(*fn_node_get_type)			(dx_node_t* node);//!<<node type of e_dx_node_t
	dx_int_t 				(*fn_node_get_length)		(dx_node_t* node);//!<<element count or sub node count
	void					(*fn_node_modify_length)			(dx_node_t* node, const int len);//!<<modify max count of list node
	
	dx_cell_t				(*fn_node_get_data_cell)	(dx_node_t* node);//!<<retrive the data content if it is data node
	
	dx_node_t*				(*fn_node_get_child)		(dx_node_t* node,int index);//!<<get child node of the node 
	dx_node_t*				(*fn_node_add_child)		(dx_node_t* parent,nxid_t name,e_dx_type_t type,int length);//!<<add child node
	dx_node_t*				(*fn_node_add_child2)		(dx_node_t* parent,const dx_node_meta_t* meta, int length);//!<<add child node with meta info
	
	void					(*fn_node_put_meta)			(dx_node_t* node,const dx_node_meta_t* meta);//!<<set meta info to the node
	const dx_node_meta_t*	(*fn_node_get_meta)			(dx_node_t* node);//!get the meta data attached to the node
	
	dx_node_iterator_t*		(*fn_node_iterator_get)		(dx_node_t* node);		 //!<<node iterator
	dx_node_t*				(*fn_node_iterator_next)		(dx_node_iterator_t* it);//!<<next iterator node
	void					(*fn_node_iterator_rewind)		(dx_node_iterator_t* it);//!<<reset iterator
	void					(*fn_node_iterator_release)		(dx_node_iterator_t* it);//!<<release iterator
	
	void 					(*fn_put_string)				(dx_node_t* node,dx_string_t 	str,int ix); //!<<set string data for the node
	void 					(*fn_put_wstring)				(dx_node_t* node,dx_wstring_t 	wstr,int ix);//!<<set string data for the node with wide format
	void					(*fn_node_data_cell_commit)		(dx_cell_t* cell);					 //!<<save data

	void		 			(*fn_node_put_tag)				(dx_node_t* node,nxid_t tag); //!<<set node tag
	int 		 			(*fn_node_get_tag_count)		(dx_node_t* node);					//!<<get node tags count
	nxid_t	  	 			(*fn_node_get_tag)				(dx_node_t* node,int ix);			//!<<get node tag by index

	//!create a data set with a uri and a intial node
	struct dx_object*  		(*fn_dx_object_create) 	(const char* 	uri,nxid_t name,e_dx_type_t type, int length);
	//!create a data set with meta info
	struct dx_object*  		(*fn_dx_object_create2) 	(const char* 	uri,const dx_node_meta_t* meta,int length);
	//!use the dataset to force memory active
	void					(*fn_dx_object_using)	(struct dx_object* 	dx);
	//!release dataset reference
	void 					(*fn_dx_object_release)	(struct dx_object*	dx);
}dx_function_table_t,dxftable_t;

typedef struct 	dx_object{//!<<data set interface
	dxftable_t* 				dxftable;
	dx_node_t*			 		node;
}dx_object_t;


//!get data set function table function prototype, a dll can implement it
typedef const dx_object_t* (*fn_get_dxftable_t)(const char* argv);

#ifdef __cplusplus
}

class dxutils
{
public:
	inline static nxid_t get_name(dx_node_t* n)
	{
		return n->dxobject->dxftable->fn_node_get_name(n);
	}
	inline static const dx_node_meta_t* get_meta(dx_node_t* n)
	{
		return n->dxobject->dxftable->fn_node_get_meta(n);
	}
	inline static e_dx_type_t get_type(dx_node_t* n)
	{
		return n->dxobject->dxftable->fn_node_get_type(n);
	}
	inline static unsigned	  get_length(dx_node_t* n)
	{
		return n->dxobject->dxftable->fn_node_get_length(n);
	}
	inline static dx_cell_t	get_data_cell(dx_node_t* n)
	{
		return n->dxobject->dxftable->fn_node_get_data_cell(n);
	}
	inline static dx_node_t*  get_child(dx_node_t* n,unsigned i)
	{
		return n->dxobject->dxftable->fn_node_get_child(n,i);
	}
	inline static dx_node_t*  add_child(dx_node_t* p,nxid_t name,e_dx_type_t type,int length)
	{
		return p->dxobject->dxftable->fn_node_add_child(p,name,type,length);
	}
	inline static dx_node_t* add_child(dx_node_t* p,const dx_node_meta_t* meta,int length)
	{
		return p->dxobject->dxftable->fn_node_add_child2(p,meta,length);
	}
	inline static void put_string(dx_node_t* n,const char* str,int ix)
	{
		n->dxobject->dxftable->fn_put_string(n,str,ix);
	}
	inline static void put_wstring(dx_node_t* n,const wchar_t* wstr,int ix)
	{
		n->dxobject->dxftable->fn_put_wstring(n,wstr,ix);
	}
	inline static bool is_data_cell(dx_node_t* n)
	{
		e_dx_type_t ty = get_type(n);
		return ty<dxnode && ty>dxnull;
	}

	inline static int get_tag_count(dx_node_t* n)
	{
		return n->dxobject->dxftable->fn_node_get_tag_count(n);
	}

	inline static nxid_t get_tag(dx_node_t* n,int ix)
	{
		return n->dxobject->dxftable->fn_node_get_tag(n,ix);
	}

	inline static void put_tag(dx_node_t* n, nxid_t tg)
	{
		return n->dxobject->dxftable->fn_node_put_tag(n, tg);
	}

};
#endif

#endif