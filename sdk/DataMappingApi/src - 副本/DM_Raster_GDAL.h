#ifndef __DM_RASTER_GDAL_H__
#define __DM_RASTER_GDAL_H__

#include "..\include\IDataMappingHandle.h"
#include "..\include\NativeDataMappingAPI.h"
#include <stdio.h>
#include <string>
#include <vector>
#include <map>
#include <direct.h>

#define MAXPATH  1024

#include "gdal_priv.h"
#include "cpl_string.h"  
#include "ogr_spatialref.h"  
#include "gdalwarper.h"  
#include "gdal_pam.h"  
#include "cpl_conv.h"  
#include "gdal.h"  
#include "gdal_vrt.h"

namespace NGIS
{
	namespace DataMapping
	{
		class DM_Raster_GADL : public IDataMappingHandle
		{
		protected:
			std::string mReadPath;
			std::string mWritePath;
			GDALDataset *mDataset;

			IUdxDatasetSchema* mUdxSchema;
			IUdxDataset* mUdxData;

			std::string mCurrentErrorInfo;
			std::vector<IUdxNodeSchema*> mNeedIndexSchemaList;

			std::vector<IUdxNodeSchema*> m_Capabilities;
		public:
			DM_Raster_GADL()
			{
				mUdxSchema = NULL;
				mUdxData = NULL;
				GDALAllRegister();

				IUdxDatasetSchema* sDataset = GetUdxSchema();
				m_Capabilities.push_back(sDataset->getChildNode(0)->getChildNode(0));
				m_Capabilities.push_back(sDataset->getChildNode(0)->getChildNode(1));
				m_Capabilities.push_back(sDataset->getChildNode(0)->getChildNode(2));
				m_Capabilities.push_back(sDataset->getChildNode(0)->getChildNode(3));
				m_Capabilities.push_back(sDataset->getChildNode(0)->getChildNode(4));
				m_Capabilities.push_back(sDataset->getChildNode(0)->getChildNode(5));
				m_Capabilities.push_back(sDataset->getChildNode(0)->getChildNode(6));
				m_Capabilities.push_back(sDataset->getChildNode(0)->getChildNode(7));
				m_Capabilities.push_back(sDataset->getChildNode(0)->getChildNode(8));

				m_Capabilities.push_back(sDataset->getChildNode(1)->getChildNode(0)->getChildNode(0));
				m_Capabilities.push_back(sDataset->getChildNode(1)->getChildNode(0)->getChildNode(1));
				m_Capabilities.push_back(sDataset->getChildNode(1)->getChildNode(0)->getChildNode(2));
				m_Capabilities.push_back(sDataset->getChildNode(1)->getChildNode(0)->getChildNode(3));
				m_Capabilities.push_back(sDataset->getChildNode(1)->getChildNode(0)->getChildNode(4));

				m_Capabilities.push_back(sDataset->getChildNode(1)->getChildNode(0)->getChildNode(5)->getChildNode(0));

				m_Capabilities.push_back(sDataset->getChildNode(2));
			}

			~DM_Raster_GADL()
			{
				if( mDataset )
				{
					GDALClose(mDataset);
					mDataset	= NULL;
				}
				if (mUdxSchema) mUdxSchema->release();
				if (mUdxData) mUdxData->release();
			}

		private:
			void findAllSchemaNode_WithListType(IUdxNodeSchema* pNode)
			{
				if (pNode->getDescription()->getKernelType() == EDTKT_LIST)
				{
					mNeedIndexSchemaList.push_back(pNode);
				}
				if (pNode->getParentNode() != NULL)
				{
					findAllSchemaNode_WithListType(pNode->getParentNode());
				}
			}

		public:
			//! 初始化数据映射Handle
			virtual bool InitHandle(const char* readFilePath, const char* writeFilePath)
			{
				mReadPath = readFilePath;
				mWritePath = writeFilePath;

				char buffer[MAXPATH];
				_getcwd(buffer,MAXPATH);
				std::string working_dir(buffer);
				working_dir = working_dir + "\\gdal-data";

				CPLSetConfigOption("GDAL_FILENAME_IS_UTF8","YES");
				CPLSetConfigOption("GDAL_DATA", working_dir.c_str() );
				
				if (mReadPath!="")
					mDataset = (GDALDataset*)GDALOpen(mReadPath.c_str(), GA_ReadOnly);

				if (false)
				{
					FILE* fp = fopen("D:\\gdal_raster_driver.txt", "w+");
					int driver_count = GDALGetDriverCount();
					for (int iDriver=0; iDriver<driver_count; iDriver++)
					{
						GDALDriverH temp_driver = GDALGetDriver(iDriver);
						const char* name	= GDALGetMetadataItem(temp_driver, GDAL_DMD_LONGNAME, "");
						const char* description	= GDALGetDescription(temp_driver);
						const char* extension = GDALGetMetadataItem(temp_driver, GDAL_DMD_EXTENSION, "");
						const char* write_capability = GDALGetMetadataItem(temp_driver, GDAL_DCAP_CREATE, "");
						const char* read_capability = GDALGetMetadataItem(temp_driver, GDAL_DCAP_OPEN, "");

						fprintf(fp, "driver name: %s\t", name);
						fprintf(fp, "description: %s\t", description);
						fprintf(fp, "extension: %s\t", extension);
						fprintf(fp, "write_capability: %s\t", write_capability);
						fprintf(fp, "read_capability: %s\n", read_capability);
					}
					fclose(fp);
				}

				if (mDataset==NULL) return false;

				return true;
			}

			//! 获取Handle的名称
			virtual const char* GetHandleName()
			{
				return "General GDAL Raster Handle";
			}

			//! 获取Handle的版本
			virtual const char* GetHandleVersion()
			{
				return GDALVersionInfo("RELEASE_NAME");
			}

			//! 获取当前数据映射Handle的关联Schema
			virtual IUdxDatasetSchema* GetUdxSchema()
			{
				if (mUdxSchema==NULL)
				{
					mUdxSchema = createUdxDatasetSchema("UdxDescription");
					IUdxNodeSchema* headerNode = mUdxSchema->addChildNode("Header", ESchemaNodeType::EDTKT_NODE);
					IUdxNodeSchema* bandsNode = mUdxSchema->addChildNode("Bands", ESchemaNodeType::EDTKT_LIST);

					headerNode->addChildNode("UpperLeftX", ESchemaNodeType::EDTKT_REAL);
					headerNode->addChildNode("PixelWidth", ESchemaNodeType::EDTKT_REAL);
					headerNode->addChildNode("TransParam1", ESchemaNodeType::EDTKT_REAL);
					headerNode->addChildNode("UpperLeftY", ESchemaNodeType::EDTKT_REAL);
					headerNode->addChildNode("TransParam2", ESchemaNodeType::EDTKT_REAL);
					headerNode->addChildNode("PixelHeight", ESchemaNodeType::EDTKT_REAL);
					headerNode->addChildNode("Width", ESchemaNodeType::EDTKT_INT);
					headerNode->addChildNode("Height", ESchemaNodeType::EDTKT_INT);
					headerNode->addChildNode("BandCount", ESchemaNodeType::EDTKT_INT);

					IUdxNodeSchema* band_node = bandsNode->addChildNode("Band_Item", ESchemaNodeType::EDTKT_NODE);
					band_node->addChildNode("NoDataValue", ESchemaNodeType::EDTKT_REAL);
					band_node->addChildNode("Scale", ESchemaNodeType::EDTKT_REAL);
					band_node->addChildNode("Offset", ESchemaNodeType::EDTKT_REAL);
					band_node->addChildNode("UnitType", ESchemaNodeType::EDTKT_STRING);
					band_node->addChildNode("DataType", ESchemaNodeType::EDTKT_INT);

					IUdxNodeSchema* value_node = band_node->addChildNode("Value", ESchemaNodeType::EDTKT_LIST);
					value_node->addChildNode("Row_Item", ESchemaNodeType::EDTKT_REAL | ESchemaNodeType::EDTKT_LIST);

					IUdxNodeSchema* projection_node = mUdxSchema->addChildNode("Projection", ESchemaNodeType::EDTKT_STRING);
				}
				return mUdxSchema;
			}

			//! 获取此Handle能够操作的SchemaNode个数
			virtual int GetCapbilityCount()
			{
				return m_Capabilities.size();
			}

			//! 迭代获取此Handle能够操作的SchemaNode
			virtual IUdxNodeSchema* getCapbilityNode(int idx) 
			{
				int count = GetCapbilityCount();
				if (idx<0 || idx>=count)
					return NULL;
				return m_Capabilities[idx];
			}

			//! 从原始数据中按照传递过来的SchemaNode读取数据内容，
			//! 并且往传递过来的DataNode中写入相应的数据内容
			virtual bool ReadToNode(IUdxNodeSchema* pSNode, IUdxNode* pDNode)
			{
				if(mDataset == NULL || pSNode==NULL || pDNode==NULL)  
				{
					return false;
				}
				EKernelType pDType = pDNode->getKernel()->getType();
				int len = 0;
				EKernelType pDType1 = getKernelTypeBySchema(pSNode->getDescription()->getKernelType());
				if (pDType1 != pDType)
				{
					return false;
				}

				bool ret=false;
				std::string name = pSNode->getName();
				printf("%s\n",name.c_str());
				if (name == "UpperLeftX")
				{
					double geoInfo[6];
					mDataset->GetGeoTransform(geoInfo);
					NGIS::Data::IUdxKernelRealValue* realKernel = (NGIS::Data::IUdxKernelRealValue*)pDNode->getKernel();
					realKernel->setTypedValue(geoInfo[0]);
					ret = true;
				}
				else if (name == "PixelWidth")
				{
					double geoInfo[6];
					mDataset->GetGeoTransform(geoInfo);
					NGIS::Data::IUdxKernelRealValue* realKernel = (NGIS::Data::IUdxKernelRealValue*)pDNode->getKernel();
					realKernel->setTypedValue(geoInfo[1]);
					ret = true;
				}
				else if (name == "TransParam1")
				{
					double geoInfo[6];
					mDataset->GetGeoTransform(geoInfo);
					NGIS::Data::IUdxKernelRealValue* realKernel = (NGIS::Data::IUdxKernelRealValue*)pDNode->getKernel();
					realKernel->setTypedValue(geoInfo[2]);
					ret = true;
				}
				else if (name == "UpperLeftY")
				{
					double geoInfo[6];
					mDataset->GetGeoTransform(geoInfo);
					NGIS::Data::IUdxKernelRealValue* realKernel = (NGIS::Data::IUdxKernelRealValue*)pDNode->getKernel();
					realKernel->setTypedValue(geoInfo[3]);
					ret = true;
				}
				else if (name == "TransParam2")
				{
					double geoInfo[6];
					mDataset->GetGeoTransform(geoInfo);
					NGIS::Data::IUdxKernelRealValue* realKernel = (NGIS::Data::IUdxKernelRealValue*)pDNode->getKernel();
					realKernel->setTypedValue(geoInfo[4]);
					ret = true;
				}
				else if (name == "PixelHeight")
				{
					double geoInfo[6];
					mDataset->GetGeoTransform(geoInfo);
					NGIS::Data::IUdxKernelRealValue* realKernel = (NGIS::Data::IUdxKernelRealValue*)pDNode->getKernel();
					realKernel->setTypedValue(geoInfo[5]);
					ret = true;
				}
				else if (name == "Width")
				{
					int xSize = mDataset->GetRasterXSize();
					NGIS::Data::IUdxKernelIntValue* realKernel = (NGIS::Data::IUdxKernelIntValue*)pDNode->getKernel();
					realKernel->setTypedValue(xSize);
					ret = true;
				}
				else if (name == "Height")
				{
					int ySize = mDataset->GetRasterYSize();
					NGIS::Data::IUdxKernelIntValue* realKernel = (NGIS::Data::IUdxKernelIntValue*)pDNode->getKernel();
					realKernel->setTypedValue(ySize);
					ret = true;
				}
				else if (name == "BandCount")
				{
					int bandCount = mDataset->GetRasterCount();
					NGIS::Data::IUdxKernelIntValue* realKernel = (NGIS::Data::IUdxKernelIntValue*)pDNode->getKernel();
					realKernel->setTypedValue(bandCount);
					ret = true;
				}
				else if (name == "NoDataValue")
				{
					int band_idx = pSNode->getParentNode()->getParentNode()->getExtension();
					double nodataVal = mDataset->GetRasterBand(band_idx+1)->GetNoDataValue();
					NGIS::Data::IUdxKernelRealValue* realKernel = (NGIS::Data::IUdxKernelRealValue*)pDNode->getKernel();
					realKernel->setTypedValue(nodataVal);
					ret = true;
				}
				else if (name == "Scale")
				{
					int band_idx = pSNode->getParentNode()->getParentNode()->getExtension();
					double scaleVal = mDataset->GetRasterBand(band_idx+1)->GetScale();
					NGIS::Data::IUdxKernelRealValue* realKernel = (NGIS::Data::IUdxKernelRealValue*)pDNode->getKernel();
					realKernel->setTypedValue(scaleVal);
					ret = true;
				}
				else if (name == "Offset")
				{
					int band_idx = pSNode->getParentNode()->getParentNode()->getExtension();
					double offsetVal = mDataset->GetRasterBand(band_idx+1)->GetOffset();
					NGIS::Data::IUdxKernelRealValue* realKernel = (NGIS::Data::IUdxKernelRealValue*)pDNode->getKernel();
					realKernel->setTypedValue(offsetVal);
					ret = true;
				}
				else if (name == "UnitType")
				{
					int band_idx = pSNode->getParentNode()->getParentNode()->getExtension();
					const char* unitVal = mDataset->GetRasterBand(band_idx+1)->GetUnitType();
					NGIS::Data::IUdxKernelStringValue* realKernel = (NGIS::Data::IUdxKernelStringValue*)pDNode->getKernel();
					realKernel->setTypedValue(unitVal);
					ret = true;
				}
				else if (name == "DataType")
				{
					int band_idx = pSNode->getParentNode()->getParentNode()->getExtension();
					GDALDataType eDT = mDataset->GetRasterBand(band_idx+1)->GetRasterDataType();
					NGIS::Data::IUdxKernelIntValue* realKernel = (NGIS::Data::IUdxKernelIntValue*)pDNode->getKernel();
					realKernel->setTypedValue((int)eDT);
					ret = true;
				}
				else if (name == "Row_Item")
				{
					int band_idx = pSNode->getParentNode()->getParentNode()->getParentNode()->getExtension();
					int row_idx = pSNode->getParentNode()->getExtension();

				 	GDALRasterBand* band = mDataset->GetRasterBand(band_idx+1);
					GDALDataType eDT = band->GetRasterDataType();
					int xSize = band->GetXSize();
					int ySize = band->GetYSize();
					double* pData = new double[xSize];

					/*if (eDT == GDT_Byte) { pData = new unsigned char(xSize); }
					else if (eDT == GDT_UInt16) { pData = new unsigned short(xSize); }
					else if (eDT == GDT_Int16) { pData = new short(xSize); }
					else if (eDT == GDT_UInt32) { pData = new unsigned long(xSize); }
					else if (eDT == GDT_Int32) { pData = new long(xSize); }
					else if (eDT == GDT_Float32 ) { pData = new float(xSize); }
					else if (eDT == GDT_Float64  ) { pData = new double(xSize); }*/

					CPLErr err = band->RasterIO(GF_Read, 0, row_idx, xSize, 1, pData, xSize, 1, GDT_Float64, 0, 0);

					NGIS::Data::IUdxKernelRealArray* realKernel = (NGIS::Data::IUdxKernelRealArray*)pDNode->getKernel();
					for (int i=0; i<xSize; i++)
					{
						realKernel->addTypedValue(pData[i]);
					}
					delete []pData;
					ret = true;
				}
				else if (name == "Projection")
				{
					char* srs = (char*) mDataset->GetProjectionRef();
					std::string wkt_str(srs);
					std::string datum_name = "";
					if (wkt_str!="")
					{
						int idx1 = wkt_str.find("GEOGCS");
						int idx2 = wkt_str.find("DATUM");
						std::string sub_str = wkt_str.substr(idx1, idx2-idx1);
						idx1 = sub_str.find_first_of('"');
						idx2 = sub_str.find_last_of('"');
						datum_name  = sub_str.substr(idx1+1, idx2-idx1-1);
					}

					OGRSpatialReference ogr_srs;
					OGRErr err = ogr_srs.importFromWkt(&srs);
					
					ogr_srs.exportToProj4(&srs);
					std::string proj_str(srs);
					int idx = proj_str.find("+datum");
					if (idx<0)
					{
						proj_str += " +datum=" + datum_name;
					}
					
					NGIS::Data::IUdxKernelStringValue* realKernel = (NGIS::Data::IUdxKernelStringValue*)pDNode->getKernel();
					realKernel->setTypedValue(proj_str.c_str());
					ret = true;
				}
				return ret;
			}

			//! 根据当前选中的SchemaNode，获取需要设置Index信息的SchemaNode的数量
			virtual int prepareNeedIndexSettingNode(IUdxNodeSchema* pSNode)
			{
				mNeedIndexSchemaList.clear();
				findAllSchemaNode_WithListType(pSNode);
				return mNeedIndexSchemaList.size();
			}

			//! 迭代获取需要设置Index信息的SchemaNode
			virtual IUdxNodeSchema* getNeedIndexSettingNode(int idx)
			{
				if (idx>=mNeedIndexSchemaList.size() || idx<0)
					return NULL;
				return mNeedIndexSchemaList[idx];
			}

			//! 准备保存到文件中
			virtual bool BeginWriteToFile()
			{
				if (mWritePath=="")
				{
					mCurrentErrorInfo = "write file path does not exist";
					return false;
				}

				return true;
			}

			//! 从UDX数据节点中读取内容，并写入到原始数据格式中
			virtual bool WriteFromNode(IUdxNode* pDNode, int formatFlag=-1)
			{
				if(pDNode==NULL)  return false;
				if(pDNode->validateSchema(GetUdxSchema()) == false) return false;

				IUdxKernel* pKernel;
				IUdxKernelRealValue* realKernel;
				IUdxKernelIntValue* intKernel;
				IUdxKernelStringValue* stringKernel;

				//////////////////////////////////////////////////////////////////////////
				pKernel = pDNode->getChildNode(0)->getChildNode(0)->getKernel();
				realKernel = (IUdxKernelRealValue*)pKernel;
				double upperLeftX = realKernel->getTypedValue();

				pKernel = pDNode->getChildNode(0)->getChildNode(1)->getKernel();
				realKernel = (IUdxKernelRealValue*)pKernel;
				double pixelWidth = realKernel->getTypedValue();

				pKernel = pDNode->getChildNode(0)->getChildNode(2)->getKernel();
				realKernel = (IUdxKernelRealValue*)pKernel;
				double transParam1 = realKernel->getTypedValue();

				pKernel = pDNode->getChildNode(0)->getChildNode(3)->getKernel();
				realKernel = (IUdxKernelRealValue*)pKernel;
				double upperLeftY = realKernel->getTypedValue();

				pKernel = pDNode->getChildNode(0)->getChildNode(4)->getKernel();
				realKernel = (IUdxKernelRealValue*)pKernel;
				double transParam2 = realKernel->getTypedValue();

				pKernel = pDNode->getChildNode(0)->getChildNode(5)->getKernel();
				realKernel = (IUdxKernelRealValue*)pKernel;
				double pixelHeight = realKernel->getTypedValue();

				pKernel = pDNode->getChildNode(0)->getChildNode(6)->getKernel();
				intKernel = (IUdxKernelIntValue*)pKernel;
				int width = intKernel->getTypedValue();

				pKernel = pDNode->getChildNode(0)->getChildNode(7)->getKernel();
				intKernel = (IUdxKernelIntValue*)pKernel;
				int height = intKernel->getTypedValue();

				pKernel = pDNode->getChildNode(0)->getChildNode(8)->getKernel();
				intKernel = (IUdxKernelIntValue*)pKernel;
				int count = intKernel->getTypedValue();

				//////////////////////////////////////////////////////////////////////////
				IUdxNode* bandsNode =  pDNode->getChildNode(1);
				int bandCount = bandsNode->getChildNodeCount();

				IUdxNode* projNode = pDNode->getChildNode(2);
				stringKernel = (IUdxKernelStringValue*)projNode->getKernel();
				std::string srs = stringKernel->getTypedValue();

				intKernel = (IUdxKernelIntValue*)bandsNode->getChildNode(0)->getChildNode(4)->getKernel();
				int dataType = intKernel->getTypedValue();
				GDALDataType eType = (GDALDataType)dataType;

				GDALDriver* poDriver = NULL;
				switch (formatFlag)
				{
				case 0: poDriver = GetGDALDriverManager()->GetDriverByName("FITS"); break;
				case 1: poDriver = GetGDALDriverManager()->GetDriverByName("HDF4Image"); break;
				case 2: poDriver = GetGDALDriverManager()->GetDriverByName("KEA"); break;
				case 3: poDriver = GetGDALDriverManager()->GetDriverByName("netCDF"); break;
				case 4: poDriver = GetGDALDriverManager()->GetDriverByName("VRT"); break;
				case 5: poDriver = GetGDALDriverManager()->GetDriverByName("GTiff"); break;
				case 6: poDriver = GetGDALDriverManager()->GetDriverByName("NITF"); break;
				case 7: poDriver = GetGDALDriverManager()->GetDriverByName("HFA"); break;
				case 8: poDriver = GetGDALDriverManager()->GetDriverByName("ELAS"); break;
				case 9: poDriver = GetGDALDriverManager()->GetDriverByName("MEM"); break;
				case 10: poDriver = GetGDALDriverManager()->GetDriverByName("BMP"); break;
				case 11: poDriver = GetGDALDriverManager()->GetDriverByName("PCIDSK"); break;
				case 12: poDriver = GetGDALDriverManager()->GetDriverByName("PCRaster"); break;
				case 13: poDriver = GetGDALDriverManager()->GetDriverByName("ILWIS"); break;
				case 14: poDriver = GetGDALDriverManager()->GetDriverByName("SGI"); break;
				case 15: poDriver = GetGDALDriverManager()->GetDriverByName("Leveller"); break;
				case 16: poDriver = GetGDALDriverManager()->GetDriverByName("Terragen"); break;
				case 17: poDriver = GetGDALDriverManager()->GetDriverByName("ISIS2"); break;
				case 18: poDriver = GetGDALDriverManager()->GetDriverByName("ERS"); break;
				case 19: poDriver = GetGDALDriverManager()->GetDriverByName("RMF"); break;
				case 20: poDriver = GetGDALDriverManager()->GetDriverByName("RST"); break;
				case 21: poDriver = GetGDALDriverManager()->GetDriverByName("INGR"); break;
				case 22: poDriver = GetGDALDriverManager()->GetDriverByName("GSBG"); break;
				case 23: poDriver = GetGDALDriverManager()->GetDriverByName("GS7BG"); break;
				case 24: poDriver = GetGDALDriverManager()->GetDriverByName("PDF"); break;
				case 25: poDriver = GetGDALDriverManager()->GetDriverByName("MRF"); break;
				case 26: poDriver = GetGDALDriverManager()->GetDriverByName("PNM"); break;
				case 27: poDriver = GetGDALDriverManager()->GetDriverByName("PAux"); break;
				case 28: poDriver = GetGDALDriverManager()->GetDriverByName("MFF"); break;
				case 29: poDriver = GetGDALDriverManager()->GetDriverByName("MFF2"); break;
				case 30: poDriver = GetGDALDriverManager()->GetDriverByName("BT"); break;
				case 31: poDriver = GetGDALDriverManager()->GetDriverByName("LAN"); break;
				case 32: poDriver = GetGDALDriverManager()->GetDriverByName("IDA"); break;
				case 33: poDriver = GetGDALDriverManager()->GetDriverByName("GTX"); break;
				case 34: poDriver = GetGDALDriverManager()->GetDriverByName("NTv2"); break;
				case 35: poDriver = GetGDALDriverManager()->GetDriverByName("CTable2"); break;
				case 36: poDriver = GetGDALDriverManager()->GetDriverByName("KRO"); break;
				case 37: poDriver = GetGDALDriverManager()->GetDriverByName("ROI_PAC"); break;
				case 38: poDriver = GetGDALDriverManager()->GetDriverByName("ENVI"); break;
				case 39: poDriver = GetGDALDriverManager()->GetDriverByName("EHdr"); break;
				case 40: poDriver = GetGDALDriverManager()->GetDriverByName("ISCE"); break;
				case 41: poDriver = GetGDALDriverManager()->GetDriverByName("ADRG"); break;
				case 42: poDriver = GetGDALDriverManager()->GetDriverByName("SAGA"); break;
				case 43: poDriver = GetGDALDriverManager()->GetDriverByName("DB2ODBC"); break;
				case 44: poDriver = GetGDALDriverManager()->GetDriverByName("GPKG"); break;
				default: poDriver = GetGDALDriverManager()->GetDriverByName("GTiff"); break;
				}

				if (poDriver==NULL)
				{
					mCurrentErrorInfo = "format does not supported";
					return false;
				}

				char **papszOptions = NULL;
				GDALDataset* gdalDataset = poDriver->Create(mWritePath.c_str(), width, height, bandCount, eType, papszOptions);
				if (srs.find("+proj")>=0)
				{
					OGRSpatialReference ogr_srs;
					ogr_srs.importFromProj4(srs.c_str());
					char * proj_info = NULL;
					ogr_srs.exportToWkt(&proj_info);
					gdalDataset->SetProjection(proj_info);
				}
				else
					gdalDataset->SetProjection(srs.c_str());

				double geoTransform[6] = { upperLeftX, pixelWidth, transParam1, upperLeftY, transParam2, pixelHeight };
				gdalDataset->SetGeoTransform(geoTransform);

				//////////////////////////////////////////////////////////////////////////
				double* val = new double[width*height];
				for (int iBand = 0; iBand<bandCount; iBand++)
				{
					IUdxNode* tempBandNode = bandsNode->getChildNode(iBand);
					realKernel = (IUdxKernelRealValue*)tempBandNode->getChildNode(0)->getKernel();
					double noDataValue = realKernel->getTypedValue();

					realKernel = (IUdxKernelRealValue*)tempBandNode->getChildNode(1)->getKernel();
					double scale = realKernel->getTypedValue();

					realKernel = (IUdxKernelRealValue*)tempBandNode->getChildNode(2)->getKernel();
					double offset = realKernel->getTypedValue();

					stringKernel = (IUdxKernelStringValue*)tempBandNode->getChildNode(3)->getKernel();
					std::string unitType = stringKernel->getTypedValue();

					intKernel = (IUdxKernelIntValue*)tempBandNode->getChildNode(4)->getKernel();
					int dataType = intKernel->getTypedValue();

					GDALRasterBand*  tempGdalBand = gdalDataset->GetRasterBand(iBand+1);
					tempGdalBand->SetNoDataValue(noDataValue);
					tempGdalBand->SetScale(scale);
					tempGdalBand->SetOffset(offset);
					tempGdalBand->SetUnitType(unitType.c_str());
					
					memset(val,0,width*height);

					IUdxNode* valNode = tempBandNode->getChildNode(5);
					for (int iRow=0; iRow<valNode->getChildNodeCount(); iRow++)
					{
						IUdxNode* tempRowNode = valNode->getChildNode(iRow);
						IUdxKernelRealArray* realArrayKernel = (IUdxKernelRealArray*)tempRowNode->getKernel();
						int length = realArrayKernel->getCount();
						for (int iVal=0; iVal<length; iVal++)
						{
							realArrayKernel->getTypedValueByIndex(iVal, val[iRow*width+iVal]);
						}
					}

					tempGdalBand->RasterIO(GF_Write,0,0,width,height,val,width,height,GDT_Float64,0,0);
				}
				delete []val;
				delete gdalDataset;

				return true;
			}

			//! 保存到文件中
			virtual bool EndWriteToFile()
			{
				if (mWritePath=="")
				{
					mCurrentErrorInfo = "write file path does not exist";
					return false;
				}

				return true;
			}

			//! 获取当前读取或者写入的错误信息
			virtual const char* GetCurrentErrorInfo()
			{
				return mCurrentErrorInfo.c_str();
			}

		};
	}
}

#endif 