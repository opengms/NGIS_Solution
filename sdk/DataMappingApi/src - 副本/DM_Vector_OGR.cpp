#include "DM_Vector_OGR.h"
#include "..\include\NativeDataMappingAPI.h"
#include "UdxDataSchemaApi.h"
#include "UdxDataApi.h"

namespace NGIS
{
	namespace DataMapping
	{

		extern "C" NATIVEDATAMAPPING_API IDataMappingHandle* NATIVEDATAMAPPING_CALLCONV 
			createGeneralVectorHandle()
		{
			DM_Vector_OGR* pVectorOGRHandle = new DM_Vector_OGR();
			return pVectorOGRHandle;
		}

	}
}