#ifndef __DM_VECTROR_OGR_H__
#define __DM_VECTROR_OGR_H__

#include "..\include\IDataMappingHandle.h"
#include "..\include\NativeDataMappingAPI.h"
#include <stdio.h>
#include <string>
#include <vector>
#include <map>

#include "gdal_priv.h"
#include "cpl_string.h"  
#include "ogr_spatialref.h"  
#include "gdalwarper.h"  
#include "gdal_pam.h"  
#include "cpl_conv.h"  
#include "gdal.h"
#include "ogrsf_frmts.h"

namespace NGIS
{
	namespace DataMapping
	{
		class DM_Vector_OGR : public IDataMappingHandle
		{
		protected:
			std::string mReadPath;
			std::string mWritePath;
			GDALDataset *mDataset;

			IUdxDatasetSchema* mUdxSchema;
			IUdxDataset* mUdxData;

			std::string mCurrentErrorInfo;
			std::vector<IUdxNodeSchema*> mNeedIndexSchemaList;

			std::vector<IUdxNodeSchema*> m_Capabilities;

		public:
			DM_Vector_OGR()
			{
				mUdxSchema = NULL;
				mUdxData = NULL;
				GDALAllRegister();

				IUdxDatasetSchema* sDataset = GetUdxSchema();
				m_Capabilities.push_back(sDataset->getChildNode(0));
				m_Capabilities.push_back(sDataset->getChildNode(1)->getChildNode(0)->getChildNode(0));
				m_Capabilities.push_back(sDataset->getChildNode(1)->getChildNode(0)->getChildNode(1));
				m_Capabilities.push_back(sDataset->getChildNode(1)->getChildNode(0)->getChildNode(2));
				m_Capabilities.push_back(sDataset->getChildNode(1)->getChildNode(0)->getChildNode(3));
				m_Capabilities.push_back(sDataset->getChildNode(1)->getChildNode(0)->getChildNode(4));
				m_Capabilities.push_back(sDataset->getChildNode(1)->getChildNode(0)->getChildNode(5));
			}

			~DM_Vector_OGR()
			{
				if( mDataset )
				{
					GDALClose(mDataset);
					mDataset	= NULL;
				}
				if (mUdxSchema) mUdxSchema->release();
				if (mUdxData) mUdxData->release();
			}
		private:
			void findAllSchemaNode_WithListType(IUdxNodeSchema* pNode)
			{
				if (pNode->getDescription()->getKernelType() == EDTKT_LIST)
				{
					mNeedIndexSchemaList.push_back(pNode);
				}
				if (pNode->getParentNode() != NULL)
				{
					findAllSchemaNode_WithListType(pNode->getParentNode());
				}
			}

			const char* Get_Vertex_Type(OGRwkbGeometryType Type)
			{
				switch( Type )
				{
				case wkbPoint25D             :	// 2.5D extension as per 99-402
				case wkbMultiPoint25D        :	// 2.5D extension as per 99-402
				case wkbLineString25D        :	// 2.5D extension as per 99-402
				case wkbMultiLineString25D   :	// 2.5D extension as per 99-402
				case wkbPolygon25D           :	// 2.5D extension as per 99-402
				case wkbMultiPolygon25D      :	// 2.5D extension as per 99-402
				case wkbGeometryCollection25D:	// 2.5D extension as per 99-402
					return "XYZ";

				default:
					return "XY";
				}
			}

			const char* ShapeType2Str(OGRwkbGeometryType Type)
			{
				switch( Type )
				{
				case wkbPoint                :	// 0-dimensional geometric object, standard WKB
					return "wkbPoint";
				case wkbPoint25D             :	// 2.5D extension as per 99-402
					return "wkbPoint25D";

				case wkbMultiPoint           :	// GeometryCollection of Points, standard WKB
					return "wkbMultiPoint";
				case wkbMultiPoint25D        :	// 2.5D extension as per 99-402
					return "wkbMultiPoint25D";

				case wkbLineString           :	// 1-dimensional geometric object with linear interpolation between Points, standard WKB
					return "wkbLineString";
				case wkbMultiLineString      :	// GeometryCollection of LineStrings, standard WKB
					return "wkbMultiLineString";
				case wkbLineString25D        :	// 2.5D extension as per 99-402
					return "wkbLineString25D";
				case wkbMultiLineString25D   :	// 2.5D extension as per 99-402
					return "wkbMultiLineString25D";

				case wkbPolygon              :	// planar 2-dimensional geometric object defined by 1 exterior boundary and 0 or more interior boundaries, standard WKB
					return "wkbPolygon";
				case wkbMultiPolygon         :	// GeometryCollection of Polygons, standard WKB
					return "wkbMultiPolygon";
				case wkbPolygon25D           :	// 2.5D extension as per 99-402
					return "wkbPolygon25D";
				case wkbMultiPolygon25D      :	// 2.5D extension as per 99-402
					return "wkbMultiPolygon25D";

				case wkbLinearRing           :	// non-standard, just for createGeometry()
					return "wkbLinearRing";
				case wkbGeometryCollection   :	// geometric object that is a collection of 1 or more geometric objects, standard WKB
					return "wkbGeometryCollection";
				case wkbGeometryCollection25D:	// 2.5D extension as per 99-402
					return "wkbGeometryCollection25D";
				case wkbNone                 :	// non-standard, for pure attribute records
					return "wkbNone";
				case wkbUnknown              :	// unknown type, non-standard
					return "wkbUnknown";
				default:
					return "wkbUnknown";
				}
			}
		
			OGRwkbGeometryType Str2ShapeType(std::string shpTypeStr)
			{
				if ( shpTypeStr == "wkbPoint")                	// 0-dimensional geometric object, standard WKB
					return wkbPoint;
				else if ( shpTypeStr ==  "wkbPoint25D")             	// 2.5D extension as per 99-402
					return wkbPoint25D;

				else if ( shpTypeStr ==  "wkbMultiPoint")           	// GeometryCollection of Points, standard WKB
					return wkbMultiPoint;
				else if ( shpTypeStr ==  "wkbMultiPoint25D")        	// 2.5D extension as per 99-402
					return wkbMultiPoint25D;

				else if ( shpTypeStr ==   "wkbLineString")          	// 1-dimensional geometric object with linear interpolation between Points, standard WKB
					return wkbLineString;
				else if ( shpTypeStr ==   "wkbMultiLineString") // GeometryCollection of LineStrings, standard WKB
					return wkbMultiLineString;
				else if ( shpTypeStr ==   "wkbLineString25D")	// 2.5D extension as per 99-402
					return wkbLineString25D;
				else if ( shpTypeStr ==   "wkbMultiLineString25D")	// 2.5D extension as per 99-402
					return wkbMultiLineString25D;

				else if ( shpTypeStr ==   "wkbPolygon")	// planar 2-dimensional geometric object defined by 1 exterior boundary and 0 or more interior boundaries, standard WKB
					return wkbPolygon;
				else if ( shpTypeStr ==   "wkbMultiPolygon")	// GeometryCollection of Polygons, standard WKB
					return wkbMultiPolygon;
				else if ( shpTypeStr ==   "wkbPolygon25D") // 2.5D extension as per 99-402
					return wkbPolygon25D;
				else if ( shpTypeStr ==   "wkbMultiPolygon25D")	// 2.5D extension as per 99-402
					return wkbMultiPolygon25D;

				else if ( shpTypeStr ==   "wkbLinearRing")  	// non-standard, just for createGeometry()
					return wkbLinearRing;
				else if ( shpTypeStr ==   "wkbGeometryCollection")	// geometric object that is a collection of 1 or more geometric objects, standard WKB
					return wkbGeometryCollection;
				else if ( shpTypeStr ==  "wkbGeometryCollection25D")	// 2.5D extension as per 99-402
					return wkbGeometryCollection25D;
				else if ( shpTypeStr ==   "wkbNone")                 	// non-standard, for pure attribute records
					return wkbNone;
				else if ( shpTypeStr ==   "wkbUnknown")              	// unknown type, non-standard
					return wkbUnknown;
				else
					return wkbUnknown;
			}
			

			const char* getCoordinateType(OGRLayerH pLayer)
			{
				const char* vertex_type = Get_Vertex_Type(OGR_FD_GetGeomType(OGR_L_GetLayerDefn(pLayer)));

				return vertex_type;
			}

			const char* getLayerType(OGRLayerH pLayer)
			{
				OGRFeatureDefnH featureDef = OGR_L_GetLayerDefn(pLayer);
				OGRwkbGeometryType geo_type	= OGR_FD_GetGeomType(featureDef);

				if( geo_type == wkbNone || geo_type == wkbUnknown )
				{
					OGR_L_ResetReading(pLayer);
					OGRFeatureH	pFeature;
					while( (geo_type == wkbNone || geo_type == wkbUnknown) && (pFeature = OGR_L_GetNextFeature(pLayer)) != NULL )
					{
						if( OGR_F_GetGeometryRef(pFeature) )
						{
							geo_type	= OGR_G_GetGeometryType(OGR_F_GetGeometryRef(pFeature));
						}

						OGR_F_Destroy(pFeature);
					}
					OGR_L_ResetReading(pLayer);
				}
				return ShapeType2Str(geo_type);
			}
		public:
			//////////////////////////////////////////////////////////////////////////
			//	
			//
			//
			//////////////////////////////////////////////////////////////////////////
			//! 初始化数据映射Handle
			virtual bool InitHandle(const char* readFilePath, const char* writeFilePath)
			{
				mReadPath = readFilePath;
				mWritePath = writeFilePath;
				CPLSetConfigOption("GDAL_FILENAME_IS_UTF8","YES");
				CPLSetConfigOption("SHAPE_ENCODING","");

				mDataset = (GDALDataset *)GDALOpenEx(readFilePath, GDAL_OF_VECTOR, NULL, NULL, NULL);
				if (mDataset==NULL) return false;

				return true;
			}

			//! 获取Handle的名称
			virtual const char* GetHandleName()
			{
				return "General OGR Vector Handle";
			}

			//! 获取Handle的版本
			virtual const char* GetHandleVersion()
			{
				return GDALVersionInfo("RELEASE_NAME");
			}

			//! 获取当前数据映射Handle的关联Schema
			virtual IUdxDatasetSchema* GetUdxSchema()
			{
				if (mUdxSchema==NULL)
				{
					mUdxSchema = createUdxDatasetSchema("UdxDescription");

					IUdxNodeSchema* layerCountNode = mUdxSchema->addChildNode("LayerCount", ESchemaNodeType::EDTKT_INT);

					IUdxNodeSchema* layerCollectionNode = mUdxSchema->addChildNode("LayerCollection", ESchemaNodeType::EDTKT_LIST);
					
					IUdxNodeSchema* layerItemNode = layerCollectionNode->addChildNode("Layer_Item", ESchemaNodeType::EDTKT_NODE);

					IUdxNodeSchema* layerNameNode = layerItemNode->addChildNode("LayerName", ESchemaNodeType::EDTKT_STRING);
					IUdxNodeSchema* shapeTypeNode = layerItemNode->addChildNode("ShapeType", ESchemaNodeType::EDTKT_STRING);
					IUdxNodeSchema* coordinateTypeNode = layerItemNode->addChildNode("CoordinateType", ESchemaNodeType::EDTKT_STRING);
					IUdxNodeSchema* featureColNode = layerItemNode->addChildNode("FeatureCollection", ESchemaNodeType::EDTKT_LIST);
					IUdxNodeSchema* attributeNode = layerItemNode->addChildNode("AttributeTable", ESchemaNodeType::EDTKT_TABLE);
					IUdxNodeSchema* spatialRefNode = layerItemNode->addChildNode("SpatialRef", ESchemaNodeType::EDTKT_STRING);

					featureColNode->addChildNode("Feature_Item", ESchemaNodeType::EDTKT_STRING);
				}
				return mUdxSchema;
			}

			//! 获取此Handle能够操作的SchemaNode个数
			virtual int GetCapbilityCount()
			{
				return m_Capabilities.size();
			}

			//! 迭代获取此Handle能够操作的SchemaNode
			virtual IUdxNodeSchema* getCapbilityNode(int idx)
			{
				int count = GetCapbilityCount();
				if (idx<0 || idx>=count)
					return NULL;
				return m_Capabilities[idx];
			}

			//////////////////////////////////////////////////////////////////////////
			//	
			//
			//
			//////////////////////////////////////////////////////////////////////////
			//! 从原始数据中按照传递过来的SchemaNode读取数据内容，
			//! 并且往传递过来的DataNode中写入相应的数据内容
			virtual bool ReadToNode(IUdxNodeSchema* pSNode, IUdxNode* pDNode)
			{
				if(mDataset == NULL || pSNode==NULL || pDNode==NULL)  
				{
					return false;
				}
				EKernelType pDType = pDNode->getKernel()->getType();
				int len = 0;
				EKernelType pDType1 = getKernelTypeBySchema(pSNode->getDescription()->getKernelType());
				if (pDType1 != pDType)
				{
					return false;
				}

				bool ret=false;
				std::string name = pSNode->getName();
				printf("%s\n",name.c_str());
				if (name == "LayerCount")
				{
					int layerCount = OGR_DS_GetLayerCount(mDataset);

					NGIS::Data::IUdxKernelIntValue* realKernel = (NGIS::Data::IUdxKernelIntValue*)pDNode->getKernel();
					realKernel->setTypedValue(layerCount);
					ret = true;
				}
				else if (name == "LayerName")
				{
					int layer_idx = pSNode->getParentNode()->getExtension();
					OGRLayerH layer = GDALDatasetGetLayer(mDataset, layer_idx);
					const char* layer_name = OGR_L_GetName(layer);

					NGIS::Data::IUdxKernelStringValue* realKernel = (NGIS::Data::IUdxKernelStringValue*)pDNode->getKernel();
					realKernel->setTypedValue(layer_name);
					ret = true;
				}
				else if (name == "ShapeType")
				{
					int layer_idx = pSNode->getParentNode()->getExtension();
					OGRLayerH layer = GDALDatasetGetLayer(mDataset, layer_idx);
					
					const char* layer_type = getLayerType(layer);
					NGIS::Data::IUdxKernelStringValue* realKernel = (NGIS::Data::IUdxKernelStringValue*)pDNode->getKernel();
					realKernel->setTypedValue(layer_type);
					ret = true;
				}
				else if (name == "CoordinateType")
				{
					int layer_idx = pSNode->getParentNode()->getExtension();
					OGRLayerH layer = GDALDatasetGetLayer(mDataset, layer_idx);

					const char* coordinate_type = getCoordinateType(layer);
					NGIS::Data::IUdxKernelStringValue* realKernel = (NGIS::Data::IUdxKernelStringValue*)pDNode->getKernel();
					realKernel->setTypedValue(coordinate_type);
					ret = true;
				}
				else if (name == "SpatialRef")
				{
					int layer_idx = pSNode->getParentNode()->getExtension();
					OGRLayerH layer = GDALDatasetGetLayer(mDataset, layer_idx);

					OGRSpatialReferenceH spatial_ref = OGR_L_GetSpatialRef(layer);
					char	*pWKT	= NULL;	OSRExportToWkt(spatial_ref, &pWKT);
					char	*pProj4	= NULL;	OSRExportToProj4(spatial_ref, &pProj4);

					if (pProj4!=NULL)
					{
						NGIS::Data::IUdxKernelStringValue* realKernel = (NGIS::Data::IUdxKernelStringValue*)pDNode->getKernel();
						realKernel->setTypedValue(pProj4);
						ret = true;
					}
					else
					{
						NGIS::Data::IUdxKernelStringValue* realKernel = (NGIS::Data::IUdxKernelStringValue*)pDNode->getKernel();
						realKernel->setTypedValue("");
						ret = false;
					}
				}
				else if (name == "FeatureCollection")
				{
					int layer_idx = pSNode->getParentNode()->getExtension();
					OGRLayerH layer = GDALDatasetGetLayer(mDataset, layer_idx);

					if (pDNode->getChildNodeCount()!=0)
						pDNode->removeChildNode(0);

					OGR_L_ResetReading(layer);
					char name_str[50];
					OGRFeatureH pFeature;
					while( (pFeature = OGR_L_GetNextFeature(layer)) != NULL)
					{
						int oid = OGR_F_GetFID(pFeature);
						OGRGeometryH pGeometry = OGR_F_GetGeometryRef(pFeature);
						if( pGeometry != NULL )
						{
							char* wkt_str = NULL;
							OGR_G_ExportToWkt(pGeometry, &wkt_str);
							memset(name_str, 0, 50);
							sprintf(name_str, "FeatureItem_%d", oid);
							NGIS::Data::IUdxNode* temp_node = pDNode->addChildNode(name_str, NGIS::Data::EKernelType::EKT_STRING);
							NGIS::Data::IUdxKernelStringValue* realKernel = (NGIS::Data::IUdxKernelStringValue*)temp_node->getKernel();
							realKernel->setTypedValue(wkt_str);
						}
						OGR_F_Destroy(pFeature);
					}
					OGR_L_ResetReading(layer);
				}
				else if (name == "AttributeTable")
				{
					int layer_idx = pSNode->getParentNode()->getExtension();
					OGRLayerH layer = GDALDatasetGetLayer(mDataset, layer_idx);
					OGRFeatureDefnH pDefn	= OGR_L_GetLayerDefn(layer);
					int field_count = OGR_FD_GetFieldCount(pDefn);
					for (int iField = 0; iField < field_count; iField++)
					{
						OGRFieldDefnH pDefnField = OGR_FD_GetFieldDefn(pDefn, iField);
						const char* field_name = OGR_Fld_GetNameRef(pDefnField);
						OGRFieldType field_type = OGR_Fld_GetType(pDefnField);
						if (field_type == OGRFieldType::OFTInteger)
						{
							pDNode->addChildNode(field_name, NGIS::Data::EKernelType::EKT_INT_LIST);
						}
						else if (field_type == OGRFieldType::OFTReal)
						{
							pDNode->addChildNode(field_name, NGIS::Data::EKernelType::EKT_REAL_LIST);
						}
						else if (field_type == OGRFieldType::OFTString)
						{
							pDNode->addChildNode(field_name, NGIS::Data::EKernelType::EKT_STRING_LIST);
						}
						else if (field_type == OGRFieldType::OFTDateTime)
						{
							pDNode->addChildNode(field_name, NGIS::Data::EKernelType::EKT_STRING_LIST);
						}
						else
						{
							pDNode->addChildNode(field_name, NGIS::Data::EKernelType::EKT_STRING_LIST);
						}
					}

					OGR_L_ResetReading(layer);
					char datetime_str[50];
					OGRFeatureH pFeature;
					while( (pFeature = OGR_L_GetNextFeature(layer)) != NULL)
					{
						for(int iField=0; iField<field_count; iField++)
						{
							OGRFieldDefnH field_def = OGR_F_GetFieldDefnRef(pFeature, iField);
							OGRFieldType field_type = OGR_Fld_GetType(field_def);
							if (field_type == OGRFieldType::OFTInteger)
							{
								int tempValue = OGR_F_GetFieldAsInteger(pFeature, iField);
								NGIS::Data::IUdxKernelIntArray* realKernel = (NGIS::Data::IUdxKernelIntArray*)(pDNode->getChildNode(iField)->getKernel());
								realKernel->addTypedValue(tempValue);
							}
							else if (field_type == OGRFieldType::OFTReal)
							{
								double tempValue = OGR_F_GetFieldAsDouble(pFeature, iField);
								NGIS::Data::IUdxKernelRealArray* realKernel = (NGIS::Data::IUdxKernelRealArray*)(pDNode->getChildNode(iField)->getKernel());
								realKernel->addTypedValue(tempValue);
							}
							else if (field_type == OGRFieldType::OFTString)
							{
								const char* tempValue = OGR_F_GetFieldAsString(pFeature, iField);
								NGIS::Data::IUdxKernelStringArray* realKernel = (NGIS::Data::IUdxKernelStringArray*)(pDNode->getChildNode(iField)->getKernel());
								realKernel->addTypedValue(tempValue);
							}
							else if (field_type == OGRFieldType::OFTDateTime)
							{
								int pnYear, pnMonth, pnDay, pnHour, pnMinute, pnSecond, pnTZFlag;
								OGR_F_GetFieldAsDateTime(pFeature, iField, &pnYear, &pnMonth, &pnDay, &pnHour, &pnMinute, &pnSecond, &pnTZFlag);
								memset(datetime_str,0,50);
								sprintf(datetime_str, "%d-%d-%d %d:%d:%d", pnYear, pnMonth, pnDay, pnHour, pnMinute, pnSecond);
								NGIS::Data::IUdxKernelStringArray* realKernel = (NGIS::Data::IUdxKernelStringArray*)(pDNode->getChildNode(iField)->getKernel());
								realKernel->addTypedValue(datetime_str);
							}
							else
							{
								const char* tempValue = OGR_F_GetFieldAsString(pFeature, iField);
								NGIS::Data::IUdxKernelStringArray* realKernel = (NGIS::Data::IUdxKernelStringArray*)(pDNode->getChildNode(iField)->getKernel());
								realKernel->addTypedValue(tempValue);
							}
						}

						OGR_F_Destroy(pFeature);
					}
					OGR_L_ResetReading(layer);
				}
			}

			//! 根据当前选中的SchemaNode，获取需要设置Index信息的SchemaNode的数量
			virtual int prepareNeedIndexSettingNode(IUdxNodeSchema* pSNode)
			{
				mNeedIndexSchemaList.clear();
				findAllSchemaNode_WithListType(pSNode);
				return mNeedIndexSchemaList.size();
			}

			//! 迭代获取需要设置Index信息的SchemaNode
			virtual IUdxNodeSchema* getNeedIndexSettingNode(int idx)
			{
				if (idx>=mNeedIndexSchemaList.size() || idx<0)
					return NULL;
				return mNeedIndexSchemaList[idx];
			}

			//////////////////////////////////////////////////////////////////////////
			//	
			//
			//
			//////////////////////////////////////////////////////////////////////////
			//! 准备保存到文件中
			virtual bool BeginWriteToFile()
			{
				if (mWritePath=="")
				{
					mCurrentErrorInfo = "write file path does not exist";
					return false;
				}

				return true;
			}

			//! 从UDX数据节点中读取内容，并写入到原始数据格式中
			virtual bool WriteFromNode(IUdxNode* pDNode, int formatFlag=-1)
			{
				if(pDNode==NULL)  return false;
				if(pDNode->validateSchema(GetUdxSchema()) == false) return false;

				IUdxKernel* pKernel;
				IUdxKernelRealValue* realKernel;
				IUdxKernelIntValue* intKernel;
				IUdxKernelStringValue* stringKernel;

				GDALDriverH pDriver = NULL;
				switch (formatFlag)
				{
				case 0: pDriver = GDALGetDriverByName("netCDF"); break;
				case 1: pDriver = GDALGetDriverByName("AmigoCloud"); break;
				case 2: pDriver = GDALGetDriverByName("PostgreSQL"); break;
				case 3: pDriver = GDALGetDriverByName("PCIDSK"); break;
				case 4: pDriver = GDALGetDriverByName("PDF"); break;
				case 5: pDriver = GDALGetDriverByName("ESRI Shapefile"); break;
				case 6: pDriver = GDALGetDriverByName("MapInfo File"); break;
				case 7: pDriver = GDALGetDriverByName("S57"); break;
				case 8: pDriver = GDALGetDriverByName("DGN"); break;
				case 9: pDriver = GDALGetDriverByName("Memory"); break;
				case 10: pDriver = GDALGetDriverByName("BNA"); break;
				case 11: pDriver = GDALGetDriverByName("CSV"); break;
				case 12: pDriver = GDALGetDriverByName("GML"); break;
				case 13: pDriver = GDALGetDriverByName("GPX"); break;
				case 14: pDriver = GDALGetDriverByName("LIBKML"); break;
				case 15: pDriver = GDALGetDriverByName("KML"); break;
				case 16: pDriver = GDALGetDriverByName("GeoJSON"); break;
				case 17: pDriver = GDALGetDriverByName("Interlis 1"); break;
				case 18: pDriver = GDALGetDriverByName("Interlis 2"); break;
				case 19: pDriver = GDALGetDriverByName("OGR_GMT"); break;
				case 20: pDriver = GDALGetDriverByName("GPKG"); break;
				case 21: pDriver = GDALGetDriverByName("SQLite"); break;
				case 22: pDriver = GDALGetDriverByName("ODBC"); break;
				case 23: pDriver = GDALGetDriverByName("WAsP"); break;
				case 24: pDriver = GDALGetDriverByName("MSSQLSpatial"); break;
				case 25: pDriver = GDALGetDriverByName("MySQL"); break;
				case 26: pDriver = GDALGetDriverByName("DXF"); break;
				case 27: pDriver = GDALGetDriverByName("Geoconcept"); break;
				case 28: pDriver = GDALGetDriverByName("GeoRSS"); break;
				case 29: pDriver = GDALGetDriverByName("GPSTrackMaker"); break;
				case 30: pDriver = GDALGetDriverByName("PGDUMP"); break;
				case 31: pDriver = GDALGetDriverByName("GPSBabel"); break;
				case 32: pDriver = GDALGetDriverByName("GFT"); break;
				case 33: pDriver = GDALGetDriverByName("CouchDB"); break;
				case 34: pDriver = GDALGetDriverByName("Cloudant"); break;
				case 35: pDriver = GDALGetDriverByName("ODS"); break;
				case 36: pDriver = GDALGetDriverByName("XLSX"); break;
				case 37: pDriver = GDALGetDriverByName("ElasticSearch"); break;
				case 38: pDriver = GDALGetDriverByName("Carto"); break;
				case 39: pDriver = GDALGetDriverByName("Selafin"); break;
				case 40: pDriver = GDALGetDriverByName("JML"); break;
				case 41: pDriver = GDALGetDriverByName("VDV"); break;
				case 42: pDriver = GDALGetDriverByName("TIGER"); break;
				default: pDriver = GDALGetDriverByName("ESRI Shapefile"); break;
				}

				GDALDatasetH	pDataset = GDALCreate(pDriver, mWritePath.c_str(), 0, 0, 0, GDT_Unknown, NULL);
				if (pDataset==NULL) return false;

				pKernel = pDNode->getChildNode(0)->getKernel();
				intKernel = (IUdxKernelIntValue*)pKernel;
				int layer_count = intKernel->getTypedValue();

				for (int iLayer=0; iLayer<layer_count; iLayer++)
				{
					NGIS::Data::IUdxNode* temp_node = pDNode->getChildNode(1)->getChildNode(iLayer);
					if (temp_node==NULL) break;

					NGIS::Data::IUdxNode* name_node = temp_node->getChildNode(0);
					NGIS::Data::IUdxNode* shptype_node = temp_node->getChildNode(1);
					NGIS::Data::IUdxNode* coordtype_node = temp_node->getChildNode(2);
					NGIS::Data::IUdxNode* features_node = temp_node->getChildNode(3);
					NGIS::Data::IUdxNode* attribute_node = temp_node->getChildNode(4);
					NGIS::Data::IUdxNode* spatialref_node = temp_node->getChildNode(5);

					stringKernel=(IUdxKernelStringValue*)name_node->getKernel();
					std::string name_str = stringKernel->getTypedValue();

					stringKernel=(IUdxKernelStringValue*)shptype_node->getKernel();
					std::string shape_type_str = stringKernel->getTypedValue();

					stringKernel=(IUdxKernelStringValue*)coordtype_node->getKernel();
					std::string coord_type_name_str = stringKernel->getTypedValue();

					int feature_count = features_node->getChildNodeCount();
					int field_count = attribute_node->getChildNodeCount();

					stringKernel=(IUdxKernelStringValue*)spatialref_node->getKernel();
					std::string spatial_ref_str = stringKernel->getTypedValue();

					//////////////////////////////////////////////////////////////////////////
					OGRSpatialReferenceH	pSRS	= OSRNewSpatialReference("");
					OSRImportFromProj4(pSRS, spatial_ref_str.c_str());
					OGRLayerH temp_layer = GDALDatasetCreateLayer(pDataset, name_str.c_str(), pSRS, Str2ShapeType(shape_type_str), NULL);

					if( !temp_layer )
						break;

					for (int iField = 0; iField < field_count; iField++)
					{
						std::string tempName = attribute_node->getChildNode(iField)->getName();

						OGRFieldDefnH oField = NULL;
						if (attribute_node->getChildNode(iField)->getKernel()->getType() == NGIS::Data::EKernelType::EKT_INT_LIST)
						{
							oField = OGR_Fld_Create(tempName.c_str(), OGRFieldType::OFTInteger);
						}
						else if (attribute_node->getChildNode(iField)->getKernel()->getType() == NGIS::Data::EKernelType::EKT_REAL_LIST)
						{
							oField = OGR_Fld_Create(tempName.c_str(), OGRFieldType::OFTReal);
						}
						else
						{
							oField = OGR_Fld_Create(tempName.c_str(), OGRFieldType::OFTString);
						}
						OGR_L_CreateField(temp_layer, oField, TRUE);
					}

					for(int iShape=0; iShape<feature_count; iShape++)
					{
						stringKernel=(IUdxKernelStringValue*)features_node->getChildNode(iShape)->getKernel();
						std::string wkt_str = stringKernel->getTypedValue();
						OGRFeatureH pFeature	= OGR_F_Create(OGR_L_GetLayerDefn(temp_layer));

						char* copy_wkt_str = new char[wkt_str.size() + 1];
						strcpy(copy_wkt_str, wkt_str.c_str());

						OGRGeometryH pGeometry = OGR_G_CreateGeometry(Str2ShapeType(shape_type_str));
						OGR_G_ImportFromWkt(pGeometry, &copy_wkt_str);
						OGR_F_SetGeometry(pFeature, pGeometry);
						OGR_G_DestroyGeometry(pGeometry);

						for(int iField=0; iField<field_count; iField++)
						{
							if (attribute_node->getChildNode(iField)->getKernel()->getType() == NGIS::Data::EKernelType::EKT_INT_LIST)
							{
								NGIS::Data::IUdxKernelIntArray* temp_kernel = (NGIS::Data::IUdxKernelIntArray*)(attribute_node->getChildNode(iField)->getKernel());
								int tempValue = 0;
								temp_kernel->getTypedValueByIndex(iShape,tempValue);
								
								OGR_F_SetFieldInteger(pFeature, iField, tempValue);
							}
							else if (attribute_node->getChildNode(iField)->getKernel()->getType() == NGIS::Data::EKernelType::EKT_REAL_LIST)
							{
								NGIS::Data::IUdxKernelRealArray* temp_kernel = (NGIS::Data::IUdxKernelRealArray*)(attribute_node->getChildNode(iField)->getKernel());
								double tempValue = 0.0;
								temp_kernel->getTypedValueByIndex(iShape, tempValue);

								OGR_F_SetFieldDouble(pFeature, iField, tempValue);
							}
							else
							{
								NGIS::Data::IUdxKernelStringArray* temp_kernel = (NGIS::Data::IUdxKernelStringArray*)(attribute_node->getChildNode(iField)->getKernel());
								std::string tempValue = "";
								temp_kernel->getTypedValueByIndex(iShape, tempValue);

								OGR_F_SetFieldString(pFeature, iField, tempValue.c_str());
							}
						}
						OGR_L_CreateFeature(temp_layer, pFeature);
						OGR_F_Destroy(pFeature);
						//delete [] copy_wkt_str;
					}
				}

				return true;
			}

			//! 结束保存到文件中
			virtual bool EndWriteToFile()
			{
				if (mWritePath=="")
				{
					mCurrentErrorInfo = "write file path does not exist";
					return false;
				}

				return true;
			}

			//! 获取当前读取或者写入的错误信息
			virtual const char* GetCurrentErrorInfo()
			{
				return mCurrentErrorInfo.c_str();
			}
		};
	}
}

#endif