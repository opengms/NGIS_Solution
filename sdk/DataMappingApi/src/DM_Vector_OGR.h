#ifndef __DM_VECTROR_OGR_H__
#define __DM_VECTROR_OGR_H__

#include "..\include\IDataMappingHandle.h"
#include "..\include\NativeDataMappingAPI.h"
#include <stdio.h>
#include <string>
#include <vector>
#include <map>

#define MAXPATH  1024

#include "gdal_priv.h"
#include "cpl_string.h"  
#include "ogr_spatialref.h"  
#include "gdalwarper.h"  
#include "gdal_pam.h"  
#include "cpl_conv.h"  
#include "gdal.h"
#include "ogrsf_frmts.h"

#include <Windows.h>
#include <io.h>
#include <direct.h>

namespace NGIS
{
	namespace DataMapping
	{
		class DM_Vector_OGR : public IDataMappingHandle
		{
		protected:
			std::string mReadPath;
			std::string mWritePath;
			GDALDataset *mDataset;

			IUdxDatasetSchema* mUdxSchema;
			IUdxDataset* mUdxData;

			std::string mCurrentErrorInfo;
			std::vector<IUdxNodeSchema*> mNeedIndexSchemaList;

			std::vector<IUdxNodeSchema*> m_Capabilities;

		public:
			DM_Vector_OGR()
			{
				mUdxSchema = NULL;
				mUdxData = NULL;
				GDALAllRegister();

				IUdxDatasetSchema* sDataset = GetUdxSchema();
				m_Capabilities.push_back(sDataset->getChildNode(0));
				m_Capabilities.push_back(sDataset->getChildNode(1)->getChildNode(0)->getChildNode(0));
				m_Capabilities.push_back(sDataset->getChildNode(1)->getChildNode(0)->getChildNode(1));
				m_Capabilities.push_back(sDataset->getChildNode(1)->getChildNode(0)->getChildNode(2));
				m_Capabilities.push_back(sDataset->getChildNode(1)->getChildNode(0)->getChildNode(3));
				m_Capabilities.push_back(sDataset->getChildNode(1)->getChildNode(0)->getChildNode(4));
				m_Capabilities.push_back(sDataset->getChildNode(1)->getChildNode(0)->getChildNode(5));
			}

			~DM_Vector_OGR()
			{
				if( mDataset )
				{
					GDALClose(mDataset);
					mDataset	= NULL;
				}
				if (mUdxSchema) mUdxSchema->release();
				if (mUdxData) mUdxData->release();
			}
		private:
			void findAllSchemaNode_WithListType(IUdxNodeSchema* pNode)
			{
				if (pNode->getDescription()->getKernelType() == EDTKT_LIST)
				{
					mNeedIndexSchemaList.push_back(pNode);
				}
				if (pNode->getParentNode() != NULL)
				{
					findAllSchemaNode_WithListType(pNode->getParentNode());
				}
			}

			const char* Get_Vertex_Type(OGRwkbGeometryType Type)
			{
				switch( Type )
				{
				case wkbPoint25D             :	// 2.5D extension as per 99-402
				case wkbMultiPoint25D        :	// 2.5D extension as per 99-402
				case wkbLineString25D        :	// 2.5D extension as per 99-402
				case wkbMultiLineString25D   :	// 2.5D extension as per 99-402
				case wkbPolygon25D           :	// 2.5D extension as per 99-402
				case wkbMultiPolygon25D      :	// 2.5D extension as per 99-402
				case wkbGeometryCollection25D:	// 2.5D extension as per 99-402
					return "XYZ";

				default:
					return "XY";
				}
			}

			const char* ShapeType2Str(OGRwkbGeometryType Type)
			{
				switch( Type )
				{
				case wkbPoint                :	// 0-dimensional geometric object, standard WKB
					return "wkbPoint";
				case wkbPoint25D             :	// 2.5D extension as per 99-402
					return "wkbPoint25D";

				case wkbMultiPoint           :	// GeometryCollection of Points, standard WKB
					return "wkbMultiPoint";
				case wkbMultiPoint25D        :	// 2.5D extension as per 99-402
					return "wkbMultiPoint25D";

				case wkbLineString           :	// 1-dimensional geometric object with linear interpolation between Points, standard WKB
					return "wkbLineString";
				case wkbMultiLineString      :	// GeometryCollection of LineStrings, standard WKB
					return "wkbMultiLineString";
				case wkbLineString25D        :	// 2.5D extension as per 99-402
					return "wkbLineString25D";
				case wkbMultiLineString25D   :	// 2.5D extension as per 99-402
					return "wkbMultiLineString25D";

				case wkbPolygon              :	// planar 2-dimensional geometric object defined by 1 exterior boundary and 0 or more interior boundaries, standard WKB
					return "wkbPolygon";
				case wkbMultiPolygon         :	// GeometryCollection of Polygons, standard WKB
					return "wkbMultiPolygon";
				case wkbPolygon25D           :	// 2.5D extension as per 99-402
					return "wkbPolygon25D";
				case wkbMultiPolygon25D      :	// 2.5D extension as per 99-402
					return "wkbMultiPolygon25D";

				case wkbLinearRing           :	// non-standard, just for createGeometry()
					return "wkbLinearRing";
				case wkbGeometryCollection   :	// geometric object that is a collection of 1 or more geometric objects, standard WKB
					return "wkbGeometryCollection";
				case wkbGeometryCollection25D:	// 2.5D extension as per 99-402
					return "wkbGeometryCollection25D";
				case wkbNone                 :	// non-standard, for pure attribute records
					return "wkbNone";
				case wkbUnknown              :	// unknown type, non-standard
					return "wkbUnknown";
				default:
					return "wkbUnknown";
				}
			}
		
			OGRwkbGeometryType Str2ShapeType(std::string shpTypeStr,  std::string wkt_str)
			{
				if ( shpTypeStr == "wkbPoint")                	// 0-dimensional geometric object, standard WKB
				{
					if (wkt_str.find("MULTI")>=0)
					{
						return wkbMultiPoint;
					}
					else
					{
						return wkbPoint;
					}
				}
				else if ( shpTypeStr ==  "wkbPoint25D")             	// 2.5D extension as per 99-402
				{
					if (wkt_str.find("MULTI")>=0)
					{
						return wkbMultiPoint25D;
					}
					else
					{
						return wkbPoint25D;
					}
				}

				else if ( shpTypeStr ==  "wkbMultiPoint")           	// GeometryCollection of Points, standard WKB
					return wkbMultiPoint;
				else if ( shpTypeStr ==  "wkbMultiPoint25D")        	// 2.5D extension as per 99-402
					return wkbMultiPoint25D;

				else if ( shpTypeStr ==   "wkbLineString")          	// 1-dimensional geometric object with linear interpolation between Points, standard WKB
				{
					if (wkt_str.find("MULTI")>=0)
					{
						return wkbMultiLineString;
					}
					else
					{
						return wkbLineString;
					}
				}
				else if ( shpTypeStr ==   "wkbMultiLineString") // GeometryCollection of LineStrings, standard WKB
					return wkbMultiLineString;
				else if ( shpTypeStr ==   "wkbLineString25D")	// 2.5D extension as per 99-402
				{
					if (wkt_str.find("MULTI")>=0)
					{
						return wkbMultiLineString25D;
					}
					else
					{
						return wkbLineString25D;
					}
				}
				else if ( shpTypeStr ==   "wkbMultiLineString25D")	// 2.5D extension as per 99-402
					return wkbMultiLineString25D;

				else if ( shpTypeStr ==   "wkbPolygon")	// planar 2-dimensional geometric object defined by 1 exterior boundary and 0 or more interior boundaries, standard WKB
				{
					if (wkt_str.find("MULTI")>=0)
					{
						return wkbMultiPolygon;
					}
					else
					{
						return wkbPolygon;
					}
				}
				else if ( shpTypeStr ==   "wkbMultiPolygon")	// GeometryCollection of Polygons, standard WKB
					return wkbMultiPolygon;
				else if ( shpTypeStr ==   "wkbPolygon25D") // 2.5D extension as per 99-402
				{
					if (wkt_str.find("MULTI")>=0)
					{
						return wkbMultiPolygon25D;
					}
					else
					{
						return wkbPolygon25D;
					}
				}
				else if ( shpTypeStr ==   "wkbMultiPolygon25D")	// 2.5D extension as per 99-402
					return wkbMultiPolygon25D;

				else if ( shpTypeStr ==   "wkbLinearRing")  	// non-standard, just for createGeometry()
					return wkbLinearRing;
				else if ( shpTypeStr ==   "wkbGeometryCollection")	// geometric object that is a collection of 1 or more geometric objects, standard WKB
					return wkbGeometryCollection;
				else if ( shpTypeStr ==  "wkbGeometryCollection25D")	// 2.5D extension as per 99-402
					return wkbGeometryCollection25D;
				else if ( shpTypeStr ==   "wkbNone")                 	// non-standard, for pure attribute records
					return wkbNone;
				else if ( shpTypeStr ==   "wkbUnknown")              	// unknown type, non-standard
					return wkbUnknown;
				else
					return wkbUnknown;
			}
			

			const char* getCoordinateType(OGRLayerH pLayer)
			{
				const char* vertex_type = Get_Vertex_Type(OGR_FD_GetGeomType(OGR_L_GetLayerDefn(pLayer)));

				return vertex_type;
			}

			const char* getLayerType(OGRLayerH pLayer)
			{
				OGRFeatureDefnH featureDef = OGR_L_GetLayerDefn(pLayer);
				OGRwkbGeometryType geo_type	= OGR_FD_GetGeomType(featureDef);

				if( geo_type == wkbNone || geo_type == wkbUnknown )
				{
					OGR_L_ResetReading(pLayer);
					OGRFeatureH	pFeature;
					while( (geo_type == wkbNone || geo_type == wkbUnknown) && (pFeature = OGR_L_GetNextFeature(pLayer)) != NULL )
					{
						if( OGR_F_GetGeometryRef(pFeature) )
						{
							geo_type	= OGR_G_GetGeometryType(OGR_F_GetGeometryRef(pFeature));
						}

						OGR_F_Destroy(pFeature);
					}
					OGR_L_ResetReading(pLayer);
				}
				return ShapeType2Str(geo_type);
			}
		public:
			//////////////////////////////////////////////////////////////////////////
			//	
			//
			//
			//////////////////////////////////////////////////////////////////////////
			//! 初始化数据映射Handle
			virtual bool InitHandle(const char* readFilePath, const char* writeFilePath)
			{
				mReadPath = readFilePath;
				mWritePath = writeFilePath;
				CPLSetConfigOption("GDAL_FILENAME_IS_UTF8","YES");
				CPLSetConfigOption("SHAPE_ENCODING","");

				mDataset = (GDALDataset *)GDALOpenEx(readFilePath, GDAL_OF_VECTOR, NULL, NULL, NULL);
				if (mDataset==NULL) return false;

				return true;
			}

			//! 获取Handle的名称
			virtual const char* GetHandleName()
			{
				return "General OGR Vector Handle";
			}

			//! 获取Handle的版本
			virtual const char* GetHandleVersion()
			{
				return GDALVersionInfo("RELEASE_NAME");
			}

			//! 获取当前数据映射Handle的关联Schema
			virtual IUdxDatasetSchema* GetUdxSchema()
			{
				if (mUdxSchema==NULL)
				{
					mUdxSchema = createUdxDatasetSchema("UdxDescription");

					IUdxNodeSchema* layerCountNode = mUdxSchema->addChildNode("LayerCount", ESchemaNodeType::EDTKT_INT);

					IUdxNodeSchema* layerCollectionNode = mUdxSchema->addChildNode("LayerCollection", ESchemaNodeType::EDTKT_LIST);
					
					IUdxNodeSchema* layerItemNode = layerCollectionNode->addChildNode("Layer_Item", ESchemaNodeType::EDTKT_NODE);

					IUdxNodeSchema* layerNameNode = layerItemNode->addChildNode("LayerName", ESchemaNodeType::EDTKT_STRING);
					IUdxNodeSchema* shapeTypeNode = layerItemNode->addChildNode("ShapeType", ESchemaNodeType::EDTKT_STRING);
					IUdxNodeSchema* coordinateTypeNode = layerItemNode->addChildNode("CoordinateType", ESchemaNodeType::EDTKT_STRING);
					IUdxNodeSchema* featureColNode = layerItemNode->addChildNode("FeatureCollection", ESchemaNodeType::EDTKT_LIST);
					IUdxNodeSchema* attributeNode = layerItemNode->addChildNode("AttributeTable", ESchemaNodeType::EDTKT_TABLE);
					IUdxNodeSchema* spatialRefNode = layerItemNode->addChildNode("SpatialRef", ESchemaNodeType::EDTKT_STRING);

					featureColNode->addChildNode("Feature_Item", ESchemaNodeType::EDTKT_STRING);
				}
				return mUdxSchema;
			}

			//! 获取此Handle能够操作的SchemaNode个数
			virtual int GetCapbilityCount()
			{
				return m_Capabilities.size();
			}

			//! 迭代获取此Handle能够操作的SchemaNode
			virtual IUdxNodeSchema* getCapbilityNode(int idx)
			{
				int count = GetCapbilityCount();
				if (idx<0 || idx>=count)
					return NULL;
				return m_Capabilities[idx];
			}

			//////////////////////////////////////////////////////////////////////////
			//	
			//
			//
			//////////////////////////////////////////////////////////////////////////
			//! 从原始数据中按照传递过来的SchemaNode读取数据内容，
			//! 并且往传递过来的DataNode中写入相应的数据内容
			virtual bool ReadToNode(IUdxNodeSchema* pSNode, IUdxNode* pDNode)
			{
				if(mDataset == NULL || pSNode==NULL || pDNode==NULL)  
				{
					return false;
				}
				EKernelType pDType = pDNode->getKernel()->getType();
				int len = 0;
				EKernelType pDType1 = getKernelTypeBySchema(pSNode->getDescription()->getKernelType());
				if (pDType1 != pDType)
				{
					return false;
				}

				bool ret=false;
				std::string name = pSNode->getName();
				//printf("%s\n",name.c_str());
				if (name == "LayerCount")
				{
					int layerCount = OGR_DS_GetLayerCount(mDataset);

					NGIS::Data::IUdxKernelIntValue* realKernel = (NGIS::Data::IUdxKernelIntValue*)pDNode->getKernel();
					realKernel->setTypedValue(layerCount);
					ret = true;
				}
				else if (name == "LayerName")
				{
					int layer_idx = pSNode->getParentNode()->getExtension();
					OGRLayerH layer = GDALDatasetGetLayer(mDataset, layer_idx);
					const char* layer_name = OGR_L_GetName(layer);

					NGIS::Data::IUdxKernelStringValue* realKernel = (NGIS::Data::IUdxKernelStringValue*)pDNode->getKernel();
					realKernel->setTypedValue(layer_name);
					ret = true;
				}
				else if (name == "ShapeType")
				{
					int layer_idx = pSNode->getParentNode()->getExtension();
					OGRLayerH layer = GDALDatasetGetLayer(mDataset, layer_idx);
					
					const char* layer_type = getLayerType(layer);
					NGIS::Data::IUdxKernelStringValue* realKernel = (NGIS::Data::IUdxKernelStringValue*)pDNode->getKernel();
					realKernel->setTypedValue(layer_type);
					ret = true;
				}
				else if (name == "CoordinateType")
				{
					int layer_idx = pSNode->getParentNode()->getExtension();
					OGRLayerH layer = GDALDatasetGetLayer(mDataset, layer_idx);

					const char* coordinate_type = getCoordinateType(layer);
					NGIS::Data::IUdxKernelStringValue* realKernel = (NGIS::Data::IUdxKernelStringValue*)pDNode->getKernel();
					realKernel->setTypedValue(coordinate_type);
					ret = true;
				}
				else if (name == "SpatialRef")
				{
					int layer_idx = pSNode->getParentNode()->getExtension();
					OGRLayerH layer = GDALDatasetGetLayer(mDataset, layer_idx);

					OGRSpatialReferenceH spatial_ref = OGR_L_GetSpatialRef(layer);
					char	*pWKT	= NULL;	OSRExportToWkt(spatial_ref, &pWKT);
					char	*pProj4	= NULL;	OSRExportToProj4(spatial_ref, &pProj4);

					if (pProj4!=NULL)
					{
						NGIS::Data::IUdxKernelStringValue* realKernel = (NGIS::Data::IUdxKernelStringValue*)pDNode->getKernel();
						realKernel->setTypedValue(pProj4);
						ret = true;
					}
					else
					{
						NGIS::Data::IUdxKernelStringValue* realKernel = (NGIS::Data::IUdxKernelStringValue*)pDNode->getKernel();
						realKernel->setTypedValue("");
						ret = false;
					}
				}
				else if (name == "FeatureCollection")
				{
					int layer_idx = pSNode->getParentNode()->getExtension();
					OGRLayerH layer = GDALDatasetGetLayer(mDataset, layer_idx);

					if (pDNode->getChildNodeCount()!=0)
						pDNode->removeChildNode(0);

					OGR_L_ResetReading(layer);
					char name_str[50];
					OGRFeatureH pFeature;
					while( (pFeature = OGR_L_GetNextFeature(layer)) != NULL)
					{
						int oid = OGR_F_GetFID(pFeature);
						OGRGeometryH pGeometry = OGR_F_GetGeometryRef(pFeature);
						if( pGeometry != NULL )
						{
							char* wkt_str = NULL;
							OGR_G_ExportToWkt(pGeometry, &wkt_str);
							memset(name_str, 0, 50);
							sprintf(name_str, "FeatureItem_%d", oid);
							NGIS::Data::IUdxNode* temp_node = pDNode->addChildNode(name_str, NGIS::Data::EKernelType::EKT_STRING);
							NGIS::Data::IUdxKernelStringValue* realKernel = (NGIS::Data::IUdxKernelStringValue*)temp_node->getKernel();
							realKernel->setTypedValue(wkt_str);
						}
						OGR_F_Destroy(pFeature);
					}
					OGR_L_ResetReading(layer);
				}
				else if (name == "Feature_Item")
				{
					int layer_idx = pSNode->getParentNode()->getParentNode()->getParentNode()->getExtension();
					OGRLayerH layer = GDALDatasetGetLayer(mDataset, layer_idx);
					int feature_idx = pSNode->getParentNode()->getExtension();

					if (pDNode->getChildNodeCount()!=0)
						pDNode->removeChildNode(0);

					OGR_L_ResetReading(layer);
					char name_str[50];
					OGRFeatureH pFeature;
					int idx_flag = 0;
					while( (pFeature = OGR_L_GetNextFeature(layer)) != NULL)
					{
						if (idx_flag != feature_idx)
						{
							idx_flag++;
							continue;
						}
						int oid = OGR_F_GetFID(pFeature);
						OGRGeometryH pGeometry = OGR_F_GetGeometryRef(pFeature);
						if( pGeometry != NULL )
						{
							char* wkt_str = NULL;
							OGR_G_ExportToWkt(pGeometry, &wkt_str);
							memset(name_str, 0, 50);
							sprintf(name_str, "FeatureItem_%d", oid);
							NGIS::Data::IUdxKernelStringValue* realKernel = (NGIS::Data::IUdxKernelStringValue*)pDNode->getKernel();
							realKernel->setTypedValue(wkt_str);
						}
						OGR_F_Destroy(pFeature);
						break;
					}
					OGR_L_ResetReading(layer);
				}
				else if (name == "AttributeTable")
				{
					int layer_idx = pSNode->getParentNode()->getExtension();
					OGRLayerH layer = GDALDatasetGetLayer(mDataset, layer_idx);
					OGRFeatureDefnH pDefn	= OGR_L_GetLayerDefn(layer);
					int field_count = OGR_FD_GetFieldCount(pDefn);
					for (int iField = 0; iField < field_count; iField++)
					{
						OGRFieldDefnH pDefnField = OGR_FD_GetFieldDefn(pDefn, iField);
						const char* field_name = OGR_Fld_GetNameRef(pDefnField);
						OGRFieldType field_type = OGR_Fld_GetType(pDefnField);
						if (field_type == OGRFieldType::OFTInteger)
						{
							pDNode->addChildNode(field_name, NGIS::Data::EKernelType::EKT_INT_LIST);
						}
						else if (field_type == OGRFieldType::OFTReal)
						{
							pDNode->addChildNode(field_name, NGIS::Data::EKernelType::EKT_REAL_LIST);
						}
						else if (field_type == OGRFieldType::OFTString)
						{
							pDNode->addChildNode(field_name, NGIS::Data::EKernelType::EKT_STRING_LIST);
						}
						else if (field_type == OGRFieldType::OFTDateTime)
						{
							pDNode->addChildNode(field_name, NGIS::Data::EKernelType::EKT_STRING_LIST);
						}
						else
						{
							pDNode->addChildNode(field_name, NGIS::Data::EKernelType::EKT_STRING_LIST);
						}
					}

					OGR_L_ResetReading(layer);
					char datetime_str[50];
					OGRFeatureH pFeature;
					while( (pFeature = OGR_L_GetNextFeature(layer)) != NULL)
					{
						for(int iField=0; iField<field_count; iField++)
						{
							OGRFieldDefnH field_def = OGR_F_GetFieldDefnRef(pFeature, iField);
							OGRFieldType field_type = OGR_Fld_GetType(field_def);
							if (field_type == OGRFieldType::OFTInteger)
							{
								int tempValue = OGR_F_GetFieldAsInteger(pFeature, iField);
								NGIS::Data::IUdxKernelIntArray* realKernel = (NGIS::Data::IUdxKernelIntArray*)(pDNode->getChildNode(iField)->getKernel());
								realKernel->addTypedValue(tempValue);
							}
							else if (field_type == OGRFieldType::OFTReal)
							{
								double tempValue = OGR_F_GetFieldAsDouble(pFeature, iField);
								NGIS::Data::IUdxKernelRealArray* realKernel = (NGIS::Data::IUdxKernelRealArray*)(pDNode->getChildNode(iField)->getKernel());
								realKernel->addTypedValue(tempValue);
							}
							else if (field_type == OGRFieldType::OFTString)
							{
								const char* tempValue = OGR_F_GetFieldAsString(pFeature, iField);
								NGIS::Data::IUdxKernelStringArray* realKernel = (NGIS::Data::IUdxKernelStringArray*)(pDNode->getChildNode(iField)->getKernel());
								realKernel->addTypedValue(tempValue);
							}
							else if (field_type == OGRFieldType::OFTDateTime)
							{
								int pnYear, pnMonth, pnDay, pnHour, pnMinute, pnSecond, pnTZFlag;
								OGR_F_GetFieldAsDateTime(pFeature, iField, &pnYear, &pnMonth, &pnDay, &pnHour, &pnMinute, &pnSecond, &pnTZFlag);
								memset(datetime_str,0,50);
								sprintf(datetime_str, "%d-%d-%d %d:%d:%d", pnYear, pnMonth, pnDay, pnHour, pnMinute, pnSecond);
								NGIS::Data::IUdxKernelStringArray* realKernel = (NGIS::Data::IUdxKernelStringArray*)(pDNode->getChildNode(iField)->getKernel());
								realKernel->addTypedValue(datetime_str);
							}
							else
							{
								const char* tempValue = OGR_F_GetFieldAsString(pFeature, iField);
								NGIS::Data::IUdxKernelStringArray* realKernel = (NGIS::Data::IUdxKernelStringArray*)(pDNode->getChildNode(iField)->getKernel());
								realKernel->addTypedValue(tempValue);
							}
						}

						OGR_F_Destroy(pFeature);
					}
					OGR_L_ResetReading(layer);
				}
			}

			//! 根据当前选中的SchemaNode，获取需要设置Index信息的SchemaNode的数量
			virtual int prepareNeedIndexSettingNode(IUdxNodeSchema* pSNode)
			{
				mNeedIndexSchemaList.clear();
				findAllSchemaNode_WithListType(pSNode);
				return mNeedIndexSchemaList.size();
			}

			//! 迭代获取需要设置Index信息的SchemaNode
			virtual IUdxNodeSchema* getNeedIndexSettingNode(int idx)
			{
				if (idx>=mNeedIndexSchemaList.size() || idx<0)
					return NULL;
				return mNeedIndexSchemaList[idx];
			}

			//////////////////////////////////////////////////////////////////////////
			//	
			//
			//
			//////////////////////////////////////////////////////////////////////////
			//! 准备保存到文件中
			virtual bool BeginWriteToFile()
			{
				if (mWritePath=="")
				{
					mCurrentErrorInfo = "write file path does not exist";
					return false;
				}

				return true;
			}

			//! 从UDX数据节点中读取内容，并写入到原始数据格式中
			virtual bool WriteFromNode(IUdxNode* pDNode, int formatFlag=-1)
			{
				if(pDNode==NULL)  return false;
				if(pDNode->validateSchema(GetUdxSchema()) == false) return false;

				IUdxKernel* pKernel;
				IUdxKernelRealValue* realKernel;
				IUdxKernelIntValue* intKernel;
				IUdxKernelStringValue* stringKernel;

				GDALDriverH pDriver = NULL;
				pDriver = GDALGetDriverByName("ESRI Shapefile");
				std::string target_format_id = "";
				std::string target_format_ext = "";
				switch (formatFlag)
				{
				case 0: 
					target_format_id = "AmigoCloud";
					target_format_ext = ".asc";
					break;
				case 1:
					target_format_id = "BNA";
					break;
				case 2: 
					target_format_id = "DXF";
					break;
				case 3: 
					target_format_id = "Carto";
					break;
				case 4: 
					target_format_id = "Cloudant";
					break;
				case 5: 
					target_format_id = "CouchDB";
					break;
				case 6: 
					target_format_id = "CSV";
					break;
				case 7: 
					target_format_id = "DB2ODBC";
					break;
				case 8: 
					target_format_id = "ElasticSearch";
					break;
				case 9: 
					target_format_id = "FileGDB";
					break;
				case 10: 
					target_format_id = "GeoJSON";
					break;
				case 11:
					target_format_id = "Geoconcept";
					break;
				case 12: 
					target_format_id = "GPKG";
					break;
				case 13: 
					target_format_id = "GeoRSS";
					break;
				case 14: 
					target_format_id = "GFT";
					break;
				case 15: 
					target_format_id = "GML";
					break;
				case 16: 
					target_format_id = "GMLAS";
					break;
				case 17: 
					target_format_id = "GMT";
					break;
				case 18: 
					target_format_id = "GPSBabel";
					break;
				case 19: 
					target_format_id = "GPX";
					break;
				case 20: 
					target_format_id = "GPSTrackMaker"; 
					break;
				case 21: 
					target_format_id = "IDB";
					break;
				case 22: 
					target_format_id = "Interlis 1";
					break;
				case 23: 
					target_format_id = "Interlis 2";
					break;
				case 24: 
					target_format_id = "INGRES";
					break;
				case 25: 
					target_format_id = "OpenJUMP";
					break;
				case 26: 
					target_format_id = "KML";
					break;
				case 27: 
					target_format_id = "LIBKML";
					break;
				case 28: 
					target_format_id = "MapInfo File";
					break;
				case 29: 
					target_format_id = "DGN";
					break;
				case 30: 
					target_format_id = "DGNv8";
					break;
				case 31: 
					target_format_id = "Memory"; 
					break;
				case 32: 
					target_format_id = "MongoDB";
					break;
				case 33: 
					target_format_id = "netCDF";
					break;
				case 34: 
					target_format_id = "OCI";
					break;
				case 35: 
					target_format_id = "MSSQLSpatial";
					break;
				case 36: 
					target_format_id = "ODS";
					break;
				case 37: 
					target_format_id = "PCIDSK";
					break;
				case 38: 
					target_format_id = "PDF";
					break;
				case 39: 
					target_format_id = "PGDump";
					break;
				case 40: 
					target_format_id = "PostgreSQL";
					break;
				case 41:
					target_format_id = "Selafin";
					break;
				case 42: 
					target_format_id = "SQLite";
					break;
				case 43: 
					target_format_id = "WFS";
					break;
				case 44: 
					target_format_id = "XLSX";
					break;
				case 45: 
					target_format_id = "VDV";
					break;
				case 46: 
					target_format_id = "WAsP";
					break;
				}

				GDALDatasetH	pDataset;
				std::string write_path = mWritePath;				
				write_path = write_path.substr(0, write_path.find_last_of('.'));
				if (target_format_id != "")
				{
					write_path += ".shp";
					pDataset = GDALCreate(pDriver, write_path.c_str(), 0, 0, 0, GDT_Unknown, NULL);
				}
				else
				{
					write_path = mWritePath;
					pDataset = GDALCreate(pDriver, mWritePath.c_str(), 0, 0, 0, GDT_Unknown, NULL);
				}
				if (pDataset==NULL) return false;

				pKernel = pDNode->getChildNode(0)->getKernel();
				intKernel = (IUdxKernelIntValue*)pKernel;
				int layer_count = intKernel->getTypedValue();

				for (int iLayer=0; iLayer<layer_count; iLayer++)
				{
					NGIS::Data::IUdxNode* temp_node = pDNode->getChildNode(1)->getChildNode(iLayer);
					if (temp_node==NULL) break;

					NGIS::Data::IUdxNode* name_node = temp_node->getChildNode(0);
					NGIS::Data::IUdxNode* shptype_node = temp_node->getChildNode(1);
					NGIS::Data::IUdxNode* coordtype_node = temp_node->getChildNode(2);
					NGIS::Data::IUdxNode* features_node = temp_node->getChildNode(3);
					NGIS::Data::IUdxNode* attribute_node = temp_node->getChildNode(4);
					NGIS::Data::IUdxNode* spatialref_node = temp_node->getChildNode(5);

					stringKernel=(IUdxKernelStringValue*)name_node->getKernel();
					std::string name_str = stringKernel->getTypedValue();

					stringKernel=(IUdxKernelStringValue*)shptype_node->getKernel();
					std::string shape_type_str = stringKernel->getTypedValue();

					stringKernel=(IUdxKernelStringValue*)coordtype_node->getKernel();
					std::string coord_type_name_str = stringKernel->getTypedValue();

					int feature_count = features_node->getChildNodeCount();
					int field_count = attribute_node->getChildNodeCount();

					stringKernel=(IUdxKernelStringValue*)spatialref_node->getKernel();
					std::string spatial_ref_str = stringKernel->getTypedValue();

					//////////////////////////////////////////////////////////////////////////
					OGRSpatialReferenceH	pSRS	= OSRNewSpatialReference("");
					OSRImportFromProj4(pSRS, spatial_ref_str.c_str());
					OGRLayerH temp_layer = GDALDatasetCreateLayer(pDataset, name_str.c_str(), pSRS, Str2ShapeType(shape_type_str, ""), NULL);

					if( !temp_layer )
						break;

					for (int iField = 0; iField < field_count; iField++)
					{
						std::string tempName = attribute_node->getChildNode(iField)->getName();

						OGRFieldDefnH oField = NULL;
						if (attribute_node->getChildNode(iField)->getKernel()->getType() == NGIS::Data::EKernelType::EKT_INT_LIST)
						{
							oField = OGR_Fld_Create(tempName.c_str(), OGRFieldType::OFTInteger);
						}
						else if (attribute_node->getChildNode(iField)->getKernel()->getType() == NGIS::Data::EKernelType::EKT_REAL_LIST)
						{
							oField = OGR_Fld_Create(tempName.c_str(), OGRFieldType::OFTReal);
						}
						else
						{
							oField = OGR_Fld_Create(tempName.c_str(), OGRFieldType::OFTString);
						}
						OGR_L_CreateField(temp_layer, oField, TRUE);
					}

					for(int iShape=0; iShape<feature_count; iShape++)
					{
						stringKernel=(IUdxKernelStringValue*)features_node->getChildNode(iShape)->getKernel();
						std::string wkt_str = stringKernel->getTypedValue();
						OGRFeatureH pFeature	= OGR_F_Create(OGR_L_GetLayerDefn(temp_layer));

						char* copy_wkt_str = new char[wkt_str.size() + 1];
						strcpy(copy_wkt_str, wkt_str.c_str());

						OGRGeometryH pGeometry = NULL; //OGR_G_CreateGeometry(Str2ShapeType(shape_type_str, ""));
						OGR_G_CreateFromWkt(&copy_wkt_str, pSRS, &pGeometry);
						//OGR_G_ImportFromWkt(pGeometry, &copy_wkt_str);
						OGR_F_SetGeometry(pFeature, pGeometry);
						OGR_G_DestroyGeometry(pGeometry);

						for(int iField=0; iField<field_count; iField++)
						{
							if (attribute_node->getChildNode(iField)->getKernel()->getType() == NGIS::Data::EKernelType::EKT_INT_LIST)
							{
								NGIS::Data::IUdxKernelIntArray* temp_kernel = (NGIS::Data::IUdxKernelIntArray*)(attribute_node->getChildNode(iField)->getKernel());
								int tempValue = 0;
								temp_kernel->getTypedValueByIndex(iShape,tempValue);
								
								OGR_F_SetFieldInteger(pFeature, iField, tempValue);
							}
							else if (attribute_node->getChildNode(iField)->getKernel()->getType() == NGIS::Data::EKernelType::EKT_REAL_LIST)
							{
								NGIS::Data::IUdxKernelRealArray* temp_kernel = (NGIS::Data::IUdxKernelRealArray*)(attribute_node->getChildNode(iField)->getKernel());
								double tempValue = 0.0;
								temp_kernel->getTypedValueByIndex(iShape, tempValue);

								OGR_F_SetFieldDouble(pFeature, iField, tempValue);
							}
							else
							{
								NGIS::Data::IUdxKernelStringArray* temp_kernel = (NGIS::Data::IUdxKernelStringArray*)(attribute_node->getChildNode(iField)->getKernel());
								std::string tempValue = "";
								temp_kernel->getTypedValueByIndex(iShape, tempValue);

								OGR_F_SetFieldString(pFeature, iField, tempValue.c_str());
							}
						}
						OGR_L_CreateFeature(temp_layer, pFeature);
						OGR_F_Destroy(pFeature);
						//delete [] copy_wkt_str;
					}
				}
				GDALClose(pDataset);

				if (target_format_id != "")
				{
					char buffer[MAXPATH];					
					GetModuleFileName(NULL, buffer, MAXPATH);
					std::string exe_full_name(buffer);
					std::string exe_dir = exe_full_name.substr(0, exe_full_name.find_last_of('\\'));
					std::string gdal_translat_path ="";
					gdal_translat_path = exe_dir + "\\ogr2ogr.exe";
					std::string cmd = gdal_translat_path + " -f " + target_format_id + " " + mWritePath + " " + write_path;
					startANewProcess(cmd.c_str(), exe_dir.c_str());
				}

				return true;
			}

			//! 结束保存到文件中
			virtual bool EndWriteToFile()
			{
				if (mWritePath=="")
				{
					mCurrentErrorInfo = "write file path does not exist";
					return false;
				}

				return true;
			}

			//! 获取当前读取或者写入的错误信息
			virtual const char* GetCurrentErrorInfo()
			{
				return mCurrentErrorInfo.c_str();
			}

			bool startANewProcess(const char* cmd_str, const char* workingPath_str)
			{
#ifdef _WINDOWS
				std::string cmd = cmd_str;
				std::string workingPath = workingPath_str;

				char sSysDir[MAX_PATH] = {0};  
				GetSystemDirectory(sSysDir, MAX_PATH);  
				std::string strFullPath = sSysDir;  
				strFullPath += ":\\cmd.exe";  
				std::string strCmdLine = " /C "; 
				strCmdLine += cmd;

				STARTUPINFO StartInfo; 
				PROCESS_INFORMATION pinfo; 

				memset(&StartInfo,0,sizeof(STARTUPINFO)); //对程序的启动信息不作任何设定，全部清0 
				StartInfo.cb = sizeof(STARTUPINFO);//设定结构的大小 
				StartInfo.dwFlags = STARTF_USESHOWWINDOW;  
				StartInfo.wShowWindow = SW_SHOW;

				BOOL ret=CreateProcess(NULL, (LPSTR)cmd.c_str(), NULL,NULL,false,
					NORMAL_PRIORITY_CLASS,NULL, 
					(LPSTR)workingPath.c_str(),
					&StartInfo,&pinfo);

				DWORD dwExitCode;  
				if(ret)  
				{
					CloseHandle(pinfo.hThread);  
					WaitForSingleObject(pinfo.hProcess,INFINITE);  
					GetExitCodeProcess(pinfo.hProcess, &dwExitCode);  
					CloseHandle(pinfo.hProcess);  
					return true;
				}
				else
				{
					return false;
				}
#else

				return false;
#endif
			}

		};
	}
}

#endif