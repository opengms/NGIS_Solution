#ifndef __DM_RASTER_GDAL_H__
#define __DM_RASTER_GDAL_H__

#include "..\include\IDataMappingHandle.h"
#include "..\include\NativeDataMappingAPI.h"
#include <stdio.h>
#include <string>
#include <vector>
#include <map>
#include <direct.h>

#define MAXPATH  1024

#include "gdal_priv.h"
#include "cpl_string.h"  
#include "ogr_spatialref.h"  
#include "gdalwarper.h"  
#include "gdal_pam.h"  
#include "cpl_conv.h"  
#include "gdal.h"  
#include "gdal_vrt.h"

#include <Windows.h>
#include <io.h>
#include <direct.h>

namespace NGIS
{
	namespace DataMapping
	{
		class DM_Raster_GADL : public IDataMappingHandle
		{
		protected:
			std::string mReadPath;
			std::string mWritePath;
			GDALDataset *mDataset;

			IUdxDatasetSchema* mUdxSchema;
			IUdxDataset* mUdxData;

			std::string mCurrentErrorInfo;
			std::vector<IUdxNodeSchema*> mNeedIndexSchemaList;

			std::vector<IUdxNodeSchema*> m_Capabilities;
		public:
			DM_Raster_GADL()
			{
				mUdxSchema = NULL;
				mUdxData = NULL;
				GDALAllRegister();

				IUdxDatasetSchema* sDataset = GetUdxSchema();
				m_Capabilities.push_back(sDataset->getChildNode(0)->getChildNode(0));
				m_Capabilities.push_back(sDataset->getChildNode(0)->getChildNode(1));
				m_Capabilities.push_back(sDataset->getChildNode(0)->getChildNode(2));
				m_Capabilities.push_back(sDataset->getChildNode(0)->getChildNode(3));
				m_Capabilities.push_back(sDataset->getChildNode(0)->getChildNode(4));
				m_Capabilities.push_back(sDataset->getChildNode(0)->getChildNode(5));
				m_Capabilities.push_back(sDataset->getChildNode(0)->getChildNode(6));
				m_Capabilities.push_back(sDataset->getChildNode(0)->getChildNode(7));
				m_Capabilities.push_back(sDataset->getChildNode(0)->getChildNode(8));

				m_Capabilities.push_back(sDataset->getChildNode(1)->getChildNode(0)->getChildNode(0));
				m_Capabilities.push_back(sDataset->getChildNode(1)->getChildNode(0)->getChildNode(1));
				m_Capabilities.push_back(sDataset->getChildNode(1)->getChildNode(0)->getChildNode(2));
				m_Capabilities.push_back(sDataset->getChildNode(1)->getChildNode(0)->getChildNode(3));
				m_Capabilities.push_back(sDataset->getChildNode(1)->getChildNode(0)->getChildNode(4));

				m_Capabilities.push_back(sDataset->getChildNode(1)->getChildNode(0)->getChildNode(5)->getChildNode(0));

				m_Capabilities.push_back(sDataset->getChildNode(2));
			}

			~DM_Raster_GADL()
			{
				if( mDataset )
				{
					GDALClose(mDataset);
					mDataset	= NULL;
				}
				if (mUdxSchema) mUdxSchema->release();
				if (mUdxData) mUdxData->release();
			}

		private:
			void findAllSchemaNode_WithListType(IUdxNodeSchema* pNode)
			{
				if (pNode->getDescription()->getKernelType() == EDTKT_LIST)
				{
					mNeedIndexSchemaList.push_back(pNode);
				}
				if (pNode->getParentNode() != NULL)
				{
					findAllSchemaNode_WithListType(pNode->getParentNode());
				}
			}

		public:
			//! 初始化数据映射Handle
			virtual bool InitHandle(const char* readFilePath, const char* writeFilePath)
			{
				mReadPath = readFilePath;
				mWritePath = writeFilePath;

				char buffer[MAXPATH];	
				GetModuleFileName(NULL, buffer, MAXPATH);
				std::string exe_full_name(buffer);
				std::string exe_dir = exe_full_name.substr(0, exe_full_name.find_last_of('\\'));
				std::string working_dir = exe_dir + "\\gdal-data";

				CPLSetConfigOption("GDAL_FILENAME_IS_UTF8","YES");
				CPLSetConfigOption("GDAL_DATA", working_dir.c_str() );
				
				if (mReadPath!="")
					mDataset = (GDALDataset*)GDALOpen(mReadPath.c_str(), GA_ReadOnly);

				if (false)
				{
					FILE* fp = fopen("D:\\gdal_raster_driver.txt", "w+");
					int driver_count = GDALGetDriverCount();
					for (int iDriver=0; iDriver<driver_count; iDriver++)
					{
						GDALDriverH temp_driver = GDALGetDriver(iDriver);
						const char* name	= GDALGetMetadataItem(temp_driver, GDAL_DMD_LONGNAME, "");
						const char* description	= GDALGetDescription(temp_driver);
						const char* extension = GDALGetMetadataItem(temp_driver, GDAL_DMD_EXTENSION, "");
						const char* write_capability = GDALGetMetadataItem(temp_driver, GDAL_DCAP_CREATE, "");
						const char* read_capability = GDALGetMetadataItem(temp_driver, GDAL_DCAP_OPEN, "");

						fprintf(fp, "driver name: %s\t", name);
						fprintf(fp, "description: %s\t", description);
						fprintf(fp, "extension: %s\t", extension);
						fprintf(fp, "write_capability: %s\t", write_capability);
						fprintf(fp, "read_capability: %s\n", read_capability);
					}
					fclose(fp);
				}

				if (mDataset==NULL) return false;

				return true;
			}

			//! 获取Handle的名称
			virtual const char* GetHandleName()
			{
				return "General GDAL Raster Handle";
			}

			//! 获取Handle的版本
			virtual const char* GetHandleVersion()
			{
				return GDALVersionInfo("RELEASE_NAME");
			}

			//! 获取当前数据映射Handle的关联Schema
			virtual IUdxDatasetSchema* GetUdxSchema()
			{
				if (mUdxSchema==NULL)
				{
					mUdxSchema = createUdxDatasetSchema("UdxDescription");
					IUdxNodeSchema* headerNode = mUdxSchema->addChildNode("Header", ESchemaNodeType::EDTKT_NODE);
					IUdxNodeSchema* bandsNode = mUdxSchema->addChildNode("Bands", ESchemaNodeType::EDTKT_LIST);

					headerNode->addChildNode("UpperLeftX", ESchemaNodeType::EDTKT_REAL);
					headerNode->addChildNode("PixelWidth", ESchemaNodeType::EDTKT_REAL);
					headerNode->addChildNode("TransParam1", ESchemaNodeType::EDTKT_REAL);
					headerNode->addChildNode("UpperLeftY", ESchemaNodeType::EDTKT_REAL);
					headerNode->addChildNode("TransParam2", ESchemaNodeType::EDTKT_REAL);
					headerNode->addChildNode("PixelHeight", ESchemaNodeType::EDTKT_REAL);
					headerNode->addChildNode("Width", ESchemaNodeType::EDTKT_INT);
					headerNode->addChildNode("Height", ESchemaNodeType::EDTKT_INT);
					headerNode->addChildNode("BandCount", ESchemaNodeType::EDTKT_INT);

					IUdxNodeSchema* band_node = bandsNode->addChildNode("Band_Item", ESchemaNodeType::EDTKT_NODE);
					band_node->addChildNode("NoDataValue", ESchemaNodeType::EDTKT_REAL);
					band_node->addChildNode("Scale", ESchemaNodeType::EDTKT_REAL);
					band_node->addChildNode("Offset", ESchemaNodeType::EDTKT_REAL);
					band_node->addChildNode("UnitType", ESchemaNodeType::EDTKT_STRING);
					band_node->addChildNode("DataType", ESchemaNodeType::EDTKT_INT);

					IUdxNodeSchema* value_node = band_node->addChildNode("Value", ESchemaNodeType::EDTKT_LIST);
					value_node->addChildNode("Row_Item", ESchemaNodeType::EDTKT_REAL | ESchemaNodeType::EDTKT_LIST);

					IUdxNodeSchema* projection_node = mUdxSchema->addChildNode("Projection", ESchemaNodeType::EDTKT_STRING);
				}
				return mUdxSchema;
			}

			//! 获取此Handle能够操作的SchemaNode个数
			virtual int GetCapbilityCount()
			{
				return m_Capabilities.size();
			}

			//! 迭代获取此Handle能够操作的SchemaNode
			virtual IUdxNodeSchema* getCapbilityNode(int idx) 
			{
				int count = GetCapbilityCount();
				if (idx<0 || idx>=count)
					return NULL;
				return m_Capabilities[idx];
			}

			//! 从原始数据中按照传递过来的SchemaNode读取数据内容，
			//! 并且往传递过来的DataNode中写入相应的数据内容
			virtual bool ReadToNode(IUdxNodeSchema* pSNode, IUdxNode* pDNode)
			{
				if(mDataset == NULL || pSNode==NULL || pDNode==NULL)  
				{
					return false;
				}
				EKernelType pDType = pDNode->getKernel()->getType();
				int len = 0;
				EKernelType pDType1 = getKernelTypeBySchema(pSNode->getDescription()->getKernelType());
				if (pDType1 != pDType)
				{
					return false;
				}

				bool ret=false;
				std::string name = pSNode->getName();
				//printf("%s\n",name.c_str());
				if (name == "UpperLeftX")
				{
					double geoInfo[6];
					mDataset->GetGeoTransform(geoInfo);
					NGIS::Data::IUdxKernelRealValue* realKernel = (NGIS::Data::IUdxKernelRealValue*)pDNode->getKernel();
					realKernel->setTypedValue(geoInfo[0]);
					ret = true;
				}
				else if (name == "PixelWidth")
				{
					double geoInfo[6];
					mDataset->GetGeoTransform(geoInfo);
					NGIS::Data::IUdxKernelRealValue* realKernel = (NGIS::Data::IUdxKernelRealValue*)pDNode->getKernel();
					realKernel->setTypedValue(geoInfo[1]);
					ret = true;
				}
				else if (name == "TransParam1")
				{
					double geoInfo[6];
					mDataset->GetGeoTransform(geoInfo);
					NGIS::Data::IUdxKernelRealValue* realKernel = (NGIS::Data::IUdxKernelRealValue*)pDNode->getKernel();
					realKernel->setTypedValue(geoInfo[2]);
					ret = true;
				}
				else if (name == "UpperLeftY")
				{
					double geoInfo[6];
					mDataset->GetGeoTransform(geoInfo);
					NGIS::Data::IUdxKernelRealValue* realKernel = (NGIS::Data::IUdxKernelRealValue*)pDNode->getKernel();
					realKernel->setTypedValue(geoInfo[3]);
					ret = true;
				}
				else if (name == "TransParam2")
				{
					double geoInfo[6];
					mDataset->GetGeoTransform(geoInfo);
					NGIS::Data::IUdxKernelRealValue* realKernel = (NGIS::Data::IUdxKernelRealValue*)pDNode->getKernel();
					realKernel->setTypedValue(geoInfo[4]);
					ret = true;
				}
				else if (name == "PixelHeight")
				{
					double geoInfo[6];
					mDataset->GetGeoTransform(geoInfo);
					NGIS::Data::IUdxKernelRealValue* realKernel = (NGIS::Data::IUdxKernelRealValue*)pDNode->getKernel();
					realKernel->setTypedValue(geoInfo[5]);
					ret = true;
				}
				else if (name == "Width")
				{
					int xSize = mDataset->GetRasterXSize();
					NGIS::Data::IUdxKernelIntValue* realKernel = (NGIS::Data::IUdxKernelIntValue*)pDNode->getKernel();
					realKernel->setTypedValue(xSize);
					ret = true;
				}
				else if (name == "Height")
				{
					int ySize = mDataset->GetRasterYSize();
					NGIS::Data::IUdxKernelIntValue* realKernel = (NGIS::Data::IUdxKernelIntValue*)pDNode->getKernel();
					realKernel->setTypedValue(ySize);
					ret = true;
				}
				else if (name == "BandCount")
				{
					int bandCount = mDataset->GetRasterCount();
					NGIS::Data::IUdxKernelIntValue* realKernel = (NGIS::Data::IUdxKernelIntValue*)pDNode->getKernel();
					realKernel->setTypedValue(bandCount);
					ret = true;
				}
				else if (name == "NoDataValue")
				{
					int band_idx = pSNode->getParentNode()->getParentNode()->getExtension();
					double nodataVal = mDataset->GetRasterBand(band_idx+1)->GetNoDataValue();
					NGIS::Data::IUdxKernelRealValue* realKernel = (NGIS::Data::IUdxKernelRealValue*)pDNode->getKernel();
					realKernel->setTypedValue(nodataVal);
					ret = true;
				}
				else if (name == "Scale")
				{
					int band_idx = pSNode->getParentNode()->getParentNode()->getExtension();
					double scaleVal = mDataset->GetRasterBand(band_idx+1)->GetScale();
					NGIS::Data::IUdxKernelRealValue* realKernel = (NGIS::Data::IUdxKernelRealValue*)pDNode->getKernel();
					realKernel->setTypedValue(scaleVal);
					ret = true;
				}
				else if (name == "Offset")
				{
					int band_idx = pSNode->getParentNode()->getParentNode()->getExtension();
					double offsetVal = mDataset->GetRasterBand(band_idx+1)->GetOffset();
					NGIS::Data::IUdxKernelRealValue* realKernel = (NGIS::Data::IUdxKernelRealValue*)pDNode->getKernel();
					realKernel->setTypedValue(offsetVal);
					ret = true;
				}
				else if (name == "UnitType")
				{
					int band_idx = pSNode->getParentNode()->getParentNode()->getExtension();
					const char* unitVal = mDataset->GetRasterBand(band_idx+1)->GetUnitType();
					NGIS::Data::IUdxKernelStringValue* realKernel = (NGIS::Data::IUdxKernelStringValue*)pDNode->getKernel();
					realKernel->setTypedValue(unitVal);
					ret = true;
				}
				else if (name == "DataType")
				{
					int band_idx = pSNode->getParentNode()->getParentNode()->getExtension();
					GDALDataType eDT = mDataset->GetRasterBand(band_idx+1)->GetRasterDataType();
					NGIS::Data::IUdxKernelIntValue* realKernel = (NGIS::Data::IUdxKernelIntValue*)pDNode->getKernel();
					realKernel->setTypedValue((int)eDT);
					ret = true;
				}
				else if (name == "Row_Item")
				{
					int band_idx = pSNode->getParentNode()->getParentNode()->getParentNode()->getExtension();
					int row_idx = pSNode->getParentNode()->getExtension();

				 	GDALRasterBand* band = mDataset->GetRasterBand(band_idx+1);
					GDALDataType eDT = band->GetRasterDataType();
					int xSize = band->GetXSize();
					int ySize = band->GetYSize();
					double* pData = new double[xSize];

					/*if (eDT == GDT_Byte) { pData = new unsigned char(xSize); }
					else if (eDT == GDT_UInt16) { pData = new unsigned short(xSize); }
					else if (eDT == GDT_Int16) { pData = new short(xSize); }
					else if (eDT == GDT_UInt32) { pData = new unsigned long(xSize); }
					else if (eDT == GDT_Int32) { pData = new long(xSize); }
					else if (eDT == GDT_Float32 ) { pData = new float(xSize); }
					else if (eDT == GDT_Float64  ) { pData = new double(xSize); }*/

					CPLErr err = band->RasterIO(GF_Read, 0, row_idx, xSize, 1, pData, xSize, 1, GDT_Float64, 0, 0);

					NGIS::Data::IUdxKernelRealArray* realKernel = (NGIS::Data::IUdxKernelRealArray*)pDNode->getKernel();
					for (int i=0; i<xSize; i++)
					{
						realKernel->addTypedValue(pData[i]);
					}
					delete []pData;
					ret = true;
				}
				else if (name == "Projection")
				{
					char* srs = (char*) mDataset->GetProjectionRef();
					std::string wkt_str(srs);
					std::string datum_name = "";
					if (wkt_str!="")
					{
						int idx1 = wkt_str.find("GEOGCS");
						int idx2 = wkt_str.find("DATUM");
						std::string sub_str = wkt_str.substr(idx1, idx2-idx1);
						idx1 = sub_str.find_first_of('"');
						idx2 = sub_str.find_last_of('"');
						datum_name  = sub_str.substr(idx1+1, idx2-idx1-1);
					}

					OGRSpatialReference ogr_srs;
					OGRErr err = ogr_srs.importFromWkt(&srs);
					
					ogr_srs.exportToProj4(&srs);
					std::string proj_str(srs);
					int idx = proj_str.find("+datum");
					if (idx<0)
					{
						proj_str += " +datum=" + datum_name;
					}
					
					NGIS::Data::IUdxKernelStringValue* realKernel = (NGIS::Data::IUdxKernelStringValue*)pDNode->getKernel();
					realKernel->setTypedValue(proj_str.c_str());
					ret = true;
				}
				return ret;
			}

			//! 根据当前选中的SchemaNode，获取需要设置Index信息的SchemaNode的数量
			virtual int prepareNeedIndexSettingNode(IUdxNodeSchema* pSNode)
			{
				mNeedIndexSchemaList.clear();
				findAllSchemaNode_WithListType(pSNode);
				return mNeedIndexSchemaList.size();
			}

			//! 迭代获取需要设置Index信息的SchemaNode
			virtual IUdxNodeSchema* getNeedIndexSettingNode(int idx)
			{
				if (idx>=mNeedIndexSchemaList.size() || idx<0)
					return NULL;
				return mNeedIndexSchemaList[idx];
			}

			//! 准备保存到文件中
			virtual bool BeginWriteToFile()
			{
				if (mWritePath=="")
				{
					mCurrentErrorInfo = "write file path does not exist";
					return false;
				}

				return true;
			}

			//! 从UDX数据节点中读取内容，并写入到原始数据格式中
			virtual bool WriteFromNode(IUdxNode* pDNode, int formatFlag=-1)
			{
				if(pDNode==NULL)  return false;
				if(pDNode->validateSchema(GetUdxSchema()) == false) return false;

				IUdxKernel* pKernel;
				IUdxKernelRealValue* realKernel;
				IUdxKernelIntValue* intKernel;
				IUdxKernelStringValue* stringKernel;

				//////////////////////////////////////////////////////////////////////////
				pKernel = pDNode->getChildNode(0)->getChildNode(0)->getKernel();
				realKernel = (IUdxKernelRealValue*)pKernel;
				double upperLeftX = realKernel->getTypedValue();

				pKernel = pDNode->getChildNode(0)->getChildNode(1)->getKernel();
				realKernel = (IUdxKernelRealValue*)pKernel;
				double pixelWidth = realKernel->getTypedValue();

				pKernel = pDNode->getChildNode(0)->getChildNode(2)->getKernel();
				realKernel = (IUdxKernelRealValue*)pKernel;
				double transParam1 = realKernel->getTypedValue();

				pKernel = pDNode->getChildNode(0)->getChildNode(3)->getKernel();
				realKernel = (IUdxKernelRealValue*)pKernel;
				double upperLeftY = realKernel->getTypedValue();

				pKernel = pDNode->getChildNode(0)->getChildNode(4)->getKernel();
				realKernel = (IUdxKernelRealValue*)pKernel;
				double transParam2 = realKernel->getTypedValue();

				pKernel = pDNode->getChildNode(0)->getChildNode(5)->getKernel();
				realKernel = (IUdxKernelRealValue*)pKernel;
				double pixelHeight = realKernel->getTypedValue();

				pKernel = pDNode->getChildNode(0)->getChildNode(6)->getKernel();
				intKernel = (IUdxKernelIntValue*)pKernel;
				int width = intKernel->getTypedValue();

				pKernel = pDNode->getChildNode(0)->getChildNode(7)->getKernel();
				intKernel = (IUdxKernelIntValue*)pKernel;
				int height = intKernel->getTypedValue();

				pKernel = pDNode->getChildNode(0)->getChildNode(8)->getKernel();
				intKernel = (IUdxKernelIntValue*)pKernel;
				int count = intKernel->getTypedValue();

				//////////////////////////////////////////////////////////////////////////
				IUdxNode* bandsNode =  pDNode->getChildNode(1);
				int bandCount = bandsNode->getChildNodeCount();

				IUdxNode* projNode = pDNode->getChildNode(2);
				stringKernel = (IUdxKernelStringValue*)projNode->getKernel();
				std::string srs = stringKernel->getTypedValue();

				intKernel = (IUdxKernelIntValue*)bandsNode->getChildNode(0)->getChildNode(4)->getKernel();
				int dataType = intKernel->getTypedValue();
				GDALDataType eType = (GDALDataType)dataType;

				GDALDriver* poDriver = NULL;
				poDriver = GetGDALDriverManager()->GetDriverByName("GTiff"); 
				std::string target_format_id = "";
				std::string target_format_ext = "";
				switch (formatFlag)
				{
				case 0: 
					target_format_id = "AAIGrid";
					target_format_ext = ".asc";
					break;
				case 1: 
					target_format_id = "ADRG";
					target_format_ext = ".gen";
					break;
				case 2: 
					target_format_id = "ARG";
					target_format_ext = ".json";
					break;
				case 3: 
					target_format_id = "BLX";
					target_format_ext = ".blx";
					break;
				case 4: 
					target_format_id = "BMP";
					target_format_ext = ".bmp";
					break;
				case 5: 
					target_format_id = "BT";
					target_format_ext = ".bt";
					break;
				case 6: 
					target_format_id = "DB2";
					target_format_ext = ".db2";
					break;
				case 7:
					target_format_id = "DDS";
					target_format_ext = ".dds";
					break;
				case 8:
					target_format_id = "DTED";
					target_format_ext = ".dt0";
					break;
				case 9: 
					target_format_id = "ECW";
					target_format_ext = ".ecw";
					break;
				case 10: 
					target_format_id = "EHdr";
					target_format_ext = ".hdr";
					break;
				case 11: 
					target_format_id = "ELAS";
					target_format_ext = ".hdr";
					break;
				case 12: 
					target_format_id = "ENVI";
					target_format_ext = ".hdr";
					break;
				case 13: 
					target_format_id = "EPSILON";
					target_format_ext = ".img";
					break;
				case 14: 
					target_format_id = "ERS";
					target_format_ext = ".ers";
					break;
				case 15: 
					target_format_id = "FIT";
					target_format_ext = ".fit";
					break;
				case 16:
					target_format_id = "FITS";
					target_format_ext = ".fits";
					break;
				case 17:
					target_format_id = "GPKG";
					target_format_ext = ".gpkg";
					break;
				case 18: 
					target_format_id = "GIF";
					target_format_ext = ".gif";
					break;
				case 19: 
					target_format_id = "GSAG";
					target_format_ext = ".gsag";
					break;
				case 20: 
					target_format_id = "GSBG";
					target_format_ext = ".gsbg";
					break;
				case 21: 
					target_format_id = "GS7BG";
					target_format_ext = ".gs7bg";
					break;
				case 22: 
					target_format_id = "GSC";
					target_format_ext = ".gsc";
					break;
				case 23: 
					target_format_id = "GTA";
					target_format_ext = ".gta";
					break;
				case 24: 
					target_format_id = "GTX";
					target_format_ext = ".gtx";
					break;
				case 25: 
					target_format_id = "HDF4";
					target_format_ext = ".hdf";
					break;
				case 26:
					target_format_id = "HF2";
					target_format_ext = ".GZ ";
					break;
				case 27: 
					target_format_id = "HFA";
					target_format_ext = ".img";
					break;
				case 28: 
					target_format_id = "IDA";
					target_format_ext = ".ida";
					break;
				case 29:
					target_format_id = "ILWIS";
					target_format_ext = ".mpr";
					break;
				case 30: 
					target_format_id = "INGR";
					target_format_ext = ".rgb";
					break;
				case 31: 
					target_format_id = "ISCE";
					target_format_ext = ".isce";
					break;
				case 32: 
					target_format_id = "ISIS2";
					target_format_ext = ".isis2";
					break;
				case 33: 
					target_format_id = "JPEG";
					target_format_ext = ".jpg";
					break;
				case 34: 
					target_format_id = "JPEGLS";
					target_format_ext = ".jpg";
					break;
				case 35:
					target_format_id = "JPEG2000";
					target_format_ext = ".jp2";
					break;
				case 36: 
					target_format_id = "JP2ECW";
					target_format_ext = ".jp2";
					break;
				case 37: 
					target_format_id = "JP2KAK";
					target_format_ext = ".jp2";
					break;
				case 38:
					target_format_id = "JP2Lura";
					target_format_ext = ".jp2";
					break;
				case 39: 
					target_format_id = "JP2MrSID";
					target_format_ext = ".jp2";
					break;
				case 40: 
					target_format_id = "JP2OpenJPEG";
					target_format_ext = ".jp2";
					break;
				case 41: 
					target_format_id = "KEA";
					target_format_ext = ".kea";
					break;
				case 42: 
					target_format_id = "KMLSUPEROVERLAY";
					target_format_ext = ".kml";
					break;
				case 43: 
					target_format_id = "KRO";
					target_format_ext = ".kro";
					break;
				case 44: 
					target_format_id = "LCP";
					target_format_ext = ".lcp";
					break;
				case 45: 
					target_format_id = "MBTiles";
					target_format_ext = ".mbtiles";
					break;
				case 46: 
					target_format_id = "MEM";
					target_format_ext = ".mem";
					break;
				case 47: 
					target_format_id = "MFF";
					target_format_ext = ".MFF";
					break;
				case 48: 
					target_format_id = "MFF2 (HKV)	";
					target_format_ext = ".MFF";
					break;
				case 49: 
					target_format_id = "MFF2 (HKV)	";
					target_format_ext = ".MFF";
					break;
				case 50: 
					target_format_id = "NITF";
					target_format_ext = ".nitf";
					break;
				case 51: 
					target_format_id = "netCDF";
					target_format_ext = ".nc";
					break;
				case 52: 
					target_format_id = "NTv2";
					target_format_ext = ".ntv2";
					break;
				case 53: 
					target_format_id = "PAux";
					target_format_ext = ".aux";
					break;
				case 54: 
					target_format_id = "PCIDSK";
					target_format_ext = ".pci";
					break;
				case 55: 
					target_format_id = "PCRaster";
					target_format_ext = ".pcraster";
					break;
				case 56: 
					target_format_id = "PDF";
					target_format_ext = ".pdf";
					break;
				case 57: 
					target_format_id = "PNG";
					target_format_ext = ".png";
					break;
				case 58: 
					target_format_id = "PNM";
					target_format_ext = ".ppm";
					break;
				case 59: 
					target_format_id = "R";
					target_format_ext = ".r";
					break;
				case 60: 
					target_format_id = "Rasterlite";
					target_format_ext = ".sqlite";
					break;
				case 61: 
					target_format_id = "RMF";
					target_format_ext = ".rsw";
					break;
				case 62: 
					target_format_id = "ROI_PAC";
					target_format_ext = ".ROI_PAC";
					break;
				case 63: 
					target_format_id = "RST";
					target_format_ext = ".rst";
					break;
				case 64: 
					target_format_id = "SAGA";
					target_format_ext = ".saga";
					break;
				case 65: 
					target_format_id = "SGI";
					target_format_ext = ".sgi";
					break;
				case 66: 
					target_format_id = "SRTMHGT";
					target_format_ext = ".hgt";
					break;
				case 67: 
					target_format_id = "TERRAGEN";
					target_format_ext = ".ter";
					break;
				case 68: 
					target_format_id = "TSX";
					target_format_ext = ".tsx";
					break;
				case 69: 
					target_format_id = "USGSDEM";
					target_format_ext = ".dem";
					break;
				case 70: 
					target_format_id = "VRT";
					target_format_ext = ".vrt";
					break;
				case 71: 
					target_format_id = "WEBP";
					target_format_ext = ".webp";
					break;
				case 72: 
					target_format_id = "XPM";
					target_format_ext = ".xpm";
					break;
				case 73: 
					target_format_id = "XYZ";
					target_format_ext = ".zyz";
					break;
				case 74: 
					target_format_id = "ZMap";
					target_format_ext = ".zmap";
					break;
				}

				if (poDriver==NULL)
				{
					mCurrentErrorInfo = "format does not supported";
					return false;
				}
				
				char **papszOptions = NULL;
				GDALDataset* gdalDataset = NULL;
				std::string write_path = mWritePath;				
				write_path = write_path.substr(0, write_path.find_last_of('.'));
				if (target_format_id != "")
				{
					write_path += ".tif";
					gdalDataset = poDriver->Create(write_path.c_str(), width, height, bandCount, eType, papszOptions);
				}
				else
				{
					write_path = mWritePath;
					gdalDataset = poDriver->Create(mWritePath.c_str(), width, height, bandCount, eType, papszOptions);
				}
				if (srs.find("+proj")>=0)
				{
					OGRSpatialReference ogr_srs;
					ogr_srs.importFromProj4(srs.c_str());
					char * proj_info = NULL;
					ogr_srs.exportToWkt(&proj_info);
					gdalDataset->SetProjection(proj_info);

					int dot_idx = mWritePath.find_last_of('.');
					std::string prj_file_path = mWritePath.substr(0, dot_idx) + ".prj";
					FILE* fp = fopen(prj_file_path.c_str(), "w");
					fprintf(fp, proj_info);
					fclose(fp);
				}
				else
				{
					gdalDataset->SetProjection(srs.c_str());
					int dot_idx = mWritePath.find_last_of('.');
					std::string prj_file_path = mWritePath.substr(0, dot_idx) + ".prj";
					FILE* fp = fopen(prj_file_path.c_str(), "w");
					fprintf(fp, srs.c_str());
					fclose(fp);
				}

				double geoTransform[6] = { upperLeftX, pixelWidth, transParam1, upperLeftY, transParam2, pixelHeight };
				gdalDataset->SetGeoTransform(geoTransform);

				//////////////////////////////////////////////////////////////////////////
				double* val = new double[width*height];
				for (int iBand = 0; iBand<bandCount; iBand++)
				{
					IUdxNode* tempBandNode = bandsNode->getChildNode(iBand);
					realKernel = (IUdxKernelRealValue*)tempBandNode->getChildNode(0)->getKernel();
					double noDataValue = realKernel->getTypedValue();

					realKernel = (IUdxKernelRealValue*)tempBandNode->getChildNode(1)->getKernel();
					double scale = realKernel->getTypedValue();

					realKernel = (IUdxKernelRealValue*)tempBandNode->getChildNode(2)->getKernel();
					double offset = realKernel->getTypedValue();

					stringKernel = (IUdxKernelStringValue*)tempBandNode->getChildNode(3)->getKernel();
					std::string unitType = stringKernel->getTypedValue();

					intKernel = (IUdxKernelIntValue*)tempBandNode->getChildNode(4)->getKernel();
					int dataType = intKernel->getTypedValue();

					GDALRasterBand*  tempGdalBand = gdalDataset->GetRasterBand(iBand+1);
					tempGdalBand->SetNoDataValue(noDataValue);
					tempGdalBand->SetScale(scale);
					tempGdalBand->SetOffset(offset);
					tempGdalBand->SetUnitType(unitType.c_str());
					
					memset(val,0,width*height);

					IUdxNode* valNode = tempBandNode->getChildNode(5);
					for (int iRow=0; iRow<valNode->getChildNodeCount(); iRow++)
					{
						IUdxNode* tempRowNode = valNode->getChildNode(iRow);
						IUdxKernelRealArray* realArrayKernel = (IUdxKernelRealArray*)tempRowNode->getKernel();
						int length = realArrayKernel->getCount();
						for (int iVal=0; iVal<length; iVal++)
						{
							realArrayKernel->getTypedValueByIndex(iVal, val[iRow*width+iVal]);
						}
					}

					tempGdalBand->RasterIO(GF_Write,0,0,width,height,val,width,height,GDT_Float64,0,0);
				}
				delete []val;
				delete gdalDataset;

				if (target_format_id != "")
				{
					char buffer[MAXPATH];					
					GetModuleFileName(NULL, buffer, MAXPATH);
					std::string exe_full_name(buffer);
					std::string exe_dir = exe_full_name.substr(0, exe_full_name.find_last_of('\\'));
					std::string gdal_translat_path ="";
					gdal_translat_path = exe_dir + "\\gdal_translate.exe";
					std::string cmd = gdal_translat_path + " -of " + target_format_id + " " + write_path + " " + mWritePath;
					startANewProcess(cmd.c_str(), exe_dir.c_str());
				}

				return true;
			}

			//! 保存到文件中
			virtual bool EndWriteToFile()
			{
				if (mWritePath=="")
				{
					mCurrentErrorInfo = "write file path does not exist";
					return false;
				}

				return true;
			}

			//! 获取当前读取或者写入的错误信息
			virtual const char* GetCurrentErrorInfo()
			{
				return mCurrentErrorInfo.c_str();
			}


			bool startANewProcess(const char* cmd_str, const char* workingPath_str)
			{
#ifdef _WINDOWS
				std::string cmd = cmd_str;
				std::string workingPath = workingPath_str;

				char sSysDir[MAX_PATH] = {0};  
				GetSystemDirectory(sSysDir, MAX_PATH);  
				std::string strFullPath = sSysDir;  
				strFullPath += ":\\cmd.exe";  
				std::string strCmdLine = " /C "; 
				strCmdLine += cmd;

				STARTUPINFO StartInfo; 
				PROCESS_INFORMATION pinfo; 

				memset(&StartInfo,0,sizeof(STARTUPINFO)); //对程序的启动信息不作任何设定，全部清0 
				StartInfo.cb = sizeof(STARTUPINFO);//设定结构的大小 
				StartInfo.dwFlags = STARTF_USESHOWWINDOW;  
				StartInfo.wShowWindow = SW_SHOW;

				BOOL ret=CreateProcess(NULL, (LPSTR)cmd.c_str(), NULL,NULL,false,
					NORMAL_PRIORITY_CLASS,NULL, 
					(LPSTR)workingPath.c_str(),
					&StartInfo,&pinfo);

				DWORD dwExitCode;  
				if(ret)  
				{
					CloseHandle(pinfo.hThread);  
					WaitForSingleObject(pinfo.hProcess,INFINITE);  
					GetExitCodeProcess(pinfo.hProcess, &dwExitCode);  
					CloseHandle(pinfo.hProcess);  
					return true;
				}
				else
				{
					return false;
				}
#else

				return false;
#endif
			}

		};
	}
}

#endif 