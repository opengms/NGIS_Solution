#ifndef __DM_3DMODEL_ASSIMP_H__
#define __DM_3DMODEL_ASSIMP_H__

#include <stdio.h>
#include <string>
#include <vector>
#include <map>
#include "..\include\IDataMappingHandle.h"
#include "..\include\NativeDataMappingAPI.h"

#include <assimp/postprocess.h>
#include <assimp/version.h>
#include <assimp/scene.h>
#include <assimp/Importer.hpp>
#include <assimp/DefaultLogger.hpp>
#include <assimp/Exporter.hpp>

namespace NGIS
{
	namespace DataMapping
	{
		class DM_3DModel_ASSIMP : public IDataMappingHandle
		{
		protected:
			std::string mReadPath;
			std::string mWritePath;

			IUdxDatasetSchema* mUdxSchema;
			IUdxDataset* mUdxData;

			std::string mCurrentErrorInfo;
			std::vector<IUdxNodeSchema*> mNeedIndexSchemaList;

			std::vector<IUdxNodeSchema*> m_Capabilities;

			Assimp::Importer* globalImporter;
			const aiScene* mScene;
		public:
			DM_3DModel_ASSIMP()
			{
				mUdxSchema = NULL;
				mUdxData = NULL;
				globalImporter = NULL;
				mScene = NULL;

				IUdxDatasetSchema* sDataset = GetUdxSchema();
				m_Capabilities.push_back(sDataset->getChildNode(0));
				m_Capabilities.push_back(sDataset->getChildNode(1));
				m_Capabilities.push_back(sDataset->getChildNode(2));
				m_Capabilities.push_back(sDataset->getChildNode(3));
				m_Capabilities.push_back(sDataset->getChildNode(4));
			}

			~DM_3DModel_ASSIMP()
			{
				if (mUdxSchema) mUdxSchema->release();
				if (mUdxData) mUdxData->release();
				if (globalImporter) delete globalImporter;
			}
		private:
			void findAllSchemaNode_WithListType(IUdxNodeSchema* pNode)
			{
				if (pNode->getDescription()->getKernelType() == EDTKT_LIST)
				{
					mNeedIndexSchemaList.push_back(pNode);
				}
				if (pNode->getParentNode() != NULL)
				{
					findAllSchemaNode_WithListType(pNode->getParentNode());
				}
			}

			// Convert a name to standard XML format
			void ConvertName(aiString& out, const aiString& in)
			{
				out.length = 0;
				for (unsigned int i = 0; i < in.length; ++i)  {
					switch (in.data[i]) {
					case '<':
						out.Append("&lt;");break;
					case '>':
						out.Append("&gt;");break;
					case '&':
						out.Append("&amp;");break;
					case '\"':
						out.Append("&quot;");break;
					case '\'':
						out.Append("&apos;");break;
					default:
						out.data[out.length++] = in.data[i];
					}
				}
				out.data[out.length] = 0;
			}

			const char* TextureTypeToString(aiTextureType in)
			{
				switch (in)
				{
				case aiTextureType_NONE:
					return "n/a";
				case aiTextureType_DIFFUSE:
					return "Diffuse";
				case aiTextureType_SPECULAR:
					return "Specular";
				case aiTextureType_AMBIENT:
					return "Ambient";
				case aiTextureType_EMISSIVE:
					return "Emissive";
				case aiTextureType_OPACITY:
					return "Opacity";
				case aiTextureType_NORMALS:
					return "Normals";
				case aiTextureType_HEIGHT:
					return "Height";
				case aiTextureType_SHININESS:
					return "Shininess";
				case aiTextureType_DISPLACEMENT:
					return "Displacement";
				case aiTextureType_LIGHTMAP:
					return "Lightmap";
				case aiTextureType_REFLECTION:
					return "Reflection";
				case aiTextureType_UNKNOWN:
					return "Unknown";
				default:
					break;
				}
				return  "BUG";    
			}
			
			static std::string encodeXML(const std::string& data) 
			{
				std::string buffer;
				buffer.reserve(data.size());
				for(size_t pos = 0; pos != data.size(); ++pos) {
					switch(data[pos]) {
					case '&':  buffer.append("&amp;");       break;
					case '\"': buffer.append("&quot;");      break;
					case '\'': buffer.append("&apos;");      break;
					case '<':  buffer.append("&lt;");        break;
					case '>':  buffer.append("&gt;");        break;
					default:   buffer.append(&data[pos], 1); break;
					}
				}
				return buffer;
			}

		public:
			//////////////////////////////////////////////////////////////////////////
			//	
			//
			//
			//////////////////////////////////////////////////////////////////////////
			//! 初始化数据映射Handle
			virtual bool InitHandle(const char* readFilePath, const char* writeFilePath)
			{
				mReadPath = readFilePath;
				mWritePath = writeFilePath;

				if (globalImporter==NULL)
					globalImporter = new Assimp::Importer();
				
				std::string filename(readFilePath);
				if (filename!="")
					mScene = globalImporter->ReadFile(readFilePath,aiProcessPreset_TargetRealtime_MaxQuality);

				if (mScene==NULL) return false;

				return true;
			}

			//! 获取Handle的名称
			virtual const char* GetHandleName()
			{
				return "General 3D Model Handle";
			}

			//! 获取Handle的版本
			virtual const char* GetHandleVersion()
			{
				int major = aiGetVersionMajor();
				int minor = aiGetVersionMinor();
				int revision = aiGetVersionRevision();
				char version_str[10];
				sprintf(version_str,"%d.%d.%d", major, minor, revision);
				return version_str;
			}

			//! 获取当前数据映射Handle的关联Schema
			virtual IUdxDatasetSchema* GetUdxSchema()
			{
				if (mUdxSchema==NULL)
				{
					mUdxSchema = createUdxDatasetSchema("UdxDescription");

					IUdxNodeSchema* nodeInfo_node = mUdxSchema->addChildNode("Node", ESchemaNodeType::EDTKT_NODE);
					nodeInfo_node->addChildNode("Name", ESchemaNodeType::EDTKT_STRING);
					nodeInfo_node->addChildNode("ChildCount", ESchemaNodeType::EDTKT_INT);
					nodeInfo_node->addChildNode("Matrix4", ESchemaNodeType::EDTKT_VECTOR4_LIST);
					nodeInfo_node->addChildNode("MeshRefs", ESchemaNodeType::EDTKT_INT_LIST);
					IUdxNodeSchema* childNode_node = nodeInfo_node->addChildNode("ChildNodes", ESchemaNodeType::EDTKT_LIST);
					nodeInfo_node = childNode_node->addChildNode("Node", ESchemaNodeType::EDTKT_NODE);
					nodeInfo_node->addChildNode("Name", ESchemaNodeType::EDTKT_STRING);
					nodeInfo_node->addChildNode("ChildCount", ESchemaNodeType::EDTKT_INT);
					nodeInfo_node->addChildNode("Matrix4", ESchemaNodeType::EDTKT_VECTOR4_LIST);
					nodeInfo_node->addChildNode("MeshRefs", ESchemaNodeType::EDTKT_INT_LIST);
					nodeInfo_node->addChildNode("ChildNodes", ESchemaNodeType::EDTKT_LIST);

					//////////////////////////////////////////////////////////////////////////
					IUdxNodeSchema* textureList_node = mUdxSchema->addChildNode("TextureList", ESchemaNodeType::EDTKT_LIST);
					IUdxNodeSchema* texture_node = textureList_node->addChildNode("Texture", ESchemaNodeType::EDTKT_NODE);
					texture_node->addChildNode("Width", ESchemaNodeType::EDTKT_INT);
					texture_node->addChildNode("Height", ESchemaNodeType::EDTKT_INT);
					texture_node->addChildNode("Compressed", ESchemaNodeType::EDTKT_INT);
					IUdxNodeSchema* texture_data_node = texture_node->addChildNode("Data", ESchemaNodeType::EDTKT_NODE);
					texture_data_node->addChildNode("Length", ESchemaNodeType::EDTKT_INT);
					texture_data_node->addChildNode("Content", ESchemaNodeType::EDTKT_TABLE);

					//////////////////////////////////////////////////////////////////////////
					IUdxNodeSchema* materialList_node = mUdxSchema->addChildNode("MaterialList", ESchemaNodeType::EDTKT_LIST);
					IUdxNodeSchema* material_node = materialList_node->addChildNode("Material", ESchemaNodeType::EDTKT_LIST);
					IUdxNodeSchema* property_node = material_node->addChildNode("MatProperty", ESchemaNodeType::EDTKT_NODE);
					property_node->addChildNode("key", ESchemaNodeType::EDTKT_STRING);
					property_node->addChildNode("type", ESchemaNodeType::EDTKT_STRING);
					property_node->addChildNode("tex_usage", ESchemaNodeType::EDTKT_STRING);
					property_node->addChildNode("tex_index", ESchemaNodeType::EDTKT_INT);
					property_node->addChildNode("mat_param", ESchemaNodeType::EDTKT_STRING);

					//////////////////////////////////////////////////////////////////////////
					IUdxNodeSchema* animationList_node = mUdxSchema->addChildNode("AnimationList", ESchemaNodeType::EDTKT_LIST);
					IUdxNodeSchema* animation_node = animationList_node->addChildNode("Animation", ESchemaNodeType::EDTKT_NODE);
					animation_node->addChildNode("name", ESchemaNodeType::EDTKT_STRING);
					animation_node->addChildNode("duration", ESchemaNodeType::EDTKT_REAL);
					animation_node->addChildNode("tick_cnt", ESchemaNodeType::EDTKT_REAL);
					IUdxNodeSchema* nodeAnimList_node = animation_node->addChildNode("NodeAnimList", ESchemaNodeType::EDTKT_LIST);
					IUdxNodeSchema* nodeAnim_node = nodeAnimList_node->addChildNode("NodeAnim", ESchemaNodeType::EDTKT_NODE);
					
					nodeAnim_node->addChildNode("nodeName", ESchemaNodeType::EDTKT_STRING);
					IUdxNodeSchema* positionKeyList_node = nodeAnim_node->addChildNode("PositionKeyList", ESchemaNodeType::EDTKT_LIST);
					IUdxNodeSchema* positionKey_node = positionKeyList_node->addChildNode("PositionKey", ESchemaNodeType::EDTKT_NODE);
					positionKey_node->addChildNode("time", ESchemaNodeType::EDTKT_REAL);
					positionKey_node->addChildNode("PositionKey", ESchemaNodeType::EDTKT_VECTOR3);

					IUdxNodeSchema* scalingKeyList_node = nodeAnim_node->addChildNode("ScalingKeyList", ESchemaNodeType::EDTKT_LIST);
					IUdxNodeSchema* scalingKey_node = scalingKeyList_node->addChildNode("ScalingKey", ESchemaNodeType::EDTKT_NODE);
					scalingKey_node->addChildNode("time", ESchemaNodeType::EDTKT_REAL);
					scalingKey_node->addChildNode("ScalingKey", ESchemaNodeType::EDTKT_VECTOR3);

					IUdxNodeSchema* rotationKeyList_node = nodeAnim_node->addChildNode("RotationKeyList", ESchemaNodeType::EDTKT_LIST);
					IUdxNodeSchema* rotationKey_node = rotationKeyList_node->addChildNode("ScalingKey", ESchemaNodeType::EDTKT_NODE);
					rotationKey_node->addChildNode("time", ESchemaNodeType::EDTKT_REAL);
					rotationKey_node->addChildNode("RotationKey", ESchemaNodeType::EDTKT_VECTOR3);

					//////////////////////////////////////////////////////////////////////////
					IUdxNodeSchema* meshList_node = mUdxSchema->addChildNode("MeshList", ESchemaNodeType::EDTKT_LIST);
					IUdxNodeSchema* mesh_node = meshList_node->addChildNode("Mesh", ESchemaNodeType::EDTKT_NODE);
					mesh_node->addChildNode("types", ESchemaNodeType::EDTKT_STRING_LIST);
					mesh_node->addChildNode("material_index", ESchemaNodeType::EDTKT_INT);

					IUdxNodeSchema* boneList_node = mesh_node->addChildNode("BoneList", ESchemaNodeType::EDTKT_LIST);
					IUdxNodeSchema* bone_node = boneList_node->addChildNode("Bone", ESchemaNodeType::EDTKT_NODE);
					bone_node->addChildNode("BoneName", ESchemaNodeType::EDTKT_STRING);
					bone_node->addChildNode("Matrix", ESchemaNodeType::EDTKT_VECTOR4_LIST);
					IUdxNodeSchema* weightList_node = bone_node->addChildNode("WeightList", ESchemaNodeType::EDTKT_LIST);
					IUdxNodeSchema* weight_node = weightList_node->addChildNode("Weight", ESchemaNodeType::EDTKT_NODE);
					weight_node->addChildNode("VertexId", ESchemaNodeType::EDTKT_INT);
					weight_node->addChildNode("Weight", ESchemaNodeType::EDTKT_REAL);

					IUdxNodeSchema* faceList_node = mesh_node->addChildNode("FaceList", ESchemaNodeType::EDTKT_LIST);
					faceList_node->addChildNode("Index", ESchemaNodeType::EDTKT_INT_LIST);

					mesh_node->addChildNode("Positions", ESchemaNodeType::EDTKT_VECTOR3_LIST);

					mesh_node->addChildNode("Normals", ESchemaNodeType::EDTKT_VECTOR3_LIST);

					mesh_node->addChildNode("Tangents", ESchemaNodeType::EDTKT_VECTOR3_LIST);

					mesh_node->addChildNode("Bitangents", ESchemaNodeType::EDTKT_VECTOR3_LIST);

					mesh_node->addChildNode("TextureCoords", ESchemaNodeType::EDTKT_VECTOR3_LIST);

					mesh_node->addChildNode("Colors", ESchemaNodeType::EDTKT_VECTOR4_LIST);
				}
				return mUdxSchema;
			}

			//! 获取此Handle能够操作的SchemaNode个数
			virtual int GetCapbilityCount()
			{
				return m_Capabilities.size();
			}

			//! 迭代获取此Handle能够操作的SchemaNode
			virtual IUdxNodeSchema* getCapbilityNode(int idx)
			{
				int count = GetCapbilityCount();
				if (idx<0 || idx>=count)
					return NULL;
				return m_Capabilities[idx];
			}

			//////////////////////////////////////////////////////////////////////////
			//	
			//
			//
			//////////////////////////////////////////////////////////////////////////
			//! 从原始数据中按照传递过来的SchemaNode读取数据内容，
			//! 并且往传递过来的DataNode中写入相应的数据内容
			virtual bool ReadToNode(IUdxNodeSchema* pSNode, IUdxNode* pDNode)
			{
				if(mScene == NULL || pSNode==NULL || pDNode==NULL)  
				{
					return false;
				}

				EKernelType pDType = pDNode->getKernel()->getType();
				int len = 0;
				EKernelType pDType1 = getKernelTypeBySchema(pSNode->getDescription()->getKernelType());
				if (pDType1 != pDType)
				{
					return false;
				}

				bool ret=false;
				std::string name = pSNode->getName();
				printf("%s\n",name.c_str());
				if (name == "Node")
				{
					aiString node_name;
					ConvertName(node_name,mScene->mRootNode->mName);
					IUdxNode* nameNode = pDNode->getChildNode(0);
					IUdxKernelStringValue* nameNodeKernel = (IUdxKernelStringValue*)nameNode->getKernel();
					nameNodeKernel->setTypedValue(node_name.C_Str());

					int child_count = mScene->mRootNode->mNumChildren;
					IUdxNode* childNode_Node = pDNode->getChildNode(1);
					IUdxKernelIntValue* childNodeKernel = (IUdxKernelIntValue*)childNode_Node->getKernel();
					childNodeKernel->setTypedValue(child_count);
										
					const aiMatrix4x4& m = mScene->mRootNode->mTransformation;
					IUdxNode* matrixNode = pDNode->getChildNode(2);
					IUdxKernelVector4dArray* realMatrixKernel = (IUdxKernelVector4dArray*)matrixNode->getKernel();
					realMatrixKernel->addTypedValue(m.a1,m.a2,m.a3,m.a4);
					realMatrixKernel->addTypedValue(m.b1,m.b2,m.b3,m.b4);
					realMatrixKernel->addTypedValue(m.c1,m.c2,m.c3,m.c4);
					realMatrixKernel->addTypedValue(m.d1,m.d2,m.d3,m.d4);

					IUdxNode* meshRefNode = pDNode->getChildNode(3);
					IUdxKernelIntArray* realMeshRefKernel = (IUdxKernelIntArray*)meshRefNode->getKernel();
					if (mScene->mRootNode->mNumMeshes > 0)
					{
						for (unsigned int i = 0; i < mScene->mRootNode->mNumMeshes;++i)
						{
							realMeshRefKernel->addTypedValue(mScene->mRootNode->mMeshes[i]);
						}
					}

					IUdxNode* nodeListNode = pDNode->getChildNode(4);
					if (mScene->mRootNode->mNumChildren > 0)
					{
						for (unsigned int i = 0; i < mScene->mRootNode->mNumChildren;++i)
						{
							aiNode* temp_child_node = mScene->mRootNode->mChildren[i];
							IUdxNode* nodeInfo_node = NULL;
							nodeInfo_node = nodeListNode->getChildNode(i);
							if (nodeInfo_node == NULL)
							{
								nodeInfo_node = nodeListNode->addChildNode("Node", EKernelType::EKT_NODE);
								nodeInfo_node->addChildNode("Name", EKernelType::EKT_STRING);
								nodeInfo_node->addChildNode("ChildCount", EKernelType::EKT_INT);
								nodeInfo_node->addChildNode("Matrix4", EKernelType::EKT_VECTOR4_LIST);
								nodeInfo_node->addChildNode("MeshRefs", EKernelType::EKT_INT_LIST);
								nodeInfo_node->addChildNode("ChildNodes", EKernelType::EKT_LIST);
							}

							aiString node_name;
							ConvertName(node_name,temp_child_node->mName);
							IUdxNode* nameNode = nodeInfo_node->getChildNode(0);
							IUdxKernelStringValue* nameNodeKernel = (IUdxKernelStringValue*)nameNode->getKernel();
							nameNodeKernel->setTypedValue(node_name.C_Str());

							int child_count = temp_child_node->mNumChildren;
							IUdxNode* childNode_Node = nodeInfo_node->getChildNode(1);
							IUdxKernelIntValue* childNodeKernel = (IUdxKernelIntValue*)nameNode->getKernel();
							childNodeKernel->setTypedValue(child_count);

							const aiMatrix4x4& m = temp_child_node->mTransformation;
							IUdxNode* matrixNode = nodeInfo_node->getChildNode(2);
							IUdxKernelVector4dArray* realMatrixKernel = (IUdxKernelVector4dArray*)matrixNode->getKernel();
							realMatrixKernel->addTypedValue(m.a1,m.a2,m.a3,m.a4);
							realMatrixKernel->addTypedValue(m.b1,m.b2,m.b3,m.b4);
							realMatrixKernel->addTypedValue(m.c1,m.c2,m.c3,m.c4);
							realMatrixKernel->addTypedValue(m.d1,m.d2,m.d3,m.d4);

							IUdxNode* meshRefNode = nodeInfo_node->getChildNode(3);
							IUdxKernelIntArray* realMeshRefKernel = (IUdxKernelIntArray*)meshRefNode->getKernel();
							if (temp_child_node->mNumMeshes > 0)
							{
								for (unsigned int i = 0; i < temp_child_node->mNumMeshes;++i)
								{
									realMeshRefKernel->addTypedValue(temp_child_node->mMeshes[i]);
								}
							}
						}// End for mScene->mRootNode->mNumChildren
					}
					else
					{
						pDNode->removeChildNode(4);
					}
				}
				else if (name == "TextureList")
				{
					if (mScene->mNumTextures > 0) 
					{
						int texture_count = mScene->mNumTextures;
						for (int iTexture; iTexture<texture_count; iTexture++)
						{
							aiTexture* temp_texture = mScene->mTextures[iTexture];
							IUdxNode* texture_node = NULL;
							texture_node = pDNode->getChildNode(iTexture);
							if (texture_node == NULL)
							{
								texture_node = pDNode->addChildNode("Texture", EKernelType::EKT_NODE);
								ConstructDataFrame(pSNode->getChildNode(0), texture_node);
							}

							bool compressed = (temp_texture->mHeight == 0);
							int width = temp_texture->mWidth;
							int height = temp_texture->mHeight;
							int dataLength = width*height*4;

							if (compressed) 
							{
								width = -1; height = -1; dataLength = temp_texture->mWidth;
								((IUdxKernelIntValue*)(texture_node->getChildNode(0)->getKernel()))->setTypedValue(width);
								((IUdxKernelIntValue*)(texture_node->getChildNode(1)->getKernel()))->setTypedValue(height);
								((IUdxKernelIntValue*)(texture_node->getChildNode(2)->getKernel()))->setTypedValue(compressed);
								IUdxNode* data_node = texture_node->getChildNode(3);
								((IUdxKernelIntValue*)(data_node->getChildNode(0)))->setTypedValue(dataLength);
								IUdxNode* content_node = data_node->getChildNode(1);
								if (content_node->getChildNodeCount()<=0)
								{
									content_node->addChildNode("data", EKernelType::EKT_INT_LIST);
								}
								IUdxNode* value_node = content_node->getChildNode(0);
								IUdxKernelIntArray* valueKernel = (IUdxKernelIntArray*)value_node->getKernel();
								for (int iData; iData<dataLength; iData++)
								{
									uint8_t temp_data = reinterpret_cast<uint8_t*>(temp_texture->pcData)[iData];
									valueKernel->addTypedValue(temp_data);
								}
							}
							else
							{
								((IUdxKernelIntValue*)(texture_node->getChildNode(0)->getKernel()))->setTypedValue(width);
								((IUdxKernelIntValue*)(texture_node->getChildNode(1)->getKernel()))->setTypedValue(height);
								((IUdxKernelIntValue*)(texture_node->getChildNode(2)->getKernel()))->setTypedValue(compressed);
								IUdxNode* data_node = texture_node->getChildNode(3);
								((IUdxKernelIntValue*)(data_node->getChildNode(0)))->setTypedValue(dataLength);
								IUdxNode* content_node = data_node->getChildNode(1);
								
								for (unsigned int y = 0; y < height;++y)
								{
									IUdxNode* row_node = content_node->getChildNode(y);
									if (row_node==NULL)
									{
										row_node = content_node->addChildNode("data", EKernelType::EKT_INT_LIST);
									}
									IUdxKernelIntArray* valueKernel = (IUdxKernelIntArray*)row_node->getKernel();
									for (unsigned int x = 0; x < width;++x)
									{
										aiTexel* tx = temp_texture->pcData + y*temp_texture->mWidth+x;
										unsigned int r = tx->r,g=tx->g,b=tx->b,a=tx->a;

										valueKernel->addTypedValue(r);
										valueKernel->addTypedValue(g);
										valueKernel->addTypedValue(b);
										valueKernel->addTypedValue(a);
									}
								}
							}//End if (compressed) 
						}// End for texture_count
					}
					else
					{
						pDNode->removeChildNode(0);
					}
				}
				else if (name == "MaterialList")
				{
					if (mScene->mNumMaterials > 0) 
					{
						int material_count = mScene->mNumMaterials;
						for (int iMaterial =0; iMaterial<material_count; iMaterial++)
						{
							aiMaterial* temp_material = mScene->mMaterials[iMaterial];

							IUdxNode* material_node = NULL;
							material_node = pDNode->getChildNode(iMaterial);
							if (material_node == NULL)
							{
								material_node = pDNode->addChildNode("Material", EKernelType::EKT_NODE);
								ConstructDataFrame(pSNode->getChildNode(0), material_node);
							}

							int property_count = temp_material->mNumProperties;
							for (int iProperty = 0; iProperty < property_count; iProperty++) 
							{
								const aiMaterialProperty* prop = temp_material->mProperties[iProperty];

								IUdxNode* property_node = NULL;
								property_node = material_node->getChildNode(iProperty);
								if (property_node == NULL)
								{
									property_node = material_node->addChildNode("MatProperty", EKernelType::EKT_NODE);
									ConstructDataFrame(pSNode->getChildNode(0)->getChildNode(0), property_node);
								}

								std::string key_str = prop->mKey.data;
								std::string usage_str = TextureTypeToString((aiTextureType)prop->mSemantic);
								int tex_index = prop->mIndex;

								std::string type_str = "";
								std::string param_str = "";
								char param[100];
								if (prop->mType == aiPTI_Float) 
								{
									type_str = "float";
									int size = static_cast<int>(prop->mDataLength/sizeof(float));
									sprintf(param, "%d: ",size);
									param_str = param;
									for (int p=0; p<size; p++)
									{
										float temp = *((float*)(prop->mData+p*sizeof(float)));
										if (p!=size-1)
											sprintf(param, "%f, ", temp);
										else
											sprintf(param, "%f", temp);
										param_str += param;
									}
								}
								else if (prop->mType == aiPTI_Integer) 
								{
									type_str = "integer";
									int size = static_cast<int>(prop->mDataLength/sizeof(int));
									sprintf(param, "%d: ",size);
									param_str = param;
									for (int p=0; p<size; p++)
									{
										int temp = *((int*)(prop->mData+p*sizeof(int)));
										if (p!=size-1)
											sprintf(param, "%d, ", temp);
										else
											sprintf(param, "%d", temp);
										param_str += param;
									}
								}
								else if (prop->mType == aiPTI_String) 
								{
									type_str = "string";
									param_str = encodeXML(prop->mData+4);
								}
								else if (prop->mType == aiPTI_Buffer) 
								{
									type_str = "binary_buffer";
									int size = static_cast<int>(prop->mDataLength);
									sprintf(param, "%d: ",size);
									param_str = param;							
									param_str += std::string(prop->mData);									
								}
								((IUdxKernelStringValue*)(property_node->getChildNode(0)->getKernel()))->setTypedValue(key_str);
								((IUdxKernelStringValue*)(property_node->getChildNode(1)->getKernel()))->setTypedValue(type_str);
								((IUdxKernelStringValue*)(property_node->getChildNode(2)->getKernel()))->setTypedValue(usage_str);
								((IUdxKernelIntValue*)(property_node->getChildNode(3)->getKernel()))->setTypedValue(tex_index);
								((IUdxKernelStringValue*)(property_node->getChildNode(4)->getKernel()))->setTypedValue(param_str);
							}// End for property_count
						}// End for material_count
					}
					else
					{
						pDNode->removeChildNode(0);
					}
				}
				else if (name == "AnimationList")
				{
					if (mScene->mNumAnimations > 0) 
					{
						int anim_count = mScene->mNumAnimations;
						for (int iAnim=0; iAnim<anim_count; iAnim++)
						{
							aiAnimation* temp_animation = mScene->mAnimations[iAnim];
							//!TODO

						}// End for anim_count
					}
					else
					{
						pDNode->removeChildNode(0);
					}
				}
				else if (name == "MeshList")
				{
					if (mScene->mNumMeshes > 0) 
					{
						int mesh_count = mScene->mNumMeshes;
						for (int iMesh=0; iMesh<mesh_count; iMesh++)
						{
							aiMesh* temp_mesh = mScene->mMeshes[iMesh];

							IUdxNode* mesh_node = NULL;
							mesh_node = pDNode->getChildNode(iMesh);
							if (mesh_node == NULL)
							{
								mesh_node = pDNode->addChildNode("Mesh", EKernelType::EKT_NODE);
								ConstructDataFrame(pSNode->getChildNode(0), mesh_node);
							}

							////////////////////////////////////////types//////////////////////////////////
							{
								IUdxNode* types_node = mesh_node->getChildNode(0);
								IUdxKernelStringArray* types_node_kernel = (IUdxKernelStringArray*)types_node->getKernel();

								std::string point_type_str = temp_mesh->mPrimitiveTypes & aiPrimitiveType_POINT    ? "points"    : "";
								std::string line_type_str = temp_mesh->mPrimitiveTypes & aiPrimitiveType_LINE     ? "lines"     : "";
								std::string triangle_type_str = temp_mesh->mPrimitiveTypes & aiPrimitiveType_TRIANGLE ? "triangles" : "";
								std::string polygon_type_str = temp_mesh->mPrimitiveTypes & aiPrimitiveType_POLYGON  ? "polygons"  : "";
								if (point_type_str!="") types_node_kernel->addTypedValue(point_type_str); 
								if (line_type_str!="") types_node_kernel->addTypedValue(line_type_str); 
								if (triangle_type_str!="") types_node_kernel->addTypedValue(triangle_type_str); 
								if (polygon_type_str!="") types_node_kernel->addTypedValue(polygon_type_str); 
							}
							///////////////////////////////////material_index///////////////////////////////////////
							{
								int material_index = temp_mesh->mMaterialIndex;

								IUdxNode* material_index_node = mesh_node->getChildNode(1);
								IUdxKernelIntValue* material_index_kernel = (IUdxKernelIntValue*)material_index_node->getKernel();
								material_index_kernel->setTypedValue(material_index);
							}

							/////////////////////////////////BoneList/////////////////////////////////////////
							{
								IUdxNode* boneList_node = mesh_node->getChildNode(2);
								if (temp_mesh->HasBones())
								{
									//TODO

								}
								else
								{
									boneList_node->removeChildNode(0);
								}
							}

							/////////////////////////////////////FaceList/////////////////////////////////////
							{
								IUdxNode* faceList_node = mesh_node->getChildNode(3);
								if (temp_mesh->mNumFaces>0) 
								{
									int face_count = temp_mesh->mNumFaces;
									for (unsigned int n = 0; n < face_count; ++n) 
									{
										IUdxNode* face_node = NULL;
										face_node = faceList_node->getChildNode(n);
										if (face_node == NULL)
										{
											face_node = faceList_node->addChildNode("Index", EKernelType::EKT_INT_LIST);
											ConstructDataFrame(pSNode->getChildNode(0)->getChildNode(3), face_node);
										}

										IUdxKernelIntArray* face_node_kernel = (IUdxKernelIntArray*)face_node->getKernel();

										aiFace& f = temp_mesh->mFaces[n];
										int index_count = f.mNumIndices;
										for (unsigned int j = 0; j < index_count;++j)
										{
											int temp_index = f.mIndices[j];
											face_node_kernel->addTypedValue(temp_index);
										}
									}
								}
								else
								{

								}
							}

							/////////////////////////////////////Positions/////////////////////////////////////
							{
								IUdxNode* positionList_node = mesh_node->getChildNode(4);
								if (temp_mesh->HasPositions()) 
								{
									int position_count = temp_mesh->mNumVertices;
									for (unsigned int n = 0; n < position_count; ++n) 
									{
										IUdxKernelVector3dArray* position_node_kernel = (IUdxKernelVector3dArray*)positionList_node->getKernel();
										position_node_kernel->addTypedValue(temp_mesh->mVertices[n].x, temp_mesh->mVertices[n].y, temp_mesh->mVertices[n].z);
									}
								}
								else
								{

								}
							}

							/////////////////////////////////////Normals/////////////////////////////////////
							{
								IUdxNode* normalList_node = mesh_node->getChildNode(5);
								if (temp_mesh->HasNormals()) 
								{
									int normal_count = temp_mesh->mNumVertices;
									for (unsigned int n = 0; n < normal_count; ++n) 
									{
										IUdxKernelVector3dArray* normal_node_kernel = (IUdxKernelVector3dArray*)normalList_node->getKernel();
										normal_node_kernel->addTypedValue(temp_mesh->mNormals[n].x, temp_mesh->mNormals[n].y, temp_mesh->mNormals[n].z);
									}
								}
								else
								{

								}
							}

							/////////////////////////////////////Tangents & Bitangents/////////////////////////////////////
							{
								IUdxNode* tangentList_node = mesh_node->getChildNode(6);
								if (temp_mesh->HasTangentsAndBitangents()) 
								{
									int tangent_count = temp_mesh->mNumVertices;
									for (unsigned int n = 0; n < tangent_count; ++n) 
									{
										IUdxKernelVector3dArray* tangent_node_kernel = (IUdxKernelVector3dArray*)tangentList_node->getKernel();
										tangent_node_kernel->addTypedValue(temp_mesh->mTangents[n].x, temp_mesh->mTangents[n].y, temp_mesh->mTangents[n].z);
									}
									//////////////////////////////////////////////////////////////////////////
									IUdxNode* bitangentList_node = mesh_node->getChildNode(7);

									int bitangent_count = temp_mesh->mNumVertices;
									for (unsigned int n = 0; n < bitangent_count; ++n) 
									{
										IUdxKernelVector3dArray* bitangent_node_kernel = (IUdxKernelVector3dArray*)bitangentList_node->getKernel();
										bitangent_node_kernel->addTypedValue(temp_mesh->mBitangents[n].x, temp_mesh->mBitangents[n].y, temp_mesh->mBitangents[n].z);
									}
								}
								else
								{

								}
							}

							/////////////////////////////////////TextureCoords/////////////////////////////////////
							{
								IUdxNode* textureCoordList_node = mesh_node->getChildNode(8);
								if (temp_mesh->HasTextureCoords(0)) 
								{
									int textureCoord_count = temp_mesh->mNumVertices;
									for (unsigned int n = 0; n < textureCoord_count; ++n) 
									{
										IUdxKernelVector3dArray* textureCoord_node_kernel = (IUdxKernelVector3dArray*)textureCoordList_node->getKernel();
										textureCoord_node_kernel->addTypedValue(temp_mesh->mTextureCoords[0][n].x, temp_mesh->mTextureCoords[0][n].y, temp_mesh->mTextureCoords[0][n].z);
									}
								}
								else
								{

								}
							}

							/////////////////////////////////////Colors/////////////////////////////////////
							{
								IUdxNode* vertexColorsList_node = mesh_node->getChildNode(9);
								if (temp_mesh->HasVertexColors(0)) 
								{
									int vertexColors_count = temp_mesh->mNumVertices;
									for (unsigned int n = 0; n < vertexColors_count; ++n) 
									{
										IUdxKernelVector4dArray* vertexColors_node_kernel = (IUdxKernelVector4dArray*)vertexColorsList_node->getKernel();
										vertexColors_node_kernel->addTypedValue(temp_mesh->mColors[0][n].r, temp_mesh->mColors[0][n].g, temp_mesh->mColors[0][n].b, temp_mesh->mColors[0][n].a);
									}
								}
								else
								{

								}
							}

						}// End for mesh_count
					}
					else
					{
						pDNode->removeChildNode(0);
					}
				}


				return true;
			}

			//! 根据当前选中的SchemaNode，获取需要设置Index信息的SchemaNode的数量
			virtual int prepareNeedIndexSettingNode(IUdxNodeSchema* pSNode)
			{
				mNeedIndexSchemaList.clear();
				findAllSchemaNode_WithListType(pSNode);
				return mNeedIndexSchemaList.size();
			}

			//! 迭代获取需要设置Index信息的SchemaNode
			virtual IUdxNodeSchema* getNeedIndexSettingNode(int idx)
			{
				if (idx>=mNeedIndexSchemaList.size() || idx<0)
					return NULL;
				return mNeedIndexSchemaList[idx];
			}

			//////////////////////////////////////////////////////////////////////////
			//	
			//
			//
			//////////////////////////////////////////////////////////////////////////
			//! 准备保存到文件中
			virtual bool BeginWriteToFile()
			{
				if (mWritePath=="")
				{
					mCurrentErrorInfo = "write file path does not exist";
					return false;
				}

				return true;
			}

			//! 从UDX数据节点中读取内容，并写入到原始数据格式中
			virtual bool WriteFromNode(IUdxNode* pDNode, int formatFlag=-1)
			{
				if(pDNode==NULL)  return false;
				if(pDNode->validateSchema(GetUdxSchema()) == false) return false;

				aiScene* pScene = new aiScene();

				IUdxKernel* pKernel;
				IUdxKernelRealValue* realKernel;
				IUdxKernelIntValue* intKernel;
				IUdxKernelStringValue* stringKernel;

				{
					IUdxNode* name_node = pDNode->getChildNode(0)->getChildNode(0);
					IUdxKernelStringValue* name_kernel = (IUdxKernelStringValue*)name_node->getKernel();
					pScene->mRootNode = new aiNode(name_kernel->getTypedValue().c_str());
				}
				{
					IUdxNode* matrix_node = pDNode->getChildNode(0)->getChildNode(2);
					IUdxKernelVector4dArray* matrix_kernel = (IUdxKernelVector4dArray*)matrix_node->getKernel();
					double v1, v2, v3, v4;
					matrix_kernel->getTypedValueByIndex(0, v1, v2, v3, v4);
					pScene->mRootNode->mTransformation.a1  = v1;
					pScene->mRootNode->mTransformation.a2  = v2;
					pScene->mRootNode->mTransformation.a3  = v3;
					pScene->mRootNode->mTransformation.a4  = v4;

					matrix_kernel->getTypedValueByIndex(1, v1, v2, v3, v4);
					pScene->mRootNode->mTransformation.b1  = v1;
					pScene->mRootNode->mTransformation.b2  = v2;
					pScene->mRootNode->mTransformation.b3  = v3;
					pScene->mRootNode->mTransformation.b4  = v4;

					matrix_kernel->getTypedValueByIndex(2, v1, v2, v3, v4);
					pScene->mRootNode->mTransformation.c1  = v1;
					pScene->mRootNode->mTransformation.c2  = v2;
					pScene->mRootNode->mTransformation.c3  = v3;
					pScene->mRootNode->mTransformation.c4  = v4;

					matrix_kernel->getTypedValueByIndex(3, v1, v2, v3, v4);
					pScene->mRootNode->mTransformation.d1  = v1;
					pScene->mRootNode->mTransformation.d2  = v2;
					pScene->mRootNode->mTransformation.d3  = v3;
					pScene->mRootNode->mTransformation.d4  = v4;
				}
				{
					IUdxNode* meshRef_node = pDNode->getChildNode(0)->getChildNode(3);
					IUdxKernelIntArray* meshRef_kernel = (IUdxKernelIntArray*)meshRef_node->getKernel();
					int meshRef_count = meshRef_kernel->getCount();
					
					pScene->mRootNode->mMeshes = new unsigned int(meshRef_count);
					for (int i=0; i<meshRef_count; i++)
					{
						int val = 0;
						meshRef_kernel->getTypedValueByIndex(i, val);
						pScene->mRootNode->mMeshes[i] = val;
					}
				}
				{
					int mesh_count = pDNode->getChildNode(4)->getChildNodeCount();
					pScene->mMeshes = new aiMesh*[mesh_count];

					int face_count = pDNode->getChildNode(4)->getChildNode(0)->getChildNodeCount();
					pScene->mMeshes[0]->mFaces = new aiFace[face_count];

					IUdxNode* temp_face_node = pDNode->getChildNode(4)->getChildNode(0)->getChildNode(0);
					IUdxKernelIntArray* temp_face_kernel = (IUdxKernelIntArray*)temp_face_node->getKernel();
					int index_count = temp_face_kernel->getCount();
					aiFace& temp_face = pScene->mMeshes[0]->mFaces[0];
					temp_face.mIndices = new unsigned int[index_count];

					for (unsigned int i = 0; i < index_count; i++) 
					{
						temp_face.mIndices[i] = 0;
					}
				}

				delete pScene;
				return true;
			}

			//! 结束保存到文件中
			virtual bool EndWriteToFile()
			{
				if (mWritePath=="")
				{
					mCurrentErrorInfo = "write file path does not exist";
					return false;
				}

				return true;
			}

			//! 获取当前读取或者写入的错误信息
			virtual const char* GetCurrentErrorInfo()
			{
				return mCurrentErrorInfo.c_str();
			}
		};
	}
}

#endif