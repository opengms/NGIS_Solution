#include "DM_3dModel_ASSIMP.h"
#include "..\include\NativeDataMappingAPI.h"
#include "UdxDataSchemaApi.h"
#include "UdxDataApi.h"

namespace NGIS
{
	namespace DataMapping
	{

		extern "C" NATIVEDATAMAPPING_API IDataMappingHandle* NATIVEDATAMAPPING_CALLCONV 
			createGeneral3dModelHandle()
		{
			DM_3DModel_ASSIMP* p3DModelHandle = new DM_3DModel_ASSIMP();
			return p3DModelHandle;
		}

	}
}