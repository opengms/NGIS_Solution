#ifndef __I_NGIS_DATA_MAPPING_API_H__
#define __I_NGIS_DATA_MAPPING_API_H__

#include "INxUnknown.h"
#include "UdxDataApi.h"
#include "UdxDataSchemaApi.h"

#include <string.h>

using namespace NGIS::Data;
using namespace NGIS::Data::Schema;

namespace NGIS
{
	namespace DataMapping
	{
		class IDataMappingHandle : public INxUnknown
		{
		public:
			//////////////////////////////////////////////////////////////////////////
			//	
			//
			//
			//////////////////////////////////////////////////////////////////////////
			//! 初始化数据映射Handle
			virtual bool InitHandle(const char* readFilePath, const char* writeFilePath) = 0;

			//! 获取Handle的名称
			virtual const char* GetHandleName() = 0;

			//! 获取Handle的版本
			virtual const char* GetHandleVersion() = 0;

			//! 获取当前数据映射Handle的关联Schema
			virtual IUdxDatasetSchema* GetUdxSchema() = 0;

			//! 获取此Handle能够操作的SchemaNode个数
			virtual int GetCapbilityCount() = 0;

			//! 迭代获取此Handle能够操作的SchemaNode
			virtual IUdxNodeSchema* getCapbilityNode(int idx) = 0;

			//////////////////////////////////////////////////////////////////////////
			//	
			//
			//
			//////////////////////////////////////////////////////////////////////////
			//! 从原始数据中按照传递过来的SchemaNode读取数据内容，
			//! 并且往传递过来的DataNode中写入相应的数据内容
			virtual bool ReadToNode(IUdxNodeSchema* pSNode, IUdxNode* pDNode) = 0;

			//! 根据当前选中的SchemaNode，获取需要设置Index信息的SchemaNode的数量
			virtual int prepareNeedIndexSettingNode(IUdxNodeSchema* pSNode) = 0;

			//! 迭代获取需要设置Index信息的SchemaNode
			virtual IUdxNodeSchema* getNeedIndexSettingNode(int idx) = 0;

			//////////////////////////////////////////////////////////////////////////
			//	
			//
			//
			//////////////////////////////////////////////////////////////////////////
			//! 准备保存到文件中
			virtual bool BeginWriteToFile() = 0;

			//! 从UDX数据节点中读取内容，并写入到原始数据格式中
			virtual bool WriteFromNode(IUdxNode* pDNode, int formatFlag=-1) = 0;

			//! 结束保存到文件中
			virtual bool EndWriteToFile() = 0;

			//! 获取当前读取或者写入的错误信息
			virtual const char* GetCurrentErrorInfo() = 0;
		};
	}
}

#endif