#ifndef __I_NGIS_NATIVE_DATA_MAPPING_API_H__
#define __I_NGIS_NATIVE_DATA_MAPPING_API_H__

// 在windows上的api开放设置
#ifdef _WINDOWS

#ifndef _NATIVEDATAMAPPING_STATIC_LIB_
#ifdef NATIVEDATAMAPPING_EXPORTS
#define NATIVEDATAMAPPING_API __declspec(dllexport)
#else
#define NATIVEDATAMAPPING_API __declspec(dllimport)
#endif // NATIVEDATAMAPPING_API
#else
#define NATIVEDATAMAPPING_API
#endif // _NATIVEDATAMAPPING_STATIC_LIB_

// Declare the calling convention.
#if defined(_STDCALL_SUPPORTED)
#define NATIVEDATAMAPPING_CALLCONV __stdcall
#else
#define NATIVEDATAMAPPING_CALLCONV __cdecl
#endif // STDCALL_SUPPORTED

#else // _IMP_WINDOWS_API_

// Force symbol export in shared libraries built with gcc.
#if (__GNUC__ >= 4) && !defined(_NGISDATASCHEMA_STATIC_LIB_) && defined(NGISDATASCHEMA_EXPORTS)
#define NATIVEDATAMAPPING_API __attribute__ ((visibility("default")))
#else
#define NATIVEDATAMAPPING_API
#endif
#define NATIVEDATAMAPPING_CALLCONV

#endif

#include "IDataMappingHandle.h"

// 以下用于导出对外开放的API接口
namespace NGIS
{
	namespace DataMapping
	{
		extern "C" NATIVEDATAMAPPING_API EKernelType NATIVEDATAMAPPING_CALLCONV
			getKernelTypeBySchema(ESchemaNodeType pSType);

		extern "C" NATIVEDATAMAPPING_API bool NATIVEDATAMAPPING_CALLCONV
			ConstructDataFrame(IUdxNodeSchema* pSNode, IUdxNode* pDNode);

		extern "C" NATIVEDATAMAPPING_API IDataMappingHandle* NATIVEDATAMAPPING_CALLCONV 
			createGeneralVectorHandle();

		extern "C" NATIVEDATAMAPPING_API IDataMappingHandle* NATIVEDATAMAPPING_CALLCONV 
			createGeneralRasterHandle();

		extern "C" NATIVEDATAMAPPING_API IDataMappingHandle* NATIVEDATAMAPPING_CALLCONV 
			createGeneral3dModelHandle();

		extern "C" NATIVEDATAMAPPING_API IDataMappingHandle* NATIVEDATAMAPPING_CALLCONV 
			createShapefileHandle();

		extern "C" NATIVEDATAMAPPING_API IDataMappingHandle* NATIVEDATAMAPPING_CALLCONV 
			createAAIGridHandle();


	}
}
#endif