#ifndef __C_NGISDATA_UDXSCHEMA_DESCRIPTION_H__
#define __C_NGISDATA_UDXSCHEMA_DESCRIPTION_H__

#include "IUdxSchemaDataset.h"
#include "UdxNodeSchema.h"
#include <vector>
#include <string>

namespace NGIS
{
	namespace Data
	{
		namespace Schema
		{
			class CUdxSchemaDescription : public INodeDescription
			{
			public:
				CUdxSchemaDescription(ESchemaNodeType pKernelType)
				{
					mKernelType = pKernelType;

					mNodeInfo = "";
					mConceptTag = "";
					mUnitTag = "";
					mDimensionTag = "";
					mSpatialRefTag = "";
					mDataTemplateTag = "";
				}

				CUdxSchemaDescription(ESchemaNodeType pKernelType, const char* pNodeDescription)
				{
					mKernelType = pKernelType;

					mNodeInfo = pNodeDescription;
					mConceptTag = "";
					mUnitTag = "";
					mDimensionTag = "";
					mSpatialRefTag = "";
					mDataTemplateTag = "";
				}

				~CUdxSchemaDescription()
				{

				}

				virtual ESchemaNodeType getKernelType()
				{
					return mKernelType;
				}

				virtual bool modifyKernelType(ESchemaNodeType pKernelType)
				{
					if (mKernelType == pKernelType) return false;
					mKernelType = pKernelType;
					return true;
				}

				virtual const char* getNodeDescription()
				{
					return mNodeInfo.c_str();
				}

				virtual bool modifyNodeDescription(const char* pNodeInfo)
				{
					std::string pInfoStr = pNodeInfo;
					if (mNodeInfo == pInfoStr)
						return false;
					mNodeInfo = pInfoStr;
					return true;
				}

				//////////////////////////////////////////////////////////////////////////
				virtual const char* getConceptTag()
				{
					return mConceptTag.c_str();
				}

				virtual bool modifyConceptTag(const char* pTag)
				{
					std::string pTagStr = pTag;
					if (mConceptTag == pTagStr)
						return false;
					mConceptTag = pTagStr;
					return true;
				}

				virtual const char* getUnitTag()
				{
					return mUnitTag.c_str();
				}

				virtual bool modifyUnitTag(const char* pTag) 
				{
					std::string pTagStr = pTag;
					if (mUnitTag == pTagStr)
						return false;
					mUnitTag = pTagStr;
					return true;
				}

				virtual const char* getDimensionTag() 
				{
					return mDimensionTag.c_str();
				}

				virtual bool modifyDimensionTag(const char* pTag)
				{
					std::string pTagStr = pTag;
					if (mDimensionTag == pTagStr)
						return false;
					mDimensionTag = pTagStr;
					return true;
				}

				virtual const char* getSpatialReferencefTag() 
				{
					return mSpatialRefTag.c_str();
				}

				virtual bool modifySpatialReferenceTag(const char* pTag)
				{
					std::string pTagStr = pTag;
					if (mSpatialRefTag == pTagStr)
						return false;
					mSpatialRefTag = pTagStr;
					return true;
				}

				virtual const char* getDataTemplateTag()
				{
					return mDataTemplateTag.c_str();
				}

				virtual bool modifyDataTemplateTag(const char* pTag)
				{
					std::string pTagStr = pTag;
					if (mDataTemplateTag == pTagStr)
						return false;
					mDataTemplateTag = pTagStr;
					return true;
				}

			protected:
				ESchemaNodeType							mKernelType;
				std::string											mNodeInfo;
				std::string											mConceptTag;
				std::string											mUnitTag;
				std::string											mDimensionTag;
				std::string											mSpatialRefTag;
				std::string											mDataTemplateTag;
			};
		}
	}
}

#endif