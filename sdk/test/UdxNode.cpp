#include "UdxNode.h"

namespace NGIS
{
	namespace Data
	{
		CUdxNode::CUdxNode(IUdxNode* pParent, const char* pName, EKernelType pType) 
			: mParentDxNode(pParent)
		{
			mName = pName;
			mKernel = NULL;
			if (pParent!=NULL)
			{
				createKernel(pName, pType);
			}
			else
			{
				mKernel = new CUdxKernel(pType, this);
			}
		}

		CUdxNode::~CUdxNode(void)
		{
			if (mKernel)
			{
				mKernel->release();
				mKernel = NULL;
			}
			for (int i=0; i<mChildNodes.size(); i++)
			{
				mChildNodes[i]->release();
			}
			mChildNodes.clear();
		}

		void CUdxNode::createKernel(const char* pName, EKernelType pType)
		{
			switch (pType)
			{
			case EKernelType::EKT_INT:
					mKernel = new CUdxKernelIntValue(pType, this);
				break;

			case EKernelType::EKT_REAL:
					mKernel = new CUdxKernelRealValue(pType, this);
				break;

			case EKernelType::EKT_STRING:
					mKernel = new CUdxKernelStringValue(pType, this);
				break;

			case EKernelType::EKT_VECTOR2:
					mKernel = new CUdxKernelVector2dValue(pType, this);
				break;

			case EKernelType::EKT_VECTOR3:
					mKernel = new CUdxKernelVector3dValue(pType, this);
				break;

			case EKernelType::EKT_VECTOR4:
					mKernel = new CUdxKernelVector4dValue(pType, this);
				break;
			case (EKernelType)(EKernelType::EKT_INT|EKernelType::EKT_LIST):
					mKernel = new CUdxKernelIntArray(pType, this);
				break;

			case (EKernelType)(EKernelType::EKT_REAL|EKernelType::EKT_LIST):
					mKernel = new CUdxKernelRealArray(pType, this);
				break;

			case (EKernelType)(EKernelType::EKT_STRING|EKernelType::EKT_LIST):
					mKernel = new CUdxKernelStringArray(pType, this);
				break;

			case (EKernelType)(EKernelType::EKT_VECTOR2|EKernelType::EKT_LIST):
					mKernel = new CUdxKernelVector2dArray(pType, this);
				break;

			case (EKernelType)(EKernelType::EKT_VECTOR3|EKernelType::EKT_LIST):
					mKernel = new CUdxKernelVector3dArray(pType, this);
				break;

			case (EKernelType)(EKernelType::EKT_VECTOR4|EKernelType::EKT_LIST):
					mKernel = new CUdxKernelVector4dArray(pType, this);
				break;

			case EKernelType::EKT_NODE:
			case EKernelType::EKT_LIST:
			case EKernelType::EKT_MAP:
			case EKernelType::EKT_TABLE:
					mKernel = new CUdxKernel(pType, this);
				break;

			}
		}

		const char* CUdxNode::getName()
		{
			return mName.c_str();
		}

		void CUdxNode::setName( const char* pName )
		{
			mName = pName;
		}

		IUdxKernel* CUdxNode::getKernel()
		{
			return mKernel;
		}

		int CUdxNode::getChildNodeCount()
		{
			return mChildNodes.size();
		}

		IUdxNode* CUdxNode::getChildNode(int idx)
		{
			if (idx<0 || idx>= mChildNodes.size())
				return NULL;
			return mChildNodes[idx];
		}

		IUdxNode* CUdxNode::addChildNode(const char* pName, EKernelType pType)
		{
			//! TODO 判断节点的类型
			CUdxNode* node = new CUdxNode(this, pName, pType);
			mChildNodes.push_back(node);
			return node;
		}

		bool CUdxNode::removeChildNode(IUdxNode* pNode)
		{
			std::vector<IUdxNode*>::iterator it = mChildNodes.begin();
			std::vector<IUdxNode*>::iterator end = mChildNodes.end();
			for (; it!=end; it++)
			{
				if (*it == pNode)
				{
					mChildNodes.erase(it);
					pNode->release();
					return true;
				}
			}
			return false;
		}

		bool CUdxNode::removeChildNode(int idx)
		{
			IUdxNode* pNode = getChildNode(idx);
			if (pNode==NULL)
			{
				return false;
			}
			std::vector<IUdxNode*>::iterator it = mChildNodes.begin();
			std::vector<IUdxNode*>::iterator end = mChildNodes.end();
			for (; it!=end; it++)
			{
				if (*it == pNode)
				{
					mChildNodes.erase(it);
					pNode->release();
					return true;
				}
			}
			return false;
		}

	}
}
