#ifndef __C_NGISDATA_UDXNODE_H__
#define __C_NGISDATA_UDXNODE_H__

#include <stdio.h>
#include <string.h>
#include <vector>
#include "IUdxNode.h"
#include "UdxKernel.h"

namespace NGIS
{
	namespace Data
	{
		class CUdxNode : public IUdxNode
		{
		public:
			CUdxNode(IUdxNode* pParent, const char* pName, EKernelType pType);

			~CUdxNode(void);

			void createKernel(const char* pName, EKernelType pType);

		public:
			virtual const char* getName();

			virtual void setName(const char* pName);

			virtual IUdxKernel* getKernel();

			virtual int getChildNodeCount();

			virtual IUdxNode* getChildNode(int idx);

			virtual IUdxNode* addChildNode(const char* pName, EKernelType pType);

			virtual bool removeChildNode(IUdxNode* pNode);

			virtual bool removeChildNode(int idx);

		protected:
			std::string								mName;
			IUdxKernel*							mKernel;
			std::vector<IUdxNode*>		mChildNodes;
			IUdxNode*							mParentDxNode;

			//////////////////////////////////////////////////////////////////////////
			friend class CUdxKernel;
			friend class CUdxKernelIntValue;
			friend class CUdxKernelRealValue;
			friend class CUdxKernelStringValue;
			friend class CUdxKernelVector2dValue;
			friend class CUdxKernelVector3dValue;
			friend class CUdxKernelVector4dValue;
			friend class CUdxKernelIntArray;
			friend class CUdxKernelRealArray;
			friend class CUdxKernelStringArray;
			friend class CUdxKernelVector2dArray;
			friend class CUdxKernelVector3dArray;
			friend class CUdxKernelVector4dArray;

		};
	}
}

#endif