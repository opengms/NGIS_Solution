#ifndef __I_NGISDATA_UDXDATASET_H__
#define __I_NGISDATA_UDXDATASET_H__

#include "INxUnknown.h"
#include "IUdxNode.h"

namespace NGIS
{
	namespace Data
	{
		class IUdxDataset : public virtual IUdxNode
		{
		public:
			virtual bool LoadXmlFile(const char* fileName) = 0;

			virtual bool FormatXmlFile(const char* fileName) = 0;

			virtual bool LoadXmlStream(const char* xmlStr) = 0;

			virtual bool FormatXmlStream(std::string& xmlStr) = 0;
		};
	}
} 

#endif