#ifndef __I_NGISDATA_UDXNODE_H__
#define __I_NGISDATA_UDXNODE_H__

#include "INxUnknown.h"
#include "IUdxKernel.h"

namespace NGIS
{
	namespace Data
	{
		class IUdxNode : public INxUnknown
		{
		public:
			virtual const char* getName() = 0;

			virtual void setName(const char* pName) = 0;

			virtual IUdxKernel* getKernel() = 0;

			virtual int getChildNodeCount() = 0;

			virtual IUdxNode* getChildNode(int idx) = 0;

			virtual IUdxNode* addChildNode(const char* pName, EKernelType pType) = 0;

			virtual bool removeChildNode(IUdxNode* pNode) = 0;

			virtual bool removeChildNode(int idx) = 0;
		};
	}
}

#endif