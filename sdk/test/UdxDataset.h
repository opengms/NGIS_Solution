#ifndef __C_NGISDATA_UDXDATASET_H__
#define __C_NGISDATA_UDXDATASET_H__

#include "IUdxDataset.h"
#include "UdxNode.h"
#include "tinyxml2.h"
//#include "json.h"

namespace NGIS
{
	namespace Data
	{
		class CUdxDataset : public virtual IUdxDataset, public virtual CUdxNode
		{
		public:
			CUdxDataset(const char* pName) : CUdxNode(NULL, pName, EKernelType::EKT_NODE)
			{
				mParentDxNode = NULL;
			}

			~CUdxDataset() 
			{
			}

			void ParseXDO(IUdxNode* containerNode, tinyxml2::XMLElement* element);

			void FormatXDO(IUdxNode* pNode, tinyxml2::XMLElement* element);

			void ParseXDO(IUdxNode* containerNode);

		public:
			virtual bool LoadXmlFile(const char* fileName);

			virtual bool FormatXmlFile(const char* fileName);

			virtual bool LoadXmlStream(const char* xmlStr);

			virtual bool FormatXmlStream(std::string& xmlStr);

			//////////////////////////////////////////////////////////////////////////

		public:
			virtual const char* getName() { return CUdxNode::getName(); }

			virtual void setName(const char* pName) { return CUdxNode::setName(pName); }

			virtual IUdxKernel* getKernel() { return CUdxNode::getKernel(); }

			virtual int getChildNodeCount() { return CUdxNode::getChildNodeCount(); }

			virtual IUdxNode* getChildNode(int idx) { return CUdxNode::getChildNode(idx); }

			virtual IUdxNode* addChildNode(const char* pName, EKernelType pType) 
			{ return CUdxNode::addChildNode(pName, pType); }

			virtual bool removeChildNode(IUdxNode* pNode) { return CUdxNode::removeChildNode(pNode); }

			virtual bool removeChildNode(int idx) { return CUdxNode::removeChildNode(idx); }
		};
	}
}

#endif