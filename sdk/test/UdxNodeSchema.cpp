#include "UdxNodeSchema.h"
#include "UdxSchemaDescription.h"

namespace NGIS
{
	namespace Data
	{
		namespace Schema
		{
			CUdxNodeSchema::CUdxNodeSchema(IUdxNodeSchema* pParent, const char* pName, INodeDescription* pDescription) :mName(pName)
			{
				mParentNode = pParent;
				mDescription = pDescription;
				if (mParentNode) mParentNode->addRef();
				if (mDescription) mDescription->addRef();
			}

			CUdxNodeSchema::~CUdxNodeSchema()
			{
				if (mParentNode) mParentNode->release();
				if (mDescription) mDescription->release();
				for (int i=0; i<mChildNodes.size(); i++)
				{
					mChildNodes[i]->release();
				}
				mChildNodes.clear();
			}

			const char* CUdxNodeSchema::getName()
			{
				return mName.c_str();
			}

			INodeDescription* CUdxNodeSchema::getDescription()
			{
				return mDescription;
			}

			bool CUdxNodeSchema::modifyName(const char* pName)
			{
				std::string pNameStr = pName;
				if (mName == pNameStr) return false;
				mName = pName;
				return true;
			}

			IUdxNodeSchema* CUdxNodeSchema::getParentNode()
			{
				return mParentNode;
			}

			int CUdxNodeSchema::getChildNodeCount()
			{
				return mChildNodes.size();
			}

			IUdxNodeSchema* CUdxNodeSchema::getChildNode(int idx)
			{
				if (idx<0 || idx >=mChildNodes.size())
				{
					return NULL;
				}
				return mChildNodes[idx];
			}

			IUdxNodeSchema* CUdxNodeSchema::addChildNode(const char* pName, INodeDescription* pDescription)
			{
				ESchemaNodeType mType = mDescription->getKernelType();
				if (mType == EDTKT_INT ||
					mType == EDTKT_REAL ||
					mType == EDTKT_STRING ||
					mType == EDTKT_VECTOR2 ||
					mType == EDTKT_VECTOR3 ||
					mType == EDTKT_VECTOR4 ||
					mType == (EDTKT_INT|EDTKT_LIST) ||
					mType == (EDTKT_REAL|EDTKT_LIST) ||
					mType == (EDTKT_STRING|EDTKT_LIST) ||
					mType == (EDTKT_VECTOR2|EDTKT_LIST) ||
					mType == (EDTKT_VECTOR3|EDTKT_LIST) ||
					mType == (EDTKT_VECTOR4|EDTKT_LIST) )
				{
					return NULL;
				}
				else if (mType == EDTKT_LIST)
				{
					if (this->mChildNodes.size() >= 1)
						return NULL;
				}
				else if (mType == EDTKT_MAP)
				{
					if (this->mChildNodes.size() >= 2)
						return NULL;
				}
				else if (mType == EDTKT_TABLE)
				{
					bool isOK=false;
					ESchemaNodeType pType = pDescription->getKernelType();
					if (pType == (EDTKT_INT|EDTKT_LIST) ||
						pType == (EDTKT_REAL|EDTKT_LIST) ||
						pType == (EDTKT_STRING|EDTKT_LIST) ||
						pType == (EDTKT_VECTOR2|EDTKT_LIST) ||
						pType == (EDTKT_VECTOR3|EDTKT_LIST) ||
						pType == (EDTKT_VECTOR4|EDTKT_LIST))
					{
						isOK = true;
					}
					if (isOK==false) return NULL;
				}
				CUdxNodeSchema* pNode = new CUdxNodeSchema(this, pName, pDescription);
				this->mChildNodes.push_back(pNode);
				return pNode;
			}

			IUdxNodeSchema* CUdxNodeSchema::addChildNode(const char* pName, int pNodeType, const char* pNodeInfo)
			{
				CUdxSchemaDescription* pDescription = new CUdxSchemaDescription((ESchemaNodeType)pNodeType, pNodeInfo);
				IUdxNodeSchema* pNode = addChildNode(pName, pDescription);
				pDescription->release();
				return pNode;
			}

			bool CUdxNodeSchema::removeChildNode(IUdxNodeSchema* pNode)
			{
				std::vector<IUdxNodeSchema*>::iterator it = mChildNodes.begin();
				std::vector<IUdxNodeSchema*>::iterator end = mChildNodes.end();
				for (; it!=end; it++)
				{
					if (*it == pNode)
					{
						mChildNodes.erase(it);
						pNode->release();
						return true;
					}
				}
				return false;
			}

			bool CUdxNodeSchema::removeChildNode(int idx)
			{
				IUdxNodeSchema* pNode = getChildNode(idx);
				if (pNode==NULL)
				{
					return false;
				}
				std::vector<IUdxNodeSchema*>::iterator it = mChildNodes.begin();
				std::vector<IUdxNodeSchema*>::iterator end = mChildNodes.end();
				for (; it!=end; it++)
				{
					if (*it == pNode)
					{
						mChildNodes.erase(it);
						pNode->release();
						return true;
					}
				}
				return false;
			}

		}
	}
}