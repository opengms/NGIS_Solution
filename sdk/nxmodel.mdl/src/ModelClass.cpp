#include "ModelClass.h"
#include "XgeModelClassApi.h"

namespace NGIS
{
	namespace Model
	{

		extern "C" NGISMODEL_API const char* NGISMODEL_CALLCONV getVersion()
		{
			return "1.0.0.1";
		}

		extern "C" NGISMODEL_API IModelClass* NGISMODEL_CALLCONV createModelClass()
		{
			return new CModelClass();
		}
	}
}