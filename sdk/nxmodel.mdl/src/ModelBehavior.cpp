#include "ModelClass.h"

namespace NGIS
{
	namespace Model
	{


		bool CModelBehavior::loadFromXml(const XMLElement* pModelClassElement)
		{
			const XMLElement* behaviorEle = pModelClassElement->FirstChildElement("Behavior");

			const XMLElement* relatedDatasetsEle = behaviorEle->FirstChildElement("RelatedDatasets");
			const XMLElement* stateGroupEle = behaviorEle->FirstChildElement("StateGroup");

			if (relatedDatasetsEle==NULL)
				relatedDatasetsEle = behaviorEle->FirstChildElement("DatasetDeclarations");

			for (const XMLElement* childEle = relatedDatasetsEle->FirstChildElement(); 
				childEle; childEle = childEle->NextSiblingElement())
			{
				ModelDatasetItem temp_item;
				temp_item.datasetName = childEle->Attribute("name");
				temp_item.datasetItemDescription = childEle->Attribute("description");
				std::string temp_type = childEle->Attribute("type");
				if (temp_type == "internal")
				{
					temp_item.datasetItemType = EMDIT_INTERNAL;
					temp_item.externalId = "";
					//! TODO
					// parse udx schema, please consider memory control
					NGIS::Data::Schema::IUdxDatasetSchema* pDataset = NGIS::Data::Schema::createUdxDatasetSchema("UdxDeclaration");
					pDataset->LoadFromXmlElement(childEle->FirstChildElement());
					temp_item.datasetItem = pDataset;
				}
				else if (temp_type == "external")
				{
					temp_item.datasetItemType = EMDIT_EXTERNAL;
					temp_item.externalId = childEle->Attribute("externalId");
					temp_item.datasetItem = NULL;
				}
				else if (temp_type == "raw")
				{
					temp_item.datasetItemType = EMDIT_RAW;
					temp_item.externalId = "";
					temp_item.datasetItem = NULL;
				}
				addModelDatasetItem(temp_item);
			}

			const XMLElement* states_ele = stateGroupEle->FirstChildElement("States");
			for (const XMLElement* childEle = states_ele->FirstChildElement(); 
				childEle; childEle = childEle->NextSiblingElement())
			{
				ModelState temp_state;
				temp_state.stateId = childEle->Attribute("id");
				temp_state.stateName = childEle->Attribute("name");
				temp_state.stateDecription = childEle->Attribute("description");
				std::string temp_type = childEle->Attribute("type");
				if (temp_type == "basic")
				{
					temp_state.stateType = EMST_BASIC;
				}
				else if (temp_type == "group")
				{
					temp_state.stateType = EMST_GROUP;
				}
				//////////////////////////////////////////////////////////////////////////
				for (const XMLElement* childEle1 = childEle->FirstChildElement(); 
					childEle1; childEle1 = childEle1->NextSiblingElement())
				{
					ModelEvent modelEvent;
					modelEvent.eventName = childEle1->Attribute("name");
					modelEvent.eventDescription = childEle1->Attribute("description");
					std::string eventType = childEle1->Attribute("type");
					if (eventType == "response")
					{
						modelEvent.eventType = EMET_RESPONSE;
					}
					else if (eventType == "noresponse")
					{
						modelEvent.eventType = EMET_NORESPONSE;
					}
					else if (eventType == "control")
					{
						modelEvent.eventType = EMET_CONTROL;
					}
					const XMLElement* param_ele = childEle1->FirstChildElement();
					if (param_ele->FindAttribute("description"))
						modelEvent.parameterDescription = param_ele->Attribute("description");
					else
						modelEvent.parameterDescription = "";
					modelEvent.datasetReference = param_ele->Attribute("datasetReference");
					modelEvent.optional = childEle1->BoolAttribute("optional");
					modelEvent.multiple = childEle1->BoolAttribute("multiple");

					temp_state.modelEvents.push_back(modelEvent);
				}
				addModelState(temp_state);
			}

			const XMLElement* state_transition_ele = stateGroupEle->FirstChildElement("StateTransitions");
			for (const XMLElement* childEle = state_transition_ele->FirstChildElement(); 
				childEle; childEle = childEle->NextSiblingElement())
			{
				ModelStateTransition modelStateTransition;
				std::string fromId = childEle->Attribute("from");
				std::string toId = childEle->Attribute("to");
				addModelStateTransition(fromId, toId);
			}

			return true;
		}

	}
}