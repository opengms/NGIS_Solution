#ifndef __I_NGISMODEL_MODEL_CLASS_H__
#define __I_NGISMODEL_MODEL_CLASS_H__

#include "INxUnknown.h"
#include "IModelAttribute.h"
#include "IModelBehavior.h"
#include "IModelRuntime.h"

#include <string>

namespace NGIS
{
	namespace Model
	{
		enum EExecutionStyle
		{
			EES_STATE_SIMULATION,
			EES_SIMPLE_CALCULATION,
			EES_TIME_SERIES
		};

		class IModelClass : public INxUnknown
		{
		public:
			virtual void setName(std::string pName) = 0;

			virtual std::string getName() = 0;

			virtual void setUID(std::string pUID) = 0;

			virtual std::string getUID()  = 0;

			virtual void setExecutionStyle(EExecutionStyle pStyle) = 0;

			virtual EExecutionStyle getExecutionStyle() = 0;

			virtual IModelAttribute* getModelAttribute() = 0;

			virtual IModelBehavior* getModelBehavior() = 0;

			virtual IModelRuntime* getModelRuntime() = 0;

			virtual bool LoadFromXmlFile(const char* fileName) = 0;

			virtual bool FormatToXmlFile(const char* fileName) = 0;

			virtual bool LoadFromXmlStream(const char* xmlStr) = 0;

			virtual bool FormatToXmlStream(std::string& xmlStr) = 0;

			//////////////////////////////////////////////////////////////////////////
			virtual bool compareOther(IModelClass* pClass, bool withRuntime, std::string& obj, std::string& name) = 0;
		};
	}
}

#endif