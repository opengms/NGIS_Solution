//!文件属于 NGIS 工程的一部分
//!作者:乐松山
//!创建时间:2016年8月5日

#ifndef __NGIS_MODELING_MDL_API_H__
#define __NGIS_MODELING_MDL_API_H__

#include "IModelClass.h"

// 在windows上的api开放设置
#ifdef _WINDOWS

#ifndef _NXMODEL_STATIC_LIB_
#ifdef NXMODELMDL_EXPORTS
#define NGISMODEL_API __declspec(dllexport)
#else
#define NGISMODEL_API __declspec(dllimport)
#endif // NGISMODEL_API
#else
#define NGISMODEL_API
#endif // _NXMODEL_STATIC_LIB_

// Declare the calling convention.
#if defined(_STDCALL_SUPPORTED)
#define NGISMODEL_CALLCONV __stdcall
#else
#define NGISMODEL_CALLCONV __cdecl
#endif // STDCALL_SUPPORTED

#else // _IMP_WINDOWS_API_

// Force symbol export in shared libraries built with gcc.
#if (__GNUC__ >= 4) && !defined(_NXMODEL_STATIC_LIB_) && defined(NXMODELMDL_EXPORTS)
#define NGISMODEL_API __attribute__ ((visibility("default")))
#else
#define NGISMODEL_API
#endif
#define NGISMODEL_CALLCONV

#endif

//////////////////////////////////

// 以下用于导出对外开放的API接口
namespace NGIS
{
	namespace Model
	{
		extern "C" NGISMODEL_API const char* NGISMODEL_CALLCONV getVersion();

		extern "C" NGISMODEL_API IModelClass* NGISMODEL_CALLCONV createModelClass();
	}

}
#endif

