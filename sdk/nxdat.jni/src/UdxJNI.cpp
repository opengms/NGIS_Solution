#include "UdxJni.h"
#include "nxdat.h"
#include "nxdat.mini.api.h"

#include <string.h>
#include <vector>
#include <Windows.h>
#include "tinyxml2.h"

const dxftable_t* dxftable;

static jstring CStr2Jstring( JNIEnv* env, const char* pat)
{
	//定义java String类 strClass
	jclass strClass = (env)->FindClass("java/lang/String");
	//获取java String类方法String(byte[],String)的构造器,用于将本地byte[]数组转换为一个新String
	jmethodID ctorID = (env)->GetMethodID(strClass, "<init>", "([BLjava/lang/String;)V");
	//建立byte数组
	jbyteArray bytes = (env)->NewByteArray((jsize)strlen(pat));
	//将char* 转换为byte数组
	(env)->SetByteArrayRegion(bytes, 0, (jsize)strlen(pat), (jbyte*)pat);
	//设置String, 保存语言类型,用于byte数组转换至String时的参数
	jstring encoding = (env)->NewStringUTF("GB2312");
	//将byte数组转换为java String,并输出
	return (jstring)(env)->NewObject(strClass, ctorID, bytes, encoding);
}

char* jstringToWindows( JNIEnv *env, jstring jstr )  
{  
	int length = env->GetStringLength(jstr);  
	const jchar* jcstr = env->GetStringChars(jstr, 0);  
	char* rtn = (char*)malloc(length*2+1);  
	int size = 0;  
	size = WideCharToMultiByte(CP_ACP, 0, (LPCWSTR)jcstr, length, rtn,(length*2+1), NULL, NULL);  
	if( size <= 0 )  
		return NULL;  
	env->ReleaseStringChars(jstr, jcstr);  
	rtn[size] = 0;  
	return rtn;  
}  

jstring WindowsTojstring( JNIEnv *env, const char* str )  
{  
	jstring rtn = 0;  
	int slen = strlen(str);  
	unsigned short* buffer = 0;  
	if( slen == 0 )  
		rtn = env->NewStringUTF(str);   
	else  
	{  
		int length = MultiByteToWideChar(CP_ACP, 0, (LPCSTR)str, slen, NULL, 0);  
		buffer = (unsigned short*)malloc(length*2 + 1);  
		if( MultiByteToWideChar(CP_ACP, 0, (LPCSTR)str, slen, (LPWSTR)buffer, length) >0)  
			rtn = env->NewString((jchar*)buffer, length);  
	}  
	if(buffer)  
		free(buffer);  
	return rtn;  
}

JNIEXPORT jint JNICALL Java_com_ngis_udx_UdxHandleApi_getVersion
	(JNIEnv *env, jobject obj)
{
	return (jint)20;
}

JNIEXPORT jint JNICALL Java_com_ngis_udx_UdxHandleApi_createUdxDataset
	(JNIEnv *env, jobject obj)
{
	dxftable = nx_get_dxftable_mini(0);
	dx_object_t* dx=dxftable->fn_dx_object_create("", nxid("dataset"), dxnode, 0);

	return (jint)dx;
}

JNIEXPORT jint JNICALL Java_com_ngis_udx_UdxHandleApi_getDatesetNode
	(JNIEnv *, jobject, jint dxObj)
{
	dx_object_t* realDx = (dx_object_t*)dxObj;
	return (jint)(realDx->node);
}

JNIEXPORT jint JNICALL Java_com_ngis_udx_UdxHandleApi_addNode
	(JNIEnv *env, jobject obj, jint parentNode, jstring name, jint type, jint length)
{
	dx_node_t* realParentNode = (dx_node_t*)parentNode;
	e_dx_type_t nodeType = (e_dx_type_t)type;

	//const char* nativeName = env->GetStringUTFChars(name, 0);
	dx_node_t* node = realParentNode->dxobject->dxftable->fn_node_add_child
		(realParentNode, jstringToWindows(env, name), nodeType, length);
	//env->ReleaseStringUTFChars(name, nativeName);

	return (jint)node;
}

JNIEXPORT void JNICALL Java_com_ngis_udx_UdxHandleApi_releaseUdxDataset
	(JNIEnv *env, jobject obj, jint dxObj)
{
	dx_object_t* realDx = (dx_object_t*)dxObj;
	dxftable->fn_dx_object_release(realDx);
}

JNIEXPORT jint JNICALL Java_com_ngis_udx_UdxHandleApi_getCellLength
	(JNIEnv *env, jobject obj, jint node)
{
	dx_node_t* realNode = (dx_node_t*)node;
	int valLen = dxftable->fn_node_get_data_cell(realNode).length;
	return valLen;
}

//////////////////////////////////////////////////////////////////////////

JNIEXPORT jboolean JNICALL Java_com_ngis_udx_UdxHandleApi_setNodeIntValue
	(JNIEnv *env, jobject obj, jint node, jint value)
{
	dx_node_t* realNode = (dx_node_t*)node;
	e_dx_type_t nodeType = realNode->dxobject->dxftable->fn_node_get_type(realNode);
	dx_cell_t cell= realNode->dxobject->dxftable->fn_node_get_data_cell(realNode);
	int cellLen = cell.length;
	if (nodeType!= e_dx_type_t::dxint || cellLen != 1)
		return false;
	else
		*cell.iVal = value;
	return true;
}

JNIEXPORT jboolean JNICALL Java_com_ngis_udx_UdxHandleApi_setNodeRealValue
	(JNIEnv *env, jobject obj, jint node, jdouble value)
{
	dx_node_t* realNode = (dx_node_t*)node;
	e_dx_type_t nodeType = realNode->dxobject->dxftable->fn_node_get_type(realNode);
	dx_cell_t cell= realNode->dxobject->dxftable->fn_node_get_data_cell(realNode);
	int cellLen = cell.length;
	if (nodeType!= e_dx_type_t::dxreal || cellLen != 1)
		return false;
	else
		*cell.rVal = value;
	return true;
}

JNIEXPORT jboolean JNICALL Java_com_ngis_udx_UdxHandleApi_setNodeStringValue
	(JNIEnv *env, jobject obj, jint node, jstring value)
{
	dx_node_t* realNode = (dx_node_t*)node;
	e_dx_type_t nodeType = realNode->dxobject->dxftable->fn_node_get_type(realNode);
	dx_cell_t cell= realNode->dxobject->dxftable->fn_node_get_data_cell(realNode);
	int cellLen = cell.length;
	if (nodeType!= e_dx_type_t::dxstring || cellLen != 1)
		return false;
	else
	{
		//const char* nativeValStr = env->GetStringUTFChars(value, 0);
		
		realNode->dxobject->dxftable->fn_put_string(realNode, jstringToWindows(env, value), 0);

		//env->ReleaseStringUTFChars(value, nativeValStr);
	}
	return true;
}

JNIEXPORT jboolean JNICALL Java_com_ngis_udx_UdxHandleApi_setNodeVector2dValue
	(JNIEnv *env, jobject obj, jint node, jdouble x, jdouble y)
{
	dx_node_t* realNode = (dx_node_t*)node;
	e_dx_type_t nodeType = realNode->dxobject->dxftable->fn_node_get_type(realNode);
	dx_cell_t cell= realNode->dxobject->dxftable->fn_node_get_data_cell(realNode);
	int cellLen = cell.length;
	if (nodeType!= e_dx_type_t::dxvector2 || cellLen != 1)
		return false;
	else
	{
		cell.vec2[0].x=x;
		cell.vec2[0].y=y;
	}
	return true;
}

JNIEXPORT jboolean JNICALL Java_com_ngis_udx_UdxHandleApi_setNodeVector3dValue
	(JNIEnv *env, jobject obj, jint node, jdouble x, jdouble y, jdouble z)
{
	dx_node_t* realNode = (dx_node_t*)node;
	e_dx_type_t nodeType = realNode->dxobject->dxftable->fn_node_get_type(realNode);
	dx_cell_t cell= realNode->dxobject->dxftable->fn_node_get_data_cell(realNode);
	int cellLen = cell.length;
	if (nodeType!= e_dx_type_t::dxvector3 || cellLen != 1)
		return false;
	else
	{
		cell.vec3[0].x=x;
		cell.vec3[0].y=y;
		cell.vec3[0].z=z;
	}
	return true;
}

JNIEXPORT jboolean JNICALL Java_com_ngis_udx_UdxHandleApi_setNodeVector4dValue
	(JNIEnv *env, jobject obj, jint node, jdouble x, jdouble y, jdouble z, jdouble m)
{
	dx_node_t* realNode = (dx_node_t*)node;
	e_dx_type_t nodeType = realNode->dxobject->dxftable->fn_node_get_type(realNode);
	dx_cell_t cell= realNode->dxobject->dxftable->fn_node_get_data_cell(realNode);
	int cellLen = cell.length;
	if (nodeType!= e_dx_type_t::dxvector4 || cellLen != 1)
		return false;
	else
	{
		cell.vec4[0].x=x;
		cell.vec4[0].y=y;
		cell.vec4[0].z=z;
		cell.vec4[0].w=m;
	}
	return true;
}

//////////////////////////////////////////////////////////////////////////

JNIEXPORT jboolean JNICALL Java_com_ngis_udx_UdxHandleApi_addNodeIntArrayValue
	(JNIEnv *env, jobject obj, jint node, jint idx, jint value)
{
	dx_node_t* realNode = (dx_node_t*)node;
	e_dx_type_t nodeType = realNode->dxobject->dxftable->fn_node_get_type(realNode);
	dx_cell_t cell= realNode->dxobject->dxftable->fn_node_get_data_cell(realNode);
	int cellLen = cell.length;
	if (nodeType!= e_dx_type_t::dxint || cellLen <= 1 || idx<0 || idx>=cellLen)
		return false;
	else
		cell.iVal[idx] = value;
	return true;
}

JNIEXPORT jboolean JNICALL Java_com_ngis_udx_UdxHandleApi_addNodeRealArrayValue
	(JNIEnv *env, jobject obj, jint node, jint idx, jdouble value)
{
	dx_node_t* realNode = (dx_node_t*)node;
	e_dx_type_t nodeType = realNode->dxobject->dxftable->fn_node_get_type(realNode);
	dx_cell_t cell= realNode->dxobject->dxftable->fn_node_get_data_cell(realNode);
	int cellLen = cell.length;
	if (nodeType!= e_dx_type_t::dxreal || cellLen <= 1 || idx<0 || idx>=cellLen)
		return false;
	else
		cell.rVal[idx] = value;
	return true;
}

JNIEXPORT jboolean JNICALL Java_com_ngis_udx_UdxHandleApi_addNodeStringArrayValue
	(JNIEnv *env, jobject obj, jint node, jint idx, jstring value)
{
	dx_node_t* realNode = (dx_node_t*)node;
	e_dx_type_t nodeType = realNode->dxobject->dxftable->fn_node_get_type(realNode);
	dx_cell_t cell= realNode->dxobject->dxftable->fn_node_get_data_cell(realNode);
	int cellLen = cell.length;
	if (nodeType!= e_dx_type_t::dxstring || cellLen <= 1 || idx<0 || idx>=cellLen)
		return false;
	else
	{
		//const char* nativeValStr = env->GetStringUTFChars(value, 0);
		realNode->dxobject->dxftable->fn_put_string(realNode, jstringToWindows(env, value), idx);
		//env->ReleaseStringUTFChars(value, nativeValStr);
	}
	return true;
}

JNIEXPORT jboolean JNICALL Java_com_ngis_udx_UdxHandleApi_addNodeVector2dArrayValue
	(JNIEnv *env, jobject obj, jint node, jint idx, jdouble x, jdouble y)
{
	dx_node_t* realNode = (dx_node_t*)node;
	e_dx_type_t nodeType = realNode->dxobject->dxftable->fn_node_get_type(realNode);
	dx_cell_t cell= realNode->dxobject->dxftable->fn_node_get_data_cell(realNode);
	int cellLen = cell.length;
	if (nodeType!= e_dx_type_t::dxvector2 || cellLen <= 1 || idx<0 || idx>=cellLen)
		return false;
	else
	{
		cell.vec2[idx].x=x;
		cell.vec2[idx].y=y;
	}
	return true;
}

JNIEXPORT jboolean JNICALL Java_com_ngis_udx_UdxHandleApi_addNodeVector3dArrayValue
	(JNIEnv *env, jobject obj, jint node, jint idx, jdouble x, jdouble y, jdouble z)
{
	dx_node_t* realNode = (dx_node_t*)node;
	e_dx_type_t nodeType = realNode->dxobject->dxftable->fn_node_get_type(realNode);
	dx_cell_t cell= realNode->dxobject->dxftable->fn_node_get_data_cell(realNode);
	int cellLen = cell.length;
	if (nodeType!= e_dx_type_t::dxvector3 || cellLen <= 1 || idx<0 || idx>=cellLen)
		return false;
	else
	{
		cell.vec3[idx].x=x;
		cell.vec3[idx].y=y;
		cell.vec3[idx].z=z;
	}
	return true;
}

JNIEXPORT jboolean JNICALL Java_com_ngis_udx_UdxHandleApi_addNodeVector4dArrayValue
	(JNIEnv *env, jobject obj, jint node, jint idx, jdouble x, jdouble y, jdouble z, jdouble m)
{
	dx_node_t* realNode = (dx_node_t*)node;
	e_dx_type_t nodeType = realNode->dxobject->dxftable->fn_node_get_type(realNode);
	dx_cell_t cell= realNode->dxobject->dxftable->fn_node_get_data_cell(realNode);
	int cellLen = cell.length;
	if (nodeType!= e_dx_type_t::dxvector4 || cellLen <= 1 || idx<0 || idx>=cellLen)
		return false;
	else
	{
		cell.vec4[idx].x=x;
		cell.vec4[idx].y=y;
		cell.vec4[idx].z=z;
		cell.vec4[idx].w=m;
	}
	return true;
}

//////////////////////////////////////////////////////////////////////////

JNIEXPORT jint JNICALL Java_com_ngis_udx_UdxHandleApi_getNodeIntValue
	(JNIEnv *env, jobject obj, jint node)
{
	dx_node_t* realNode = (dx_node_t*)node;
	e_dx_type_t nodeType = realNode->dxobject->dxftable->fn_node_get_type(realNode);
	dx_cell_t cell= realNode->dxobject->dxftable->fn_node_get_data_cell(realNode);
	int cellLen = cell.length;
	if (nodeType!= e_dx_type_t::dxint || cellLen != 1)
		return 0;
	else 
		return cell.iVal[0];
}

JNIEXPORT jdouble JNICALL Java_com_ngis_udx_UdxHandleApi_getNodeRealValue
	(JNIEnv *env, jobject obj, jint node)
{
	dx_node_t* realNode = (dx_node_t*)node;
	e_dx_type_t nodeType = realNode->dxobject->dxftable->fn_node_get_type(realNode);
	dx_cell_t cell= realNode->dxobject->dxftable->fn_node_get_data_cell(realNode);
	int cellLen = cell.length;
	if (nodeType!= e_dx_type_t::dxreal || cellLen != 1)
		return 0;
	else 
		return cell.rVal[0];
}

JNIEXPORT jstring JNICALL Java_com_ngis_udx_UdxHandleApi_getNodeStringValue
	(JNIEnv *env, jobject obj, jint node)
{
	dx_node_t* realNode = (dx_node_t*)node;
	e_dx_type_t nodeType = realNode->dxobject->dxftable->fn_node_get_type(realNode);
	dx_cell_t cell= realNode->dxobject->dxftable->fn_node_get_data_cell(realNode);
	int cellLen = cell.length;
	if (nodeType!= e_dx_type_t::dxstring || cellLen != 1)
		return 0;
	else 
	{
		return WindowsTojstring(env, cell.str);
		//return CStr2Jstring(env, cell.str);
	}
}

JNIEXPORT jdoubleArray JNICALL Java_com_ngis_udx_UdxHandleApi_getNodeVector2dValue
	(JNIEnv *env, jobject obj, jint node)
{
	dx_node_t* realNode = (dx_node_t*)node;
	e_dx_type_t nodeType = realNode->dxobject->dxftable->fn_node_get_type(realNode);
	dx_cell_t cell= realNode->dxobject->dxftable->fn_node_get_data_cell(realNode);
	int cellLen = cell.length;
	if (nodeType!= e_dx_type_t::dxvector2 || cellLen != 1)
		return 0;
	else 
	{
		jdoubleArray doubleArray = env->NewDoubleArray(2);
		double *temp = new double[2];
		temp[0] = cell.vec2[0].x;
		temp[1] = cell.vec2[0].y;
		env->SetDoubleArrayRegion(doubleArray, 0, 2, (const jdouble*)temp);
		return doubleArray;
	}
}

JNIEXPORT jdoubleArray JNICALL Java_com_ngis_udx_UdxHandleApi_getNodeVector3dValue
	(JNIEnv *env, jobject obj, jint node)
{
	dx_node_t* realNode = (dx_node_t*)node;
	e_dx_type_t nodeType = realNode->dxobject->dxftable->fn_node_get_type(realNode);
	dx_cell_t cell= realNode->dxobject->dxftable->fn_node_get_data_cell(realNode);
	int cellLen = cell.length;
	if (nodeType!= e_dx_type_t::dxvector3 || cellLen != 1)
		return 0;
	else 
	{
		jdoubleArray doubleArray = env->NewDoubleArray(3);
		double *temp = new double[3];
		temp[0] = cell.vec3[0].x;
		temp[1] = cell.vec3[0].y;
		temp[2] = cell.vec3[0].z;
		env->SetDoubleArrayRegion(doubleArray, 0, 3, (const jdouble*)temp);
		return doubleArray;
	}
}

JNIEXPORT jdoubleArray JNICALL Java_com_ngis_udx_UdxHandleApi_getNodeVector4dValue
	(JNIEnv *env, jobject obj, jint node)
{
	dx_node_t* realNode = (dx_node_t*)node;
	e_dx_type_t nodeType = realNode->dxobject->dxftable->fn_node_get_type(realNode);
	dx_cell_t cell= realNode->dxobject->dxftable->fn_node_get_data_cell(realNode);
	int cellLen = cell.length;
	if (nodeType!= e_dx_type_t::dxvector4 || cellLen != 1)
		return 0;
	else 
	{
		jdoubleArray doubleArray = env->NewDoubleArray(4);
		double *temp = new double[4];
		temp[0] = cell.vec4[0].x;
		temp[1] = cell.vec4[0].y;
		temp[2] = cell.vec4[0].z;
		temp[3] = cell.vec4[0].w;
		env->SetDoubleArrayRegion(doubleArray, 0, 4, (const jdouble*)temp);
		return doubleArray;
	}
}

//////////////////////////////////////////////////////////////////////////

JNIEXPORT jint JNICALL Java_com_ngis_udx_UdxHandleApi_getNodeIntArrayValue
	(JNIEnv *env, jobject obj, jint node, jint idx)
{
	dx_node_t* realNode = (dx_node_t*)node;
	e_dx_type_t nodeType = realNode->dxobject->dxftable->fn_node_get_type(realNode);
	dx_cell_t cell= realNode->dxobject->dxftable->fn_node_get_data_cell(realNode);
	int cellLen = cell.length;
	if (nodeType!= e_dx_type_t::dxint || cellLen <= 1 || idx<0 || idx>=cellLen)
		return 0;
	else 
		return cell.iVal[idx];
}

JNIEXPORT jdouble JNICALL Java_com_ngis_udx_UdxHandleApi_getNodeRealArrayValue
	(JNIEnv *env, jobject obj, jint node, jint idx)
{
	dx_node_t* realNode = (dx_node_t*)node;
	e_dx_type_t nodeType = realNode->dxobject->dxftable->fn_node_get_type(realNode);
	dx_cell_t cell= realNode->dxobject->dxftable->fn_node_get_data_cell(realNode);
	int cellLen = cell.length;
	if (nodeType!= e_dx_type_t::dxreal || cellLen <= 1 || idx<0 || idx>=cellLen)
		return 0;
	else 
		return cell.rVal[idx];
}

JNIEXPORT jstring JNICALL Java_com_ngis_udx_UdxHandleApi_getNodeStringArrayValue
	(JNIEnv *env, jobject obj, jint node, jint idx)
{
	dx_node_t* realNode = (dx_node_t*)node;
	e_dx_type_t nodeType = realNode->dxobject->dxftable->fn_node_get_type(realNode);
	dx_cell_t cell= realNode->dxobject->dxftable->fn_node_get_data_cell(realNode);
	int cellLen = cell.length;
	if (nodeType!= e_dx_type_t::dxstring || cellLen <= 1 || idx<0 || idx>=cellLen)
		return 0;
	else 
	{
		return WindowsTojstring(env, cell.pstr[idx]);
		//return CStr2Jstring(env, cell.pstr[idx]);
	}
}

JNIEXPORT jdoubleArray JNICALL Java_com_ngis_udx_UdxHandleApi_getNodeVector2dArrayValue
	(JNIEnv *env, jobject obj, jint node, jint idx)
{
	dx_node_t* realNode = (dx_node_t*)node;
	e_dx_type_t nodeType = realNode->dxobject->dxftable->fn_node_get_type(realNode);
	dx_cell_t cell= realNode->dxobject->dxftable->fn_node_get_data_cell(realNode);
	int cellLen = cell.length;
	if (nodeType!= e_dx_type_t::dxvector2 || cellLen <= 1 || idx<0 || idx>=cellLen)
		return 0;
	else 
	{
		jdoubleArray doubleArray = env->NewDoubleArray(2);
		double *temp = new double[2];
		temp[0] = cell.vec2[idx].x;
		temp[1] = cell.vec2[idx].y;
		env->SetDoubleArrayRegion(doubleArray, 0, 2, (const jdouble*)temp);
		return doubleArray;
	}
}

JNIEXPORT jdoubleArray JNICALL Java_com_ngis_udx_UdxHandleApi_getNodeVector3dArrayValue
	(JNIEnv *env, jobject obj, jint node, jint idx)
{
	dx_node_t* realNode = (dx_node_t*)node;
	e_dx_type_t nodeType = realNode->dxobject->dxftable->fn_node_get_type(realNode);
	dx_cell_t cell= realNode->dxobject->dxftable->fn_node_get_data_cell(realNode);
	int cellLen = cell.length;
	if (nodeType!= e_dx_type_t::dxvector3 || cellLen <= 1 || idx<0 || idx>=cellLen)
		return 0;
	else 
	{
		jdoubleArray doubleArray = env->NewDoubleArray(3);
		double *temp = new double[3];
		temp[0] = cell.vec3[idx].x;
		temp[1] = cell.vec3[idx].y;
		temp[2] = cell.vec3[idx].z;
		env->SetDoubleArrayRegion(doubleArray, 0, 3, (const jdouble*)temp);
		return doubleArray;
	}
}

JNIEXPORT jdoubleArray JNICALL Java_com_ngis_udx_UdxHandleApi_getNodeVector4dArrayValue
	(JNIEnv *env, jobject obj, jint node, jint idx)
{
	dx_node_t* realNode = (dx_node_t*)node;
	e_dx_type_t nodeType = realNode->dxobject->dxftable->fn_node_get_type(realNode);
	dx_cell_t cell= realNode->dxobject->dxftable->fn_node_get_data_cell(realNode);
	int cellLen = cell.length;
	if (nodeType!= e_dx_type_t::dxvector4 || cellLen <= 1 || idx<0 || idx>=cellLen)
		return 0;
	else 
	{
		jdoubleArray doubleArray = env->NewDoubleArray(4);
		double *temp = new double[4];
		temp[0] = cell.vec4[idx].x;
		temp[1] = cell.vec4[idx].y;
		temp[2] = cell.vec4[idx].z;
		temp[3] = cell.vec4[idx].w;
		env->SetDoubleArrayRegion(doubleArray, 0, 4, (const jdouble*)temp);
		return doubleArray;
	}
}


//////////////////////////////////////////////////////////////////////////

const char* KernelType2String(e_dx_type_t pType, int len)
{
	switch(pType)
	{
	case dxint:
		{
			if (len==1) return "int";
			else return "int_array";
		}
	case dxreal:
		{
			if (len==1) return "real";
			else return "real_array";
		}
	case dxstring:
		{
			if (len==1) return "string";
			else return "string_array";
		}
	case dxvector2:
		{
			if (len==1) return "vector2d";
			else return "vector2d_array";
		}
	case dxvector3:
		{
			if (len==1) return "vector3d";
			else return "vector3d_array";
		}
	case dxvector4:
		{
			if (len==1) return "vector4d";
			else return "vector4d_array";
		}
	case dxnode:
		return "any";
	case dxlist:
		return "list";
	case dxmap:
		return "map";
	case dxtable:
		return "table";
	}
}

e_dx_type_t String2KernelType(const char* kernelType)
{
	std::string pStr = kernelType;
	if (pStr=="int" || pStr=="int_array")
		return dxint;
	else if (pStr == "real" || pStr=="real_array")
		return dxreal;
	else if (pStr == "string" || pStr == "string_array")
		return dxstring;
	else if (pStr == "vector2d" || pStr == "vector2d_array")
		return dxvector2;
	else if (pStr == "vector3d" || pStr == "vector3d_array")
		return dxvector3;
	else if (pStr == "vector4d" || pStr == "vector4d_array")
		return dxvector4;
	else if (pStr == "any")
		return dxnode;
	else if (pStr == "list")
		return dxlist;
	else if (pStr == "map")
		return dxmap;
	else if (pStr == "table")
		return dxtable;
}

void FormatXDO(dx_node_t* pNode, tinyxml2::XMLElement* element)
{
	dx_object_t* dx = pNode->dxobject;
	e_dx_type_t  kernelType = dx->dxftable->fn_node_get_type(pNode);
	nxid_t       name =dx->dxftable->fn_node_get_name(pNode);
	dx_cell_t ce = dx->dxftable->fn_node_get_data_cell(pNode);
	int valueLen = ce.length;

	tinyxml2::XMLDocument* doc = element->GetDocument();
	tinyxml2::XMLElement* childEle = doc->NewElement("XDO");
	element->LinkEndChild(childEle);
	childEle->SetAttribute("name", name);
	if (kernelType == dxint)
	{
		if (valueLen ==1)
		{
			childEle->SetAttribute("kernelType", KernelType2String(kernelType, 1));
			childEle->SetAttribute("value", ce.iVal[0]);
		}
		else if (valueLen>1)
		{
			std::string valStr = "";
			for (int iVal=0; iVal<valueLen; iVal++)
			{
				char tempValChar[100];
				if (iVal!=valueLen-1)
					sprintf_s(tempValChar, "%d, ", ce.iVal[iVal]);
				else
					sprintf_s(tempValChar, "%d", ce.iVal[iVal]);
				valStr += tempValChar;
			}
			childEle->SetAttribute("kernelType", KernelType2String(kernelType, valueLen));
			childEle->SetAttribute("value", valStr.c_str());
		}
	}
	else if (kernelType == dxreal)
	{
		if (valueLen ==1)
		{
			childEle->SetAttribute("kernelType", KernelType2String(kernelType, 1));
			childEle->SetAttribute("value", ce.rVal[0]);
		}
		else if (valueLen>1)
		{
			std::string valStr = "";
			for (int iVal=0; iVal<valueLen; iVal++)
			{
				char tempValChar[100];
				if (iVal!=valueLen-1)
					sprintf_s(tempValChar, "%lf, ", ce.rVal[iVal]);
				else
					sprintf_s(tempValChar, "%lf", ce.rVal[iVal]);
				valStr += tempValChar;
			}
			childEle->SetAttribute("kernelType", KernelType2String(kernelType, valueLen));
			childEle->SetAttribute("value", valStr.c_str());
		}
	}
	else if (kernelType == dxstring)
	{
		if (valueLen ==1)
		{
			childEle->SetAttribute("kernelType", KernelType2String(kernelType, 1));
			childEle->SetAttribute("value", ce.str);
		}
		else if (valueLen>1)
		{
			std::string valStr = "";
			for (int iVal=0; iVal<valueLen; iVal++)
			{
				char tempValChar[100];
				if (iVal!=valueLen-1)
				{
					valStr += ce.pstr[iVal];
					valStr += "; ";
				}
				else
					valStr += ce.pstr[iVal];
			}
			childEle->SetAttribute("kernelType", KernelType2String(kernelType, valueLen));
			childEle->SetAttribute("value", valStr.c_str());
		}
	}
	else if (kernelType == dxvector2)
	{
		if (valueLen ==1)
		{
			char vector2dStr[100];
			sprintf_s(vector2dStr, "%lf,%lf", ce.vec2[0].x, ce.vec2[0].y);
			childEle->SetAttribute("kernelType", KernelType2String(kernelType, 1));
			childEle->SetAttribute("value", vector2dStr);
		}
		else if (valueLen>1)
		{
			std::string valStr = "";
			for (int iVal=0; iVal<valueLen; iVal++)
			{
				char tempValChar[100];
				if (iVal!=valueLen-1)
				{
					sprintf_s(tempValChar, "%lf,%lf; ", ce.vec2[iVal].x, ce.vec2[iVal].y);
				}
				else
					sprintf_s(tempValChar, "%lf,%lf", ce.vec2[iVal].x, ce.vec2[iVal].y);
				valStr += tempValChar;
			}
			childEle->SetAttribute("kernelType", KernelType2String(kernelType, valueLen));
			childEle->SetAttribute("value", valStr.c_str());
		}
	}
	else if (kernelType == dxvector3)
	{
		if (valueLen ==1)
		{
			char vector3dStr[100];
			sprintf_s(vector3dStr, "%f,%f,%f", ce.vec3[0].x, ce.vec3[0].y, ce.vec3[0].z);
			childEle->SetAttribute("kernelType", KernelType2String(kernelType, 1));
			childEle->SetAttribute("value", vector3dStr);
		}
		else if (valueLen>1)
		{
			std::string valStr = "";
			for (int iVal=0; iVal<valueLen; iVal++)
			{
				char tempValChar[100];
				if (iVal!=valueLen-1)
				{
					sprintf_s(tempValChar, "%lf,%lf,%lf; ", ce.vec3[iVal].x, ce.vec3[iVal].y, ce.vec3[iVal].z);
				}
				else
					sprintf_s(tempValChar, "%lf,%lf,%lf", ce.vec3[iVal].x, ce.vec3[iVal].y, ce.vec3[iVal].z);
				valStr += tempValChar;
			}
			childEle->SetAttribute("kernelType", KernelType2String(kernelType, valueLen));
			childEle->SetAttribute("value", valStr.c_str());
		}
	}
	else if (kernelType == dxvector4)
	{
		if (valueLen ==1)
		{
			char vector4dStr[100];
			sprintf_s(vector4dStr, "%f,%f,%f,%f", ce.vec4[0].x, ce.vec4[0].y, ce.vec4[0].z, ce.vec4[0].w);
			childEle->SetAttribute("kernelType", KernelType2String(kernelType, 1));
			childEle->SetAttribute("value", vector4dStr);
		}
		else if (valueLen>1)
		{
			std::string valStr = "";
			for (int iVal=0; iVal<valueLen; iVal++)
			{
				char tempValChar[100];
				if (iVal!=valueLen-1)
				{
					sprintf_s(tempValChar, "%lf,%lf,%lf,%lf; ", ce.vec4[iVal].x, ce.vec4[iVal].y, ce.vec4[iVal].z, ce.vec4[iVal].w);
				}
				else
					sprintf_s(tempValChar, "%lf,%lf,%lf,%lf", ce.vec4[iVal].x, ce.vec4[iVal].y, ce.vec4[iVal].z, ce.vec4[iVal].w);
				valStr += tempValChar;
			}
			childEle->SetAttribute("kernelType", KernelType2String(kernelType, valueLen));
			childEle->SetAttribute("value", valStr.c_str());
		}
	}
	else if (kernelType == dxnode ||
		kernelType == dxlist || 
		kernelType == dxmap ||
		kernelType == dxtable)
	{
		childEle->SetAttribute("kernelType", KernelType2String(kernelType, 1));

		int count = dx->dxftable->fn_node_get_length(pNode);
		for (int iNode=0; iNode<count; iNode++)
		{
			dx_node_t* tempNode = dx->dxftable->fn_node_get_child(pNode, iNode);
			FormatXDO(tempNode, childEle);
		}
	}
}


JNIEXPORT jint JNICALL Java_com_ngis_udx_UdxHandleApi_formatToXmlFile
	(JNIEnv *env, jobject obj, jint dxObj, jstring filePath)
{
	dx_object_t* realDx = (dx_object_t*)dxObj;

	tinyxml2::XMLDocument* doc = new tinyxml2::XMLDocument();
	tinyxml2::XMLElement* element = doc->NewElement("dataset");
	doc->LinkEndChild(element);

	int count = realDx->dxftable->fn_node_get_length(realDx->node);
	for (int iNode=0; iNode<count; iNode++)
	{
		dx_node_t* tempNode = realDx->dxftable->fn_node_get_child(realDx->node, iNode);
		FormatXDO(tempNode, element);
	}
	
	doc->SaveFile(jstringToWindows(env, filePath));
	delete doc;

	return 0;
}


void split(const std::string& s, const std::string& delim, std::vector<std::string>* ret) 
{  
	size_t last = 0;   
	size_t index=s.find_first_of(delim,last);  
	while (index!=std::string::npos)  
	{   
		std::string subString = s.substr(last,index-last);
		int first_space = subString.find_first_not_of(' ');
		int last_space = subString.find_last_not_of(' ');
		subString = subString.substr(first_space, last_space-first_space+1);

		ret->push_back(subString);   
		last=index+1;    
		index=s.find_first_of(delim,last);  
	}   
	if (index-last>0)  
	{     
		std::string subString = s.substr(last,index-last);
		int first_space = subString.find_first_not_of(' ');
		int last_space = subString.find_last_not_of(' ');
		subString = subString.substr(first_space, last_space-first_space+1);

		ret->push_back(subString);
	}
}
void ParseXDO(dx_node_t* containerNode, tinyxml2::XMLElement* element, 
	JNIEnv *env, jobject javaClass, 
	jmethodID callbackMethod, jmethodID method_onPushNode, jmethodID method_onPopNode)
{
	dx_object_t* dx = containerNode->dxobject;

	std::string typeStr = element->Attribute("kernelType");
	const char* name = element->Attribute("name");
	e_dx_type_t kernelType = String2KernelType(typeStr.c_str());

	if (typeStr == "int")
	{
		int val = element->IntAttribute("value");
		int objId = env->CallIntMethod(javaClass, callbackMethod, 0, WindowsTojstring(env, name), (jint)kernelType, 1);
		dx_node_t* nx = (dx_node_t*)objId;
		dx_cell_t cell= dx->dxftable->fn_node_get_data_cell(nx);
		//dx_node_t*		nxInt=dx->dxftable->fn_node_add_child(containerNode,nxid(name),dxint,1);
		//dx_cell_t cell= dx->dxftable->fn_node_get_data_cell(nxInt);
		*cell.iVal = val;
	}
	else if (typeStr == "real")
	{
		double val = element->DoubleAttribute("value");
		int objId = env->CallIntMethod(javaClass, callbackMethod, 0, WindowsTojstring(env, name), (jint)kernelType, 1);
		dx_node_t* nx = (dx_node_t*)objId;
		dx_cell_t cell= dx->dxftable->fn_node_get_data_cell(nx);
		//dx_node_t*		nxReal=dx->dxftable->fn_node_add_child(containerNode,nxid(name),dxreal,1);
		//dx_cell_t cell= dx->dxftable->fn_node_get_data_cell(nxReal);
		*cell.rVal = val;
	}
	else if (typeStr == "string")
	{
		const char* val = element->Attribute("value");
		int objId = env->CallIntMethod(javaClass, callbackMethod, 0, WindowsTojstring(env, name), (jint)kernelType, 1);
		dx_node_t* nx = (dx_node_t*)objId;
		//dx_node_t* nx=dx->dxftable->fn_node_add_child(containerNode,nxid(name),dxstring,1);
		dx->dxftable->fn_put_string(nx, val, 0);
	}
	else if (typeStr == "vector2d")
	{
		const char* valStr = element->Attribute("value");
		double x, y;
		sscanf_s(valStr, "%lf,%lf", &x, &y);

		int objId = env->CallIntMethod(javaClass, callbackMethod, 0, WindowsTojstring(env, name), (jint)kernelType, 1);
		dx_node_t* nx = (dx_node_t*)objId;
		dx_cell_t cell= dx->dxftable->fn_node_get_data_cell(nx);
		//dx_node_t*		nx=dx->dxftable->fn_node_add_child(containerNode,nxid(name),dxvector2, 1);
		cell.vec2[0].x = x;
		cell.vec2[1].y = y;
	}
	else if (typeStr == "vector3d")
	{
		const char* valStr = element->Attribute("value");
		double x, y, z;
		sscanf_s(valStr, "%lf,%lf,%lf", &x, &y, &z);

		//dx_node_t*		nx=dx->dxftable->fn_node_add_child(containerNode,nxid(name),dxvector3, 1);
		int objId = env->CallIntMethod(javaClass, callbackMethod, 0, WindowsTojstring(env, name), (jint)kernelType, 1);
		dx_node_t* nx = (dx_node_t*)objId;
		dx_cell_t cell= dx->dxftable->fn_node_get_data_cell(nx);
		cell.vec3[0].x = x;
		cell.vec3[1].y = y;
		cell.vec3[2].z = z;
	}
	else if (typeStr == "vector4d")
	{
		const char* valStr = element->Attribute("value");
		double x, y, z, m;
		sscanf_s(valStr, "%lf,%lf,%lf,%lf", &x, &y, &z, &m);

		//dx_node_t*		nx=dx->dxftable->fn_node_add_child(containerNode,nxid(name),dxvector4, 1);
		int objId = env->CallIntMethod(javaClass, callbackMethod, 0, WindowsTojstring(env, name), (jint)kernelType, 1);
		dx_node_t* nx = (dx_node_t*)objId;
		dx_cell_t cell= dx->dxftable->fn_node_get_data_cell(nx);
		cell.vec4[0].x = x;
		cell.vec4[1].y = y;
		cell.vec4[2].z = z;
		cell.vec4[3].w = m;
	}
	else if (typeStr == "int_array")
	{
		std::string valStr = element->Attribute("value");
		std::vector<std::string> ret;
		split(valStr, ",", &ret);
		int count = ret.size();
		if (count == 1)
		{
			//dx_node_t*		nxInt=dx->dxftable->fn_node_add_child(containerNode,nxid(name),dxint,1);
			int objId = env->CallIntMethod(javaClass, callbackMethod, 0, WindowsTojstring(env, name), (jint)kernelType, count);
			dx_node_t* nx = (dx_node_t*)objId;
			dx_cell_t cell= dx->dxftable->fn_node_get_data_cell(nx);
			*cell.iVal = atoi(ret[0].c_str());
		}
		else
		{
			//dx_node_t*		nxInt=dx->dxftable->fn_node_add_child(containerNode,nxid(name),dxint, count);
			int objId = env->CallIntMethod(javaClass, callbackMethod, 0, WindowsTojstring(env, name), (jint)kernelType, count);
			dx_node_t* nx = (dx_node_t*)objId;
			dx_cell_t cell= dx->dxftable->fn_node_get_data_cell(nx);
			for (int iVal=0; iVal<count; iVal++)
			{
				cell.iVal[iVal] = atoi(ret[iVal].c_str());
			}
		}
	}
	else if (typeStr == "real_array")
	{
		std::string valStr = element->Attribute("value");
		std::vector<std::string> ret;
		split(valStr, ",", &ret);
		int count = ret.size();
		if (count == 1)
		{
			//dx_node_t*		nx=dx->dxftable->fn_node_add_child(containerNode,nxid(name),dxreal,1);
			int objId = env->CallIntMethod(javaClass, callbackMethod, 0, WindowsTojstring(env, name), (jint)kernelType, count);
			dx_node_t* nx = (dx_node_t*)objId;
			dx_cell_t cell= dx->dxftable->fn_node_get_data_cell(nx);
			*cell.rVal = atof(ret[0].c_str());
		}
		else
		{
			//dx_node_t*		nx=dx->dxftable->fn_node_add_child(containerNode,nxid(name),dxreal, count);
			int objId = env->CallIntMethod(javaClass, callbackMethod, 0, WindowsTojstring(env, name), (jint)kernelType, count);
			dx_node_t* nx = (dx_node_t*)objId;
			dx_cell_t cell= dx->dxftable->fn_node_get_data_cell(nx);
			for (int iVal=0; iVal<count; iVal++)
			{
				cell.rVal[iVal] = atof(ret[iVal].c_str());
			}
		}
	}
	else if (typeStr == "string_array")
	{
		std::string valStr = element->Attribute("value");
		std::vector<std::string> ret;
		split(valStr, ";", &ret);
		int count = ret.size();
		if (count == 1)
		{
			//dx_node_t*		nx=dx->dxftable->fn_node_add_child(containerNode,nxid(name),dxstring,1);
			int objId = env->CallIntMethod(javaClass, callbackMethod, 0, WindowsTojstring(env, name), (jint)kernelType, count);
			dx_node_t* nx = (dx_node_t*)objId;
			dx->dxftable->fn_put_string(nx, ret[0].c_str(), 0);
		}
		else
		{
			//dx_node_t*		nx=dx->dxftable->fn_node_add_child(containerNode,nxid(name),dxstring, count);
			int objId = env->CallIntMethod(javaClass, callbackMethod, 0, WindowsTojstring(env, name), (jint)kernelType, count);
			dx_node_t* nx = (dx_node_t*)objId;
			for (int iVal=0; iVal<count; iVal++)
			{
				dx->dxftable->fn_put_string(nx, ret[iVal].c_str(), iVal);
			}
		}
	}
	else if (typeStr == "vector2d_array")
	{
		std::string valStr = element->Attribute("value");
		std::vector<std::string> ret;
		split(valStr, ";", &ret);
		int count = ret.size();
		if (count == 1)
		{
			//dx_node_t*		nx=dx->dxftable->fn_node_add_child(containerNode,nxid(name),dxvector2,1);
			int objId = env->CallIntMethod(javaClass, callbackMethod, 0, WindowsTojstring(env, name), (jint)kernelType, count);
			dx_node_t* nx = (dx_node_t*)objId;
			dx_cell_t cell= dx->dxftable->fn_node_get_data_cell(nx);
			double x, y;
			sscanf_s(ret[0].c_str(), "%lf,%lf", &x, &y);
			cell.vec2[0].x = x;
			cell.vec2[0].y = y;
		}
		else
		{
			//dx_node_t*		nx=dx->dxftable->fn_node_add_child(containerNode,nxid(name),dxvector2, count);
			int objId = env->CallIntMethod(javaClass, callbackMethod, 0, WindowsTojstring(env, name), (jint)kernelType, count);
			dx_node_t* nx = (dx_node_t*)objId;
			dx_cell_t cell= dx->dxftable->fn_node_get_data_cell(nx);
			for (int iVal=0; iVal<count; iVal++)
			{
				double x, y;
				sscanf_s(ret[iVal].c_str(), "%lf,%lf", &x, &y);
				cell.vec2[iVal].x = x;
				cell.vec2[iVal].y = y;
			}
		}
	}
	else if (typeStr == "vector3d_array")
	{
		const char* valStr = element->Attribute("value");
		std::vector<std::string> ret;
		split(valStr, ";", &ret);
		int count = ret.size();
		if (count == 1)
		{
			//dx_node_t*		nx=dx->dxftable->fn_node_add_child(containerNode,nxid(name),dxvector3,1);
			int objId = env->CallIntMethod(javaClass, callbackMethod, 0, WindowsTojstring(env, name), (jint)kernelType, count);
			dx_node_t* nx = (dx_node_t*)objId;
			dx_cell_t cell= dx->dxftable->fn_node_get_data_cell(nx);
			double x, y, z;
			sscanf_s(ret[0].c_str(), "%lf,%lf,%lf", &x, &y, &z);
			cell.vec3[0].x = x;
			cell.vec3[0].y = y;
			cell.vec3[0].z = z;
		}
		else
		{
			//dx_node_t*		nx=dx->dxftable->fn_node_add_child(containerNode,nxid(name),dxvector3, count);
			int objId = env->CallIntMethod(javaClass, callbackMethod, 0, WindowsTojstring(env, name), (jint)kernelType, count);
			dx_node_t* nx = (dx_node_t*)objId;
			dx_cell_t cell= dx->dxftable->fn_node_get_data_cell(nx);
			for (int iVal=0; iVal<count; iVal++)
			{
				double x, y, z;
				sscanf_s(ret[iVal].c_str(), "%lf,%lf,%lf", &x, &y, &z);
				cell.vec3[iVal].x = x;
				cell.vec3[iVal].y = y;
				cell.vec3[iVal].z = z;
			}
		}
	}
	else if (typeStr == "vector4d_array")
	{
		std::string valStr = element->Attribute("value");
		std::vector<std::string> ret;
		split(valStr, ";", &ret);
		int count = ret.size();
		if (count == 1)
		{
			//dx_node_t*		nx=dx->dxftable->fn_node_add_child(containerNode,nxid(name),dxvector4,1);
			int objId = env->CallIntMethod(javaClass, callbackMethod, 0, WindowsTojstring(env, name), (jint)kernelType, count);
			dx_node_t* nx = (dx_node_t*)objId;
			dx_cell_t cell= dx->dxftable->fn_node_get_data_cell(nx);
			double x, y, z, m;
			sscanf_s(ret[0].c_str(), "%lf,%lf,%lf,%lf", &x, &y, &z, &m);
			cell.vec4[0].x = x;
			cell.vec4[0].y = y;
			cell.vec4[0].z = z;
			cell.vec4[0].w = m;
		}
		else
		{
			//dx_node_t*		nx=dx->dxftable->fn_node_add_child(containerNode,nxid(name),dxvector4, count);
			int objId = env->CallIntMethod(javaClass, callbackMethod, 0, WindowsTojstring(env, name), (jint)kernelType, count);
			dx_node_t* nx = (dx_node_t*)objId;
			dx_cell_t cell= dx->dxftable->fn_node_get_data_cell(nx);
			for (int iVal=0; iVal<count; iVal++)
			{
				double x, y, z, m;
				sscanf_s(ret[0].c_str(), "%lf,%lf,%lf,%lf", &x, &y, &z, &m);
				cell.vec4[iVal].x = x;
				cell.vec4[iVal].y = y;
				cell.vec4[iVal].z = z;
				cell.vec4[iVal].w = m;
			}
		}
	}
	else if (typeStr == "any" ||
		typeStr == "list" ||
		typeStr == "map" ||
		typeStr == "table")
	{
		//dx_node_t* nx=dx->dxftable->fn_node_add_child(contain erNode,nxid(name), kernelType, 1);
		int objId = env->CallIntMethod(javaClass, callbackMethod, 0, WindowsTojstring(env, name), (jint)kernelType, 1);
		dx_node_t* nx = (dx_node_t*)objId;
		env->CallVoidMethod(javaClass, method_onPushNode);
		for (tinyxml2::XMLElement* childEle = element->FirstChildElement(); childEle; childEle = childEle->NextSiblingElement())
		{
			ParseXDO(nx, childEle, env, javaClass, callbackMethod, method_onPushNode, method_onPopNode);
		}
		env->CallVoidMethod(javaClass, method_onPopNode);
	}
}

JNIEXPORT jint JNICALL Java_com_ngis_udx_UdxHandleApi_loadFromXmlFile
	(JNIEnv *env, jobject obj, jint dxObj, jstring filePath, jobject javaClass)
{
	dx_object_t* realDx = (dx_object_t*)dxObj;

	tinyxml2::XMLDocument doc;
	doc.LoadFile( jstringToWindows(env, filePath) );
	tinyxml2::XMLElement* rootEle = doc.RootElement();

	jclass callback_udxNode = env->GetObjectClass(javaClass);
	jmethodID method_onReturnAddChildNode = env->GetMethodID(callback_udxNode, "onReturnAddChildNode", "(ILjava/lang/String;II)I");
	jmethodID method_onPushNode = env->GetMethodID(callback_udxNode, "onPushNode", "()V");
	jmethodID method_onPopNode = env->GetMethodID(callback_udxNode, "onPopNode", "()V");

	for (tinyxml2::XMLElement* ele=rootEle->FirstChildElement(); ele; ele=ele->NextSiblingElement())
	{
		ParseXDO(realDx->node, ele, env, javaClass, method_onReturnAddChildNode, method_onPushNode, method_onPopNode);
	}

	return 0;
}


JNIEXPORT jstring JNICALL Java_com_ngis_udx_UdxHandleApi_formatToXmlStream
	(JNIEnv *env, jobject obj, jint dxObj)
{
	dx_object_t* realDx = (dx_object_t*)dxObj;

	tinyxml2::XMLDocument doc;
	tinyxml2::XMLElement* element = doc.NewElement("dataset");
	doc.LinkEndChild(element);

	int count = realDx->dxftable->fn_node_get_length(realDx->node);
	for (int iNode=0; iNode<count; iNode++)
	{
		dx_node_t* tempNode = realDx->dxftable->fn_node_get_child(realDx->node, iNode);
		FormatXDO(tempNode, element);
	}

	tinyxml2::XMLPrinter printer;
	doc.Print( &printer );
	std::string xml_str = printer.CStr();

	return WindowsTojstring(env, xml_str.c_str());
}

JNIEXPORT jint JNICALL Java_com_ngis_udx_UdxHandleApi_loadFromXmlStream
	(JNIEnv *env, jobject obj, jint dxObj, jstring xmlStr, jobject javaClass)
{
	dx_object_t* realDx = (dx_object_t*)dxObj;

	tinyxml2::XMLDocument doc;

	std::string xml_str = jstringToWindows(env, xmlStr);
	tinyxml2::XMLError error_info = doc.Parse(xml_str.c_str());
	tinyxml2::XMLElement* rootEle = doc.RootElement();

	jclass callback_udxNode = env->GetObjectClass(javaClass);
	jmethodID method_onReturnAddChildNode = env->GetMethodID(callback_udxNode, "onReturnAddChildNode", "(ILjava/lang/String;II)I");
	jmethodID method_onPushNode = env->GetMethodID(callback_udxNode, "onPushNode", "()V");
	jmethodID method_onPopNode = env->GetMethodID(callback_udxNode, "onPopNode", "()V");

	for (tinyxml2::XMLElement* ele=rootEle->FirstChildElement(); ele; ele=ele->NextSiblingElement())
	{
		ParseXDO(realDx->node, ele, env, javaClass, method_onReturnAddChildNode, method_onPushNode, method_onPopNode);
	}

	return 0;
}
