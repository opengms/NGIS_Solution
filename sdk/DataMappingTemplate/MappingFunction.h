#ifndef __MAPPING_FUNCTION_H__
#define __MAPPING_FUNCTION_H__

#include <string>
//#include "UdxDataSchemaApi.h"
//#include "UdxDataApi.h"

//using namespace NGIS::Data;
//using namespace NGIS::Data::Schema;

#define MAX_NODE_DEPTH 10
int g_node_read_index[MAX_NODE_DEPTH];
const char* supported_schema=NULL;

typedef struct udx_data_read
{
	unsigned (*read_to_xml)(void* buffer, unsigned buf_size);
	unsigned (*read_to_file)(void* buffer, unsigned buf_size, const char* filename);
}udx_data_read_t;

unsigned read_to_xml_imp(void* buffer, unsigned buf_size)
{
	printf("size: %d\n", buf_size);
	printf((char*)buffer);
	return buf_size;
};

unsigned read_to_file_imp(void* buffer, unsigned buf_size, const char* filename)
{
	FILE* fp = fopen(filename, "w");
	fprintf(fp, (char*)buffer);
	fclose(fp);
	return buf_size;
};

udx_data_read_t g_native_reader=
{
	read_to_xml_imp,
	read_to_file_imp
};

typedef struct udx_mapping_function_table
{
	const char*					(*fn_get_mapping_version)		(void);
	const char*					(*fn_get_mapping_name)			(void);
	const char*					(*fn_get_supported_schema)		(void);
	int								(*fn_get_capability_count)			(void);
	const char*					(*fn_get_capability_by_idx)			(int);
	bool								(*fn_set_node_read_index)			(int, int);
	bool								(*fn_read_to_node)					(const char*, const char*, const char*);
	bool								(*fn_write_from_node)				(const char*, const char*);
}udx_mapping_function_table_t, udxftable_t;

const char*		fn_get_mapping_version_impl();
const char*		fn_get_mapping_name_impl();
const char*		fn_get_supported_schema_impl();
int					fn_get_capability_count_impl();
const char*		fn_get_capability_by_idx_impl(int);
bool					fn_set_node_read_index_impl(int,int);
bool					fn_read_to_node_impl(const char*, const char*, const char*);
bool					fn_write_from_node_impl(const char*, const char*);

udxftable_t g_udxftable_imp=
{
	fn_get_mapping_version_impl,
	fn_get_mapping_name_impl,
	fn_get_supported_schema_impl,
	fn_get_capability_count_impl,
	fn_get_capability_by_idx_impl,
	fn_set_node_read_index_impl,
	fn_read_to_node_impl,
	fn_write_from_node_impl
};

//! 获取映射方法的版本
const char* fn_get_mapping_version_impl()
{
	return "1.0.0.1";
}

//! 获取映射方法的名称
const char* fn_get_mapping_name_impl()
{
	return "DemoMappingMethod";
}

//! 获取映射方法支持的UDX Schema类型
const char* fn_get_supported_schema_impl()
{
	if (supported_schema==NULL)
	{
		supported_schema = "<UdxDeclaration name=\"UdxDescription\" description=\"\">\n" \
			"  <UdxNode>\n" \
			"    <UdxNode name=\"Header\" type=\"DTKT_ANY\" description="">\n" \
			"        <UdxNode name=\"UpperLeftX\" type=\"DTKT_REAL\" description=\"\"/>\n" \
			"        <UdxNode name=\"PixelWidth\" type=\"DTKT_REAL\" description=\"\"/>\n" \
			"        <UdxNode name=\"TransParam1\" type=\"DTKT_REAL\" description=\"\"/>\n" \
			"        <UdxNode name=\"UpperLeftY\" type=\"DTKT_REAL\" description=\"\"/>\n" \
			"        <UdxNode name=\"TransParam2\" type=\"DTKT_REAL\" description=\"\"/>\n" \
			"        <UdxNode name=\"PixelHeight\" type=\"DTKT_REAL\" description=\"\"/>\n" \
			"        <UdxNode name=\"Width\" type=\"DTKT_INT\" description=\"\"/>\n" \
			"        <UdxNode name=\"Height\" type=\"DTKT_INT\" description=\"\"/>\n" \
			"        <UdxNode name=\"BandCount\" type=\"DTKT_INT\" description=\"\"/>\n" \
			"    </UdxNode>\n" \
			"    <UdxNode name=\"Bands\" type=\"DTKT_LIST\" description=\"\">\n" \
			"        <UdxNode name=\"Band_Item\" type=\"DTKT_ANY\" description=\"\">\n" \
			"          <UdxNode name=\"NoDataValue\" type=\"DTKT_REAL\" description=\"\"/>\n" \
			"          <UdxNode name=\"Scale\" type=\"DTKT_REAL\" description=\"\"/>\n" \
			"          <UdxNode name=\"Offset\" type=\"DTKT_REAL\" description=\"\"/>\n" \
			"          <UdxNode name=\"UnitType\" type=\"DTKT_STRING\" description=\"\"/>\n" \
			"          <UdxNode name=\"DataType\" type=\"DTKT_INT\" description=\"\"/>\n" \
			"          <UdxNode name=\"Value\" type=\"DTKT_LIST\" description=\"\">\n" \
			"            <UdxNode name=\"Row_Item\" type=\"DTKT_REAL | DTKT_LIST\" description=\"\"/>\n" \
			"          </UdxNode>\n" \
			"        </UdxNode>\n" \
			"    </UdxNode>\n" \
			"    <UdxNode name=\"Projection\" type=\"DTKT_STRING\" description=\"\"/>\n" \
			"  </UdxNode>\n" \
			"  <SemanticAttachment>\n" \
			"    <Concepts/>\n" \
			"    <SpatialRefs/>\n" \
			"    <Units/>\n" \
			"    <DataTemplates/>\n" \
			"  </SemanticAttachment>\n" \
			"</UdxDeclaration>\n";
	}

	return supported_schema;
}

//! 获取映射方法支持的读取节点个数
int fn_get_capability_count_impl()
{
	return 10;
}

//! 获取映射方法支持的读取节点名称
const char* fn_get_capability_by_idx_impl(int idx)
{
	if (idx==0)
		return "Header";
	if (idx==1)
		return "UpperLeftX";
	if (idx==2)
		return "PixelWidth";
	if (idx==3)
		return "TransParam1";
	if (idx==4)
		return "UpperLeftY";
	if (idx==5)
		return "TransParam2";
	if (idx==6)
		return "PixelHeight";
	if (idx==7)
		return "Width";
	if (idx==8)
		return "Height";
	if (idx==9)
		return "BandCount";
	else 
		return "";
}

//! 设置按照Schema读取数据时，每个List节点的索引
bool fn_set_node_read_index_impl(int node_idx, int read_idx)
{
	if (node_idx>=MAX_NODE_DEPTH)
		return false;

	g_node_read_index[node_idx] = read_idx;
	return true;
}

//! 从原始数据中读取到UDX中
bool fn_read_to_node_impl(const char* format_filename, const char* node_name, const char* save_filename)
{
	//! 根据g_node_read_index的信息，来确定读取List节点中的哪些节点
	std::string name = node_name;
	if (name == "header")
	{
		//! 加载Geotif数据
		//! 根据name读取数据内容

		std::string xml_str="";
		xml_str += "<dataset>\n";
		xml_str += "  <XDO name=\"Header\" kernelType=\"any\">\n";
		xml_str += "      <XDO name=\"UpperLeftX\" kernelType=\"real\" value=\"471090.08257249475\"/>\n";
		xml_str += "      <XDO name=\"PixelWidth\" kernelType=\"real\" value=\"30\"/>\n";
		xml_str += "      <XDO name=\"TransParam1\" kernelType=\"real\" value=\"0\"/>\n";
		xml_str += "      <XDO name=\"UpperLeftY\" kernelType=\"real\" value=\"231352.35339681862\"/>\n";
		xml_str += "      <XDO name=\"TransParam2\" kernelType=\"real\" value=\"0\"/>\n";
		xml_str += "      <XDO name=\"PixelHeight\" kernelType=\"real\" value=\"-30\"/>\n";
		xml_str += "      <XDO name=\"Width\" kernelType=\"int\" value=\"786\"/>\n";
		xml_str += "      <XDO name=\"Height\" kernelType=\"int\" value=\"767\"/>\n";
		xml_str += "      <XDO name=\"BandCount\" kernelType=\"int\" value=\"1\"/>\n";
		xml_str += "  </XDO>\n";
		xml_str += "<dataset>";

		if (save_filename==NULL)
			g_native_reader.read_to_xml((void*)xml_str.c_str(), xml_str.size());
		else
			g_native_reader.read_to_file((void*)xml_str.c_str(), xml_str.size(), save_filename);
	}
	else
	{
		printf("Error");
	}

	return true;
}

//! 从UDX数据写入到原始数据中
bool fn_write_from_node_impl(const char* udx_filename, const char* save_filename)
{
	//! Validate Schema

	//! Load UDX
	FILE* fp = fopen(save_filename, "w");
	fprintf(fp,"test");
	fclose(fp);

	return true;
}


#endif
