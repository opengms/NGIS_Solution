#include <stdio.h>
#include <conio.h>
#include "MappingFunction.h"

void do_help()
{
	printf("Usage : DataMappingTemplate.exe [-h] [-v] [-c] [-u] udx.xml [-f] format.* [-i] [1,1,1]\n\n" \
		"  -h  Show help information\n" \
		"  -v  Show version\n" \
		"  -c  List Capabilitier\n" \
		"  -u  UDX file path\n" \
		"  -f   Original format file path\n" \
		"  -n  Name of try read node\n" \
		"  -i   Set read index for each related node\n\n");
}

void main(int argc, char *argv[])
{
	int i = 0;
	const char* udx_filename=NULL;
	const char* format_filename=NULL;
	const char* node_idx_array=NULL;
	const char* read_node_name=NULL;

	int read_write_flag = -1; //0表示读UDX，1表示写原始数据

	if (argc == 1)
	{
		printf("**************Data Mapping Method**************\n");
		printf("version: %s \n", g_udxftable_imp.fn_get_mapping_version());
	}
	else
	{
		for (i=1; i<argc; i++)
		{
			if ((*argv[i])=='-')
			{
				const char *p=argv[i]+1;

				while ((*p)!='\0')
				{
					char c=*(p++);;
					if ((c=='h') || (c=='H'))
					{
						do_help();
					}
					else if ((c=='v') || (c=='V'))
					{
						printf("version: %s \n", g_udxftable_imp.fn_get_mapping_version());
					}
					else if ((c=='s') || (c=='S'))
					{
						printf("Supported Schema: \n%s\n", g_udxftable_imp.fn_get_supported_schema());
					}
					else if ((c=='c') || (c=='C'))
					{
						int capablity_count = g_udxftable_imp.fn_get_capability_count();
						printf("capability count: %d \n", capablity_count);
						for (int cap=0; cap<capablity_count; cap++)
						{
							const char* cap_name = g_udxftable_imp.fn_get_capability_by_idx(cap);
							printf("capability name: %s \n", cap_name);
						}
					}
					else if ((c=='w') || (c=='W'))
					{
						read_write_flag = 1;
					}
					else if ((c=='r') || (c=='R'))
					{
						read_write_flag = 0;
					}
					else if ((c=='u') || (c=='U'))
					{
						udx_filename=argv[i+1];
						printf("UDX file name: %s\n", udx_filename);
					}
					else if ((c=='f') || (c=='F'))
					{
						format_filename=argv[i+1];
						printf("format file name: %s\n", format_filename);
					}
					else if ((c=='i') || (c=='I'))
					{
						node_idx_array=argv[i+1];
						std::string node_idx_array_str = node_idx_array;
						node_idx_array_str.find(',');
						printf("node index: %s\n", node_idx_array_str.c_str());
					}
					else if ((c=='n') || (c=='N'))
					{
						read_node_name=argv[i+1];
						printf("read node name: %s\n", read_node_name);
					}
					else
					{
						do_help();
						return;
					}
				}
			}
			else
			{
			}
		}
	}

	if (read_write_flag==0 && format_filename!=NULL && read_node_name!=NULL)
	{
		printf("executing read udx node from formated file\n");
		g_udxftable_imp.fn_read_to_node(format_filename, read_node_name, udx_filename);
	}
	if (read_write_flag==1 && udx_filename!=NULL && format_filename!=NULL)
	{
		printf("executing write formated file from udx node\n");
		g_udxftable_imp.fn_write_from_node(udx_filename, format_filename);
	}
}