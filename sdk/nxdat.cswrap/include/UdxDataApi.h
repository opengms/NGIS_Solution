//!文件属于 NGIS 工程的一部分
//!作者:乐松山
//!创建时间:2016年8月5日

#ifndef __NGIS_UDX_API_H__
#define __NGIS_UDX_API_H__

// 在windows上的api开放设置
#ifdef _WINDOWS

#ifndef _NGISDATA_STATIC_LIB_
#ifdef NXDATCSWRAP_EXPORTS
#define NGISDATA_API __declspec(dllexport)
#else
#define NGISDATA_API __declspec(dllimport)
#endif // NGISDATA_API
#else
#define NGISDATA_API
#endif // _NGISDATA_STATIC_LIB_

// Declare the calling convention.
#if defined(_STDCALL_SUPPORTED)
#define NGISDATA_CALLCONV __stdcall
#else
#define NGISDATA_CALLCONV __cdecl
#endif // STDCALL_SUPPORTED

#else // _IMP_WINDOWS_API_

// Force symbol export in shared libraries built with gcc.
#if (__GNUC__ >= 4) && !defined(_NGISDATA_STATIC_LIB_) && defined(NGISDATA_EXPORTS)
#define NGISDATA_API __attribute__ ((visibility("default")))
#else
#define NGISDATA_API
#endif
#define NGISDATA_CALLCONV

#endif

//////////////////////////////////

// 以下用于导出对外开放的API接口
namespace NGIS
{
	namespace Data
	{
		extern "C" NGISDATA_API int NGISDATA_CALLCONV getVersion();

		extern "C" NGISDATA_API int NGISDATA_CALLCONV createUdxDataset(const char* pName);

		extern "C" NGISDATA_API int NGISDATA_CALLCONV getDatesetNode(int dxObj);

		extern "C" NGISDATA_API int NGISDATA_CALLCONV addNode(int parentNode, const char* name, int type, int length);

		extern "C" NGISDATA_API void NGISDATA_CALLCONV releaseUdxDataset(int dxObj);

		extern "C" NGISDATA_API int NGISDATA_CALLCONV getCellLength(int node);


		//////////////////////////////////////////////////////////////////////////
		extern "C" NGISDATA_API bool NGISDATA_CALLCONV setNodeIntValue(int node, int value);

		extern "C" NGISDATA_API bool NGISDATA_CALLCONV setNodeRealValue(int node, double value);

		extern "C" NGISDATA_API bool NGISDATA_CALLCONV setNodeStringValue(int node, const char* value);

		extern "C" NGISDATA_API bool NGISDATA_CALLCONV setNodeVector2dValue(int node, double x, double y);

		extern "C" NGISDATA_API bool NGISDATA_CALLCONV setNodeVector3dValue(int node, double x, double y, double z);

		extern "C" NGISDATA_API bool NGISDATA_CALLCONV setNodeVector4dValue(int node, double x, double y, double z, double m);

		//////////////////////////////////////////////////////////////////////////
		extern "C" NGISDATA_API bool NGISDATA_CALLCONV addNodeIntArrayValue(int node, int idx, int value);

		extern "C" NGISDATA_API bool NGISDATA_CALLCONV addNodeRealArrayValue(int node, int idx, double value);

		extern "C" NGISDATA_API bool NGISDATA_CALLCONV addNodeStringArrayValue(int node, int idx, const char* value);

		extern "C" NGISDATA_API bool NGISDATA_CALLCONV addNodeVector2dArrayValue(int node, int idx, double x, double y);

		extern "C" NGISDATA_API bool NGISDATA_CALLCONV addNodeVector3dArrayValue(int node, int idx, double x, double y, double z);

		extern "C" NGISDATA_API bool NGISDATA_CALLCONV addNodeVector4dArrayValue(int node, int idx, double x, double y, double z, double m);

		//////////////////////////////////////////////////////////////////////////
		extern "C" NGISDATA_API int NGISDATA_CALLCONV getNodeIntValue(int node);

		extern "C" NGISDATA_API double NGISDATA_CALLCONV getNodeRealValue(int node);

		extern "C" NGISDATA_API const char* NGISDATA_CALLCONV getNodeStringValue(int node);

		extern "C" NGISDATA_API bool NGISDATA_CALLCONV getNodeVector2dValue(int node, double& x, double& y);

		extern "C" NGISDATA_API bool NGISDATA_CALLCONV getNodeVector3dValue(int node, double& x, double& y, double& z);

		extern "C" NGISDATA_API bool NGISDATA_CALLCONV getNodeVector4dValue(int node, double& x, double& y, double& z, double& m);

		//////////////////////////////////////////////////////////////////////////
		extern "C" NGISDATA_API int NGISDATA_CALLCONV getNodeIntArrayValue(int node, int idx);

		extern "C" NGISDATA_API double NGISDATA_CALLCONV getNodeRealArrayValue(int node, int idx);

		extern "C" NGISDATA_API const char* NGISDATA_CALLCONV getNodeStringArrayValue(int node, int idx);

		extern "C" NGISDATA_API bool NGISDATA_CALLCONV getNodeVector2dArrayValue(int node, int idx, double& x, double& y);

		extern "C" NGISDATA_API bool NGISDATA_CALLCONV getNodeVector3dArrayValue(int node, int idx, double& x, double& y, double& z);

		extern "C" NGISDATA_API bool NGISDATA_CALLCONV getNodeVector4dArrayValue(int node, int idx, double& x, double& y, double& z, double& m);

		//////////////////////////////////////////////////////////////////////////
		typedef int (__stdcall *onReturnAddChildNode)(int nodeObj, const char* pName, int pType, int pLength);
		typedef void (__stdcall *onPushNode)();
		typedef void (__stdcall *onPopNode)();

		extern "C" NGISDATA_API int NGISDATA_CALLCONV formatToXmlFile(int dxObj, const char* filePath);

		extern "C" NGISDATA_API int NGISDATA_CALLCONV loadFromXmlFile(int dxObj, const char* filePath, 
			onReturnAddChildNode callback_onReturnAddChildNode, 
			onPushNode callback_onPushNode,
			onPopNode callback_onPopNode);

		extern "C" NGISDATA_API const char* NGISDATA_CALLCONV formatToXmlStream(int dxObj);

		extern "C" NGISDATA_API int NGISDATA_CALLCONV loadFromXmlStream(int dxObj, const char* xmlStr,
			onReturnAddChildNode callback_onReturnAddChildNode, 
			onPushNode callback_onPushNode,
			onPopNode callback_onPopNode);
	}
}

#endif

