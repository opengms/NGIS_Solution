#ifndef __UX_DAT_MINI_API
#define __UX_DAT_MINI_API
#include "nxdat.h"
#ifndef NXDAT_MINI_EXPORT
	#define NXDAT_MINI_API __declspec(dllimport)
#else 
	#define NXDAT_MINI_API __declspec(dllexport)
#endif

#ifdef __cplusplus	//C++
	extern "C" 
	{
#endif
		NXDAT_MINI_API
		const dxftable_t* (nx_get_dxftable_mini)(const char* argv);
		
		typedef struct dx_stream_write	//write stream interface for dx
		{
			void* handle;
			unsigned (*write)(void* h,void* pbuf,unsigned byteSize);
		}dx_stream_write_t;
				
		typedef struct dx_stream_read	//read stream interface for dx
		{
			void* handle;
			unsigned (*read)(void* h,void* pbuf,unsigned byteSize);
		}dx_stream_read_t;

		typedef struct dx_node_meta_lib	//a interface for meta lib to help dx io
		{
			void* lib;
			const dx_node_meta* (*meta)(void* lib,nxid_t id);
		}dx_node_meta_lib_t;

		NXDAT_MINI_API
			int dx_write_node_to_bin(dx_stream_write_t* stream,dx_node_t* node);

		NXDAT_MINI_API
			dx_node_t* dx_read_node_from_bin(dx_stream_read_t* stream,dx_node_t* parent,dx_node_meta_lib_t* metalib);

		NXDAT_MINI_API
			int dx_write_node_to_json(dx_stream_write_t* stream, dx_node_t* node);

		NXDAT_MINI_API
			dx_node_t* dx_read_node_from_json(dx_stream_read_t* stream, dx_node_t* parent, dx_node_meta_lib_t* metalib);

		NXDAT_MINI_API
			int dx_write_node_to_xml(dx_stream_write_t* stream, dx_node_t* node);

		NXDAT_MINI_API
			dx_node_t* dx_read_node_from_xml(dx_stream_read_t* stream, dx_node_t* parent, dx_node_meta_lib_t* metalib);

#ifdef __cplusplus	//C++
	}
#endif
	
#endif