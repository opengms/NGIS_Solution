#include "nxdat_bin_io.h"

namespace dx{

	class c_dx_io_bin_write_t
	{
		typedef unsigned short ushort;
		typedef unsigned char  uchar;
	public:

		//!构造函数
		c_dx_io_bin_write_t(dx_stream_write_t* stream,dx_node_t* dx)
			:_length(0),_stream(stream)
		{
			_stream  = stream;
			cacheid(dx);
			//static char mask[]={'u','d','x','.','0','1','0','0'};
			//write(mask,8);				  _length+=8;
			ushort len = _ids.size();
			write(&len,sizeof(ushort));	  _length+=sizeof(ushort);
			for(int i=0;i<len;i++)
			{
				ushort sz=strlen(_ids[i])+1;
				write(&sz,sizeof(ushort));_length+=sizeof(ushort);
				write((void*)_ids[i],sz); _length+=sz;
			}
			write_node(dx);
		}
		//!析构函数
		~c_dx_io_bin_write_t(){}
		
		int					_length;
	public:
		inline void cacheid(dx_node_t* dx)
		{
			nxid_t id= dxutils::get_name(dx);
			idx(id);

			const dx_node_meta_t* meta = dxutils::get_meta(dx);
			if(meta)idx(meta->name);
			if(!dxutils::is_data_cell(dx))
			{
				for(int i=0,n=dxutils::get_length(dx);i<n;i++)
				{
					cacheid(dxutils::get_child(dx,i));
				}
			}
		}

		inline void write_node(dx_node_t* dx)
		{
			uchar ty = dxutils::get_type(dx);
			ushort nid = idx(dxutils::get_name(dx));
			ushort metaid = 0xFFFF;
			const dx_node_meta_t* meta = dxutils::get_meta(dx);
			if(meta)metaid=idx(meta->name);
			write(&nid,sizeof(ushort));		_length+=sizeof(ushort);
			write(&metaid,sizeof(ushort));	_length+=sizeof(ushort);
			write(&ty,sizeof(uchar));		_length+=sizeof(uchar);
			unsigned len = dxutils::get_length(dx);
			write(&len,sizeof(unsigned));	_length+=sizeof(unsigned);	
			if(dxutils::is_data_cell(dx))
			{
				dx_cell_t cell = dxutils::get_data_cell(dx);
				if(ty==dxint)
					{ write(cell.pdata,sizeof(dx_int_t)*len);	  _length+=sizeof(dx_int_t)*len;}	
				else if(ty==dxreal)
					{ write(cell.pdata,sizeof(dx_real_t)*len);	  _length+=sizeof(dx_real_t)*len;}	
				else if(ty==dxvector2)
					{ write(cell.pdata,sizeof(dx_vector2_t*)*len);_length+=sizeof(dx_vector2_t)*len;}	
				else if(ty==dxvector3)
					{ write(cell.pdata,sizeof(dx_vector3_t*)*len);_length+=sizeof(dx_vector3_t)*len;}	
				else if(ty==dxvector4)
					{ write(cell.pdata,sizeof(dx_vector4_t*)*len);_length+=sizeof(dx_vector4_t)*len;}
				else if(ty==dxstring)
				{
					if(len==1)
					{
						ushort sz = strlen(cell.str)+1;
						write(&sz,sizeof(ushort));	_length+=sizeof(ushort);
						write((void*)cell.str,sz);  _length+=sz;
					}
					else
					{
						for(int i=0;i<len;i++)
						{
							ushort sz = strlen(cell.pstr[i])+1;
							write(&sz,sizeof(ushort));			_length+=sizeof(ushort);
							write((void*)cell.pstr[i],sz);		_length+=sz;
						}
					}
				}		
				else if(ty==dxwstring) 
				{
					if(len==1)
					{
						ushort sz = wcslen(cell.wstr)+1;
						write(&sz,sizeof(ushort));				_length+=sizeof(ushort);
						write((void*)cell.wstr,sz*2);			_length+=sz*2;
					}
					else
					{
						for(int i=0;i<len;i++)
						{
							ushort sz = wcslen(cell.pwstr[i])+1;
							write(&sz,sizeof(ushort));			_length+=sizeof(ushort);
							write((void*)cell.pwstr[i],sz*2);	_length+=sz*2;
						}
					}
				}
				else
				{
					_ASSERT(false);
				}
			}
			else
			{
				for(int i=0;i<len;i++)
				{
					dx_node_t* ch = dxutils::get_child(dx,i);
					write_node(ch);
				}
			}
		}

		inline ushort idx(nxid_t id){
			for(int i=0,n = _ids.size();i<n;i++)
				if(_ids[i] == id)return i;
			_ids.push_back(id);
			return _ids.size()-1;
		}
		inline void  write(void* pbuf,int byteSize)
		{
			_stream->write(_stream->handle,pbuf,byteSize);
		}
		dx_stream_write_t* _stream;
		vector<nxid_t>	   _ids;
	};
	class c_dx_io_bin_read_t
	{
		typedef unsigned short ushort;
		typedef unsigned char  uchar;
	public:
		c_dx_io_bin_read_t(dx_stream_read_t* stream, dx_node_t* panode,dx_node_meta_lib_t* metalib)
			:_stream(stream),_metalib(metalib),_node(NULL)
		{
			//static char mask[]={'u','d','x','.','0','1','0','0'};
			//write(mask,8);				  _length+=8;

			read_id_cache();
			_node = read_next_node(panode);
		}
	public:
		inline void read_id_cache()
		{
			ushort len =0;
			read(&len,sizeof(ushort));
			for(int i=0;i<len;i++)
			{
				ushort sz=0;
				read(&sz,sizeof(ushort));
				_ASSERT(sz<0xFFFF-1);
				static char stringBuf[0xFFFF];
				read(stringBuf,sz);
				_ids.push_back(nxid(stringBuf));
			}
		}

		inline dx_node_t* read_next_node(dx_node_t* panode)
		{
			uchar	ty=0;
			ushort	nid=0;
			ushort	metaid = 0xFFFF;
			const dx_node_meta_t* meta = NULL;
			unsigned len;
			read(&nid,sizeof(ushort));
			read(&metaid,sizeof(ushort));
			read(&ty,sizeof(uchar));
			read(&len,sizeof(unsigned));

			nxid_t _nid = _ids[nid];
			nxid_t _metaid = 0;
			if(metaid!=0xFFFF && _metalib)
			{
				_metaid = _ids[metaid];
				meta = _metalib->meta(_metalib->lib,_metaid);
				if(meta)
				{
					_ASSERT(meta->type == ty);
				}
			}
			dx_node_t* node = NULL;
			if(ty<dxnode)
			{
				if(!meta)
					node = dxutils::add_child(panode,_nid,(e_dx_type_t)ty,len);
				else
					node = dxutils::add_child(panode,meta,len);

				dx_cell_t cell = dxutils::get_data_cell(node);
				if(ty==dxint)
					{ read(cell.pdata,sizeof(dx_int_t)*len);}	
				else if(ty==dxreal)
					{ read(cell.pdata,sizeof(dx_real_t)*len);}	
				else if(ty==dxvector2)
					{ read(cell.pdata,sizeof(dx_vector2_t*)*len);}	
				else if(ty==dxvector3)
					{ read(cell.pdata,sizeof(dx_vector3_t*)*len);}	
				else if(ty==dxvector4)
					{ read(cell.pdata,sizeof(dx_vector4_t*)*len);}
				else if(ty==dxstring)
				{
						for(int i=0;i<len;i++)
						{
							ushort sz =0;//
							read(&sz,sizeof(ushort));
							static char stringBuf[0xFFFF];
							read(stringBuf,sz);
							dxutils::put_string(node,stringBuf,i);
						}
				}		
				else if(ty==dxwstring) 
				{
					for(int i=0;i<len;i++)
					{
						ushort sz =0;
						read(&sz,sizeof(ushort));
						static wchar_t stringBuf[0xFFFF];
						read(stringBuf,sz*2);
						dxutils::put_wstring(node,stringBuf,i);
					}
				}
				else
				{
					_ASSERT(false);
				}

			}
			else
			{
				if(!meta)
					node = dxutils::add_child(panode,_nid,(e_dx_type_t)ty,0);
				else
					node = dxutils::add_child(panode,meta,0);
				for(int i=0;i<len;i++)
				{
					read_next_node(node);
				}
			}
			return node;
		}

		inline unsigned read(void* pbuf,unsigned byteSize)
		{
			return _stream->read(_stream->handle,pbuf,byteSize);
		}
	public:
		dx_stream_read_t*  _stream;
		vector<nxid_t>	   _ids;
		dx_node_meta_lib*  _metalib;
		dx_node_t*		   _node;
	};



}	
extern "C"{
	NXDAT_MINI_API
		int dx_write_node_to_bin(dx_stream_write_t* stream,dx_node_t* node)
	{
		dx::c_dx_io_bin_write_t write_(stream,node);
		return write_._length;
	}

	NXDAT_MINI_API
		dx_node_t* dx_read_node_from_bin(dx_stream_read_t* stream,dx_node_t* parent,dx_node_meta_lib_t* metalib)
	{
		dx::c_dx_io_bin_read_t read_(stream,parent,metalib);
		return read_._node;
	}
}