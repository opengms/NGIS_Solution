#ifndef __UX_DAT_DIRECT_H__
#define __UX_DAT_DIRECT_H__
#include "nxdat.h"
#include <vector>
using namespace std;
namespace dx{
	
	class c_dx_datset_mini_t;
	//!Ux节点的基类
	class c_dx_node_mini_t:public dx_node_t
	{
	public:
		c_dx_node_mini_t(nxid_t name):m_name(name),m_meta(NULL){dxobject=NULL;}
		//!析构函数
		virtual ~c_dx_node_mini_t(){}
		//!节点名称
		nxid_t						name(){return m_name;}
		//!长度
		virtual int					length()=0;
		//!类型
		virtual e_dx_type_t			type()=0;
		//!获得UxCell
		virtual dx_cell_t			cell()=0;
		//!获取子节点
		virtual c_dx_node_mini_t*	item(int index)=0;
		//!修改长度
		virtual void			modifyLength(int len) =0;
		//!添加tag
		virtual	nxid_t				get_tag(int ix){return m_tags[ix];}
		//!得到tag的个数
		virtual int					get_tag_count(){return m_tags.size();}
		//!添加tag
		virtual void				add_tag(nxid_t tag){m_tags.push_back(tag);}
		//!获得元信息
		virtual const dx_node_meta_t*		get_meta(){return m_meta;}
		//!设置meta
		virtual void				put_meta(const dx_node_meta_t* m){m_meta=m;}
	public:
		nxid_t						m_name;
		const dx_node_meta_t*		m_meta;
		vector<nxid_t>				m_tags;
	};

	//!数据节点
	class c_dx_cell_t:public c_dx_node_mini_t
	{
	public:
		c_dx_cell_t(nxid_t name):c_dx_node_mini_t(name){}
		//!析构函数
		virtual ~c_dx_cell_t(){}
		//!长度
		virtual int				length()=0;
		//!获取子节点
		virtual c_dx_node_mini_t*	item(int index){return NULL;}
		//!修改长度
		virtual void			modifyLength(int len) {};
	};

	//!数据节点的实现模板
	template<class cellT,e_dx_type_t enumT>
	class c_dx_cell_impl_tt:public c_dx_cell_t
	{
	public:
		c_dx_cell_impl_tt(nxid_t name):c_dx_cell_t(name){
			memset(&m_cell_data,0,sizeof(m_cell_data));
		}
		//!长度
		virtual int		length(){return 1;}
		//!类型
		e_dx_type_t		type()	{return enumT;}
		//!获得UxCell
		dx_cell_t			cell(){
			dx_cell_t cell;
			cell.node = this;
			cell.length=1;
			cell.type = type();
			cell.pdata=&m_cell_data;
			return cell;
		}
	public:
		cellT	m_cell_data;
	};

	//!数据节点的实现模板
	template<class cellT,e_dx_type_t enumT>
	class c_dx_cell_array_impl_tt:public c_dx_cell_t
	{
	public:
		c_dx_cell_array_impl_tt(nxid_t name,int len):c_dx_cell_t(name){
			m_data_cell_array.resize(len);
		}
		//!长度
		virtual int				length(){return m_data_cell_array.size();}
		//!类型
		virtual e_dx_type_t		type()	{return enumT;}
		//!获得UxCell
		virtual dx_cell_t		cell()
		{
			dx_cell_t cell;
			cell.node = this;
			cell.length=m_data_cell_array.size();
			cell.type = type();
			cell.pdata=&m_data_cell_array[0];
			return cell;
		}
		//!修改长度
		virtual void			modifyLength(int len) 
		{
			m_data_cell_array.resize(len);
		};
	public:
		vector<cellT>	m_data_cell_array;
	};

	//容器节点
	class c_dx_xnode_t:public c_dx_node_mini_t
	{
	public:
		//!构造函数
		c_dx_xnode_t(nxid_t name);
		//!析构函数
		~c_dx_xnode_t();
		//!长度
		virtual int						length();
		//!获得UxCell
		virtual dx_cell_t				cell();
		//!获取子节点
		virtual c_dx_node_mini_t*		item(int index);
		//!修改长度
		virtual void			modifyLength(int len) {};
		//!添加新的数据
		c_dx_node_mini_t*				add(nxid_t name,e_dx_type_t typ,int len,const dx_node_meta_t* meta);

	protected:
		vector<c_dx_node_mini_t*>		m_children;
	};

	template<e_dx_type_t enumT>
	class c_dx_xnode_impl_tt:public c_dx_xnode_t
	{
	public:
		//!构造函数
		c_dx_xnode_impl_tt(nxid_t name):c_dx_xnode_t(name){}
		//!析构函数
		~c_dx_xnode_impl_tt(){}
		//!类型
		virtual e_dx_type_t		type()	{return enumT;}
	};

	//字符串
	template<class stringT,class chT,e_dx_type_t enumT>
	class c_dx_cell_string_tt:public c_dx_cell_t
	{
	public:
		c_dx_cell_string_tt(nxid_t name):c_dx_cell_t(name){
		}
		//!长度
		virtual int		length(){return 1;}
		//!类型
		e_dx_type_t		type()	{return enumT;}
		//!获得UxCell
		dx_cell_t		cell(){
			dx_cell_t cell;
			cell.node = this;
			cell.length=1;
			cell.type = type();
			cell.pdata=(void*)m_string.c_str();
			return cell;
		}
	public:
		void put_string(const chT* str){
			m_string=str;	
		}
	protected:
		stringT m_string;
	};
	
	//字符串
	template<class stringT,class chT,e_dx_type_t enumT>
	class c_dx_cell_string_array_tt:public c_dx_cell_t
	{
	public:
		c_dx_cell_string_array_tt(nxid_t name,int len):c_dx_cell_t(name){
			m_string_list.resize(len);
			m_string_list_ptr.resize(len,NULL);
		}
		//!长度
		virtual int			length(){return m_string_list.size();}
		//!类型
		e_dx_type_t			type()	{return enumT;}
		//!获得UxCell
		dx_cell_t			cell(){
			dx_cell_t cell;
			cell.node = this;
			cell.length=m_string_list.size();
			cell.type = type();
			cell.pdata=&m_string_list_ptr[0];
			return cell;
		}
		//!修改长度
		virtual void			modifyLength(int len) 
		{
			m_string_list.resize(len);
			m_string_list_ptr.resize(len,NULL);
		};
	public:
		void put_string(const chT* str,int ix){
			m_string_list[ix]=str;
			m_string_list_ptr[ix]=m_string_list[ix].c_str();
		}
	protected:
		vector<stringT>		m_string_list;
		vector<const chT*>	m_string_list_ptr;
	};
}

#endif