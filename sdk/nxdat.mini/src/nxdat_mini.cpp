#include "nxdat.h"
#include "nxdat.mini.api.h"
#include "nxdat_mini.h"
namespace dx{

	//!构造函数
	c_dx_xnode_t::c_dx_xnode_t(nxid_t name)
		:c_dx_node_mini_t(name)
	{

	}

	//!析构函数
	c_dx_xnode_t::~c_dx_xnode_t()
	{
		for(int i=0,n=m_children.size();i<n;i++)
		{
			delete m_children[i];
		}
		m_children.clear();
	}

	//!长度
	int	c_dx_xnode_t::length()
	{
		return m_children.size();
	}

	//!获得UxCell
	dx_cell_t	c_dx_xnode_t::cell()
	{
		dx_cell_t cell;
		cell.length=length();
		cell.node =this;
		cell.pdata=NULL;
		cell.type = type();
		return cell;
	}

	//!获取子节点
	c_dx_node_mini_t*		c_dx_xnode_t::item(int index)
	{
		_ASSERT(index<m_children.size());
		return m_children[index];
	}

	typedef c_dx_cell_impl_tt<dx_int_t,		dxint>		c_dx_cell_int_t;
	typedef c_dx_cell_impl_tt<dx_real_t,	dxreal>		c_dx_cell_real_t;
	typedef c_dx_cell_impl_tt<dx_vector2_t,	dxvector2>	c_dx_cell_vector2_t;
	typedef c_dx_cell_impl_tt<dx_vector3_t,	dxvector3>	c_dx_cell_vector3_t;
	typedef c_dx_cell_impl_tt<dx_vector4_t,	dxvector4>	c_dx_cell_vector4_t;

	typedef c_dx_cell_array_impl_tt<dx_int_t,		dxint>		c_dx_cell_int_array_t;
	typedef c_dx_cell_array_impl_tt<dx_real_t,		dxreal>		c_dx_cell_real_array_t;
	typedef c_dx_cell_array_impl_tt<dx_vector2_t,	dxvector2>	c_dx_cell_vector2_array_t;
	typedef c_dx_cell_array_impl_tt<dx_vector3_t,	dxvector3>	c_dx_cell_vector3_array_t;
	typedef c_dx_cell_array_impl_tt<dx_vector4_t,	dxvector4>	c_dx_cell_vector4_array_t;

	typedef c_dx_xnode_impl_tt<dxnode>		c_dx_node_t;
	typedef c_dx_xnode_impl_tt<dxlist>		c_dx_node_list_t;
	typedef c_dx_xnode_impl_tt<dxmap>		c_dx_node_map_t;
	typedef c_dx_xnode_impl_tt<dxtable>		c_dx_node_table_t;

	typedef c_dx_cell_string_tt<string,char,dxstring>				c_dx_cell_string_t;
	typedef c_dx_cell_string_tt<wstring,wchar_t,dxwstring>			c_dx_cell_wstring_t;
	typedef c_dx_cell_string_array_tt<string,char,dxstring>			c_dx_cell_string_array_t;
	typedef c_dx_cell_string_array_tt<wstring,wchar_t,dxwstring>	c_dx_cell_wstring_array_t;

	//!添加新的数据
	c_dx_node_mini_t*	create_node_impl(nxid_t name,e_dx_type_t typ,int len,const dx_node_meta_t* meta)
	{
		c_dx_node_mini_t* node=NULL;
		if(typ==dxnull)
			return NULL;
		if(typ<dxnode)
		{
			if(len<1)
				return NULL;
			if(len==1)
			{
				if(typ==dxint)		 node=new c_dx_cell_int_t(name);
				if(typ==dxreal)		 node=new c_dx_cell_real_t(name);
				if(typ==dxvector2)	 node=new c_dx_cell_vector2_t(name);
				if(typ==dxvector3)	 node=new c_dx_cell_vector3_t(name);
				if(typ==dxvector4)	 node=new c_dx_cell_vector4_t(name);
				if(typ==dxstring)    node=new c_dx_cell_string_t(name);
				if(typ==dxwstring)   node=new c_dx_cell_wstring_t(name);
			}
			else 
			{			
				if(typ==dxint)		node=new c_dx_cell_int_array_t(name,len);
				if(typ==dxreal)		node=new c_dx_cell_real_array_t(name,len);
				if(typ==dxvector2)	node=new c_dx_cell_vector2_array_t(name,len);
				if(typ==dxvector3)	node=new c_dx_cell_vector3_array_t(name,len);
				if(typ==dxvector4)	node=new c_dx_cell_vector4_array_t(name,len);
				if(typ==dxstring)   node=new c_dx_cell_string_array_t(name,len);
				if(typ==dxwstring)  node=new c_dx_cell_wstring_array_t(name,len);
			}
		}
		else if(typ<dxcount)
		{
			if(typ==dxnode)	node = new c_dx_node_t(name);
			if(typ==dxlist)	node = new c_dx_node_list_t(name);
			if(typ==dxmap)	node = new c_dx_node_map_t(name);
			if(typ==dxtable)node = new c_dx_node_table_t(name);
		}
		_ASSERT(node!=NULL);
		node->m_meta = meta;
		return node;
	}


	//!添加新的数据
	c_dx_node_mini_t*	c_dx_xnode_t::add(nxid_t name,e_dx_type_t typ,int len,const dx_node_meta_t* meta)
	{
		c_dx_node_mini_t* node = create_node_impl(name,typ,len,meta);
		if(node)
		{
			node->dxobject = this->dxobject;
			m_children.push_back(node);
		}
		return node;
	}

	nxid_t 					fn_node_get_name_impl(dx_node_t* node)//!<<node name
	{
		c_dx_node_mini_t* impl = static_cast<c_dx_node_mini_t*>(node);
		return impl->name();
	}

	e_dx_type_t				fn_node_get_type_impl(dx_node_t* node)//!<<node type of e_dx_node_t
	{	
		c_dx_node_mini_t* impl = static_cast<c_dx_node_mini_t*>(node);
		return impl->type();
	}

	dx_int_t 				fn_node_get_length_impl(dx_node_t* node)//!<<element count or sub node count
	{
		c_dx_node_mini_t* impl = static_cast<c_dx_node_mini_t*>(node);
		return impl->length();
	}

	void					fn_node_modify_length_impl(dx_node_t* node, const int len)
	{
		c_dx_node_mini_t* impl = static_cast<c_dx_node_mini_t*>(node);
		impl->modifyLength(len);
	}

	dx_cell_t				fn_node_get_data_cell_impl(dx_node_t* node)//!<<retrive the data content if it is data node
	{
		c_dx_node_mini_t* impl = static_cast<c_dx_node_mini_t*>(node);
		return impl->cell();
	}
	dx_node_t*				fn_node_get_child_impl(dx_node_t* node,int index)//!<<get child node of the node
	{
		c_dx_node_mini_t* impl = static_cast<c_dx_node_mini_t*>(node);
		_ASSERT(impl->type()>=dxnode);
		c_dx_xnode_t* impl2 = static_cast<c_dx_xnode_t*>(impl);
		return impl2->item(index);
	}
	dx_node_t*				fn_node_add_child_impl(dx_node_t* parent,nxid_t name,e_dx_type_t type,int length)//!<<add child node
	{
		c_dx_node_mini_t* impl = static_cast<c_dx_node_mini_t*>(parent);
		_ASSERT(impl->type()>=dxnode);
		c_dx_xnode_t* impl2 = static_cast<c_dx_xnode_t*>(impl);
		return impl2->add(name,type,length,0);
	}
	dx_node_t*				fn_node_add_child2_impl(dx_node_t* parent,const dx_node_meta_t* meta, int length)//!<<add child node with meta info
	{
		c_dx_node_mini_t* impl = static_cast<c_dx_node_mini_t*>(parent);
		_ASSERT(impl->type()>=dxnode);
		c_dx_xnode_t* impl2 = static_cast<c_dx_xnode_t*>(impl);
		return impl2->add(meta->name,meta->type,length,meta);
	}
	void			fn_node_put_meta_impl(dx_node_t* node,const dx_node_meta_t* meta)//!<<set meta info to the node
	{
		c_dx_node_mini_t* impl = static_cast<c_dx_node_mini_t*>(node);
		impl->put_meta(meta);
	}
	const dx_node_meta_t*	fn_node_get_meta_impl(dx_node_t* node)//!get the meta data attached to the node
	{
		c_dx_node_mini_t* impl = static_cast<c_dx_node_mini_t*>(node);
		return impl->get_meta();
	}

	class c_dx_node_iterator_mini_t:public dx_node_iterator_t
	{
	public:
		c_dx_node_iterator_mini_t(c_dx_xnode_t* n){impl=n;node = n;rewind();}
		int ix;
		c_dx_xnode_t* impl;
	public:
		void rewind(){ix=-1;}
		c_dx_node_mini_t* next(){
			ix++;
			if(impl->get_tag_count()<=ix)
				return impl->item(ix);
			return NULL;}
	};

	dx_node_iterator_t* fn_node_iterator_get_impl(dx_node_t* node)	 //!<<node iterator
	{
		c_dx_node_mini_t* impl = static_cast<c_dx_node_mini_t*>(node);
		if(impl->type()<dxnode)
			return NULL;
		c_dx_xnode_t* impl2 = static_cast<c_dx_xnode_t*>(node);
		return new c_dx_node_iterator_mini_t(impl2);
	}

	dx_node_t* fn_node_iterator_next_impl(dx_node_iterator_t* it)//!<<next iterator node
	{
		c_dx_node_iterator_mini_t* impl = static_cast<c_dx_node_iterator_mini_t*>(it);
		return impl->next();
	}
	void fn_node_iterator_rewind_impl(dx_node_iterator_t* it)//!<<reset iterator
	{
		c_dx_node_iterator_mini_t* impl = static_cast<c_dx_node_iterator_mini_t*>(it);
		impl->rewind();
	}

	void fn_node_iterator_release_impl(dx_node_iterator_t* it)//!<<release iterator
	{
		c_dx_node_iterator_mini_t* impl = static_cast<c_dx_node_iterator_mini_t*>(it);
		delete impl;
	}

	void fn_put_string_impl(dx_node_t* node,dx_string_t 	str,int ix) //!<<set string data for the node
	{
		c_dx_node_mini_t* impl = static_cast<c_dx_node_mini_t*>(node);
		_ASSERT(impl->type()==dxstring);
		c_dx_cell_string_t* st  = dynamic_cast<c_dx_cell_string_t*>(impl);
		if(st)
			st->put_string(str);
		else 
		{	
			c_dx_cell_string_array_t* st2  = dynamic_cast<c_dx_cell_string_array_t*>(impl);
			st2->put_string(str,ix);
		}
	}
	void fn_put_wstring_impl(dx_node_t* node,dx_wstring_t 	wstr,int ix)//!<<set string data for the node with wide format
	{
		c_dx_node_mini_t* impl = static_cast<c_dx_node_mini_t*>(node);
		_ASSERT(impl->type()==dxwstring);
		c_dx_cell_wstring_t* st  = dynamic_cast<c_dx_cell_wstring_t*>(impl);
		if(st)
			st->put_string((wchar_t*)wstr);
		else 
		{	
			c_dx_cell_wstring_array_t* st2  = dynamic_cast<c_dx_cell_wstring_array_t*>(impl);
			st2->put_string((wchar_t*)wstr,ix);
		}
	}

	void fn_node_data_cell_commit_impl(dx_cell_t* cell)				 //!<<save data
	{
		//nothing todo
	}
	//void					fn_node_using_impl(dx_node_t* node);						 //!<<use the node and force it stay in the memory
	//{
	//	
	//}
	//void					fn_node_release_impl(dx_node_t* node);						 //!<<release node
	//{
	//
	//}
	void	fn_node_put_tag_impl(dx_node_t* node,nxid_t tag) //!<<set node tag
	{
		c_dx_node_mini_t* impl = static_cast<c_dx_node_mini_t*>(node);
		_ASSERT(impl->type()==dxwstring);
		impl->add_tag(tag);
	}

	int	fn_node_get_tag_count_impl(dx_node_t* node)//!<<get node tags count
	{
		c_dx_node_mini_t* impl = static_cast<c_dx_node_mini_t*>(node);
		return impl->get_tag_count();
	}

	nxid_t	fn_node_get_tag_impl(dx_node_t* node,int ix)//!<<get node tag by index
	{
		c_dx_node_mini_t* impl = static_cast<c_dx_node_mini_t*>(node);
		return impl->get_tag(ix);
	}
	extern dxftable_t g_dxfable_mini;

	class c_dx_object_mini_t:public dx_object_t
	{
		int m_reference_counter;
	public:
		c_dx_object_mini_t():m_reference_counter(1)
		{
			dxftable=&g_dxfable_mini;
			node = NULL;
		}
		~c_dx_object_mini_t()
		{
			if(node)
			{
				c_dx_node_mini_t* impl = static_cast<c_dx_node_mini_t*>(node);
				delete impl;
			}
		}
		void create_inline_node(nxid_t name,e_dx_type_t type, int length)
		{
			node = create_node_impl(name,type,length,NULL);
			node->dxobject = this;
		}

		void create_inline_node2(const dx_node_meta_t* meta,int length)
		{
			node = create_node_impl(meta->name,meta->type,length,meta);
			node->dxobject = this;
		}

		void uxing(){
			m_reference_counter++;
		}
		void release(){
			m_reference_counter--;
			if(!m_reference_counter)delete this;
		}
	public:
		//!create a data set with a uri and a intial node
		static dx_object_t*  		fn_dx_object_create(const char* 	uri,nxid_t name,e_dx_type_t type, int length)
		{
			nxid_t iid = nxid(uri);
			c_dx_object_mini_t* dx = new c_dx_object_mini_t();
			dx->create_inline_node(name,type,length);
			return dx;
		}
		//!create a data set with meta info
		static dx_object_t*  		fn_dx_object_create2(const char* 	uri,const dx_node_meta_t* meta,int length)
		{
			c_dx_object_mini_t* dx = new c_dx_object_mini_t();
			dx->create_inline_node2(meta,length);
			return dx;
		}
		//!use the dataset to force memory active
		static void fn_dx_object_using(dx_object_t* 	dx)
		{
			c_dx_object_mini_t* impl = static_cast<c_dx_object_mini_t*>(dx);
			impl->uxing();
		}
		//!release dataset reference
		static void fn_dx_object_release(dx_object_t* 	dx)
		{
			c_dx_object_mini_t* impl = static_cast<c_dx_object_mini_t*>(dx);
			impl->release();
		}
	};

	dxftable_t g_dxfable_mini=
	{
		fn_node_get_name_impl,		 
		fn_node_get_type_impl,			 
		fn_node_get_length_impl,
		fn_node_modify_length_impl,

		fn_node_get_data_cell_impl,	 

		fn_node_get_child_impl,			 
		fn_node_add_child_impl,			 
		fn_node_add_child2_impl,		 

		fn_node_put_meta_impl,		 
		fn_node_get_meta_impl,	 

		fn_node_iterator_get_impl,	 
		fn_node_iterator_next_impl, 
		fn_node_iterator_rewind_impl,	 
		fn_node_iterator_release_impl, 

		fn_put_string_impl,	 
		fn_put_wstring_impl,		 
		fn_node_data_cell_commit_impl,	 

		fn_node_put_tag_impl, 
		fn_node_get_tag_count_impl,	 
		fn_node_get_tag_impl,
		
		c_dx_object_mini_t::fn_dx_object_create,
		c_dx_object_mini_t::fn_dx_object_create2,
		c_dx_object_mini_t::fn_dx_object_using,
		c_dx_object_mini_t::fn_dx_object_release
	};
}

NXDAT_MINI_API
const dxftable_t* (nx_get_dxftable_mini)(const char* argv)
{
	return &dx::g_dxfable_mini;
}