package com.ngis.udx;

public class Vector2d {
	public Vector2d()
	{
		x = 0;
		y = 0;
	}
	public Vector2d(double _x, double _y)
	{
		x = _x;
		y = _y;
	}
	public double x;
	public double y;
}
