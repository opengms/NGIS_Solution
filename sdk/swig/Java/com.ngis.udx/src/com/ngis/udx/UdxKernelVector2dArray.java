package com.ngis.udx;

import java.util.ArrayList;

public class UdxKernelVector2dArray extends UdxKernel 
{
	public UdxKernelVector2dArray(EKernelType pType, UdxNode pNode)
	{
		super(pType, pNode);
	}

	public boolean addTypedValue(double pX, double pY)
	{
		mValueList.add(new Vector2d(pX, pY));
		return true;
	}

	public Vector2d getTypedValueByIndex(int idx)
	{
		if (idx<0 || idx>=mValueList.size()) 
			return null;
		return mValueList.get(idx);
	}

	public int getCount()
	{
		return mValueList.size();
	}
	
	private ArrayList<Vector2d> mValueList = new ArrayList<Vector2d>();
}
