package com.ngis.udx;

import java.util.ArrayList;

public class UdxKernelIntArray extends UdxKernel 
{
	public UdxKernelIntArray(EKernelType pType, UdxNode pNode)
	{
		super(pType, pNode);
	}

	public boolean addTypedValue(int pValue)
	{
		mValueList.add(pValue);
		return true;
	}

	public int getTypedValueByIndex(int idx)
	{
		if (idx<0 || idx>=mValueList.size()) 
			return 0;
		return mValueList.get(idx);
	}

	public int getCount()
	{
		return mValueList.size();
	}
	
	private ArrayList<Integer> mValueList = new ArrayList<Integer>();
}
