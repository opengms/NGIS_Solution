package com.ngis.udx;

import java.util.ArrayList;

public class UdxKernelVector4dArray extends UdxKernel 
{
	public UdxKernelVector4dArray(EKernelType pType, UdxNode pNode)
	{
		super(pType, pNode);
	}

	public boolean addTypedValue(double pX, double pY, double pZ, double pM)
	{
		mValueList.add(new Vector4d(pX, pY, pZ, pM));
		return true;
	}

	public Vector4d getTypedValueByIndex(int idx)
	{
		if (idx<0 || idx>=mValueList.size()) 
			return null;
		return mValueList.get(idx);
	}

	public int getCount()
	{
		return mValueList.size();
	}
	
	private ArrayList<Vector4d> mValueList = new ArrayList<Vector4d>();
}
