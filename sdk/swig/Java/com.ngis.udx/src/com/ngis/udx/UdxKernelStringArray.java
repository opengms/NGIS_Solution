package com.ngis.udx;

import java.util.ArrayList;

public class UdxKernelStringArray extends UdxKernel 
{
	public UdxKernelStringArray(EKernelType pType, UdxNode pNode)
	{
		super(pType, pNode);
	}

	public boolean addTypedValue(String pValue)
	{
		mValueList.add(pValue);
		return true;
	}

	public String getTypedValueByIndex(int idx)
	{
		if (idx<0 || idx>=mValueList.size()) 
			return "";
		return mValueList.get(idx);
	}

	public int getCount()
	{
		return mValueList.size();
	}
	
	private ArrayList<String> mValueList = new ArrayList<String>();
}
