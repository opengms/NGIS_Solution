package com.ngis.udx;

public class Vector4d {
	public Vector4d()
	{
		x = 0;
		y = 0;
		z = 0;
	}
	public Vector4d(double _x, double _y, double _z, double _m)
	{
		x = _x;
		y = _y;
		z = _z;
		m = _m;
	}
	public double x;
	public double y;
	public double z;
	public double m;
}
