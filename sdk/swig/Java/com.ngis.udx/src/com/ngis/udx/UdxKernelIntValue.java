package com.ngis.udx;

public class UdxKernelIntValue extends UdxKernel 
{
	public UdxKernelIntValue(EKernelType pType, UdxNode pNode)
	{
		super(pType, pNode);
		mValue = 0;
	}
	

	public boolean setTypedValue(int pValue)
	{
		if (pValue == mValue)
			return false;
		mValue = pValue;
		return true;
	}

	public int getTypedValue()
	{
		return mValue;
	}
	
	private int mValue;
}
