package com.ngis.udx;

public class UdxKernelVector2dValue extends UdxKernel 
{
	public UdxKernelVector2dValue(EKernelType pType, UdxNode pNode)
	{
		super(pType, pNode);
	}
	

	public boolean setTypedValue(double pX, double pY)
	{
		mValue.x = pX;
		mValue.y = pY;
		return true;
	}

	public Vector2d getTypedValue()
	{
		return mValue;
	}
	
	private Vector2d mValue = new Vector2d();
}
