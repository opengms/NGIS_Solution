package com.ngis.udx.test;

import com.ngis.udx.EKernelType;
import com.ngis.udx.UdxDataset;
import com.ngis.udx.UdxKernel;
import com.ngis.udx.UdxKernelIntArray;
import com.ngis.udx.UdxKernelIntValue;
import com.ngis.udx.UdxKernelRealArray;
import com.ngis.udx.UdxKernelRealValue;
import com.ngis.udx.UdxKernelStringArray;
import com.ngis.udx.UdxKernelStringValue;
import com.ngis.udx.UdxKernelVector2dArray;
import com.ngis.udx.UdxKernelVector2dValue;
import com.ngis.udx.UdxKernelVector3dArray;
import com.ngis.udx.UdxKernelVector3dValue;
import com.ngis.udx.UdxKernelVector4dArray;
import com.ngis.udx.UdxKernelVector4dValue;
import com.ngis.udx.UdxNode;
import com.ngis.udx.Vector2d;
import com.ngis.udx.Vector3d;
import com.ngis.udx.Vector4d;
import com.ngis.udxschema.ESchemaNodeType;
import com.ngis.udxschema.UdxDatasetSchema;
import com.ngis.udxschema.UdxNodeSchema;

public class AppTest {

	static void IterateNode(UdxNode pNode) {
		String name = pNode.getName();
		EKernelType kernelType = pNode.getKernel().getType();
		UdxKernel kernel = pNode.getKernel();
		System.out.print("\n");
		System.out.printf("name=%s\n", name);
		System.out.printf("kernelType=%s\n", kernelType.toString());
		if (kernelType == EKernelType.EKT_INT) 
		{
			UdxKernelIntValue realKernel = (UdxKernelIntValue) kernel;
			int val = realKernel.getTypedValue();
			System.out.printf("value = %d \n", val);
		} 
		else if (kernelType == EKernelType.EKT_REAL) 
		{
			UdxKernelRealValue realKernel = (UdxKernelRealValue) kernel;
			double val = realKernel.getTypedValue();
			System.out.printf("value = %f \n", val);
		}
		else if (kernelType == EKernelType.EKT_STRING) 
		{
			UdxKernelStringValue realKernel = (UdxKernelStringValue) kernel;
			String val = realKernel.getTypedValue();
			System.out.printf("value = %s \n", val);
		}
		else if (kernelType == EKernelType.EKT_VECTOR2) 
		{
			UdxKernelVector2dValue realKernel = (UdxKernelVector2dValue) kernel;
			Vector2d vec = realKernel.getTypedValue();
			System.out.printf("value = %f, %f \n", vec.x, vec.y);
		} 
		else if (kernelType == EKernelType.EKT_VECTOR3)
		{
			UdxKernelVector3dValue realKernel = (UdxKernelVector3dValue) kernel;
			Vector3d vec = realKernel.getTypedValue();
			System.out.printf("value = %f, %f, %f \n", vec.x, vec.y, vec.z);
		} 
		else if (kernelType == EKernelType.EKT_VECTOR4)
		{
			UdxKernelVector4dValue realKernel = (UdxKernelVector4dValue) kernel;
			Vector4d vec = realKernel.getTypedValue();
			System.out.printf("value = %f, %f, %f, %f \n", vec.x, vec.y, vec.z, vec.m);
		} 
		else if (kernelType == EKernelType.EKT_INT_LIST)
		{
			UdxKernelIntArray realKernel = (UdxKernelIntArray) kernel;
			int valueLen = realKernel.getCount();
			for (int iVal = 0; iVal < valueLen; iVal++) {
				int val = realKernel.getTypedValueByIndex(iVal);
				System.out.printf("value = %d \n", val);
			}
			System.out.print("\n");
		} 
		else if (kernelType == EKernelType.EKT_REAL_LIST)
		{
			UdxKernelRealArray realKernel = (UdxKernelRealArray) kernel;
			int valueLen = realKernel.getCount();
			for (int iVal = 0; iVal < valueLen; iVal++) {
				double val = realKernel.getTypedValueByIndex(iVal);
				System.out.printf("value = %f \n", val);
			}
			System.out.print("\n");
		}
		else if (kernelType == EKernelType.EKT_STRING_LIST)
		{
			UdxKernelStringArray realKernel = (UdxKernelStringArray) kernel;
			int valueLen = realKernel.getCount();
			for (int iVal = 0; iVal < valueLen; iVal++) {
				String val = realKernel.getTypedValueByIndex(iVal);
				System.out.printf("value = %s \n", val);
			}
			System.out.print("\n");
		} 
		else if (kernelType == EKernelType.EKT_VECTOR2_LIST)
		{
			UdxKernelVector2dArray realKernel = (UdxKernelVector2dArray) kernel;
			int valueLen = realKernel.getCount();
			for (int iVal = 0; iVal < valueLen; iVal++) {
				Vector2d vec = realKernel.getTypedValueByIndex(iVal);
				if (vec != null)
					System.out.printf("value = %f, %f; \n", vec.x, vec.y);
			}
			System.out.print("\n");
		} 
		else if (kernelType == EKernelType.EKT_VECTOR3_LIST)
		{
			UdxKernelVector3dArray realKernel = (UdxKernelVector3dArray) kernel;
			int valueLen = realKernel.getCount();
			for (int iVal = 0; iVal < valueLen; iVal++) {
				Vector3d vec = realKernel.getTypedValueByIndex(iVal);
				if (vec != null)
					System.out.printf("value = %f, %f, %f; \n", vec.x, vec.y, vec.z);
			}
			System.out.print("\n");
		} 
		else if (kernelType == EKernelType.EKT_VECTOR4_LIST)
		{
			UdxKernelVector4dArray realKernel = (UdxKernelVector4dArray) kernel;
			int valueLen = realKernel.getCount();
			for (int iVal = 0; iVal < valueLen; iVal++) {
				Vector4d vec = realKernel.getTypedValueByIndex(iVal);
				if (vec != null)
					System.out.printf("value = %f, %f, %f, %f; \n", vec.x, vec.y, vec.z, vec.m);
			}
			System.out.print("\n");
		} 
		else if (kernelType == EKernelType.EKT_NODE
				|| kernelType == EKernelType.EKT_LIST
				|| kernelType == EKernelType.EKT_MAP
				|| kernelType == EKernelType.EKT_TABLE) 
		{
			int nodeCount = pNode.getChildNodeCount();
			for (int iNode = 0; iNode < nodeCount; iNode++) {
				IterateNode(pNode.getChildNode(iNode));
			}
		}

	}
	
	public static void genTestUdx()
	{
		UdxDataset udxDataset = new UdxDataset();
		udxDataset.addChildNode("Node1", EKernelType.EKT_INT);
		udxDataset.addChildNode("Node2", EKernelType.EKT_NODE);
	
		UdxNode firstNode = udxDataset.getChildNode(0);
		{
			((UdxKernelIntValue)firstNode.getKernel()).setTypedValue(1234);
		}
	
		UdxNode secondNode = udxDataset.getChildNode(1);
		{
			UdxNode realValueNode = secondNode.addChildNode("我是Node2_node1", EKernelType.EKT_REAL);
			((UdxKernelRealValue)realValueNode.getKernel()).setTypedValue(10.234);
	
			//////////////////////////////////////////////////////////////////////////
			UdxNode stringValueNode = secondNode.addChildNode("Node2_node2", EKernelType.EKT_STRING);
			((UdxKernelStringValue)stringValueNode.getKernel()).setTypedValue("woshishui我是谁");
	
			//////////////////////////////////////////////////////////////////////////
			UdxNode vector4dArrayNode = secondNode.addChildNode("Node2_node3", EKernelType.EKT_VECTOR4_LIST);
			UdxKernelVector4dArray realKernel = (UdxKernelVector4dArray)vector4dArrayNode.getKernel();
			realKernel.addTypedValue(1.0, 2.0, 3.0, 4.0);
			realKernel.addTypedValue(5.0, 6.0, 7.0, 8.0);
			realKernel.addTypedValue(9.0, 10.0, 11.0, 12.0);
			realKernel.addTypedValue(13.0, 14.0, 15.0, 16.0);
		}
		
		UdxNode thirdNode = udxDataset.addChildNode("Node3", EKernelType.EKT_NODE);
		{
			UdxNode stringArrayNode = thirdNode.addChildNode("node3_node1", EKernelType.EKT_STRING_LIST);
			UdxKernelStringArray realKernel = (UdxKernelStringArray)stringArrayNode.getKernel();
			realKernel.addTypedValue("DECP");
			realKernel.addTypedValue("LAND");
			realKernel.addTypedValue("OCAN");
			realKernel.addTypedValue("LAND");
			realKernel.addTypedValue("OCAN");
		}
		
		/////////////////////////////////////////////////////////////
		for(int iNode=0; iNode<udxDataset.getChildNodeCount(); iNode++)
		{
			IterateNode(udxDataset.getChildNode(iNode));
		}
		
		udxDataset.FormatToXmlFile("D:\\testUdxInJava.xml");
	}
	
	public static void LoadUdx()
	{
		UdxDataset udxDataset = new UdxDataset();
		udxDataset.LoadFromXmlFile("D:\\testUdxInJava.xml");
		//udxDataset.LoadFromXmlFile("D:\\ch_run.xml");
	
		for(int iNode=0; iNode<udxDataset.getChildNodeCount(); iNode++)
		{
			IterateNode(udxDataset.getChildNode(iNode));
		}
	
		udxDataset.FormatToXmlFile("D:\\testUdxInJava.xml");
		
		String formated_xml_str = udxDataset.FormatToXmlStream();
		System.out.print(formated_xml_str);
		int len = formated_xml_str.length();
		System.out.print(len);
	}
	
	////////////////////////////////////////////////////////////////////////
	public static void genTestUdxSchema()
	{
		{
			UdxDatasetSchema pDataset = new UdxDatasetSchema(null, "UdxDeclaration");
	        UdxNodeSchema pNode1 = pDataset.addChildNode("node1", ESchemaNodeType.EDTKT_INT, "d分类d");
	        UdxNodeSchema pNode2 = pDataset.addChildNode("node2", ESchemaNodeType.EDTKT_REAL, "f分类f");
	        UdxNodeSchema pNode3 = pDataset.addChildNode("node3", ESchemaNodeType.EDTKT_NODE, "s分类s");

	        UdxNodeSchema pNode3_1 = pNode3.addChildNode("node3_1", ESchemaNodeType.EDTKT_TABLE, "a分类a");
	        UdxNodeSchema pNode3_2 = pNode3.addChildNode("node3_2", ESchemaNodeType.EDTKT_INT,"");
	        UdxNodeSchema pNode3_3 = pNode3.addChildNode("node3_3", ESchemaNodeType.EDTKT_LIST,"");
	        pNode3_3.addChildNode("Node3_3_1", ESchemaNodeType.EDTKT_NODE, "");
	        
	        UdxNodeSchema pNode3_1_1 = pNode3_1.addChildNode("node3_1_1", ESchemaNodeType.EDTKT_INT_LIST, "kkk");
	        UdxNodeSchema pNode3_1_2 = pNode3_1.addChildNode("node3_1_2", ESchemaNodeType.EDTKT_REAL_LIST, "kkk");
	        UdxNodeSchema pNode3_1_3 = pNode3_1.addChildNode("node3_1_3", ESchemaNodeType.EDTKT_STRING_LIST, "ttt");

	        String xmlStr = pDataset.FormatToXmlStream();
	        System.out.println(xmlStr);
	        
	        pDataset.FormatToXmlFile("D:\\testUdxSchemaInJava.xml");
		}
	}
	
	
	public static void main(String[] args) {
		//System.out.println("Test JNI UDX");	
		//LoadUdx();
		genTestUdx();
	
		genTestUdxSchema();
		
		/////////////////////////////////////////////////////////////////////////////////////////////////////
		UdxDataset udxDataset = new UdxDataset();
		UdxNode node1 = udxDataset.addChildNode("Node1", EKernelType.EKT_INT);
		UdxNode node2 = udxDataset.addChildNode("Node2", EKernelType.EKT_NODE);
		((UdxKernelIntValue)node1.getKernel()).setTypedValue(1234);
		
		UdxDataset udxDataset1 = new UdxDataset();
		UdxNode node1_1 = udxDataset1.addChildNode("Node1", EKernelType.EKT_INT);
		udxDataset1.addChildNode("Node2", EKernelType.EKT_NODE);
		((UdxKernelIntValue)node1_1.getKernel()).setTypedValue(4567);
		
		for(int iNode=0; iNode<udxDataset.getChildNodeCount(); iNode++)
		{
			IterateNode(udxDataset.getChildNode(iNode));
		}
	
		for(int iNode=0; iNode<udxDataset1.getChildNodeCount(); iNode++)
		{
			IterateNode(udxDataset1.getChildNode(iNode));
		}
	}

	
}
