package com.ngis.udx;

public class UdxKernel 
{
	protected UdxNode mNode;
	protected EKernelType mType;
	
	public UdxKernel(EKernelType pType, UdxNode pNode)
	{
		mNode = pNode;
		mType = pType;
	}
	
	public UdxNode getNode()
	{
		return mNode;
	}

	public EKernelType getType()
	{
		return mType;
	}
}
