package com.ngis.udx;

import java.util.ArrayList;

import com.ngis.udxschema.ESchemaNodeType;

public class UdxNode 
{
	protected String mName;
	protected UdxNode mParentNode;
	protected UdxKernel mKernel;
	
	private ArrayList<UdxNode> mChildNodes;
	
	protected int nativeNodeObj;
	
	public UdxNode(UdxNode parentNode, String pName, EKernelType pType)
	{
		mChildNodes = new ArrayList<UdxNode>();
		if (parentNode!=null)
		{
			mName = pName;
			mParentNode = parentNode;
			createKernel(pName, pType);
		}
	}
	
	private void createKernel(String pName, EKernelType pType) 
	{
		if (pType == EKernelType.EKT_NULL || pType == EKernelType.EKT_COUNT)
			return;
		else if (pType == EKernelType.EKT_INT)
			mKernel = new UdxKernelIntValue(pType, this);
		else if (pType == EKernelType.EKT_REAL)
			mKernel = new UdxKernelRealValue(pType, this);
		else if (pType == EKernelType.EKT_STRING)
			mKernel = new UdxKernelStringValue(pType, this);
		else if (pType == EKernelType.EKT_VECTOR2)
			mKernel = new UdxKernelVector2dValue(pType, this);
		else if (pType == EKernelType.EKT_VECTOR3)
			mKernel = new UdxKernelVector3dValue(pType, this);
		else if (pType == EKernelType.EKT_VECTOR4)
			mKernel = new UdxKernelVector4dValue(pType, this);
		else if (pType == EKernelType.EKT_INT_LIST)
			mKernel = new UdxKernelIntArray(pType, this);
		else if (pType == EKernelType.EKT_REAL_LIST)
			mKernel = new UdxKernelRealArray(pType, this);
		else if (pType == EKernelType.EKT_STRING_LIST)
			mKernel = new UdxKernelStringArray(pType, this);
		else if (pType == EKernelType.EKT_VECTOR2_LIST)
			mKernel = new UdxKernelVector2dArray(pType, this);
		else if (pType == EKernelType.EKT_VECTOR3_LIST)
			mKernel = new UdxKernelVector3dArray(pType, this);
		else if (pType == EKernelType.EKT_VECTOR4_LIST)
			mKernel = new UdxKernelVector4dArray(pType, this);
		else if (pType == EKernelType.EKT_NODE || pType == EKernelType.EKT_LIST
				|| pType == EKernelType.EKT_MAP || pType == EKernelType.EKT_TABLE) 
		{
			mKernel = new UdxKernel(pType, this);
		}
	}
	
	public String getName()
	{
		return mName;
	}

	public UdxKernel getKernel()
	{
		return mKernel;
	}

	public int getChildNodeCount()
	{
		return mChildNodes.size();
	}

	public UdxNode getChildNode(int idx)
	{
		if (idx<0 || idx>= mChildNodes.size())
			return null;
		return mChildNodes.get(idx);
	}

	public UdxNode addChildNode(String pName, EKernelType pType)
	{
		UdxNode node = new UdxNode(this, pName, pType);
		mChildNodes.add(node);
		return node;
	}

	public boolean removeChildNode(UdxNode pNode)
	{
		if (mChildNodes.contains(pNode))
		{
			mChildNodes.remove(pNode);
			return true;
		}
		return false;
	}

	public boolean removeChildNode(int idx)
	{
		if (idx<0 || idx>=mChildNodes.size())
			return false;
		mChildNodes.remove(idx);
		return true;
	}
}
