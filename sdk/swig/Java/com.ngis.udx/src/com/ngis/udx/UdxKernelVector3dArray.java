package com.ngis.udx;

import java.util.ArrayList;

public class UdxKernelVector3dArray extends UdxKernel 
{
	public UdxKernelVector3dArray(EKernelType pType, UdxNode pNode)
	{
		super(pType, pNode);
	}

	public boolean addTypedValue(double pX, double pY, double pZ)
	{
		mValueList.add(new Vector3d(pX, pY, pZ));
		return true;
	}

	public Vector3d getTypedValueByIndex(int idx)
	{
		if (idx<0 || idx>=mValueList.size()) 
			return null;
		return mValueList.get(idx);
	}

	public int getCount()
	{
		return mValueList.size();
	}
	
	private ArrayList<Vector3d> mValueList = new ArrayList<Vector3d>();
}
