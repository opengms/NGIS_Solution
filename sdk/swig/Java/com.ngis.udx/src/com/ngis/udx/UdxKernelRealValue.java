package com.ngis.udx;

public class UdxKernelRealValue  extends UdxKernel 
{
	public UdxKernelRealValue(EKernelType pType, UdxNode pNode)
	{
		super(pType, pNode);
		mValue = 0.0;
	}
	

	public boolean setTypedValue(double pValue)
	{
		if (pValue == mValue)
			return false;
		mValue = pValue;
		return true;
	}

	public double getTypedValue()
	{
		return mValue;
	}
	
	private double mValue;
}
