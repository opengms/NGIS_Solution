package com.ngis.udx;

public enum EKernelType 
{
	EKT_NULL(0),
	EKT_INT(2),			//!<integer
	EKT_REAL(4),			//!<float with 64bit precise
	EKT_STRING(8),			//!<a string with NULL terminated
	EKT_VECTOR2(16),		//!<2 dimensions vector with x and y
	EKT_VECTOR3(32),		//!<3 dimensions vector with x, y and z
	EKT_VECTOR4(64),		//!<4 dimensions vector with x, y, z and w
	
	EKT_NODE(128),			//!<a no-constraints node container
	EKT_LIST(256),			//!<a container with same structure node
	EKT_MAP(512),			//!<a k-v map container
	EKT_TABLE(1024),			//!<a data table container

    EKT_INT_LIST(2|256),
    EKT_REAL_LIST(4|256),
    EKT_STRING_LIST(8|256),
    EKT_VECTOR2_LIST(16|256),
    EKT_VECTOR3_LIST(32|256),
    EKT_VECTOR4_LIST(64|256),
	
	EKT_COUNT(16);

	private int mValue;
	 
    private EKernelType(int value) {
        this.mValue = value;
    }
}
