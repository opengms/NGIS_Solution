package com.ngis.udx;

public class UdxKernelVector3dValue extends UdxKernel 
{
	public UdxKernelVector3dValue(EKernelType pType, UdxNode pNode)
	{
		super(pType, pNode);
	}
	

	public boolean setTypedValue(double pX, double pY, double pZ)
	{
		mValue.x = pX;
		mValue.y = pY;
		mValue.z = pZ;
		return true;
	}

	public Vector3d getTypedValue()
	{
		return mValue;
	}

	private Vector3d mValue = new Vector3d();
}
