package com.ngis.udx;

public class UdxKernelStringValue extends UdxKernel 
{
	public UdxKernelStringValue(EKernelType pType, UdxNode pNode)
	{
		super(pType, pNode);
	}
	

	public boolean setTypedValue(String pValue)
	{
		if (pValue.equals(mValue))
			return false;
		mValue = pValue;
		return true;
	}

	public String getTypedValue()
	{
		return mValue;
	}

	private String mValue;
}
