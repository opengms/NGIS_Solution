package com.ngis.udx;

public class UdxKernelVector4dValue extends UdxKernel 
{
	public UdxKernelVector4dValue(EKernelType pType, UdxNode pNode)
	{
		super(pType, pNode);
	}
	

	public boolean setTypedValue(double pX, double pY, double pZ, double pM)
	{
		mValue.x = pX;
		mValue.y = pY;
		mValue.z = pZ;
		mValue.m = pM;
		return true;
	}

	public Vector4d getTypedValue()
	{
		return mValue;
	}

	private Vector4d mValue = new Vector4d();
}
