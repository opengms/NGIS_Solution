package com.ngis.udx;

public class Vector3d {
	public Vector3d()
	{
		x = 0;
		y = 0;
		z = 0;
	}
	public Vector3d(double _x, double _y, double _z)
	{
		x = _x;
		y = _y;
		z = _z;
	}
	public double x;
	public double y;
	public double z;
}
