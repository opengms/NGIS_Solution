package com.ngis.udx;

import java.util.ArrayList;

public class UdxKernelRealArray extends UdxKernel 
{
	public UdxKernelRealArray(EKernelType pType, UdxNode pNode)
	{
		super(pType, pNode);
	}

	public boolean addTypedValue(double pValue)
	{
		mValueList.add(pValue);
		return true;
	}

	public double getTypedValueByIndex(int idx)
	{
		if (idx<0 || idx>=mValueList.size()) 
			return 0;
		return mValueList.get(idx);
	}

	public int getCount()
	{
		return mValueList.size();
	}
	
	private ArrayList<Double> mValueList = new ArrayList<Double>();
}
