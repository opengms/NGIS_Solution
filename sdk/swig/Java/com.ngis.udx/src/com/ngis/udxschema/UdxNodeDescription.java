package com.ngis.udxschema;

public class UdxNodeDescription {
	protected ESchemaNodeType mKernelType;
    protected String mNodeInfo;
    protected String mConceptTag;
    protected String mUnitTag;
    protected String mDimensionTag;
    protected String mSpatialRefTag;
    protected String mDataTemplateTag;
    
    public UdxNodeDescription(ESchemaNodeType pType, String pDescription)
    {
        mKernelType = pType;
        mNodeInfo = pDescription;
        mConceptTag = "";
        mUnitTag = "";
        mDimensionTag = "";
        mSpatialRefTag = "";
        mDataTemplateTag = "";
    }

    public static String SchemaNodeType2String(ESchemaNodeType pType)
    {
        if (pType == ESchemaNodeType.EDTKT_INT)
            return "DTKT_INT";
        else if (pType == ESchemaNodeType.EDTKT_REAL)
            return "DTKT_REAL";
        else if (pType == ESchemaNodeType.EDTKT_STRING)
            return "DTKT_STRING";
        else if (pType == ESchemaNodeType.EDTKT_VECTOR2)
            return "DTKT_VECTOR2D";
        else if (pType == ESchemaNodeType.EDTKT_VECTOR3)
            return "DTKT_VECTOR3D";
        else if (pType == ESchemaNodeType.EDTKT_VECTOR4)
            return "DTKT_VECTOR4D";
        else if (pType == ESchemaNodeType.EDTKT_INT_LIST)
            return "DTKT_INT | DTKT_LIST";
        else if (pType == ESchemaNodeType.EDTKT_REAL_LIST)
            return "DTKT_REAL | DTKT_LIST";
        else if (pType == ESchemaNodeType.EDTKT_STRING_LIST)
            return "DTKT_STRING | DTKT_LIST";
        else if (pType == ESchemaNodeType.EDTKT_VECTOR2_LIST)
            return "DTKT_VECTOR2D | DTKT_LIST";
        else if (pType == ESchemaNodeType.EDTKT_VECTOR3_LIST)
            return "DTKT_VECTOR3D | DTKT_LIST";
        else if (pType == ESchemaNodeType.EDTKT_VECTOR4_LIST)
            return "DTKT_VECTOR4D | DTKT_LIST";
        else if (pType == ESchemaNodeType.EDTKT_NODE)
            return "DTKT_ANY";
        else if (pType == ESchemaNodeType.EDTKT_LIST)
            return "DTKT_LIST";
        else if (pType == ESchemaNodeType.EDTKT_MAP)
            return "DTKT_MAP";
        else if (pType == ESchemaNodeType.EDTKT_TABLE)
            return "DTKT_TABLE";
        return "DTKT_ANY";
    }

    public static ESchemaNodeType String2SchemaNodeType(String pType)
    {
        String pTypeStr = pType;
        ESchemaNodeType pNodeType = ESchemaNodeType.EDTKT_NODE;
        if (pTypeStr.equals("DTKT_INT"))
            pNodeType = ESchemaNodeType.EDTKT_INT;
        else if (pTypeStr.equals("DTKT_REAL"))
            pNodeType = ESchemaNodeType.EDTKT_REAL;
        else if (pTypeStr.equals("DTKT_STRING"))
            pNodeType = ESchemaNodeType.EDTKT_STRING;
        else if (pTypeStr.equals("DTKT_VECTOR2D"))
            pNodeType = ESchemaNodeType.EDTKT_VECTOR2;
        else if (pTypeStr.equals("DTKT_VECTOR3D"))
            pNodeType = ESchemaNodeType.EDTKT_VECTOR3;
        else if (pTypeStr.equals("DTKT_VECTOR4D"))
            pNodeType = ESchemaNodeType.EDTKT_VECTOR4;
        else if (pTypeStr.equals("DTKT_INT | DTKT_LIST"))
            pNodeType = ESchemaNodeType.EDTKT_INT_LIST;
        else if (pTypeStr.equals("DTKT_REAL | DTKT_LIST"))
            pNodeType = ESchemaNodeType.EDTKT_REAL_LIST;
        else if (pTypeStr.equals("DTKT_STRING | DTKT_LIST"))
            pNodeType = ESchemaNodeType.EDTKT_STRING_LIST;
        else if (pTypeStr.equals("DTKT_VECTOR2D | DTKT_LIST"))
            pNodeType = ESchemaNodeType.EDTKT_VECTOR2_LIST;
        else if (pTypeStr.equals("DTKT_VECTOR3D | DTKT_LIST"))
            pNodeType = ESchemaNodeType.EDTKT_VECTOR3_LIST;
        else if (pTypeStr.equals("DTKT_VECTOR4D | DTKT_LIST"))
            pNodeType = ESchemaNodeType.EDTKT_VECTOR4_LIST;
        else if (pTypeStr.equals("DTKT_ANY"))
            pNodeType = ESchemaNodeType.EDTKT_NODE;
        else if (pTypeStr.equals("DTKT_LIST"))
            pNodeType = ESchemaNodeType.EDTKT_LIST;
        else if (pTypeStr.equals("DTKT_MAP"))
            pNodeType = ESchemaNodeType.EDTKT_MAP;
        else if (pTypeStr.equals("DTKT_TABLE"))
            pNodeType = ESchemaNodeType.EDTKT_TABLE;

        return pNodeType;
    }

    public ESchemaNodeType getKernelType()
    {
        return mKernelType;
    }

    public boolean modifyKernelType(ESchemaNodeType pKernelType)
    {
        if (mKernelType == pKernelType) return false;
        mKernelType = pKernelType;
        return true;
    }

    public String getNodeDescription()
    {
        return mNodeInfo;
    }

    public boolean modifyNodeDescription(String pNodeInfo)
    {
        if (mNodeInfo.equals(pNodeInfo)) return false;
        mNodeInfo = pNodeInfo;
        return true;
    }

    //////////////////////////////////////////////////////////////////////////
    public String getConceptTag()
    {
        return mConceptTag;
    }

    public boolean modifyConceptTag(String pTag)
    {
        if (mConceptTag.equals(pTag)) return false;
        mConceptTag = pTag;
        return true;
    }

    public String getUnitTag()
    {
        return mUnitTag;
    }

    public boolean modifyUnitTag(String pTag)
    {
        if (mUnitTag.equals(pTag)) return false;
        mUnitTag = pTag;
        return true;
    }

    public String getDimensionTag()
    {
        return mDimensionTag;
    }

    public boolean modifyDimensionTag(String pTag)
    {
        if (mDimensionTag.equals(pTag)) return false;
        mDimensionTag = pTag;
        return true;
    }

    public String getSpatialReferencefTag()
    {
        return mSpatialRefTag;
    }

    public boolean modifySpatialReferenceTag(String pTag)
    {
        if (mSpatialRefTag.equals(pTag)) return false;
        mSpatialRefTag = pTag;
        return true;
    }

    public String getDataTemplateTag()
    {
        return mDataTemplateTag;
    }

    public boolean modifyDataTemplateTag(String pTag)
    {
        if (mDataTemplateTag.equals(pTag)) return false;
        mDataTemplateTag = pTag;
        return true;
    }
}
