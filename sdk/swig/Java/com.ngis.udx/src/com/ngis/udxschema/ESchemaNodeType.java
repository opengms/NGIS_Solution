package com.ngis.udxschema;

public enum ESchemaNodeType {
    EDTKT_INT(2),				//!<integer
    EDTKT_REAL(4),				//!<float with 64bit precise
    EDTKT_STRING(8),			//!<a string with NULL terminated
    EDTKT_VECTOR2(16),		//!<2 dimensions vector with x and y
    EDTKT_VECTOR3(32),		//!<3 dimensions vector with x, y and z
    EDTKT_VECTOR4(64),		//!<4 dimensions vector with x, y, z and w
    
    EDTKT_NODE(128),			//!<a no-constraints node container
    EDTKT_LIST(256),				//!<a container with same structure node
    EDTKT_MAP(512),				//!<a k-v map container
    EDTKT_TABLE(1024),			//!<a data table container
    
    EDTKT_INT_LIST(2|256),
    EDTKT_REAL_LIST(4|256),
    EDTKT_STRING_LIST(8|256),
    EDTKT_VECTOR2_LIST(16|256),
    EDTKT_VECTOR3_LIST(32|256),
    EDTKT_VECTOR4_LIST(64|256);
	
	private int mValue;
	 
    private ESchemaNodeType(int value) {
        this.mValue = value;
    }
    
}
