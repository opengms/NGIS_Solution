package com.ngis.udxschema;
import java.util.ArrayList;

public class UdxNodeSchema {
	protected String											mName;
    protected UdxNodeDescription            		mDescription;
    protected ArrayList<UdxNodeSchema>       mChildNodes;
    protected UdxNodeSchema                 		mParentNode;
    protected int										   		mExtension;

    public UdxNodeSchema(UdxNodeSchema pParent, String pName, UdxNodeDescription pDescription)
    {
        mName = pName;
        mParentNode = pParent;
        mDescription = pDescription;
        mChildNodes = new ArrayList<UdxNodeSchema>();
    }

    public String getName()
    {
        return mName;
    }

    public UdxNodeDescription getDescription()
    {
        return mDescription;
    }

    public boolean modifyName(String pName)
    {
		if (mName.equals(pName)) return false;
		mName = pName;
		return true;
    }

    public UdxNodeSchema getParentNode()
    {
		return mParentNode;
    }

    public int getChildNodeCount()
    {
        return mChildNodes.size();
    }

    public UdxNodeSchema getChildNode(int idx)
    {
        if (idx < 0 || idx >= mChildNodes.size()) return null;
        return mChildNodes.get(idx);
    }

    public UdxNodeSchema addChildNode(String pName, UdxNodeDescription pDescription)
    {
        ESchemaNodeType mType = mDescription.getKernelType();
        if (mType == ESchemaNodeType.EDTKT_INT ||
            mType == ESchemaNodeType.EDTKT_REAL ||
            mType == ESchemaNodeType.EDTKT_STRING ||
            mType == ESchemaNodeType.EDTKT_VECTOR2 ||
            mType == ESchemaNodeType.EDTKT_VECTOR3 ||
            mType == ESchemaNodeType.EDTKT_VECTOR4 ||
            mType == ESchemaNodeType.EDTKT_INT_LIST ||
            mType == ESchemaNodeType.EDTKT_REAL_LIST ||
            mType == ESchemaNodeType.EDTKT_STRING_LIST ||
            mType == ESchemaNodeType.EDTKT_VECTOR2_LIST ||
            mType == ESchemaNodeType.EDTKT_VECTOR3_LIST ||
            mType == ESchemaNodeType.EDTKT_VECTOR4_LIST)
        {
            return null;
        }
        else if (mType == ESchemaNodeType.EDTKT_LIST)
        {
            if (mChildNodes.size() >= 1)
                return null;
        }
        else if (mType == ESchemaNodeType.EDTKT_MAP)
        {
            if (mChildNodes.size() >= 2)
                return null;
        }
        else if (mType == ESchemaNodeType.EDTKT_TABLE)
        {
            boolean isOK = false;
            ESchemaNodeType pType = pDescription.getKernelType();
            if (pType == ESchemaNodeType.EDTKT_INT_LIST ||
                pType == ESchemaNodeType.EDTKT_REAL_LIST||
                pType == ESchemaNodeType.EDTKT_STRING_LIST ||
                pType == ESchemaNodeType.EDTKT_VECTOR2_LIST ||
                pType == ESchemaNodeType.EDTKT_VECTOR3_LIST ||
                pType == ESchemaNodeType.EDTKT_VECTOR4_LIST)
            {
                isOK = true;
            }
            if (isOK == false) return null;
        }
        UdxNodeSchema pNode = new UdxNodeSchema(this, pName, pDescription);
        mChildNodes.add(pNode);
        return pNode;
    }

    public UdxNodeSchema addChildNode(String pName, ESchemaNodeType pNodeType, String pNodeInfo)
    {
        UdxNodeDescription pDescription = new UdxNodeDescription(pNodeType, pNodeInfo);
        UdxNodeSchema pNode = addChildNode(pName, pDescription);
        return pNode;
    }

    public UdxNodeSchema addChildNode(String pName, ESchemaNodeType pNodeType)
    {
        UdxNodeDescription pDescription = new UdxNodeDescription(pNodeType, "");
        UdxNodeSchema pNode = addChildNode(pName, pDescription);
        return pNode;
    }

    public boolean removeChildNode(UdxNodeSchema pNode)
    {
        for (int i = 0; i < mChildNodes.size(); i++)
        {
            if (mChildNodes.get(i) == pNode)
            {
                mChildNodes.remove(i);
                return true;
            }
        }
        return false;
    }

    public boolean removeChildNode(int idx)
    {
        if (idx >= 0 && idx < mChildNodes.size())
        {
            mChildNodes.remove(idx);
            return true;
        }
        return false;
    }

    public void setExtension(int flag)
    {
    	mExtension = flag;
    }

    public int getExtension()
    {
    	return mExtension;
    }

    public boolean compareOther(UdxNodeSchema pNode)
    {
    	if (compareNodeInfo(this, pNode) == false)
		{
			return false;
		}
		for (int iChild = 0; iChild < pNode.getChildNodeCount(); iChild++)
		{
			UdxNodeSchema tempNode1 = this.getChildNode(iChild);
			UdxNodeSchema tempNode2 = pNode.getChildNode(iChild);
			
			if (tempNode1.compareOther(tempNode2)==false)
			{
				return false;
			}
		}
		return true;
    }

	private boolean compareNodeInfo(UdxNodeSchema pNode1, UdxNodeSchema pNode2)
	{
		int count1 = pNode1.getChildNodeCount();
		int count2 = pNode2.getChildNodeCount();
		if (count1 != count2)
		{
			return false;
		}
		if (pNode1.getDescription().getKernelType() != pNode2.getDescription().getKernelType())
		{
			return false;
		}
		String name1 = pNode1.getName();
		String name2 = pNode2.getName();
		if (name1.equals(name2)==false)
		{
			return false;
		}
		String desc1 = pNode1.getDescription().getNodeDescription();
		String desc2 = pNode2.getDescription().getNodeDescription();
		if (desc1.equals(desc2)==false)
		{
			return false;
		}
		String tag1 = pNode1.getDescription().getConceptTag();
		String tag2 = pNode2.getDescription().getConceptTag();
		if (tag1.equals(tag2)==false)
		{
			return false;
		}
		tag1 = pNode1.getDescription().getSpatialReferencefTag();
		tag2 = pNode2.getDescription().getSpatialReferencefTag();
		if (tag1.equals(tag2)==false)
		{
			return false;
		}
		tag1 = pNode1.getDescription().getUnitTag();
		tag2 = pNode2.getDescription().getUnitTag();
		if (tag1.equals(tag2)==false)
		{
			return false;
		}
		tag1 = pNode1.getDescription().getDimensionTag();
		tag2 = pNode2.getDescription().getDimensionTag();
		if (tag1.equals(tag2)==false)
		{
			return false;
		}
		tag1 = pNode1.getDescription().getDataTemplateTag();
		tag2 = pNode2.getDescription().getDataTemplateTag();
		if (tag1.equals(tag2)==false)
		{
			return false;
		}
		return true;
	}

}
