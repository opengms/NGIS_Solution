package com.ngis.model.mdl;

import java.util.ArrayList;

public class LocalAttribute {
	public LocalAttribute() {
		localType = ELocalizationType.ELT_COUNT;
		localName = "Name";
		abstractInfo = "Abstract";
		wikiUrl = "";
		keywords = new ArrayList<String>();
		keywords.clear();
		this.mechanismItems = new ArrayList<ModelMechanismItem>();
		this.sptimeScaleType = ESpatiotemporalScaleType.EMSST_Unknown;
		this.sslX = new SpatiotemporalStepLength();
		this.sslY = new SpatiotemporalStepLength();
		this.sslZ = new SpatiotemporalStepLength();
		this.sslT = new SpatiotemporalStepLength();
		this.spatialScope = new SpatialScope();
	}

	public boolean compareOther(LocalAttribute pLocal) {
		if (localType.equals(pLocal.localType) == false)
			return false;
		if (localName.equals(pLocal.localName) == false)
			return false;
		if (abstractInfo.equals(pLocal.abstractInfo) == false)
			return false;
		if (wikiUrl.equals(pLocal.wikiUrl) == false)
			return false;
		if (keywords.size() != pLocal.keywords.size())
			return false;
		for (int iKey = 0; iKey < keywords.size(); iKey++) {
			if (keywords.get(iKey).equals(pLocal.keywords.get(iKey)) == false) {
				return false;
			}
		}
		for (int iItem = 0; iItem < mechanismItems.size(); iItem++){
			if (mechanismItems.get(iItem).Name.equals(pLocal.mechanismItems.get(iItem).Name) == false) return false;
			if (mechanismItems.get(iItem).Type.equals(pLocal.mechanismItems.get(iItem).Type) == false) return false;
			if (mechanismItems.get(iItem).Value.equals(pLocal.mechanismItems.get(iItem).Value) == false) return false;
		}
		if (spatialScope.equals(pLocal.spatialScope) == false) return false;

		if (sslX.Min != pLocal.sslX.Min) return false;
		if (sslX.Max != pLocal.sslX.Max) return false;
		if (sslX.Unit != pLocal.sslX.Unit) return false;

		if (sslY.Min != pLocal.sslY.Min) return false;
		if (sslY.Max != pLocal.sslY.Max) return false;
		if (sslY.Unit != pLocal.sslY.Unit) return false;

		if (sslZ.Min != pLocal.sslZ.Min) return false;
		if (sslZ.Max != pLocal.sslZ.Max) return false;
		if (sslZ.Unit != pLocal.sslZ.Unit) return false;
		
		if (sslT.Min != pLocal.sslT.Min) return false;
		if (sslT.Max != pLocal.sslT.Max) return false;
		if (sslT.Unit != pLocal.sslT.Unit) return false;
		
		for (int index = 0; index < spatialScope.Extents.size(); index++) {
			if (this.spatialScope.Extents.get(index).XMin != pLocal.spatialScope.Extents.get(index).XMin) return false;
			if (this.spatialScope.Extents.get(index).XMax != pLocal.spatialScope.Extents.get(index).XMax) return false;
			if (this.spatialScope.Extents.get(index).YMin != pLocal.spatialScope.Extents.get(index).YMin) return false;
			if (this.spatialScope.Extents.get(index).YMax != pLocal.spatialScope.Extents.get(index).YMax) return false;
			if (this.spatialScope.Extents.get(index).SpatialReference.equals(pLocal.spatialScope.Extents.get(index).SpatialReference) == false) return false;
		}
		
		for (int index = 0; index < spatialScope.AreaNames.size(); index++) {
			if (this.spatialScope.AreaNames.get(index).equals(pLocal.spatialScope.AreaNames.get(index)) == false) return false;
		}

		return true;
	}

	public ELocalizationType localType;
	public String localName;
	public String abstractInfo;
	public String wikiUrl;
	public ArrayList<String> keywords;
	
	public ArrayList<ModelMechanismItem> mechanismItems;
	public ESpatiotemporalScaleType sptimeScaleType;
	public SpatiotemporalStepLength sslX;
	public SpatiotemporalStepLength sslY;
	public SpatiotemporalStepLength sslZ;
	public SpatiotemporalStepLength sslT;
	public SpatialScope spatialScope;
}
