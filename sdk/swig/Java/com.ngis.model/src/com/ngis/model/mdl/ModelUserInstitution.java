package com.ngis.model.mdl;

public class ModelUserInstitution {
	public String Name;
	public String Country;
	public String City;
	public String Address;

    public ModelUserInstitution(String name, String country, String city, String address)
    {
        this.Name = name;
        this.Country = country;
        this.City = city;
        this.Address = address;
    }
}
