package com.ngis.model.mdl;

public class SupportiveResource {
	public String resourceType;
	public String resourceName;

	public boolean compareOther(SupportiveResource pResource) {
		if (resourceType.equals(pResource.resourceType)==false)
			return false;
		if (resourceName.equals(pResource.resourceName)==false)
			return false;
		return true;
	}
}
