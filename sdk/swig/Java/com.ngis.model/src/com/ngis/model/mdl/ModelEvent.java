package com.ngis.model.mdl;

public class ModelEvent {
	public ModelEvent() {
		eventName = "";
		eventType = EModelEventType.EMET_CONTROL;
		eventDescription = "";
		datasetReference = "unknown";
		parameterDescription = "";
		optional = false;
		multiple = false;
	}

	public boolean compareOther(ModelEvent pEvent) {
		if (eventName.equals(pEvent.eventName) == false) {
			return false;
		}
		if (eventType.equals(pEvent.eventType) == false) {
			return false;
		}
		if (eventDescription.equals(pEvent.eventDescription) == false) {
			return false;
		}
		if (datasetReference.equals(pEvent.datasetReference) == false) {
			return false;
		}
		if (parameterDescription.equals(pEvent.parameterDescription) == false) {
			return false;
		}
		if (optional != pEvent.optional) {
			return false;
		}
		if (multiple != pEvent.multiple){
			return false;
		}

		return true;
	}

	public String eventName;
	public EModelEventType eventType;
	public String eventDescription;
	public String datasetReference;
	public String parameterDescription;
	public boolean optional;
	public boolean multiple;
}
