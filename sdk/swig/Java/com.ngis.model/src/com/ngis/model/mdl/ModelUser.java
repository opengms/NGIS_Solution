package com.ngis.model.mdl;

import java.util.ArrayList;

public class ModelUser {
    public String Name;
    public String Account;
    public ModelUserInstitution Institution;
    public ArrayList<ModelUserContact> Contacts;

    public ModelUser(String name, String account, ModelUserInstitution ins, ArrayList<ModelUserContact> contacts)
    {
        this.Name = name;
        this.Account = account;
        this.Institution = ins;
        this.Contacts = contacts;
    }
}
