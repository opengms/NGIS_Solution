package com.ngis.model.mdl;

//! Spatial Scope Extent
public class SpatialExtent
{
    public double XMin;
    public double XMax;
    public double YMin;
    public double YMax;

    public String SpatialReference;

    public SpatialExtent(double xmin, double xmax, double ymin, double ymax, String sreference)
    {
        this.XMin = xmin;
        this.XMax = xmax;
        this.YMin = ymin;
        this.YMax = ymax;
        this.SpatialReference = sreference;
    }
};