package com.ngis.model.mdl;

import java.util.ArrayList;
import org.dom4j.Element;

public class LocalizationHelper {
	public static String LocalizationType2String(ELocalizationType pType) {
		if (pType == ELocalizationType.ELT_ZH_CN)
			return "ZH_CN";
		else if (pType == ELocalizationType.ELT_EN_US)
			return "EN_US";
		else if (pType == ELocalizationType.ELT_DE_DE)
			return "DE_DE";
		else if (pType == ELocalizationType.ELT_RU_RU)
			return "RU_RU";
		else if (pType == ELocalizationType.ELT_AR)
			return "AR";
		else if (pType == ELocalizationType.ELT_ES)
			return "ES";
		return "Unknown";
	}

	public static ELocalizationType String2LocalizationType(String pTypeStr) {
		if (pTypeStr.equals("ZH_CN"))
			return ELocalizationType.ELT_ZH_CN;
		else if (pTypeStr.equals("EN_US"))
			return ELocalizationType.ELT_EN_US;
		else if (pTypeStr.equals("DE_DE"))
			return ELocalizationType.ELT_DE_DE;
		else if (pTypeStr.equals("RU_RU"))
			return ELocalizationType.ELT_RU_RU;
		else if (pTypeStr.equals("AR"))
			return ELocalizationType.ELT_AR;
		else if (pTypeStr.equals("ES"))
			return ELocalizationType.ELT_ES;
		return ELocalizationType.ELT_COUNT;
	}
	
	public static String MechanismItemType2String(EMechanismItemType type){
		if (type.equals(EMechanismItemType.EMIT_Algorithm)) {
			return "Algorithm";
		}
		else if(type.equals(EMechanismItemType.EMIT_Formula)) {
			return "Formula";
		}
		else if(type.equals(EMechanismItemType.EMIT_Text)) {
			return "Text";
		}
		return "Unknown";
	}
	
	public static EMechanismItemType String2MechanismItemType(String stype){
		if (stype.equals("Algorithm")) {
			return EMechanismItemType.EMIT_Algorithm;
		}
		else if(stype.equals("Formula")) {
			return EMechanismItemType.EMIT_Formula;
		}
		else if(stype.equals("Text")) {
			return EMechanismItemType.EMIT_Text;
		}
		return EMechanismItemType.EMIT_Unknown;
	}
	
	public static String ESpatiotemporalScaleType2String(ESpatiotemporalScaleType type) {
		if (type.equals(ESpatiotemporalScaleType.EMSST_Global)) {
			return "Global";
		}
		else if(type.equals(ESpatiotemporalScaleType.EMSST_Region_Large)) {
			return "Regional";
		}
		else if(type.equals(ESpatiotemporalScaleType.EMSST_Region_Middle)) {
			return "Regional";
		}
		else if(type.equals(ESpatiotemporalScaleType.EMSST_Region_Small)) {
			return "Regional";
		}
		return "Unknown";
	}

	public static String ESpatiotemporalScale2String(ESpatiotemporalScaleType type) {
		if(type.equals(ESpatiotemporalScaleType.EMSST_Region_Large)) {
			return "Large";
		}
		else if(type.equals(ESpatiotemporalScaleType.EMSST_Region_Middle)) {
			return "Middle";
		}
		else if(type.equals(ESpatiotemporalScaleType.EMSST_Region_Small)) {
			return "Small";
		}
		return "Unknown";
	}
	
	public static ESpatiotemporalScaleType String2ESpatiotemporalScaleType(String sType, String sScale) {
		if(sType == "Global") {
			return ESpatiotemporalScaleType.EMSST_Global;
		}
		else if(sType == "Regional" && sScale == "Large") {
			return ESpatiotemporalScaleType.EMSST_Region_Large;
		}
		else if(sType == "Regional" && sScale == "Middle") {
			return ESpatiotemporalScaleType.EMSST_Region_Middle;
		}
		else if(sType == "Regional" && sScale == "Small") {
			return ESpatiotemporalScaleType.EMSST_Region_Small;
		}
		return ESpatiotemporalScaleType.EMSST_Unknown;
	}
	
	public static EContactType String2ContactType(String sContact) {
        if ((sContact == "Email")) {
            return EContactType.ECT_Email;
        }
        else if ((sContact == "Fax")) {
            return EContactType.ECT_Fax;
        }
        else if ((sContact == "Phone")) {
            return EContactType.ECT_Phone;
        }
        
        return EContactType.ECT_Unknown;
    }
    
    public static String ContactType2String(EContactType eContact) {
        if ((eContact == EContactType.ECT_Email)) {
            return "Email";
        }
        else if ((eContact == EContactType.ECT_Fax)) {
            return "Fax";
        }
        else if ((eContact == EContactType.ECT_Phone)) {
            return "Phone";
        }
        
        return "Unknown";
    }
    
    public static ArrayList<ModelUser> Xml2ModelUser(Element mele) {
    	ArrayList<ModelUser> list_users = new ArrayList<ModelUser>();
        for (Element ele : mele.elements()) {
            String name = ele.attributeValue("name");
            String account = ele.attributeValue("account");
            Element ins_ele = mele.elements().get(0);
            String insname = ele.attributeValue("name");
            String countryname = ele.attributeValue("country");
            String city = ele.attributeValue("city");
            String address = ele.attributeValue("address");
            ModelUserInstitution mui = new ModelUserInstitution(insname, countryname, city, address);
            Element contact_ele = mele.elements().get(1);
            ArrayList<ModelUserContact> list_contacts = new ArrayList<ModelUserContact>();
            for (Element cele : contact_ele.elements()) {
                String type = cele.attributeValue("type");
                String value = cele.attributeValue("value");
                list_contacts.add(new ModelUserContact(LocalizationHelper.String2ContactType(type), value));
            }
            
            ModelUser mu = new ModelUser(name, account, mui, list_contacts);
            list_users.add(mu);
        }
        
        return list_users;
    }
}
