package com.ngis.model.mdl;

public class ModelStateTransition {
	public ModelStateTransition() {
	}

	public boolean compareOther(ModelStateTransition pTrans) {
		if (fromState.compareOther(pTrans.fromState) == false) {
			return false;
		}
		if (toState.compareOther(pTrans.toState) == false) {
			return false;
		}
		return true;
	}

	public ModelState fromState;
	public ModelState toState;
}
