package com.ngis.model.mdl;

public enum EModelDatasetItemType {
	EMDIT_INTERNAL, EMDIT_EXTERNAL, EMDIT_RAW
}
