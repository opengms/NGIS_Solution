package com.ngis.model.mdl;

//! Spatio-temporal Scale Type
public enum ESpatiotemporalScaleType
{
    EMSST_Unknown,
    EMSST_Global,
    EMSST_Region_Large,
    EMSST_Region_Middle,
    EMSST_Region_Small
};