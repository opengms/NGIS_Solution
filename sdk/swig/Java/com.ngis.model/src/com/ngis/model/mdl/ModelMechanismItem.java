package com.ngis.model.mdl;

//! Model Mechanism Item
public class ModelMechanismItem
{
    public String Name;
    public EMechanismItemType Type;
    public String Value;

    public ModelMechanismItem(String name, EMechanismItemType type, String value)
    {
        this.Name = name;
        this.Type = type;
        this.Value = value;
    }
};