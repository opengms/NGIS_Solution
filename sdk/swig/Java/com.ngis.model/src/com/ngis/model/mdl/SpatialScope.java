package com.ngis.model.mdl;

import java.util.ArrayList;

//! Spatial Scope
public class SpatialScope
{
    public ArrayList<SpatialExtent> Extents;
    public ArrayList<String> AreaNames;
    
    public SpatialScope() {
    	this.Extents = new ArrayList<SpatialExtent>();
    	this.AreaNames = new ArrayList<String>();
    }
};