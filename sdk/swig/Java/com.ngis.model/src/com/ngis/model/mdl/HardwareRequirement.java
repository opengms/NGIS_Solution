package com.ngis.model.mdl;

public class HardwareRequirement {
	public String requirementKey;
	public String requirementValue;

	public boolean compareOther(HardwareRequirement pRequirement) {
		if (requirementKey.equals(pRequirement.requirementKey) == false)
			return false;
		if (requirementValue.equals(pRequirement.requirementValue) == false)
			return false;
		return true;
	}
}
