package com.ngis.model.mdl;

public enum EContactType {
    ECT_Unknown,
    ECT_Email,
    ECT_Fax,
    ECT_Phone
}
