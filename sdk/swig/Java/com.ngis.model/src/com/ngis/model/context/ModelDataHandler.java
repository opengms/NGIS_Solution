package com.ngis.model.context;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileSystem;
import java.util.ArrayList;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

public class ModelDataHandler 
{
	private String mExecutionPath;
    private String mExecutionName;

    private String mSavePath;
    private String mSaveName;

    private String mReturnFileFullName;

    private ModelServiceContext mModelServiceContext;

    public ModelDataHandler(ModelServiceContext pContext)
    {
        mModelServiceContext = pContext;
    }

    //! 链接数据映射方法，使用相对路径
    public void connectDataMappingMethod(String pExecutionName)
    {
        mExecutionName = pExecutionName;
        mExecutionPath = mModelServiceContext.getMappingLibraryDirectory();
        if (mExecutionPath.lastIndexOf('\\') != mExecutionPath.length() - 1)
        {
            mExecutionPath += "\\";
        }

        mSavePath = "";
        mSaveName = "";
        mReturnFileFullName = "";
    }

    //! 配置数据映射方法的工作空间，数据映射结果的保存路径
    public void configureWorkingDirectory(String pSavePath)
    {
        String pSavePathStr = pSavePath;
        if (pSavePathStr == "")
        {
            mSavePath = mModelServiceContext.getModelInstanceDirectory();
            if (mSavePath.lastIndexOf('\\') != mSavePath.length() - 1)
            {
                mSavePath += "\\";
            }
        }
        else
        {
            mSavePath = pSavePath;
            if (mSavePath.lastIndexOf('\\') != mSavePath.length() - 1)
            {
                mSavePath += "\\";
            }
        }
    }

    //! 启用UDX映射原始数据方法，结果保存相对路径
    public boolean conductUDXMapping(String pResultSaveName)
    {
        if (mModelServiceContext.getRequestDataFlag() != ERequestResponseDataFlag.ERDF_OK)
        {
            mSavePath = "";
            mSaveName = "";
            mReturnFileFullName = "";
            return false;
        }

        mSavePath = mModelServiceContext.getCurrentDataDirectory();
        mSaveName = pResultSaveName.trim();

        ERequestResponseDataMIME pMIME = mModelServiceContext.getRequestDataMIME();
        if (pMIME == ERequestResponseDataMIME.ERDM_XML_FILE)
        {
            String dataBody = mModelServiceContext.getRequestDataBody();
            String xml_file = dataBody;
            if (mSaveName == "")
            {
                String eventName = mModelServiceContext.getCurrentRunningEvent();
                //! 怎么处理后缀名的问题？？
                mSaveName = eventName + ".raw";
            }

            String cmd = mExecutionPath + mExecutionName + " -w -u ";
            cmd += xml_file;
            cmd += " -f ";
            cmd += mSavePath + mSaveName;

            boolean retVal = mModelServiceContext.startANewProcess(cmd, mSavePath);

            mReturnFileFullName = mSavePath + mSaveName;
            return retVal;
        }
        else if (pMIME == ERequestResponseDataMIME.ERDM_ZIP_FILE)
        {
            String dataBody = mModelServiceContext.getRequestDataBody();
            int idx = dataBody.lastIndexOf('\\');
            String dataPath = dataBody.substring(0, idx + 1);

            String runningDir = mModelServiceContext.getMappingLibraryDirectory();

            String cmd = runningDir + "miniunz.exe -e -o ";
            cmd += dataBody;
            cmd += " -d ";
            cmd += mSavePath;

            boolean ret = mModelServiceContext.startANewProcess(cmd, mSavePath);
            if (ret == false)
            {
                return false;
            }

            String configureFileName = mSavePath + "configure.udxcfg";

    		SAXReader reader = new SAXReader();
            Document doc = null;
			try {
				doc = reader.read(new File(configureFileName));
			} 
			catch (DocumentException e1) {
				e1.printStackTrace();
			}

            Element rootEle = doc.getRootElement();
            Element nameEle = rootEle.element("Name");
            String fileName = "";
            int file_count = Integer.parseInt(nameEle.attributeValue("count"));
            if (file_count == 1)
            {
            	Element ele = nameEle.elements().get(0);
                fileName = ele.attributeValue("value");

                if (mSaveName == "")
                {
                    mSaveName = fileName;
                    mReturnFileFullName = mSavePath + mSaveName;
                    return true;
                }
                //////////////////////////////////////////////////////////////////////////
                if (mSaveName != "" && mSaveName != fileName)
                {
                    if (mSaveName.lastIndexOf('.') == mSaveName.length() - 1 ||
                        mSaveName.lastIndexOf('.') < 0)
                    {
                        int idx1 = fileName.lastIndexOf('.');
                        String ext = fileName.substring(idx1, fileName.length() - idx1);
                        mSaveName += ext;
                    }
                    String oldName = mSavePath + fileName;
                    String newName = mSavePath + mSaveName;
                    try
                    {
						int bytesum = 0;
						int byteread = 0;
						InputStream inStream = new FileInputStream(oldName); // 读入原文件
						FileOutputStream fs = new FileOutputStream(newName);
						byte[] buffer = new byte[1024];
						int length;
						while ((byteread = inStream.read(buffer)) != -1) {
							bytesum += byteread; // 字节数 文件大小
							//System.out.println(bytesum);
							fs.write(buffer, 0, byteread);
						}
						inStream.close();
                    }
                    catch (Exception e)
                    {
                        mSaveName = fileName;
                        mReturnFileFullName = mSavePath + mSaveName;
                        return false;
                    }
                    File newFile = new File(newName);
                    if (newFile.exists() == false)
                    {
                        mSaveName = fileName;
                        mReturnFileFullName = mSavePath + mSaveName;
                        return false;
                    }
                    mReturnFileFullName = mSavePath + mSaveName;
                }
            }
            else if (mSaveName != "")
            {
                int last_dot = mSaveName.lastIndexOf('.');
                if (last_dot > 0 && last_dot != mSaveName.length())
                {
                    mSaveName = mSaveName.substring(0, last_dot); //去掉后缀名
                }
                
                int count = nameEle.elements().size();
                for (int iChild = 0; iChild < count; iChild++)
                {
                    Element ele = nameEle.elements().get(iChild);
                    fileName = ele.attributeValue("value");
                    String ext = fileName.substring(idx, fileName.length() - idx);

                    String oldName = mSavePath + fileName;
                    String newName = mSavePath + mSaveName + ext;
                    try
                    {
                    	int bytesum = 0;
						int byteread = 0;
						InputStream inStream = new FileInputStream(oldName); // 读入原文件
						FileOutputStream fs = new FileOutputStream(newName);
						byte[] buffer = new byte[1024];
						int length;
						while ((byteread = inStream.read(buffer)) != -1) {
							bytesum += byteread; // 字节数 文件大小
							System.out.println(bytesum);
							fs.write(buffer, 0, byteread);
						}
						inStream.close();
                    }
                    catch (Exception e)
                    {
                        mSaveName = fileName;
                        break;
                    }
                    File newFile = new File(newName);
                    if (newFile.exists() == false)
                    {
                        mSaveName = fileName;
                        break;
                    }
                }
                mReturnFileFullName = mSavePath + mSaveName;
            }
            return true;
        }
        else if (pMIME == ERequestResponseDataMIME.ERDM_RAW_FILE)
        {
            String dataBody = mModelServiceContext.getRequestDataBody();
            int idx = dataBody.lastIndexOf('\\');
            String dataPath = dataBody.substring(0, idx + 1);
            String dataName = dataBody.substring(idx + 1);

            mSavePath = dataPath;
            mSaveName = dataName;
            mReturnFileFullName = mSavePath + mSaveName;
            return true;
        }
        else if (pMIME == ERequestResponseDataMIME.ERDM_XML_STREAM)
        {
            //! TODO 数据流的暂时还不支持，没时间写啊
        }
        else if (pMIME == ERequestResponseDataMIME.ERDM_ZIP_STREAM)
        {
            //! TODO 数据流的暂时还不支持，没时间写啊
        }
        else if (pMIME == ERequestResponseDataMIME.ERDM_RAW_STREAM)
        {
            //! TODO 数据流的暂时还不支持，没时间写啊
        }
        mReturnFileFullName = "";
        return false;
    }

    //! 启用原始数据映射UDX方法，结果保存相对路径
    public boolean conductFileMapping(String pResultSaveName, ArrayList<String> rawFiles, int rawFileCount)
    {
        if (mModelServiceContext.getResponseDataFlag() != ERequestResponseDataFlag.ERDF_OK)
        {
            mSavePath = "";
            mSaveName = "";
            mReturnFileFullName = "";
            return false;
        }

        mSavePath = mModelServiceContext.getCurrentDataDirectory();
        mSaveName = pResultSaveName.trim();

        if (this.mModelServiceContext.getResponseDataFlag() == ERequestResponseDataFlag.ERDF_OK)
        {
            switch (this.mModelServiceContext.getResponseDataMIME())
            {
                case ERDM_XML_FILE:
                    {
                        String format_file = rawFiles.get(0);

                        String cmd = mExecutionPath + mExecutionName + " -r -f ";
                        cmd += format_file;
                        cmd += " -u ";
                        cmd += mSavePath + mSaveName;

                        //! 利用数据映射方法，将原始文件转换成UDX的XML格式
                        boolean ret = mModelServiceContext.startANewProcess(cmd, mSavePath);
                        if (ret)
                        {
                            File pFile = new File(mSavePath + mSaveName);
                            if (pFile.exists() == false)
                            {
                                mSavePath = "";
                                mSaveName = "";
                                mReturnFileFullName = "";
                                return false;
                            }
                            mReturnFileFullName = mSavePath + mSaveName;
                            return true;
                        }
                        else
                        {
                            mSavePath = "";
                            mSaveName = "";
                            mReturnFileFullName = "";
                            return false;
                        }
                    }
			case ERDM_ZIP_FILE:
                    {
                		Document doc = DocumentHelper.createDocument();
                        Element rootEle = doc.addElement("UDXZip");

                        Element fileNameEle = rootEle.addElement("FileName");
                        fileNameEle.addAttribute("count", String.valueOf(rawFileCount));
                        for (int iFile = 0; iFile < rawFileCount; iFile++)
                        {
                            Element fileItemEle = fileNameEle.addElement("Add");

                            String temp_raw_file = rawFiles.get(iFile);
                            int idx = temp_raw_file.lastIndexOf('\\');
                            temp_raw_file = temp_raw_file.substring(idx);
                            fileItemEle.addAttribute("value", temp_raw_file);
                        }
                        //! TODO Add UDX Schema	

                        String cfgFile = mSavePath + "configure.udxcfg";
                		try {
                			XMLWriter writer;
                			writer = new XMLWriter(new FileWriter(cfgFile));
                			writer.write(doc);
                			writer.close();
                		} 
                		catch (IOException e) {
                			e.printStackTrace();
                		}

                        String runningDir = mModelServiceContext.getMappingLibraryDirectory();

                        String cmd = runningDir + "minizip.exe -o -5 -j "; //如果加上访问码的话 -p 123
                        cmd += mSavePath + mSaveName;
                        for (int iFile = 0; iFile < rawFileCount; iFile++)
                        {
                            cmd += " " + rawFiles.get(iFile);
                        }
                        cmd += " " + cfgFile;

                        boolean ret = mModelServiceContext.startANewProcess(cmd, mSavePath);
                        if (ret == false)
                        {
                            mSavePath = "";
                            mSaveName = "";
                            mReturnFileFullName = "";
                            return false;
                        }
                        mReturnFileFullName = mSavePath + mSaveName;
                        return true;
                    }
			case ERDM_RAW_FILE:
                    {
                        for (int iFile = 0; iFile < rawFileCount; iFile++)
                        {
                            String temp_raw_file = rawFiles.get(iFile);
                            mReturnFileFullName += temp_raw_file;
                            if (iFile != rawFileCount - 1)
                            {
                                mReturnFileFullName += ";";
                            }
                        }
                        return true;
                    }
			case ERDM_XML_STREAM:
                    {
                        //! TODO 数据流的暂时还不支持，没时间写啊
                    }
                    break;
                case ERDM_ZIP_STREAM:
                    {
                        //! TODO 数据流的暂时还不支持，没时间写啊
                    }
                    break;
                case ERDM_RAW_STREAM:
                    {
                        //! TODO 数据流的暂时还不支持，没时间写啊
                    }
                    break;
            }
        }

        return false;
    }

    //! 启用原始数据映射UDX方法，结果保存相对路径
    public boolean conductFileMapping(String pResultSaveName, String rawFile)
    {
        ArrayList<String> rawFiles = new ArrayList<String>();
        rawFiles.add(rawFile);
        return conductFileMapping(pResultSaveName, rawFiles, 1);
    }

    //! 获取真实的数据名
    public String getRealResultSaveName()
    {
        return mReturnFileFullName;
    }
}
