package com.ngis.model.mdl;

import java.util.ArrayList;

public class ModelState {
	public ModelState() {
		stateId = "";
		stateName = "State Name";
		stateType = EModelStateType.EMST_BASIC;
		stateDecription = "";
		modelEvents = new ArrayList<ModelEvent>();
		modelEvents.clear();
	}

	public boolean compareOther(ModelState pState) {
		if (stateId.equals(pState.stateId) == false) {
			return false;
		}
		if (stateName.equals(pState.stateName) == false) {
			return false;
		}
		if (stateType != pState.stateType) {
			return false;
		}
		if (stateDecription.equals(pState.stateDecription) == false) {
			return false;
		}
		if (modelEvents.size() != pState.modelEvents.size()) {
			return false;
		}
		for (int iEvent = 0; iEvent < modelEvents.size(); iEvent++) {
			ModelEvent event1 = modelEvents.get(iEvent);
			ModelEvent event2 = pState.modelEvents.get(iEvent);
			if (event1.compareOther(event2) == false)
				return false;
		}
		return true;
	}

	public String stateId;
	public String stateName;
	public EModelStateType stateType;
	public String stateDecription;
	public ArrayList<ModelEvent> modelEvents;
}
