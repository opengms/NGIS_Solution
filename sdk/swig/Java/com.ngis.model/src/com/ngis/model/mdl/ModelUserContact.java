package com.ngis.model.mdl;

public class ModelUserContact {
    public EContactType Type;
    public String Value;

    public ModelUserContact(EContactType type, String value)
    {
        this.Type = type;
        this.Value = value;
    }
}
