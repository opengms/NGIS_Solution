package com.ngis.model.mdl;

public class ModelAssembly {
	public String assemblyName;
	public String assemblyPath;

	public boolean compareOther(ModelAssembly pAssembly) {
		if (assemblyName.equals(pAssembly.assemblyName) == false)
			return false;
		if (assemblyPath.equals(pAssembly.assemblyPath) == false)
			return false;
		return true;
	}
}
