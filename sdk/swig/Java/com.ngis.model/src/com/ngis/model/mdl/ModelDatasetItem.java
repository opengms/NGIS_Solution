package com.ngis.model.mdl;

import com.ngis.udxschema.UdxDatasetSchema;

public class ModelDatasetItem {
	public ModelDatasetItem() {
		datasetItem = null;
		datasetName = "";
		datasetItemType = EModelDatasetItemType.EMDIT_INTERNAL;
		datasetItemDescription = "";
		externalId = "";
	}

	public boolean compareOther(ModelDatasetItem pDatasetItem) {
		if (datasetItem == null && pDatasetItem.datasetItem != null
				|| datasetItem != null && pDatasetItem.datasetItem == null) {
			return false;
		}
		if (datasetItem != null && pDatasetItem.datasetItem != null) {
			if (datasetItem.compareOther(pDatasetItem.datasetItem) == false)
				return false;
		}
		if (datasetName.equals(pDatasetItem.datasetName)==false) {
			return false;
		}
		if (datasetItemType.equals(pDatasetItem.datasetItemType)==false) {
			return false;
		}
		if (datasetItemDescription.equals(pDatasetItem.datasetItemDescription)==false) {
			return false;
		}
		if (externalId.equals(pDatasetItem.externalId)==false) {
			return false;
		}

		return true;
	}

	public UdxDatasetSchema datasetItem;
	public String datasetName;
	public EModelDatasetItemType datasetItemType;
	public String datasetItemDescription;
	public String externalId;
}
