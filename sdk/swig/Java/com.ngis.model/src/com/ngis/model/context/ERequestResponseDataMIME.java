package com.ngis.model.context;

public enum ERequestResponseDataMIME 
{
    ERDM_XML_STREAM,
    ERDM_ZIP_STREAM,
    ERDM_RAW_STREAM,
    ERDM_XML_FILE,
    ERDM_ZIP_FILE,
    ERDM_RAW_FILE,
    ERDM_UNKNOW
}
