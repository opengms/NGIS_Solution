package com.ngis.model.mdl.test;

import com.ngis.model.mdl.CompareFlag;
import com.ngis.model.mdl.EExecutionStyle;
import com.ngis.model.mdl.ELocalizationType;
import com.ngis.model.mdl.EModelDatasetItemType;
import com.ngis.model.mdl.EModelEventType;
import com.ngis.model.mdl.EModelStateType;
import com.ngis.model.mdl.HardwareRequirement;
import com.ngis.model.mdl.LocalAttribute;
import com.ngis.model.mdl.ModelAssembly;
import com.ngis.model.mdl.ModelAttribute;
import com.ngis.model.mdl.ModelBehavior;
import com.ngis.model.mdl.ModelCategory;
import com.ngis.model.mdl.ModelClass;
import com.ngis.model.mdl.ModelDatasetItem;
import com.ngis.model.mdl.ModelEvent;
import com.ngis.model.mdl.ModelRuntime;
import com.ngis.model.mdl.ModelState;
import com.ngis.model.mdl.SoftwareRequirement;
import com.ngis.model.mdl.SupportiveResource;
import com.ngis.udxschema.ESchemaNodeType;
import com.ngis.udxschema.UdxDatasetSchema;
import com.ngis.udxschema.UdxNodeSchema;

public class AppTest 
{
	public static void main(String[] args) {
		ModelClass modelClass = new ModelClass();
		
		modelClass.setName("Test Model");
		modelClass.setUID("{B5031C06-2A44-46FA-B359-ABDB19F5198A}");
		modelClass.setExecutionStyle(EExecutionStyle.EES_STATE_SIMULATION);

		//////////////////////////////////////////////////////////////////////////
		//
		// ModelAttribute
		//
		//////////////////////////////////////////////////////////////////////////
		{
			ModelAttribute modelAttribute = modelClass.getModelAttribute();
			ModelCategory modelCategory1 = new ModelCategory();
			modelCategory1.principle = "VGE";
			modelCategory1.path = "DEM/TauDEM/BasicParameter";
			modelAttribute.addCategoryInfo(modelCategory1);

			LocalAttribute modelLocalAttribute1 = new LocalAttribute();
			modelLocalAttribute1.localType = ELocalizationType.ELT_EN_US;
			modelLocalAttribute1.localName = "English Name";
			modelLocalAttribute1.wikiUrl = "http://en.nnu.edu.cn/";
			modelLocalAttribute1.keywords.add("Keyword1");
			modelLocalAttribute1.keywords.add("Keyword2");
			modelLocalAttribute1.keywords.add("Keyword3");
			modelLocalAttribute1.abstractInfo = "Abstract Information";
			modelAttribute.addLocalAttributeInfo(modelLocalAttribute1);

			LocalAttribute modelLocalAttribute2 = new LocalAttribute();
			modelLocalAttribute2.localType = ELocalizationType.ELT_ZH_CN;
			modelLocalAttribute2.localName = "中文名称";
			modelLocalAttribute2.wikiUrl = "http://www.njnu.edu.cn/";
			modelLocalAttribute2.keywords.add("关键字1");
			modelLocalAttribute2.keywords.add("关键字2");
			modelLocalAttribute2.keywords.add("关键字3");
			modelLocalAttribute2.abstractInfo = "摘要";
			modelAttribute.addLocalAttributeInfo(modelLocalAttribute2);

			//////////////////////////////////////////////////////////////////////////
			modelCategory1.path = "New Path1";
			modelAttribute.updateCategory(0, modelCategory1);

			ModelCategory temp_model_cate = new ModelCategory("Principle", "New Path2");
			modelAttribute.updateCategory(0, temp_model_cate);
		}

		//////////////////////////////////////////////////////////////////////////
		//
		// ModelBehavior
		//
		//////////////////////////////////////////////////////////////////////////
		{
			ModelBehavior modelBehavior = modelClass.getModelBehavior();
			{
				ModelDatasetItem modelDatasetItem = new ModelDatasetItem();
				UdxDatasetSchema dataset1 = new UdxDatasetSchema(null, "Init Parameter");
				{
					UdxNodeSchema pNode1 = dataset1.addChildNode("node1", ESchemaNodeType.EDTKT_INT, "int");
					UdxNodeSchema pNode2 = dataset1.addChildNode("node2", ESchemaNodeType.EDTKT_REAL, "real");
					UdxNodeSchema pNode3 = dataset1.addChildNode("node3", ESchemaNodeType.EDTKT_LIST);
					UdxNodeSchema pNode4 = dataset1.addChildNode("node4", ESchemaNodeType.EDTKT_NODE);
					UdxNodeSchema pNode5 = pNode4.addChildNode("node5", ESchemaNodeType.EDTKT_TABLE, "我也不知道");
					UdxNodeSchema pNode5_1 = pNode5.addChildNode("node5_1", ESchemaNodeType.EDTKT_INT_LIST);
					UdxNodeSchema pNode5_2 = pNode5.addChildNode("node5_2", ESchemaNodeType.EDTKT_REAL_LIST);
					UdxNodeSchema pNode5_3 = pNode5.addChildNode("node5_3", ESchemaNodeType.EDTKT_VECTOR2_LIST);

					pNode5_1.getDescription().modifyConceptTag("{941671BD-9ED8-4146-979D-10F7C856DF2A}");
					pNode5_1.getDescription().modifySpatialReferenceTag("{941671BD-9ED8-4146-979D-10F7C856DF2A}");
					pNode5_1.getDescription().modifyUnitTag("{941671BD-9ED8-4146-979D-10F7C856DF2A}");
					pNode5_1.getDescription().modifyDataTemplateTag("{941671BD-9ED8-4146-979D-10F7C856DF2A}");
				}
				modelDatasetItem.datasetName = "Related Data1";
				modelDatasetItem.datasetItemType = EModelDatasetItemType.EMDIT_INTERNAL;
				modelDatasetItem.datasetItemDescription = "应该是什么描述";
				modelDatasetItem.externalId = "";
				modelDatasetItem.datasetItem = dataset1;

				modelBehavior.addModelDatasetItem(modelDatasetItem);
				
				//////////////////////////////////////////////////////////////////////////
				ModelDatasetItem modelDatasetItem1 = new ModelDatasetItem();
				UdxDatasetSchema dataset2 = new UdxDatasetSchema(null, "Parameter1");
				{
					dataset2.addChildNode("Node1", ESchemaNodeType.EDTKT_INT, "Param1");
					dataset2.addChildNode("Node2", ESchemaNodeType.EDTKT_INT, "Param2");
					UdxNodeSchema data_node1 =  dataset2.addChildNode("Node3", ESchemaNodeType.EDTKT_TABLE, "ParamTable");
					data_node1.addChildNode("Node3_1", ESchemaNodeType.EDTKT_VECTOR4_LIST, "ppp");
					data_node1.addChildNode("Node3_1", ESchemaNodeType.EDTKT_VECTOR3_LIST, "ppp");
					data_node1.addChildNode("Node3_1", ESchemaNodeType.EDTKT_REAL_LIST, "ppp");
				}
				modelDatasetItem1.datasetName = "Related Data2";
				modelDatasetItem1.datasetItemType = EModelDatasetItemType.EMDIT_INTERNAL;
				modelDatasetItem1.datasetItemDescription = "应该是什么描述1";
				modelDatasetItem1.externalId = "";
				modelDatasetItem1.datasetItem = dataset2;

				modelBehavior.addModelDatasetItem(modelDatasetItem1);

				//////////////////////////////////////////////////////////////////////////
				ModelDatasetItem modelDatasetItem2 = new ModelDatasetItem();
				modelDatasetItem2.datasetName = "Related Data3";
				modelDatasetItem2.datasetItemType = EModelDatasetItemType.EMDIT_EXTERNAL;
				modelDatasetItem2.datasetItemDescription = "应该是什么描述1";
				modelDatasetItem2.externalId = "{3CC9478F-252E-4631-BA6B-2C5C5CF94E35}";
				modelDatasetItem2.datasetItem = null;

				modelBehavior.addModelDatasetItem(modelDatasetItem2);
			}// end model dataset

			// begin model state
			{
				ModelState modelState1 = new ModelState();
				modelState1.stateId = "{3DCD4B5D-E097-4269-9C0F-6CDE362F33E7}";
				modelState1.stateName = "Step1";
				modelState1.stateType = EModelStateType.EMST_BASIC;
				modelState1.stateDecription = "状态的Description";

				ModelState modelState2 = new ModelState();
				modelState2.stateId = "{81D10275-F549-46D3-8741-48EEFEF45F63}";
				modelState2.stateName = "Step2";
				modelState2.stateType = EModelStateType.EMST_BASIC;
				modelState2.stateDecription = "状态的Description";

				ModelState modelState3 = new ModelState();
				modelState3.stateId = "{EF1CA40E-A02E-4EB8-A979-2A6C9B36ED54}";
				modelState3.stateName = "Step3";
				modelState3.stateType = EModelStateType.EMST_BASIC;
				modelState3.stateDecription = "状态的Description";

				//////////////////////////////////////////////////////////////////////////
				ModelEvent event1 = new ModelEvent();
				event1.eventName = "Init Event";
				event1.eventType = EModelEventType.EMET_RESPONSE;
				event1.eventDescription = "初始化事件";
				event1.datasetReference = "Related Data1";
				event1.parameterDescription = "参数描述";
				modelState1.modelEvents.add(event1);

				//////////////////////////////////////////////////////////////////////////
				modelBehavior.addModelState(modelState1);
				modelBehavior.addModelState(modelState2);
				modelBehavior.addModelState(modelState3);

				modelBehavior.addModelStateTransition(modelState1, modelState2);
				modelBehavior.addModelStateTransition(modelState2, modelState3);

				//////////////////////////////////////////////////////////////////////////
				ModelState temp_state = modelBehavior.getModelState(1);
				temp_state.stateName = "New Name";

				ModelEvent event2 = new ModelEvent();
				event2.eventName = "Init Event";
				event2.eventType = EModelEventType.EMET_NORESPONSE;
				event2.eventDescription = "初始化结束事件";
				event2.datasetReference = "Related Data2";
				event2.parameterDescription = "参数描述";
				temp_state.modelEvents.add(event2);

				modelBehavior.updateModelState(1, temp_state);

				//////////////////////////////////////////////////////////////////////////
				ModelState temp_state1 = modelBehavior.getModelState(2);
				
				ModelEvent event3 = new ModelEvent();
				event3.eventName = "Load Control Param";
				event3.eventType = EModelEventType.EMET_CONTROL;
				event3.eventDescription = "控制参数";
				event3.parameterDescription = "参数的描述信息";
				ModelDatasetItem related_data = modelBehavior.getModelDatasetItem(2);
				event3.datasetReference = related_data.datasetName;
				temp_state1.modelEvents.add(event3);

				modelBehavior.updateModelState(2, temp_state1);
			}// end model state
		}

		//////////////////////////////////////////////////////////////////////////
		//
		// ModelRuntime
		//
		//////////////////////////////////////////////////////////////////////////
		{
			ModelRuntime modelRuntime = modelClass.getModelRuntime();
			modelRuntime.setName("model name");
			modelRuntime.setVersion("5.4");
			modelRuntime.setBaseDirectory("$installPath/taudem/");
			modelRuntime.setEntry("slope.exe");

			HardwareRequirement hardwareRequirement = new HardwareRequirement();
			hardwareRequirement.requirementKey = "Main Frequency";
			hardwareRequirement.requirementValue = "2.8";
			modelRuntime.addHardwareRequirement(hardwareRequirement);

			SoftwareRequirement softwareRequirement = new SoftwareRequirement();
			softwareRequirement.requirementKey = "Microsoft Visual C++ 2010";
			softwareRequirement.requirementValue = "10";
			softwareRequirement.requirementPlatform = "x64";
			modelRuntime.addSoftwareRequirement(softwareRequirement);

			ModelAssembly modelAssembly = new ModelAssembly();
			modelAssembly.assemblyName = "dll";
			modelAssembly.assemblyPath = "./mpi.dll";
			modelRuntime.addModelAssembly(modelAssembly);

			SupportiveResource modelSupportiveResource = new SupportiveResource();
			modelSupportiveResource.resourceType = "data";
			modelSupportiveResource.resourceName = "./preload data.txt";
			modelRuntime.addSupportiveResource(modelSupportiveResource);
		}

		String xml_str = modelClass.FormatToXmlStream();
		System.out.print(xml_str);
		System.out.print("\n******************************************\n\n");

		//////////////////////////////////////////////////////////////////////////
		{
			ModelClass modelClass1 = new ModelClass();
			modelClass1.LoadFromXmlStream(xml_str);
	
			String xml_str1 = modelClass1.FormatToXmlStream();
			System.out.print(xml_str1);
			System.out.print("\n\n");
		}
		//////////////////////////////////////////////////////////////////////////
		{
			ModelClass modelClass1 = new ModelClass();
			boolean loadRet1 = modelClass1.LoadFromXmlFile("F:\\NGIS_Solution\\data\\AspectAnalysis_FrmDiff.mdl");
			System.out.println("*******************************************");
			System.out.print(modelClass1.FormatToXmlStream());
			System.out.print("\n");

			
			ModelClass modelClass2 = new ModelClass();
			boolean loadRet2 = modelClass2.LoadFromXmlFile("F:\\NGIS_Solution\\data\\AspectAnalysis_FrmDiff1.mdl");
			System.out.println("*******************************************");
			System.out.print(modelClass2.FormatToXmlStream());
			System.out.print("\n");

			if (loadRet1 && loadRet2)
			{
				CompareFlag compareFlag = new CompareFlag();
				boolean compareRet = modelClass1.compareOther(modelClass2, true, compareFlag);
				System.out.print("\n******************************************\n\n");
				System.out.println(compareRet);
				System.out.println(compareFlag.Obj);
				System.out.println(compareFlag.Name);
			}
		}
	}
	
}
