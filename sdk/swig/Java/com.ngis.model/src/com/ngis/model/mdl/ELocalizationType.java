package com.ngis.model.mdl;

public enum ELocalizationType {
	ELT_ZH_CN, // 中文
	ELT_EN_US, // 英文
	ELT_DE_DE, // 法文
	ELT_RU_RU, // 俄文
	ELT_AR, // 阿拉伯文
	ELT_ES, // 西班牙文
	ELT_COUNT
}
