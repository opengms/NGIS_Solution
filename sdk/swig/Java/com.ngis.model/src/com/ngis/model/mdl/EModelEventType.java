package com.ngis.model.mdl;

public enum EModelEventType {
	EMET_RESPONSE, EMET_NORESPONSE, EMET_CONTROL
}
