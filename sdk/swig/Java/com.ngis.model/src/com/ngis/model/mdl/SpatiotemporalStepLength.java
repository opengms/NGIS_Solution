package com.ngis.model.mdl;

//! Spatio-temportal Step Length
public class SpatiotemporalStepLength
{
    public double Min;
    public double Max;
    public String Unit;
    
    public SpatiotemporalStepLength()
    {
        this.Min = 0;
        this.Max = 0;
        this.Unit = "";
    }

    public SpatiotemporalStepLength(double min, double max, String unit)
    {
        this.Min = min;
        this.Max = max;
        this.Unit = unit;
    }

    public SpatiotemporalStepLength(String step, String unit)
    {
        if (step.isEmpty())
        {
            this.Min = 0;
            this.Max = 0;
        }
        else 
        {
            String sstep = step.substring(1, step.length() - 2);
            String[] nums = sstep.split(",");
            this.Min = Double.parseDouble(nums[0]);
            this.Max = Double.parseDouble(nums[1]);
        }
        this.Unit = unit;
    }
};