package com.ngis.model.context;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

public class ModelServiceContext 
{
	private Thread MyThread = null;
    private EModelContextStatus mContextStatus;

    private int mPort;
    private String mHost;
    private String mInstaceId;
    private String mMappingLibDir; //! 数据映射方法统一安装目录
    private String mInstanceDir; //! 当前模型运行实例的数据目录（以State文件夹组织）

    private DataOutputStream dos = null;
    private DataInputStream dis = null;
	private boolean bConnected = false;
    private Socket mClientSocket;
    private String mSendBuf;
    private String mReceiveBuf;

    private String mReceivedString;

    private String mCurrentState; //! 模型实例运行过程中当前的State
    private String mCurrentEvent; //! 模型实例运行过程中当前的Event

    private ERequestResponseDataFlag mRequestDataFlag;
    private ERequestResponseDataMIME mRequestDataMIME;
    private String mRequestDataBody;

    private ERequestResponseDataFlag mResponseDataFlag;
    private ERequestResponseDataMIME mResponseDataMIME;
    private String mResponseDataBody;

    public ModelServiceContext()
    {
        mContextStatus = EModelContextStatus.EMCS_UNKOWN;
    }

    //! 触发-初始化
    public String onInitialize(String pHost, String pPort, String pInstanceID)
    {
        mHost = pHost;
        mPort = Integer.parseInt(pPort);
        mInstaceId = pInstanceID;

        int retVal = bindSocket();
        if (retVal != 0) return "";

        //! 启动监听线程
        MyThread = new Thread(new RecvThread(this));
        MyThread.start();
        if (MyThread == null)
        {
            System.out.print("create thread error\n");
        }

        mContextStatus = EModelContextStatus.EMCS_INIT_BEGIN;
        mContextStatus = EModelContextStatus.EMCS_INIT;
        mSendBuf = "";
        mSendBuf = "{init}" + mInstaceId;
        sendMessage(mSendBuf);

        wait(1000, EModelContextStatus.EMCS_INIT_END);

        int idx = mReceivedString.indexOf('}');
        String header = mReceivedString.substring(0, idx + 1);
        String body = mReceivedString.substring(idx + 1, mReceivedString.length());

        int idx1 = body.indexOf('[');
        int idx2 = body.indexOf(']');
        mMappingLibDir = body.substring(idx1 + 1, idx2);

        idx1 = body.lastIndexOf('[');
        idx2 = body.lastIndexOf(']');
        mInstanceDir = body.substring(idx1 + 1, idx2);

        return mInstaceId;
    }

    //! 触发-进入状态
    public int onEnterState(String pInstanceId, String pStateId)
    {
        mCurrentState = pStateId;
        this.mContextStatus = EModelContextStatus.EMCS_STATE_ENTER_BEGIN;
        mSendBuf = "";
        mSendBuf = "{onEnterState}" + pInstanceId + "&" + pStateId;
        this.mContextStatus = EModelContextStatus.EMCS_STATE_ENTER;
        sendMessage(mSendBuf);

        this.wait(1000, EModelContextStatus.EMCS_STATE_ENTER_END);

        int idx = mReceivedString.indexOf('}');
        String header = mReceivedString.substring(0, idx+1);
        String body = mReceivedString.substring(idx + 1, mReceivedString.length());

        header += "\n"; System.out.print(header);
        return 0;
    }

    //! 触发-引发事件
    public int onFireEvent(String pInstanceId, String pStateId, String pEventId)
    {
        mCurrentEvent = pEventId;
        this.mContextStatus = EModelContextStatus.EMCS_EVENT_BEGIN;
        mSendBuf = "";
        mSendBuf = "{onFireEvent}" + pInstanceId + "&" + pStateId + "&" + pEventId;

        this.mContextStatus = EModelContextStatus.EMCS_EVENT;
        sendMessage(mSendBuf);

        this.wait(1000, EModelContextStatus.EMCS_EVENT_END);

        int idx = mReceivedString.indexOf('}');
        String header = mReceivedString.substring(0, idx + 1);
        String body = mReceivedString.substring(idx + 1, mReceivedString.length());

        header += "\n"; System.out.print(header);
        return 0;
    }

    //! 触发-向外请求数据
    public int onRequestData(String pInstanceId, String pStateId, String pEventId)
    {
        this.mContextStatus = EModelContextStatus.EMCS_REQUEST_BEGIN;
        mSendBuf = "";
        mSendBuf = "{onRequestData}" + pInstanceId + "&" + pStateId + "&" + pEventId;

        this.mContextStatus = EModelContextStatus.EMCS_REQUEST;
        sendMessage(mSendBuf);

        this.wait(1000, EModelContextStatus.EMCS_REQUEST_END);

        int idx = mReceivedString.indexOf('}');
        String header = mReceivedString.substring(0, idx + 1);
        idx = mReceivedString.lastIndexOf(']');
        String body = mReceivedString.substring(idx + 1, mReceivedString.length());

        header += "\n"; System.out.print(header);

        //////////////////////////////////////////////////////////////////////////
        int idx1 = mReceivedString.indexOf('[');
        int idx2 = mReceivedString.indexOf(']');
        String data_flag = mReceivedString.substring(idx1 + 1, idx2);

        if (data_flag.equals("OK"))
        {
            mRequestDataFlag = ERequestResponseDataFlag.ERDF_OK;
        }
        else
        {
            mRequestDataMIME = ERequestResponseDataMIME.ERDM_UNKNOW;
            mRequestDataBody = "";
            return 0;
        }

        idx1 = mReceivedString.lastIndexOf('[');
        idx2 = mReceivedString.lastIndexOf(']');
        String data_mime = mReceivedString.substring(idx1 + 1, idx2);

        if (data_mime.equals("XML|STREAM"))
        {
            mRequestDataMIME = ERequestResponseDataMIME.ERDM_XML_STREAM;
        }
        else if (data_mime.equals("ZIP|STREAM"))
        {
            mRequestDataMIME = ERequestResponseDataMIME.ERDM_ZIP_STREAM;
        }
        else if (data_mime.equals("RAW|STREAM"))
        {
            mRequestDataMIME = ERequestResponseDataMIME.ERDM_RAW_STREAM;
        }
        else if (data_mime.equals("XML|FILE"))
        {
            mRequestDataMIME = ERequestResponseDataMIME.ERDM_XML_FILE;
        }
        else if (data_mime.equals("ZIP|FILE"))
        {
            mRequestDataMIME = ERequestResponseDataMIME.ERDM_ZIP_FILE;
        }
        else if (data_mime.equals("RAW|FILE"))
        {
            mRequestDataMIME = ERequestResponseDataMIME.ERDM_RAW_FILE;
        }

        mRequestDataBody = body;

        return 0;
    }

    //! 触发-向外提供数据
    public int onResponseData(String pInstanceId, String pStateId, String pEventId)
    {
        if (mResponseDataFlag == ERequestResponseDataFlag.ERDF_OK)
        {
            this.mContextStatus = EModelContextStatus.EMCS_RESPONSE_BEGIN;
            int size = mResponseDataBody.length();
            mSendBuf = "";
            mSendBuf = "{onResponseData}" + pInstanceId + "&" + pStateId + "&" + pEventId + "&" + size + "[OK]";

            String sendBufStr = mSendBuf;

            if (mResponseDataMIME == ERequestResponseDataMIME.ERDM_XML_STREAM)
            {
                sendBufStr += "[XML|STREAM]";
            }
            else if (mResponseDataMIME == ERequestResponseDataMIME.ERDM_ZIP_STREAM)
            {
                sendBufStr += "[ZIP|STREAM]";
            }
            else if (mResponseDataMIME == ERequestResponseDataMIME.ERDM_RAW_STREAM)
            {
                sendBufStr += "[RAW|STREAM]";
            }
            else if (mResponseDataMIME == ERequestResponseDataMIME.ERDM_XML_FILE)
            {
                sendBufStr += "[XML|FIL]";
            }
            else if (mResponseDataMIME == ERequestResponseDataMIME.ERDM_ZIP_FILE)
            {
                sendBufStr += "[ZIP|FIL]";
            }
            else if (mResponseDataMIME == ERequestResponseDataMIME.ERDM_RAW_FILE)
            {
                sendBufStr += "[RAW|FIL]";
            }
            this.mContextStatus = EModelContextStatus.EMCS_RESPONSE;
            sendMessage(sendBufStr);

            this.wait(1000, EModelContextStatus.EMCS_RESPONSE_END);

            int idx = mReceivedString.indexOf('}');
            String header = mReceivedString.substring(0, idx + 1);
            String body = mReceivedString.substring(idx + 1, mReceivedString.length());

            header += "\n"; System.out.print(header);

            //////////////////////////////////////////////////////////////////////////
            this.mContextStatus = EModelContextStatus.EMCS_RESPONSE_BEGIN;
            sendBufStr += mResponseDataBody;
            this.mContextStatus = EModelContextStatus.EMCS_RESPONSE;
            sendMessage(sendBufStr);

            this.wait(1000, EModelContextStatus.EMCS_RESPONSE_END);

            mReceivedString += "\n"; System.out.print(mReceivedString);
        }
        else if (mResponseDataFlag == ERequestResponseDataFlag.ERDF_ERROR)
        {
            this.mContextStatus = EModelContextStatus.EMCS_RESPONSE_BEGIN;
            mSendBuf = "";
            mSendBuf = "{onResponseData}" + pInstanceId + "&" + pStateId + "&" + pEventId + "&0" + "[ERROR]";

            this.mContextStatus = EModelContextStatus.EMCS_RESPONSE;
            sendMessage(mSendBuf);

            this.wait(1000, EModelContextStatus.EMCS_RESPONSE_END);

            int idx = mReceivedString.indexOf('}');
            String header = mReceivedString.substring(0, idx + 1);
            String body = mReceivedString.substring(idx + 1, mReceivedString.length() - idx - 1);

            header += "\n"; System.out.print(header);
        }
        else if (mResponseDataFlag == ERequestResponseDataFlag.ERDF_NOTREADY)
        {
            this.mContextStatus = EModelContextStatus.EMCS_RESPONSE_BEGIN;
            int size = mResponseDataBody.length();
            mSendBuf = "";
            mSendBuf = "{onResponseData}" + pInstanceId + "&" + pStateId + "&" + pEventId + "&" + size + "[NOTREADY]";

            this.mContextStatus = EModelContextStatus.EMCS_RESPONSE;
            sendMessage(mSendBuf);

            this.wait(1000, EModelContextStatus.EMCS_RESPONSE_END);

            int idx = mReceivedString.indexOf('}');
            String header = mReceivedString.substring(0, idx + 1);
            String body = mReceivedString.substring(idx + 1, mReceivedString.length() - idx - 1);

            header += "\n"; System.out.print(header);
        }

        return 0;
    }

    //! 触发-提交错误信息
    public int onPostErrorInfo(String pInstanceId)
    {
        this.mContextStatus = EModelContextStatus.EMCS_POST_BEGIN;
        mSendBuf = "";
        mSendBuf = "{onPostErrorInfo}" + pInstanceId;
        this.mContextStatus = EModelContextStatus.EMCS_POST;
        sendMessage(mSendBuf);

        this.wait(1000, EModelContextStatus.EMCS_POST_END);

        int idx = mReceivedString.indexOf('}');
        String header = mReceivedString.substring(0, idx + 1);
        String body = mReceivedString.substring(idx + 1, mReceivedString.length() - idx - 1);

        header += "\n"; System.out.print(header);

        return 0;
    }

    //! 触发-离开状态
    public int onLeaveState(String pInstanceId, String pStateId)
    {
        this.mContextStatus = EModelContextStatus.EMCS_STATE_LEAVE_BEGIN;
        mSendBuf = "";
        mSendBuf = "{onLeaveState}" + pInstanceId + "&" + pStateId;
        this.mContextStatus = EModelContextStatus.EMCS_STATE_LEAVE;
        sendMessage(mSendBuf);

        this.wait(1000, EModelContextStatus.EMCS_STATE_LEAVE_END);

        int idx = mReceivedString.indexOf('}');
        String header = mReceivedString.substring(0, idx + 1);
        String body = mReceivedString.substring(idx + 1, mReceivedString.length());

        header += "\n"; System.out.print(header);

        return 0;
    }

    //! 触发-完成
    public int onFinalize(String pInstanceId)
    {
        this.mContextStatus = EModelContextStatus.EMCS_FINALIZE_BEGIN;
        mSendBuf = "";
        mSendBuf = "{onFinalize}" + pInstanceId;
        this.mContextStatus = EModelContextStatus.EMCS_FINALIZE;
        sendMessage(mSendBuf);

        this.wait(1000, EModelContextStatus.EMCS_FINALIZE_END);

        int idx = mReceivedString.indexOf('}');
        String header = mReceivedString.substring(0, idx + 1);
        String body = mReceivedString.substring(idx + 1, mReceivedString.length());

        header += "\n"; System.out.print(header);

        MyThread.stop();
        closeSocket();

        return 0;
    }

    //////////////////////////////////////////////////////////////////////////
    //! 请求到的数据Flag
    public ERequestResponseDataFlag getRequestDataFlag()
    {
        return mRequestDataFlag;
    }

    //! 请求到的数据类型
    public ERequestResponseDataMIME getRequestDataMIME()
    {
        return mRequestDataMIME;
    }

    //! 请求到的数据内容
    public String getRequestDataBody()
    {
        return mRequestDataBody;
    }

    //! 发送的数据标记Flag
    public void setResponseDataFlag(ERequestResponseDataFlag pFlag)
    {
        mResponseDataFlag = pFlag;
    }

    //! 发送的数据MIME类型
    public void setResponseDataMIME(ERequestResponseDataMIME pMIME)
    {
        mResponseDataMIME = pMIME;
    }

    //! 发送的数据内容
    public void setResponseDataBody(String pDataBody)
    {
        mResponseDataBody = pDataBody;
    }

    //! 发送出去的数据Flag
    public ERequestResponseDataFlag getResponseDataFlag()
    {
        return mResponseDataFlag;
    }

    //! 发送出去的数据MIME
    public ERequestResponseDataMIME getResponseDataMIME()
    {
        return mResponseDataMIME;
    }

    //! 发送出去的数据内容
    public String getResponseDataBody()
    {
        return mResponseDataBody;
    }

    //! 获取当前与外部通信的状态
    public EModelContextStatus getCurrentStatus()
    {
        return mContextStatus;
    }

    //////////////////////////////////////////////////////////////////////////
    public boolean startANewProcess(String cmd, String workingPath)
    {
    	try {
			ProcessUtility.callCmd(cmd);
			return true;
		} 
    	catch (IOException e) {
			e.printStackTrace();
			return false;
		}
    }

    //////////////////////////////////////////////////////////////////////////
    public String getCurrentDataDirectory()
    {
        String instanceDir = getModelInstanceDirectory();
        String stateDir = getCurrentRunningState() + "\\";
        String eventDir = getCurrentRunningEvent() + "\\";
        String path = instanceDir + stateDir + eventDir;
        return path;
    }

    //////////////////////////////////////////////////////////////////////////
    private int bindSocket()
    {
        try {
	        InetAddress ip;
			ip = InetAddress.getByName(mHost);
			mClientSocket = new Socket(ip, mPort);
			dos = new DataOutputStream(mClientSocket.getOutputStream());
			dis = new DataInputStream(mClientSocket.getInputStream());
		} 
        catch (IOException e) {
			e.printStackTrace();
		}
        return 0;
    }

    private int sendMessage(String pMessage)
    {
		try {
			PrintWriter write = new PrintWriter(mClientSocket.getOutputStream());
			write.print(pMessage+"\0");
			write.flush();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
    	
        return 0;
    }

    private String receiveMessage()
    {
		try {
			String str = "";
			byte[] b = new byte[1024];
            int r = dis.read(b);
            if(r>-1){
                str = new String(b);
                str = str.substring(0, r);
            }
			return str;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "";
    }

    private void closeSocket()
    {
        try {
			mClientSocket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
    }

    private void wait(int seconds, EModelContextStatus pStatus)
    {
        while (true)
        {
            if (mContextStatus == pStatus)
            {
                break;
            }
            try {
				Thread.currentThread().sleep(seconds);
			}
            catch (InterruptedException e) {
				e.printStackTrace();
			}
        }
    }

    private void setCurrentContextStatus(EModelContextStatus pStatus)
    {
        mContextStatus = pStatus;
    }

    private void setCurrentReceivedInfo(String pInfo)
    {
        mReceivedString = pInfo;
    }

    public String getMappingLibraryDirectory()
    {
        if (mMappingLibDir.lastIndexOf('\\') != mMappingLibDir.length() - 1)
        {
            mMappingLibDir += "\\";
        }
        return mMappingLibDir;
    }

    public String getModelInstanceDirectory()
    {
        if (mInstanceDir.lastIndexOf('\\') != mInstanceDir.length() - 1)
        {
            mInstanceDir += "\\";
        }
        return mInstanceDir;
    }

    //! 获取模型运行实例当前的State
    public String getCurrentRunningState() { return mCurrentState; }

    //! 获取模型运行实例当前的Event
    public String getCurrentRunningEvent() { return mCurrentEvent; }

    private static class RecvThread implements Runnable 
    {
    	private ModelServiceContext realContext = null;
        private volatile boolean isStop = false;
        public RecvThread(ModelServiceContext pModelServiceContext)
        {
        	realContext = pModelServiceContext;
        }
        
        @Override
        public void run() 
        {
			while (true) 
			{
				if (isStop)
				{
					break;
				}
				String receiveBuf = realContext.receiveMessage();
				if (receiveBuf.equals(""))
					continue;

				System.out.println("********" + receiveBuf);				

				realContext.setCurrentReceivedInfo(receiveBuf);
				int idx = receiveBuf.lastIndexOf('}');
				String header = receiveBuf.substring(0, idx + 1);
				if (header.equals("{Initialized}")==true) {
					realContext.setCurrentContextStatus(EModelContextStatus.EMCS_INIT_END);
				} 
				else if (header.equals("{Enter State Notified}")==true) {
					realContext.setCurrentContextStatus(EModelContextStatus.EMCS_STATE_ENTER_END);
				} 
				else if (header.equals("{Fire Event Notified}")==true) {
					realContext.setCurrentContextStatus(EModelContextStatus.EMCS_EVENT_END);
				} 
				else if (header.equals("{Request Data Notified}")==true) {
					realContext.setCurrentContextStatus(EModelContextStatus.EMCS_REQUEST_END);
				} 
				else if (header.equals("{Response Data Notified}")==true) {
					realContext.setCurrentContextStatus(EModelContextStatus.EMCS_RESPONSE_END);
				} 
				else if (header.equals("{Response Data Received}")==true) {
					realContext.setCurrentContextStatus(EModelContextStatus.EMCS_RESPONSE_END);
				} 
				else if (header.equals("{Post Error Info Notified}")==true) {
					realContext.setCurrentContextStatus(EModelContextStatus.EMCS_POST_END);
				} 
				else if (header.equals("{Leave State Notified}")==true) {
					realContext.setCurrentContextStatus(EModelContextStatus.EMCS_STATE_LEAVE_END);
				} 
				else if (header.equals("{Finalize Notified}")==true) {
					realContext.setCurrentContextStatus(EModelContextStatus.EMCS_FINALIZE_END);
					break;
				}

				if (header.equals("{kill}")==true) {
					System.exit(0);
				}
				try {
					Thread.currentThread().sleep(1000);
				} 
				catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
        }

        public void setStop(boolean stop){
            this.isStop = stop;
        }
    }
}
