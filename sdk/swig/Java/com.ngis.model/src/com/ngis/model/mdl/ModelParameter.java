package com.ngis.model.mdl;

public class ModelParameter {
    public String Key;
    public String Description;
    public String DefaultValue;
    
    public ModelParameter() {
    	
    }

    public ModelParameter(String key, String description, String defaultValue){
        this.Key = key;
        this.Description = description;
        this.DefaultValue = defaultValue;
    }
}
