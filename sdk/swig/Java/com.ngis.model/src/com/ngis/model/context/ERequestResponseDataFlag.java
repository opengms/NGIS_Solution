package com.ngis.model.context;

public enum ERequestResponseDataFlag 
{
    ERDF_OK,
    ERDF_NOTREADY,
    ERDF_ERROR,
    ERDF_UNKNOW
}
