package com.ngis.model.mdl;

public class SoftwareRequirement {
	public String requirementKey;
	public String requirementValue;
	public String requirementPlatform;

	public boolean compareOther(SoftwareRequirement pRequirement) {
		if (requirementKey.equals(pRequirement.requirementKey) == false)
			return false;
		if (requirementValue.equals(pRequirement.requirementValue) == false)
			return false;
		if (requirementPlatform.equals(pRequirement.requirementPlatform) == false)
			return false;
		return true;
	}
}
