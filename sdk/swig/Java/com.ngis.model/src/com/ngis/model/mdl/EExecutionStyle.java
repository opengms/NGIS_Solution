package com.ngis.model.mdl;

public enum EExecutionStyle {
	EES_STATE_SIMULATION, EES_SIMPLE_CALCULATION, EES_TIME_SERIES
}
