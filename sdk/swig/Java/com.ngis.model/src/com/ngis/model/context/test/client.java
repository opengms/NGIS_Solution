package com.ngis.model.context.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;

import com.ngis.model.context.ERequestResponseDataFlag;
import com.ngis.model.context.ERequestResponseDataMIME;
import com.ngis.model.context.ModelDataHandler;
import com.ngis.model.context.ModelServiceContext;
import com.ngis.udx.UdxDataset;
import com.ngis.udx.UdxKernel;
import com.ngis.udx.UdxKernelIntValue;

public class client 
{/**
     * 创建文件
     * @param fileName  文件名称
     * @param filecontent   文件内容
     * @return  是否创建成功，成功则返回true
     */
    public static boolean createFile(String fileName,String filecontent){
        Boolean bool = false;
        File file = new File(fileName);
        try {
            //如果文件不存在，则创建新的文件
            if(!file.exists()){
                file.createNewFile();
                bool = true;
                System.out.println("success create file,the file is "+fileName);
                //创建文件成功后，写入内容到文件里
                writeFileContent(fileName, filecontent);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return bool;
    }
    
    /**
     * 向文件中写入内容
     * @param filepath 文件路径与名称
     * @param newstr  写入的内容
     * @return
     * @throws IOException
     */
    public static boolean writeFileContent(String filepath,String newstr) throws IOException{
        Boolean bool = false;
        String filein = newstr+"\r\n";//新写入的行，换行
        String temp  = "";
        
        FileInputStream fis = null;
        InputStreamReader isr = null;
        BufferedReader br = null;
        FileOutputStream fos  = null;
        PrintWriter pw = null;
        try {
            File file = new File(filepath);//文件路径(包括文件名称)
            //将文件读入输入流
            fis = new FileInputStream(file);
            isr = new InputStreamReader(fis);
            br = new BufferedReader(isr);
            StringBuffer buffer = new StringBuffer();
            
            //文件原有内容
            for(int i=0;(temp =br.readLine())!=null;i++){
                buffer.append(temp);
                // 行与行之间的分隔符 相当于“\n”
                buffer = buffer.append(System.getProperty("line.separator"));
            }
            buffer.append(filein);
            
            fos = new FileOutputStream(file);
            pw = new PrintWriter(fos);
            pw.write(buffer.toString().toCharArray());
            pw.flush();
            bool = true;
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }finally {
            //不要忘记关闭
            if (pw != null) {
                pw.close();
            }
            if (fos != null) {
                fos.close();
            }
            if (br != null) {
                br.close();
            }
            if (isr != null) {
                isr.close();
            }
            if (fis != null) {
                fis.close();
            }
        }
        return bool;
    }
    
    public static void main(String[] args)
    {
    	ModelServiceContext pModelServiceContext = new ModelServiceContext();
        ModelDataHandler nativeDataHandler = new ModelDataHandler(pModelServiceContext);

        String host = "";
        String port = "";
        String instanceId = "";
        if (args.length < 3)
        {
            host = "127.0.0.1";
            port = "6000";
            instanceId = "eefee7a5-c31a-48ce-8cb1-74eee8c74111";
        }
        else
        {
        	host = args[0];
        	port = args[1];
        	instanceId = args[2];
        }

        String pInstanceID = pModelServiceContext.onInitialize(host, port, instanceId);
        if (pInstanceID == "")
            System.out.println("Init Failed");

        String pStateId1 = "state1";
        pModelServiceContext.onEnterState(pInstanceID, pStateId1);

        String pEventId1 = "event1";
        pModelServiceContext.onFireEvent(pInstanceID, pStateId1, pEventId1);

        pModelServiceContext.onRequestData(pInstanceID, pStateId1, pEventId1);

        if (pModelServiceContext.getRequestDataFlag() == ERequestResponseDataFlag.ERDF_OK)
        {
            //! 两种用法，一种是文件的，一种是直接加载UDX XML的
            if (pModelServiceContext.getRequestDataMIME() == ERequestResponseDataMIME.ERDM_ZIP_FILE)
            {
                nativeDataHandler.connectDataMappingMethod("DemoMappingMethod.exe");
                nativeDataHandler.conductUDXMapping("test.tif");
                String name = nativeDataHandler.getRealResultSaveName();
            }
            else if (pModelServiceContext.getRequestDataMIME() == ERequestResponseDataMIME.ERDM_XML_STREAM)
            {
                String xml_str = pModelServiceContext.getRequestDataBody();

                UdxDataset pDataset = new UdxDataset();
                pDataset.LoadFromXmlStream(xml_str);
                UdxKernel pKernel1 = pDataset.getChildNode(0).getKernel();
                UdxKernel pKernel2 = pDataset.getChildNode(1).getKernel();
                int k1 = ((UdxKernelIntValue)pKernel1).getTypedValue();
                int k2 = ((UdxKernelIntValue)pKernel2).getTypedValue();
            }
        }

        //////////////////////////////////////////////////////////////////////////
        String file_save_path = pModelServiceContext.getCurrentDataDirectory();
        int a = 12;
        int b = 13;
        int c = a * b;
        String stream = "***********For TEST**********\n";
        stream += "C = " + String.valueOf(c);
        
        createFile("E:\\test.txt", stream);
        //////////////////////////////////////////////////////////////////////////

        String pEventId2 = "event2";
        pModelServiceContext.onFireEvent(pInstanceID, pStateId1, pEventId2);

        pModelServiceContext.setResponseDataFlag(ERequestResponseDataFlag.ERDF_OK);
        pModelServiceContext.setResponseDataMIME(ERequestResponseDataMIME.ERDM_ZIP_FILE);

        ArrayList<String> rawFiles = new ArrayList<String>();
        rawFiles.add("E:\\test.txt");
        nativeDataHandler.connectDataMappingMethod("DemoMappingMethod.exe");
        nativeDataHandler.conductFileMapping("test.zip", rawFiles, 1);
        String name1 = nativeDataHandler.getRealResultSaveName();

        pModelServiceContext.setResponseDataBody(name1);

        pModelServiceContext.onResponseData(pInstanceID, pStateId1, pEventId1);

        pModelServiceContext.onLeaveState(pInstanceID, pStateId1);

        String pStateId2 = "state2";
        pModelServiceContext.onEnterState(pInstanceID, pStateId2);

        String pEventId3 = "event1";
        pModelServiceContext.onFireEvent(pInstanceID, pStateId2, pEventId3);

        pModelServiceContext.onLeaveState(pInstanceID, pStateId2);

        pModelServiceContext.onFinalize(pInstanceID);

        try {
			System.in.read();
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
}
