package com.ngis.model.mdl;

public class ModelCategory {
	public ModelCategory() {
		principle = "";
		path = "";
	}

	public ModelCategory(String pPrinciple, String pPath) {
		principle = pPrinciple;
		path = pPath;
	}

	public boolean compareOther(ModelCategory pCategory) {
		if (principle.equals(pCategory.principle) == false)
			return false;
		if (path.equals(pCategory.path) == false)
			return false;
		return true;
	}

	public String principle;
	public String path;
}
