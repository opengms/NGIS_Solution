package com.ngis.model.mdl;

public enum EMechanismItemType {
    EMIT_Unknown,   // Unknown type
    EMIT_Text,      // Text to describe model mechanism
    EMIT_Algorithm, // Algorithm to describe model mechanism
    EMIT_Formula    // Formula to describe model mechanism
}
