from IAttributeSet import Category, LocalAttribute
from IBehavior import ModelDatasetItem, ModelEvent, ModelParameter, ModelState, ModelStateTransition
from IRuntime import RequriementConfig, SoftwareConfig
from ModelClass import ModelClass

model = ModelClass('IDW_Extract', '3f6857ba-c2d2-4e27-b220-6e5367803a12', 'SimpleCalculation')

attr = model.getModelAttribute()

localAttr_EN = LocalAttribute('EN_US', 'IDW_Extract', 'https://desktop.arcgis.com/en/arcmap/10.3/tools/3d-analyst-toolbox/idw.htm', ['GIS', 'IDW'], 'Interpolates a raster surface from points using an inverse distance weighted (IDW) technique.')

attr.addLocalAttributeInfo(localAttr_EN)

cate1 = Category('GISTool', 'IDW')
attr.addCategoryInfo(cate1)

behavior = model.getBehavior()

shp_tmp = ModelDatasetItem('Shapefile_Template', 'external', 'Shapefile data', 'F73F31FF-2F23-4C7A-A57D-39D0C7A6C4E6')
behavior.addModelDatasetItem(shp_tmp)

wth_tmp = ModelDatasetItem('Proj_Template', 'external', 'Projection data', '6717dd5d-77db-40c0-b14c-15a794c3c517')
behavior.addModelDatasetItem(wth_tmp)

csv_tmp = ModelDatasetItem('CSV_Template', 'external', 'CSV data', 'ba1eb6d0-4112-4737-b70c-88328ede7b40')
behavior.addModelDatasetItem(csv_tmp)

zip_tmp = ModelDatasetItem('ZIP_Template', 'external', 'GeoTIFF data', 'd5d42bc9-e1c8-431b-a61f-441e7c55f683')
behavior.addModelDatasetItem(zip_tmp)

# state - site building
state1 = ModelState('8a98d615-8b30-44bb-8844-85fcc8e84d16', 'SitesDataBuilding', 'basic', 'Air sites data building')
event_date = ModelEvent('Date', 'response', 'Input date data', 'CSV_Template', 'Load', False)
event_airsites = ModelEvent('Airsites', 'response', 'Input air sites data', 'Shapefile_Template', 'Load', False)
event_msites = ModelEvent('Meteorologicalsites', 'response', 'Input meteorological sites data', 'CSV_Template', 'Load', False)
event_msitesfile = ModelEvent('Meteorologicalfieldsfile', 'response', 'Input meteorological fields file data', 'CSV_Template', 'Load', False)
event_mdata = ModelEvent('Meteorologicaldata', 'response', 'Input meteorological data', 'ZIP_Template', 'Load', False)
event_site = ModelEvent('SitesData', 'noresponse', 'Output sites data', 'ZIP_Template', 'Dispatch', False)
state1.events.append(event_date)
state1.events.append(event_airsites)
state1.events.append(event_msites)
state1.events.append(event_msitesfile)
state1.events.append(event_mdata)
state1.events.append(event_site)

behavior.addModelState(state1)

# state - IDW

state2 = ModelState('5066a883-c97d-4f33-a3f4-59bcfd25d830', 'IDW', 'basic', 'IDW')
event_prj = ModelEvent('PRJ', 'response', 'nput .prj data', 'Proj_Template', 'Load', False)
event_sitef = ModelEvent('Sitesdatafull', 'noresponse', 'Output full sites data', 'ZIP_Template', 'Dispatch', False)

state2.events.append(event_prj)
state2.events.append(event_sitef)

behavior.addModelState(state2)

runtime = model.getRuntime()

# runtime.setName('TestName')
runtime.setVersion('1.2.0.0')
runtime.setBaseDirectory('$(ModelServicePath)\\')
runtime.setEntry('IDW_Extract.py')

runtime.addHardwareRequirement(RequriementConfig('Main Frequency', '1.0'))
runtime.addHardwareRequirement(RequriementConfig('Memory Size', '1024MB'))

runtime.addModelAssembly(RequriementConfig('python.exe', '$(DataMappingPath)\\Python27\\'))

model.formatToXMLFile('mdl_IDW_extract.mdl')

print('finished!')