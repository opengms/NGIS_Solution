#ifndef __I_NGISMODEL_MODEL_RUNTIME_H__
#define __I_NGISMODEL_MODEL_RUNTIME_H__

#include "../udx/INxUnknown.h"

namespace NGIS
{
	namespace Model
	{
		struct HardwareRequirement
		{
			std::string requirementKey;
			std::string requirementValue;

			bool compareOther(HardwareRequirement pRequirement)
			{
				if (requirementKey != pRequirement.requirementKey) return false;
				if (requirementValue != pRequirement.requirementValue) return false;
				return true;
			}
		};

		struct SoftwareRequirement
		{
			std::string requirementKey;
			std::string requirementValue;

			bool compareOther(SoftwareRequirement pRequirement)
			{
				if (requirementKey != pRequirement.requirementKey) return false;
				if (requirementValue != pRequirement.requirementValue) return false;
				return true;
			}
		};

		struct ModelAssembly
		{
			std::string assemblyName;
			std::string assemblyPath;

			bool compareOther(ModelAssembly pAssembly)
			{
				if (assemblyName != pAssembly.assemblyName) return false;
				if (assemblyPath != pAssembly.assemblyPath) return false;
				return true;
			}
		};

		struct SupportiveResource
		{
			std::string resourceType;
			std::string resourceName;

			bool compareOther(SupportiveResource pResource)
			{
				if (resourceType != pResource.resourceType) return false;
				if (resourceName != pResource.resourceName) return false;
				return true;
			}
		};

		class IModelRuntime : public INxUnknown
		{
		public:
			virtual std::string getName() = 0;

			virtual void setName(std::string pName) = 0;

			virtual std::string getVersion() = 0;

			virtual void setVersion(std::string pVersion) = 0;

			virtual std::string getEntry() = 0;

			virtual void setEntry(std::string pEntry) = 0;

			virtual std::string getBaseDirectory() = 0;

			virtual void setBaseDirectory(std::string pDirectory) = 0;

			//////////////////////////////////////////////////////////////////////////
			virtual bool addHardwareRequirement(HardwareRequirement& pRequirement) = 0;

			virtual bool addSoftwareRequirement(SoftwareRequirement& pRequirement) = 0;

			virtual bool addModelAssembly(ModelAssembly& pRequirement) = 0;

			virtual bool addSupportiveResource(SupportiveResource& pRequirement) = 0;

			//////////////////////////////////////////////////////////////////////////
			virtual int getHardwareRequirementCount() = 0;

			virtual int getSoftwareRequirementCount() = 0;

			virtual int getModelAssemblyCount() = 0;

			virtual int getSupportiveResourceCount() = 0;

			virtual bool getHardwareRequirement(int idx, HardwareRequirement& pRequirement) = 0;

			virtual bool getSoftwareRequirement(int idx, SoftwareRequirement& pRequirement) = 0;

			virtual bool getModelAssembly(int idx, ModelAssembly& pRequirement) = 0;

			virtual bool getSupportiveResource(int idx, SupportiveResource& pRequirement) = 0;

			//////////////////////////////////////////////////////////////////////////
			virtual bool removeHardwareRequirement(HardwareRequirement& pRequirement) = 0;

			virtual bool removeSoftwareRequirement(SoftwareRequirement& pRequirement) = 0;

			virtual bool removeModelAssembly(ModelAssembly& pRequirement) = 0;

			virtual bool removeSupportiveResource(SupportiveResource& pRequirement) = 0;

			virtual bool removeHardwareRequirement(int idx) = 0;

			virtual bool removeSoftwareRequirement(int idx) = 0;

			virtual bool removeModelAssembly(int idx) = 0;

			virtual bool removeSupportiveResource(int idx) = 0;

			//////////////////////////////////////////////////////////////////////////
			virtual bool compareOther(IModelRuntime* pRuntime, std::string& obj, std::string& name) = 0;
		};
	}
}

#endif