#ifndef __I_NGISMODEL_MODEL_ATTRIBUTE_H__
#define __I_NGISMODEL_MODEL_ATTRIBUTE_H__

#include "../udx/INxUnknown.h"
#include <string>
#include <vector>

namespace NGIS
{
	namespace Model
	{
		enum ELocalizationType
		{
			ELT_ZH_CN, //中文
			ELT_EN_US, //英文
			ELT_DE_DE, //法文
			ELT_RU_RU, //俄文
			ELT_AR, //阿拉伯文
			ELT_ES, //西班牙文
			ELT_COUNT
		};

		struct LocalAttribute
		{
			LocalAttribute()
			{
				localType = ELT_COUNT;
				localName = "Name";
				abstractInfo = "Abstract";
				wikiUrl = "";
				keywords.clear();
			}

			bool compareOther(LocalAttribute pLocal)
			{
				if (localType != pLocal.localType) return false;
				if (localName != pLocal.localName) return false;
				if (abstractInfo != pLocal.abstractInfo) return false;
				if (wikiUrl != pLocal.wikiUrl) return false;
				if (keywords.size() != pLocal.keywords.size()) return false;
				for (int iKey=0; iKey<keywords.size(); iKey++)
				{
					if (keywords[iKey] != pLocal.keywords[iKey])
					{
						return false;
					}
				}
				return true;
			}

			ELocalizationType localType;
			std::string localName;
			std::string abstractInfo;
			std::string wikiUrl;
			std::vector<std::string> keywords;
		};

		struct ModelCategory
		{
			ModelCategory()
			{
				principle = "";
				path = "";
			}
			ModelCategory(std::string pPrinciple, std::string pPath)
			{
				principle = pPrinciple;
				path = pPath;
			}
			bool compareOther(ModelCategory pCategory)
			{
				if (principle != pCategory.principle) return false;
				if (path != pCategory.path) return false;
				return true;
			}
			std::string principle;
			std::string path;
		};

		static std::string LocalizationType2String(ELocalizationType pType)
		{
			if (pType == ELT_ZH_CN) return "ZH_CN";
			else if (pType == ELT_EN_US) return "EN_US";
			else if (pType == ELT_DE_DE) return "DE_DE";
			else if (pType == ELT_RU_RU) return "RU_RU";
			else if (pType == ELT_AR) return "AR";
			else if (pType == ELT_ES) return "ES";
			return "Unknown";
		}

		static ELocalizationType String2LocalizationType(std::string pTypeStr)
		{
			if (pTypeStr == "ZH_CN") return ELT_ZH_CN;
			else if (pTypeStr == "EN_US") return ELT_EN_US;
			else if (pTypeStr == "DE_DE") return ELT_DE_DE;
			else if (pTypeStr == "RU_RU") return ELT_RU_RU;
			else if (pTypeStr == "AR") return ELT_AR;
			else if (pTypeStr == "ES") return ELT_ES;
			return ELT_COUNT;
		}

		class IModelAttribute : public INxUnknown
		{
		public:
			virtual int getLocalAttributeCount() = 0;

			virtual bool getLocalAttribute(int idx, LocalAttribute& pLocalAttribute) = 0;

			virtual bool getLocalAttribute(ELocalizationType pLocalType, LocalAttribute& pLocalAttribute) = 0;

			virtual bool addLocalAttributeInfo(LocalAttribute& pLocalAttribute) = 0;

			virtual bool removeLocalAttribute(int idx) = 0;

			virtual bool removeLocalAttribute(LocalAttribute& pLocalAttribute) = 0;

			virtual bool updateLocalAttribute(int idx, LocalAttribute& pLocalAttribute) = 0;

			virtual bool updateLocalAttribute(ELocalizationType pLocalType, LocalAttribute& pLocalAttribute) = 0;

			//////////////////////////////////////////////////////////////////////////
			virtual int getCategoryCount() = 0;

			virtual bool getCategory(int idx, ModelCategory& pModelCategory) = 0;

			virtual bool addCategoryInfo(ModelCategory& pCategoryInfo) = 0;

			virtual bool removeCategory(int idx) = 0;

			virtual bool removeCategory(ModelCategory& pCategoryInfo) = 0;

			virtual bool updateCategory(int idx, ModelCategory& pCategoryInfo) = 0;

			//////////////////////////////////////////////////////////////////////////
			virtual bool compareOther(IModelAttribute* pAttribute, std::string& obj, std::string& name) = 0;
		};
	}
}

#endif