#ifndef __I_NGISDATA_UDXSCHEMA_DATASET_H__
#define __I_NGISDATA_UDXSCHEMA_DATASET_H__

#include "INxUnknown.h"
#include "IUdxSchemaNode.h"

namespace NGIS
{
	namespace Data
	{
		namespace Schema
		{
			class IUdxDatasetSchema : public virtual IUdxNodeSchema
			{
			public:
				virtual bool LoadFromXmlFile(const char* fileName) = 0;

				virtual bool FormatToXmlFile(const char* fileName) = 0;

				virtual bool LoadFromXmlStream(const char* xmlStr) = 0;

				virtual bool FormatToXmlStream(std::string& xmlStr) = 0;

				virtual bool LoadFromXmlElement(const void* xmlElement) = 0;

				virtual bool FormatToXmlElement(void* xmlElement) = 0;
			};
		}
	}
}

#endif