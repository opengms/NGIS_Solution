#ifndef __I_NGISDATA_UDXSCHEMA_NODE_H__
#define __I_NGISDATA_UDXSCHEMA_NODE_H__

#include "INxUnknown.h"
#include "IUdxSchemaDescription.h"

namespace NGIS
{
	namespace Data
	{
		namespace Schema
		{
			class IUdxNodeSchema : public INxUnknown
			{
			public:
				virtual const char* getName() = 0;

				virtual INodeDescription* getDescription() = 0;

				virtual bool modifyName(const char* pName) = 0;

				virtual IUdxNodeSchema* getParentNode() = 0;

				virtual int getChildNodeCount() = 0;

				virtual IUdxNodeSchema* getChildNode(int idx) = 0;

				virtual IUdxNodeSchema* addChildNode(const char* pName, INodeDescription* pDescription) = 0;

				virtual IUdxNodeSchema* addChildNode(const char* pName, int pNodeType, const char* pNodeInfo="") = 0;

				virtual bool removeChildNode(IUdxNodeSchema* pNode) = 0;

				virtual bool removeChildNode(int idx) = 0;

				virtual void setExtension(int flag) = 0;

				virtual int getExtension() = 0;

				virtual bool compareOther(IUdxNodeSchema* pNode) = 0;
			};
		}
	}
}


#endif