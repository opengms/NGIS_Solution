﻿/**************************************************************************************
*
* Filename: IMetaDataDocument.cs
* Description: 
* Author: Songshan Yue
* Company: Nanjing Normal University OpenGMS
* Date: 2018/9/20
*
**************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Xml;

namespace NGIS
{
    namespace Model
    {
        public enum ELocalizationType
        {
            ELT_ZH_CN, //中文
            ELT_EN_US, //英文
            ELT_DE_DE, //法文
            ELT_RU_RU, //俄文
            ELT_AR, //阿拉伯文
            ELT_ES, //西班牙文
            ELT_COUNT
        };

        public struct LocalAttribute
		{

            public bool compareOther(LocalAttribute pLocal)
			{
				if (localType != pLocal.localType) return false;
				if (localName != pLocal.localName) return false;
				if (abstractInfo != pLocal.abstractInfo) return false;
				if (wikiUrl != pLocal.wikiUrl) return false;
				if (keywords.Count != pLocal.keywords.Count) return false;
				for (int iKey=0; iKey<keywords.Count; iKey++)
				{
					if (keywords[iKey] != pLocal.keywords[iKey])
					{
						return false;
					}
				}
				return true;
			}

            public static bool operator == (LocalAttribute laA, LocalAttribute laB)
            {
                return laA.compareOther(laB);
            }

            public static bool operator !=(LocalAttribute laA, LocalAttribute laB)
            {
                return !(laA == laB);
            }

            public ELocalizationType localType;
            public string localName;
            public string abstractInfo;
            public string wikiUrl;
            public List<string> keywords;
		};

        public struct ModelCategory
		{
            public ModelCategory(string pPrinciple, string pPath)
			{
				principle = pPrinciple;
				path = pPath;
			}
            public bool compareOther(ModelCategory pCategory)
			{
				if (principle != pCategory.principle) return false;
				if (path != pCategory.path) return false;
				return true;
			}
            public string principle;
            public string path;

            public static bool operator ==(ModelCategory mcA, ModelCategory mcB)
            {
                return mcA.compareOther(mcB);
            }

            public static bool operator !=(ModelCategory mcA, ModelCategory mcB)
            {
                return !(mcA == mcB);
            }
		};

        //! Model User Institution
        public struct ModelUserInstitution
        {
            public string Name;
            public string Country;
            public string City;
            public string Address;

            public ModelUserInstitution(string name, string country, string city, string address)
            {
                this.Name = name;
                this.Country = country;
                this.City = city;
                this.Address = address;
            }

            public static bool operator == (ModelUserInstitution insA, ModelUserInstitution insB)
            {
                if (insA.Name == insB.Name && insA.Country == insB.Country && insA.City == insB.City && insA.Address == insB.Address)
                {
                    return true;
                }
                return false;
            }

            public static bool operator !=(ModelUserInstitution insA, ModelUserInstitution insB)
            {
                return !(insA == insB);
            }
        };

        //! Model User Contact
        public enum EContactType
        {
            ECT_Unknown = 0,
            ECT_Email = 1,
            ECT_Fax = 2,
            ECT_Phone = 3
        };

        //! Model User Contact
        public struct ModelUserContact
        {
            public EContactType Type;
            public string Value;

            public ModelUserContact(EContactType type, string value)
            {
                this.Type = type;
                this.Value = value;
            }

            public static bool operator ==(ModelUserContact mucA, ModelUserContact mucB)
            {
                if (mucA.Type == mucB.Type && mucA.Value == mucB.Value) return true;
                return false;
            }

            public static bool operator !=(ModelUserContact mucA, ModelUserContact mucB)
            {
                return !(mucA == mucB);
            }
        }

        public struct ModelUser
        {
            public string Name;
            public string Account;
            public ModelUserInstitution Institution;
            public List<ModelUserContact> Contacts;

            public ModelUser(string name, string account, ModelUserInstitution ins, List<ModelUserContact> contacts)
            {
                this.Name = name;
                this.Account = account;
                this.Institution = ins;
                this.Contacts = contacts;
            }

            public static bool operator ==(ModelUser userA, ModelUser userB)
            {
                if (userA.Name == userB.Name && userA.Name == userB.Name && userA.Name == userB.Name && userA.Name == userB.Name && userA.Name == userB.Name && userA.Name == userB.Name)
                {
                    return true;
                }
                return false;
            }

            public static bool operator !=(ModelUser userA, ModelUser userB)
            {
                return !(userA != userB);
            }
        };

        public class LocalizationHelper
        {
            public static string LocalizationType2String(ELocalizationType pType)
            {
                if (pType == ELocalizationType.ELT_ZH_CN) return "ZH_CN";
                else if (pType == ELocalizationType.ELT_EN_US) return "EN_US";
                else if (pType == ELocalizationType.ELT_DE_DE) return "DE_DE";
                else if (pType == ELocalizationType.ELT_RU_RU) return "RU_RU";
                else if (pType == ELocalizationType.ELT_AR) return "AR";
                else if (pType == ELocalizationType.ELT_ES) return "ES";
                return "Unknown";
            }

            public static ELocalizationType String2LocalizationType(string pTypeStr)
            {
                if (pTypeStr == "ZH_CN") return ELocalizationType.ELT_ZH_CN;
                else if (pTypeStr == "EN_US") return ELocalizationType.ELT_EN_US;
                else if (pTypeStr == "DE_DE") return ELocalizationType.ELT_DE_DE;
                else if (pTypeStr == "RU_RU") return ELocalizationType.ELT_RU_RU;
                else if (pTypeStr == "AR") return ELocalizationType.ELT_AR;
                else if (pTypeStr == "ES") return ELocalizationType.ELT_ES;
                return ELocalizationType.ELT_COUNT;
            }

            public static EContactType String2ContactType(string sContact)
            {
                if (sContact == "Email") return EContactType.ECT_Email;
                else if (sContact == "Fax") return EContactType.ECT_Fax;
                else if (sContact == "Phone") return EContactType.ECT_Phone;
                return EContactType.ECT_Unknown;
            }

            public static string ContactType2String(EContactType eContact)
            {
                if (eContact == EContactType.ECT_Email) return "Email";
                else if (eContact == EContactType.ECT_Fax) return "Fax";
                else if (eContact == EContactType.ECT_Phone) return "Phone";
                return "Unknown";
            }

            public static List<ModelUser> Xml2ModelUser(XmlElement mele)
            {
                List<ModelUser> list_users = new List<ModelUser>();
                foreach (XmlElement ele in mele.ChildNodes)
                {
                    string name = ele.GetAttribute("name");
                    string account = ele.GetAttribute("account");

                    XmlElement ins_ele = (XmlElement)mele.ChildNodes[0];
                    string insname = ele.GetAttribute("name");
                    string countryname = ele.GetAttribute("country");
                    string city = ele.GetAttribute("city");
                    string address = ele.GetAttribute("address");
                    ModelUserInstitution mui = new ModelUserInstitution(insname, countryname, city, address);

                    XmlElement contact_ele = (XmlElement)mele.ChildNodes[0];

                    List<ModelUserContact> list_contacts = new List<ModelUserContact>();
                    foreach (XmlElement cele in contact_ele.ChildNodes)
                    {
                        string type = cele.GetAttribute("type");
                        string value = cele.GetAttribute("value");
                        list_contacts.Add(new ModelUserContact(LocalizationHelper.String2ContactType(type), value));
                    }
                    ModelUser mu = new ModelUser(name, account, mui, list_contacts);
                    list_users.Add(mu);
                }
                return list_users;
            }
        };

        public interface IModelAttribute
        {
            int getLocalAttributeCount();

            bool getLocalAttribute(int idx, ref LocalAttribute pLocalAttribute);

            bool getLocalAttribute(ELocalizationType pLocalType, ref LocalAttribute pLocalAttribute);

            bool addLocalAttributeInfo(ref LocalAttribute pLocalAttribute);

            bool removeLocalAttribute(int idx);

            bool removeLocalAttribute(ref LocalAttribute pLocalAttribute);

            bool updateLocalAttribute(int idx, ref LocalAttribute pLocalAttribute);

            bool updateLocalAttribute(ELocalizationType pLocalType, ref LocalAttribute pLocalAttribute);

            //////////////////////////////////////////////////////////////////////////
            int getCategoryCount();

            bool getCategory(int idx, ref ModelCategory pModelCategory);

            bool addCategoryInfo(ref ModelCategory pCategoryInfo);

            bool removeCategory(int idx);

            bool removeCategory(ref ModelCategory pCategoryInfo);

            bool updateCategory(int idx, ref ModelCategory pCategoryInfo);
            
			//////////////////////////////////////////////////////////////////////////
			bool compareOther(IModelAttribute pAttribute, ref string obj, ref string name);
        };
    }
}
