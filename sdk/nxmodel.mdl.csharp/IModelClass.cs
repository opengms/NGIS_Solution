﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NGIS
{
    namespace Model
    {
		public enum EExecutionStyle
		{
			EES_STATE_SIMULATION,
			EES_SIMPLE_CALCULATION,
			EES_TIME_SERIES
		};

        public interface IModelClass
        {
            void setName(string pName);
            
            string getName();
            
            void setUID(string pUID);
            
            string getUID() ;
            
            void setExecutionStyle(EExecutionStyle pStyle);
            
            EExecutionStyle getExecutionStyle();
            
            IModelAttribute getModelAttribute();
            
            IModelBehavior getModelBehavior();
            
            IModelRuntime getModelRuntime();
            
            bool LoadFromXmlFile(string fileName);
            
            bool FormatToXmlFile(string fileName);
            
            bool LoadFromXmlStream(string xmlStr);
            
            bool FormatToXmlStream(ref string xmlStr);
			//////////////////////////////////////////////////////////////////////////
            bool compareOther(IModelClass pClass, bool withRuntime, ref string obj, ref string name);
        }
    }
}
