﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NGIS
{
    namespace Model
    {
        public struct HardwareRequirement
		{
            public string requirementKey;
            public string requirementValue;

            public bool compareOther(HardwareRequirement pRequirement)
			{
				if (requirementKey != pRequirement.requirementKey) return false;
				if (requirementValue != pRequirement.requirementValue) return false;
				return true;
			}

            public static bool operator == (HardwareRequirement pRequirementA, HardwareRequirement pRequirementB)
            {
                return pRequirementA.compareOther(pRequirementB);
            }

            public static bool operator !=(HardwareRequirement pRequirementA, HardwareRequirement pRequirementB)
            {
                return !(pRequirementA == pRequirementB);
            }
		};

        public struct SoftwareRequirement
		{
            public string requirementKey;
            public string requirementValue;
            public string requirementPlatform;

            public bool compareOther(SoftwareRequirement pRequirement)
			{
                if (requirementKey != pRequirement.requirementKey) return false;
                if (requirementValue != pRequirement.requirementValue) return false;
                if (requirementPlatform != pRequirement.requirementPlatform) return false;
				return true;
			}

            public static bool operator ==(SoftwareRequirement srA, SoftwareRequirement srB)
            {
                return srA.compareOther(srB);
            }

            public static bool operator !=(SoftwareRequirement srA, SoftwareRequirement srB)
            {
                return !(srA == srB);
            }

		};

        public struct ModelAssembly
		{
            public string assemblyName;
            public string assemblyPath;

            public bool compareOther(ModelAssembly pAssembly)
			{
				if (assemblyName != pAssembly.assemblyName) return false;
				if (assemblyPath != pAssembly.assemblyPath) return false;
				return true;
			}

            public static bool operator == (ModelAssembly maA, ModelAssembly maB)
            {
                return maA.compareOther(maB);
            }

            public static bool operator !=(ModelAssembly maA, ModelAssembly maB)
            {
                return !(maA == maB);
            }
		};

        public struct SupportiveResource
		{
            public string resourceType;
            public string resourceName;

            public bool compareOther(SupportiveResource pResource)
			{
				if (resourceType != pResource.resourceType) return false;
				if (resourceName != pResource.resourceName) return false;
				return true;
			}

            public static bool operator ==(SupportiveResource srA, SupportiveResource srB)
            {
                return srA.compareOther(srB);
            }

            public static bool operator !=(SupportiveResource srA, SupportiveResource srB)
            {
                return !(srA == srB);
            }
		};

        public interface IModelRuntime
        {
             string getName();

			 void setName(string pName);

			 string getVersion();

			 void setVersion(string pVersion);

			 string getEntry();

			 void setEntry(string pEntry);

			 string getBaseDirectory();

			 void setBaseDirectory(string pDirectory);

			//////////////////////////////////////////////////////////////////////////
			 bool addHardwareRequirement(ref HardwareRequirement pRequirement);

			 bool addSoftwareRequirement(ref SoftwareRequirement pRequirement);

			 bool addModelAssembly(ref ModelAssembly pRequirement);

			 bool addSupportiveResource(ref SupportiveResource pRequirement);

			//////////////////////////////////////////////////////////////////////////
			 int getHardwareRequirementCount();

			 int getSoftwareRequirementCount();

			 int getModelAssemblyCount();

			 int getSupportiveResourceCount();

			 bool getHardwareRequirement(int idx, ref HardwareRequirement pRequirement);

			 bool getSoftwareRequirement(int idx, ref SoftwareRequirement pRequirement);

			 bool getModelAssembly(int idx, ref ModelAssembly pRequirement);

			 bool getSupportiveResource(int idx, ref SupportiveResource pRequirement);

			//////////////////////////////////////////////////////////////////////////
			 bool removeHardwareRequirementref (HardwareRequirement pRequirement);

			 bool removeSoftwareRequirement(ref SoftwareRequirement pRequirement);

			 bool removeModelAssembly(ref ModelAssembly pRequirement);

			 bool removeSupportiveResource(ref SupportiveResource pRequirement);

			 bool removeHardwareRequirement(int idx);

			 bool removeSoftwareRequirement(int idx);

			 bool removeModelAssembly(int idx);

			 bool removeSupportiveResource(int idx);

			//////////////////////////////////////////////////////////////////////////
			 bool compareOther(IModelRuntime pRuntime, ref string obj, ref string name);
        }
    }
}