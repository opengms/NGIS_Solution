#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>
#include "IDataMappingHandle.h"
#include "NativeDataMappingAPI.h"

using namespace NGIS::Data;
using namespace NGIS::Data::Schema;
using namespace NGIS::DataMapping;

namespace NGIS
{
	namespace DataMapping
	{
		class GDALRasterMapping
		{
		private:
			IDataMappingHandle* mHandle;

			std::string udx_filename;
			std::string format_filename;
			std::string node_idx_array;
			std::string read_node_name;

			int read_write_flag; //0表示读UDX，1表示写原始数据
		public:
			GDALRasterMapping(int argc, char *argv[])
			{
				mHandle = createGeneralRasterHandle();
				udx_filename="";
				format_filename="";
				node_idx_array="";
				read_node_name="";

				int read_write_flag = -1; //0表示读UDX，1表示写原始数据

				parse_arguments(argc, argv);
			}

			~GDALRasterMapping()
			{
				mHandle->release();
				mHandle = NULL;
			}

			void parse_arguments(int argc, char *argv[])
			{

				if (argc == 1)
				{
					printf("**************Data Mapping Method**************\n");
					do_getName();
					do_help();
				}
				else
				{
					for (int i=1; i<argc; i++)
					{
						if ((*argv[i])=='-')
						{
							const char *p=argv[i]+1;

							while ((*p)!='\0')
							{
								char c=*(p++);;
								if ((c=='h') || (c=='H'))
								{
									do_help();
								}
								else if ((c=='v') || (c=='V'))
								{
									do_getVersion();
								}
								else if ((c=='s') || (c=='S'))
								{
									do_getSchema();
								}
								else if ((c=='c') || (c=='C'))
								{
									do_getCapablity();
								}
								else if ((c=='w') || (c=='W'))
								{
									read_write_flag = 1;
								}
								else if ((c=='r') || (c=='R'))
								{
									read_write_flag = 0;
								}
								else if ((c=='u') || (c=='U'))
								{
									udx_filename=argv[i+1];
									//printf("UDX file name: %s\n", udx_filename.c_str());
								}
								else if ((c=='f') || (c=='F'))
								{
									format_filename=argv[i+1];
									//printf("raw file name: %s\n", format_filename.c_str());
								}
								else if ((c=='i') || (c=='I'))
								{
									node_idx_array=argv[i+1];
									std::string node_idx_array_str = node_idx_array;
									node_idx_array_str.find(',');
									//printf("node index: %s\n", node_idx_array_str.c_str());
								}
								else if ((c=='n') || (c=='N'))
								{
									read_node_name=argv[i+1];
									//printf("read node name: %s\n", read_node_name.c_str());
								}
								else
								{
									do_help();
									return;
								}
							}
						}
						else
						{
						}
					}
				}
			}

			void do_help()
			{
				printf("Usage : DataMappingTemplate.exe [-h] [-v] [-c] [-u] udx.xml [-f] format.* [-i] [1,1,1]\n\n" \
					"  -h\tShow help information\n" \
					"  -v\tShow version\n" \
					"  -c\tList Capabilitier\n" \
					"  -u\tUDX file path\n" \
					"  -r\tRead UDX node from raw data\n" \
					"  -w\tWrite raw data from UDX node\n" \
					"  -f\tOriginal format file path\n" \
					"  -n\tName of try read node\n" \
					"  -i\tSet read index for each related node\n\n");
			}

			void do_getVersion()
			{
				printf("name: %s \n", mHandle->GetHandleVersion());
			}

			void do_getName()
			{
				printf("version: %s \n", mHandle->GetHandleName());
			}

			void do_getSchema()
			{
				IUdxDatasetSchema* dataSchema = mHandle->GetUdxSchema();
				std::string xml_str = "";
				dataSchema->FormatToXmlStream(xml_str);
				printf("Supported Schema: \n%s\n", xml_str.c_str());
			}

			void do_getCapablity()
			{
				const char* name = mHandle->GetHandleName();
				int capabilityCount = mHandle->GetCapbilityCount();
				printf("%s: Capability Count = %d\n", name, capabilityCount);
				for (int iC=0; iC<capabilityCount; iC++)
				{
					IUdxNodeSchema* temp_node = mHandle->getCapbilityNode(iC);
					printf("name = %s, kernel = %s\n", temp_node->getName(), 
						SchemaNodeType2String(temp_node->getDescription()->getKernelType()));
				}
			}

			void do_dataMapping()
			{
				if (read_write_flag==0 && format_filename!="" && read_node_name!="")
				{
					//printf("executing read udx node from raw file with name\n");
					mHandle->InitHandle(format_filename.c_str(),"");

					IUdxDatasetSchema* dataSchema = mHandle->GetUdxSchema();
					IUdxDataset* udxDataset = createUdxDataset("dataset");
					ConstructDataFrame(dataSchema, udxDataset);

					if (read_node_name == "Header")
					{
						IUdxNodeSchema* headerNode_S = dataSchema->getChildNode(0);
						IUdxNode* headerNode_D = udxDataset->getChildNode(0);
						int count = headerNode_S->getChildNodeCount();
						for (int iNode=0; iNode<count; iNode++)
						{
							mHandle->ReadToNode(headerNode_S->getChildNode(iNode), headerNode_D->getChildNode(iNode));
						}
						std::string xml_str = "";
						udxDataset->removeChildNode(1);
						udxDataset->removeChildNode(1);
						udxDataset->FormatToXmlStream(xml_str);
						printf(xml_str.c_str());
					}
					else if (read_node_name == "UpperLeftX")
					{
						IUdxNodeSchema* Node_S = dataSchema->getChildNode(0)->getChildNode(0);
						IUdxNode* Node_D = udxDataset->getChildNode(0)->getChildNode(0);
						mHandle->ReadToNode(Node_S, Node_D);

						std::string xml_str = "";
						udxDataset->removeChildNode(1);
						udxDataset->removeChildNode(1);
						udxDataset->getChildNode(0)->removeChildNode(1);
						udxDataset->getChildNode(0)->removeChildNode(1);
						udxDataset->getChildNode(0)->removeChildNode(1);
						udxDataset->getChildNode(0)->removeChildNode(1);
						udxDataset->getChildNode(0)->removeChildNode(1);
						udxDataset->getChildNode(0)->removeChildNode(1);
						udxDataset->getChildNode(0)->removeChildNode(1);
						udxDataset->getChildNode(0)->removeChildNode(1);
						udxDataset->FormatToXmlStream(xml_str);
						printf(xml_str.c_str());
					}
					else if (read_node_name == "PixelWidth")
					{
						IUdxNodeSchema* Node_S = dataSchema->getChildNode(0)->getChildNode(1);
						IUdxNode* Node_D = udxDataset->getChildNode(0)->getChildNode(1);
						mHandle->ReadToNode(Node_S, Node_D);

						std::string xml_str = "";
						udxDataset->removeChildNode(1);
						udxDataset->removeChildNode(1);
						udxDataset->getChildNode(0)->removeChildNode(0);
						udxDataset->getChildNode(0)->removeChildNode(1);
						udxDataset->getChildNode(0)->removeChildNode(1);
						udxDataset->getChildNode(0)->removeChildNode(1);
						udxDataset->getChildNode(0)->removeChildNode(1);
						udxDataset->getChildNode(0)->removeChildNode(1);
						udxDataset->getChildNode(0)->removeChildNode(1);
						udxDataset->getChildNode(0)->removeChildNode(1);
						udxDataset->FormatToXmlStream(xml_str);
						printf(xml_str.c_str());
					}
					else if (read_node_name == "TransParam1")
					{
						IUdxNodeSchema* Node_S = dataSchema->getChildNode(0)->getChildNode(2);
						IUdxNode* Node_D = udxDataset->getChildNode(0)->getChildNode(2);
						mHandle->ReadToNode(Node_S, Node_D);

						std::string xml_str = "";
						udxDataset->removeChildNode(1);
						udxDataset->removeChildNode(1);
						udxDataset->getChildNode(0)->removeChildNode(0);
						udxDataset->getChildNode(0)->removeChildNode(0);
						udxDataset->getChildNode(0)->removeChildNode(1);
						udxDataset->getChildNode(0)->removeChildNode(1);
						udxDataset->getChildNode(0)->removeChildNode(1);
						udxDataset->getChildNode(0)->removeChildNode(1);
						udxDataset->getChildNode(0)->removeChildNode(1);
						udxDataset->getChildNode(0)->removeChildNode(1);
						udxDataset->FormatToXmlStream(xml_str);
						printf(xml_str.c_str());
					}
					else if (read_node_name == "UpperLeftY")
					{
						IUdxNodeSchema* Node_S = dataSchema->getChildNode(0)->getChildNode(3);
						IUdxNode* Node_D = udxDataset->getChildNode(0)->getChildNode(3);
						mHandle->ReadToNode(Node_S, Node_D);

						std::string xml_str = "";
						udxDataset->removeChildNode(1);
						udxDataset->removeChildNode(1);
						udxDataset->getChildNode(0)->removeChildNode(0);
						udxDataset->getChildNode(0)->removeChildNode(0);
						udxDataset->getChildNode(0)->removeChildNode(0);
						udxDataset->getChildNode(0)->removeChildNode(1);
						udxDataset->getChildNode(0)->removeChildNode(1);
						udxDataset->getChildNode(0)->removeChildNode(1);
						udxDataset->getChildNode(0)->removeChildNode(1);
						udxDataset->getChildNode(0)->removeChildNode(1);
						udxDataset->FormatToXmlStream(xml_str);
						printf(xml_str.c_str());
					}
					else if (read_node_name == "TransParam2")
					{
						IUdxNodeSchema* Node_S = dataSchema->getChildNode(0)->getChildNode(4);
						IUdxNode* Node_D = udxDataset->getChildNode(0)->getChildNode(4);
						mHandle->ReadToNode(Node_S, Node_D);

						std::string xml_str = "";
						udxDataset->removeChildNode(1);
						udxDataset->removeChildNode(1);
						udxDataset->getChildNode(0)->removeChildNode(0);
						udxDataset->getChildNode(0)->removeChildNode(0);
						udxDataset->getChildNode(0)->removeChildNode(0);
						udxDataset->getChildNode(0)->removeChildNode(0);
						udxDataset->getChildNode(0)->removeChildNode(1);
						udxDataset->getChildNode(0)->removeChildNode(1);
						udxDataset->getChildNode(0)->removeChildNode(1);
						udxDataset->getChildNode(0)->removeChildNode(1);
						udxDataset->FormatToXmlStream(xml_str);
						printf(xml_str.c_str());
					}
					else if (read_node_name == "PixelHeight")
					{
						IUdxNodeSchema* Node_S = dataSchema->getChildNode(0)->getChildNode(5);
						IUdxNode* Node_D = udxDataset->getChildNode(0)->getChildNode(5);
						mHandle->ReadToNode(Node_S, Node_D);

						std::string xml_str = "";
						udxDataset->removeChildNode(1);
						udxDataset->removeChildNode(1);
						udxDataset->getChildNode(0)->removeChildNode(0);
						udxDataset->getChildNode(0)->removeChildNode(0);
						udxDataset->getChildNode(0)->removeChildNode(0);
						udxDataset->getChildNode(0)->removeChildNode(0);
						udxDataset->getChildNode(0)->removeChildNode(0);
						udxDataset->getChildNode(0)->removeChildNode(1);
						udxDataset->getChildNode(0)->removeChildNode(1);
						udxDataset->getChildNode(0)->removeChildNode(1);
						udxDataset->FormatToXmlStream(xml_str);
						printf(xml_str.c_str());
					}
					else if (read_node_name == "Width")
					{
						IUdxNodeSchema* Node_S = dataSchema->getChildNode(0)->getChildNode(6);
						IUdxNode* Node_D = udxDataset->getChildNode(0)->getChildNode(6);
						mHandle->ReadToNode(Node_S, Node_D);

						std::string xml_str = "";
						udxDataset->removeChildNode(1);
						udxDataset->removeChildNode(1);
						udxDataset->getChildNode(0)->removeChildNode(0);
						udxDataset->getChildNode(0)->removeChildNode(0);
						udxDataset->getChildNode(0)->removeChildNode(0);
						udxDataset->getChildNode(0)->removeChildNode(0);
						udxDataset->getChildNode(0)->removeChildNode(0);
						udxDataset->getChildNode(0)->removeChildNode(0);
						udxDataset->getChildNode(0)->removeChildNode(1);
						udxDataset->getChildNode(0)->removeChildNode(1);
						udxDataset->FormatToXmlStream(xml_str);
						printf(xml_str.c_str());
					}
					else if (read_node_name == "Height")
					{
						IUdxNodeSchema* Node_S = dataSchema->getChildNode(0)->getChildNode(7);
						IUdxNode* Node_D = udxDataset->getChildNode(0)->getChildNode(7);
						mHandle->ReadToNode(Node_S, Node_D);

						std::string xml_str = "";
						udxDataset->removeChildNode(1);
						udxDataset->removeChildNode(1);
						udxDataset->getChildNode(0)->removeChildNode(0);
						udxDataset->getChildNode(0)->removeChildNode(0);
						udxDataset->getChildNode(0)->removeChildNode(0);
						udxDataset->getChildNode(0)->removeChildNode(0);
						udxDataset->getChildNode(0)->removeChildNode(0);
						udxDataset->getChildNode(0)->removeChildNode(0);
						udxDataset->getChildNode(0)->removeChildNode(0);
						udxDataset->getChildNode(0)->removeChildNode(1);
						udxDataset->FormatToXmlStream(xml_str);
						printf(xml_str.c_str());
					}
					else if (read_node_name == "BandCount")
					{
						IUdxNodeSchema* Node_S = dataSchema->getChildNode(0)->getChildNode(8);
						IUdxNode* Node_D = udxDataset->getChildNode(0)->getChildNode(8);
						mHandle->ReadToNode(Node_S, Node_D);

						std::string xml_str = "";
						udxDataset->removeChildNode(1);
						udxDataset->removeChildNode(1);
						udxDataset->getChildNode(0)->removeChildNode(0);
						udxDataset->getChildNode(0)->removeChildNode(0);
						udxDataset->getChildNode(0)->removeChildNode(0);
						udxDataset->getChildNode(0)->removeChildNode(0);
						udxDataset->getChildNode(0)->removeChildNode(0);
						udxDataset->getChildNode(0)->removeChildNode(0);
						udxDataset->getChildNode(0)->removeChildNode(0);
						udxDataset->getChildNode(0)->removeChildNode(0);
						udxDataset->FormatToXmlStream(xml_str);
						printf(xml_str.c_str());
					}
					else if (read_node_name == "NoDataValue")
					{
						IUdxNodeSchema* Node_S = dataSchema->getChildNode(1)->getChildNode(0)->getChildNode(0);
						IUdxNode* Node_D = udxDataset->getChildNode(1)->getChildNode(0)->getChildNode(0);
						dataSchema->getChildNode(1)->setExtension(0);
						dataSchema->getChildNode(1)->getChildNode(0)->setExtension(0);
						mHandle->ReadToNode(Node_S, Node_D);

						std::string xml_str = "";
						udxDataset->removeChildNode(0);
						udxDataset->removeChildNode(1);
						udxDataset->getChildNode(0)->getChildNode(0)->removeChildNode(1);
						udxDataset->getChildNode(0)->getChildNode(0)->removeChildNode(1);
						udxDataset->getChildNode(0)->getChildNode(0)->removeChildNode(1);
						udxDataset->getChildNode(0)->getChildNode(0)->removeChildNode(1);
						udxDataset->getChildNode(0)->getChildNode(0)->removeChildNode(1);
						udxDataset->FormatToXmlStream(xml_str);
						printf(xml_str.c_str());
					}
					else if (read_node_name == "Scale")
					{
						IUdxNodeSchema* Node_S = dataSchema->getChildNode(1)->getChildNode(0)->getChildNode(1);
						IUdxNode* Node_D = udxDataset->getChildNode(1)->getChildNode(0)->getChildNode(1);
						dataSchema->getChildNode(1)->setExtension(0);
						dataSchema->getChildNode(1)->getChildNode(0)->setExtension(0);
						mHandle->ReadToNode(Node_S, Node_D);

						std::string xml_str = "";
						udxDataset->removeChildNode(0);
						udxDataset->removeChildNode(1);
						udxDataset->getChildNode(0)->getChildNode(0)->removeChildNode(0);
						udxDataset->getChildNode(0)->getChildNode(0)->removeChildNode(1);
						udxDataset->getChildNode(0)->getChildNode(0)->removeChildNode(1);
						udxDataset->getChildNode(0)->getChildNode(0)->removeChildNode(1);
						udxDataset->getChildNode(0)->getChildNode(0)->removeChildNode(1);
						udxDataset->FormatToXmlStream(xml_str);
						printf(xml_str.c_str());
					}
					else if (read_node_name == "Offset")
					{
						IUdxNodeSchema* Node_S = dataSchema->getChildNode(1)->getChildNode(0)->getChildNode(2);
						IUdxNode* Node_D = udxDataset->getChildNode(1)->getChildNode(0)->getChildNode(2);
						dataSchema->getChildNode(1)->setExtension(0);
						dataSchema->getChildNode(1)->getChildNode(0)->setExtension(0);
						mHandle->ReadToNode(Node_S, Node_D);

						std::string xml_str = "";
						udxDataset->removeChildNode(0);
						udxDataset->removeChildNode(1);
						udxDataset->getChildNode(0)->getChildNode(0)->removeChildNode(0);
						udxDataset->getChildNode(0)->getChildNode(0)->removeChildNode(0);
						udxDataset->getChildNode(0)->getChildNode(0)->removeChildNode(1);
						udxDataset->getChildNode(0)->getChildNode(0)->removeChildNode(1);
						udxDataset->getChildNode(0)->getChildNode(0)->removeChildNode(1);
						udxDataset->FormatToXmlStream(xml_str);
						printf(xml_str.c_str());
					}
					else if (read_node_name == "UnitType")
					{
						IUdxNodeSchema* Node_S = dataSchema->getChildNode(1)->getChildNode(0)->getChildNode(3);
						IUdxNode* Node_D = udxDataset->getChildNode(1)->getChildNode(0)->getChildNode(3);
						dataSchema->getChildNode(1)->setExtension(0);
						dataSchema->getChildNode(1)->getChildNode(0)->setExtension(0);
						mHandle->ReadToNode(Node_S, Node_D);

						std::string xml_str = "";
						udxDataset->removeChildNode(0);
						udxDataset->removeChildNode(1);
						udxDataset->getChildNode(0)->getChildNode(0)->removeChildNode(0);
						udxDataset->getChildNode(0)->getChildNode(0)->removeChildNode(0);
						udxDataset->getChildNode(0)->getChildNode(0)->removeChildNode(0);
						udxDataset->getChildNode(0)->getChildNode(0)->removeChildNode(1);
						udxDataset->getChildNode(0)->getChildNode(0)->removeChildNode(1);
						udxDataset->FormatToXmlStream(xml_str);
						printf(xml_str.c_str());
					}
					else if (read_node_name == "DataType")
					{
						IUdxNodeSchema* Node_S = dataSchema->getChildNode(1)->getChildNode(0)->getChildNode(4);
						IUdxNode* Node_D = udxDataset->getChildNode(1)->getChildNode(0)->getChildNode(4);
						dataSchema->getChildNode(1)->setExtension(0);
						dataSchema->getChildNode(1)->getChildNode(0)->setExtension(0);
						mHandle->ReadToNode(Node_S, Node_D);

						std::string xml_str = "";
						udxDataset->removeChildNode(0);
						udxDataset->removeChildNode(1);
						udxDataset->getChildNode(0)->getChildNode(0)->removeChildNode(0);
						udxDataset->getChildNode(0)->getChildNode(0)->removeChildNode(0);
						udxDataset->getChildNode(0)->getChildNode(0)->removeChildNode(0);
						udxDataset->getChildNode(0)->getChildNode(0)->removeChildNode(0);
						udxDataset->getChildNode(0)->getChildNode(0)->removeChildNode(1);
						udxDataset->FormatToXmlStream(xml_str);
						printf(xml_str.c_str());
					}
					else if (read_node_name == "Row_Item")
					{
						IUdxNodeSchema* Node_S = dataSchema->getChildNode(1)->getChildNode(0)->getChildNode(5)->getChildNode(0);
						IUdxNode* Node_D = udxDataset->getChildNode(1)->getChildNode(0)->getChildNode(5)->getChildNode(0);
						dataSchema->getChildNode(1)->setExtension(0);
						dataSchema->getChildNode(1)->getChildNode(0)->setExtension(0);
						dataSchema->getChildNode(1)->getChildNode(0)->getChildNode(5)->setExtension(0);
						mHandle->ReadToNode(Node_S, Node_D);

						std::string xml_str = "";
						udxDataset->removeChildNode(0);
						udxDataset->removeChildNode(1);
						udxDataset->getChildNode(0)->getChildNode(0)->removeChildNode(0);
						udxDataset->getChildNode(0)->getChildNode(0)->removeChildNode(0);
						udxDataset->getChildNode(0)->getChildNode(0)->removeChildNode(0);
						udxDataset->getChildNode(0)->getChildNode(0)->removeChildNode(0);
						udxDataset->getChildNode(0)->getChildNode(0)->removeChildNode(0);
						udxDataset->FormatToXmlStream(xml_str);
						printf(xml_str.c_str());
					}
					udxDataset->release();
					mHandle->release();
				}
				else if (read_write_flag==0 && format_filename!="" && udx_filename!="")
				{
					printf("executing read udx node from raw file\n");
					mHandle->InitHandle(format_filename.c_str(),"");

					IUdxDatasetSchema* dataSchema = mHandle->GetUdxSchema();
					IUdxDataset* udxDataset = createUdxDataset("dataset");
					ConstructDataFrame(dataSchema, udxDataset);

					IUdxNodeSchema* headerNode_S = dataSchema->getChildNode(0);
					IUdxNode* headerNode_D = udxDataset->getChildNode(0);
					int count = headerNode_S->getChildNodeCount();
					for (int iNode=0; iNode<count; iNode++)
					{
						mHandle->ReadToNode(headerNode_S->getChildNode(iNode), headerNode_D->getChildNode(iNode));
					}

					//////////////////////////////////////////////////////////////////////////
					IUdxNodeSchema* bandItemNode_S = dataSchema->getChildNode(1)->getChildNode(0);
					IUdxNodeSchema* nodataValueNode_S = bandItemNode_S->getChildNode(0);
					IUdxNodeSchema* scaleNode_S = bandItemNode_S->getChildNode(1);
					IUdxNodeSchema* offsetNode_S = bandItemNode_S->getChildNode(2);
					IUdxNodeSchema* unitTypeNode_S = bandItemNode_S->getChildNode(3);
					IUdxNodeSchema* dataTypeNode_S = bandItemNode_S->getChildNode(4);

					IUdxNode* bandItemNode_D = udxDataset->getChildNode(1)->getChildNode(0);
					IUdxNode* nodataValueNode_D = bandItemNode_D->getChildNode(0);
					IUdxNode* scaleNode_D = bandItemNode_D->getChildNode(1);
					IUdxNode* offsetNode_D = bandItemNode_D->getChildNode(2);
					IUdxNode* unitTypeNode_D = bandItemNode_D->getChildNode(3);
					IUdxNode* dataTypeNode_D = bandItemNode_D->getChildNode(4);

					dataSchema->getChildNode(1)->setExtension(0); //第几个波段

					mHandle->ReadToNode(nodataValueNode_S, nodataValueNode_D);
					mHandle->ReadToNode(scaleNode_S, scaleNode_D);
					mHandle->ReadToNode(offsetNode_S, offsetNode_D);
					mHandle->ReadToNode(unitTypeNode_S, unitTypeNode_D);
					mHandle->ReadToNode(dataTypeNode_S, dataTypeNode_D);

					//////////////////////////////////////////////////////////////////////////
					IUdxNode* bandCountNode = udxDataset->getChildNode(0)->getChildNode(8);
					IUdxKernelIntValue* bandCountKernel = (IUdxKernelIntValue*)bandCountNode->getKernel();
					int band_count = bandCountKernel->getTypedValue();
					for (int iBand=0; iBand<band_count; iBand++)
					{
						int countflag = 0;
						IUdxNode* heightNode = udxDataset->getChildNode(0)->getChildNode(7);
						IUdxKernelIntValue* heightKernel = (IUdxKernelIntValue*)heightNode->getKernel();
						int rowCount = heightKernel->getTypedValue();
						while(true)
						{
							IUdxNodeSchema* rowItemNode_S = bandItemNode_S->getChildNode(5)->getChildNode(0);

							bandItemNode_S->getChildNode(5)->setExtension(countflag); //第几行
							dataSchema->getChildNode(1)->setExtension(iBand); //第几个波段

							IUdxNode* rowItemNode_D = bandItemNode_D->getChildNode(5)->getChildNode(countflag);
							if (rowItemNode_D==NULL)
							{
								rowItemNode_D = bandItemNode_D->getChildNode(5)->addChildNode("Row_Item", 
									bandItemNode_D->getChildNode(5)->getChildNode(0)->getKernel()->getType());
							}
							bool ret = mHandle->ReadToNode(rowItemNode_S, rowItemNode_D);
							if (ret==false)
								break;
							countflag++;
							if (countflag>=rowCount)
								break;
						}
					}

					IUdxNodeSchema* projNode_S = dataSchema->getChildNode(2);
					IUdxNode* projNode_D = udxDataset->getChildNode(2);
					mHandle->ReadToNode(projNode_S, projNode_D);

					//////////////////////////////////////////////////////////////////////////
					udxDataset->FormatToXmlFile(udx_filename.c_str());
					udxDataset->release();
				}
				//////////////////////////////////////////////////////////////////////////
				//
				//
				//
				//////////////////////////////////////////////////////////////////////////
				if (read_write_flag==1 && udx_filename!="" && format_filename!="")
				{
					printf("executing write raw file from udx node\n");
					mHandle->InitHandle("",format_filename.c_str());

					IUdxDataset* pExistData = createUdxDataset("dataset");
					bool loadFlag = pExistData->LoadFromXmlFile(udx_filename.c_str());
					if (loadFlag==true)
					{
						int write_flag = -1;
						FILE* fp = fopen("writeflag.txt", "r");
						if (fp!=NULL)
						{
							fscanf(fp, "%d", &write_flag);
							fclose(fp);
						}
						mHandle->BeginWriteToFile();
						mHandle->WriteFromNode(pExistData, write_flag);
						mHandle->EndWriteToFile();
					}
					pExistData->release();
				}

			}
		};
	}
}

void main(int argc, char *argv[])
{
	GDALRasterMapping* dataMappingShell = new GDALRasterMapping(argc, argv);
	dataMappingShell->do_dataMapping();
	
	return;
}
