#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>
#include "IDataMappingHandle.h"
#include "NativeDataMappingAPI.h"

using namespace NGIS::Data;
using namespace NGIS::Data::Schema;
using namespace NGIS::DataMapping;

namespace NGIS
{
	namespace DataMapping
	{
		class OGRVectorMapping
		{
		private:
			IDataMappingHandle* mHandle;

			std::string udx_filename;
			std::string format_filename;
			std::string node_idx_array;
			std::string read_node_name;

			int read_write_flag; //0表示读UDX，1表示写原始数据
		public:
			OGRVectorMapping(int argc, char *argv[])
			{
				mHandle = createGeneral3dModelHandle();
				udx_filename="";
				format_filename="";
				node_idx_array="";
				read_node_name="";

				int read_write_flag = -1; //0表示读UDX，1表示写原始数据

				parse_arguments(argc, argv);
			}

			~OGRVectorMapping()
			{
				mHandle->release();
				mHandle = NULL;
			}

			void parse_arguments(int argc, char *argv[])
			{

				if (argc == 1)
				{
					printf("**************Data Mapping Method**************\n");
					do_getName();
					do_help();
				}
				else
				{
					for (int i=1; i<argc; i++)
					{
						if ((*argv[i])=='-')
						{
							const char *p=argv[i]+1;

							while ((*p)!='\0')
							{
								char c=*(p++);;
								if ((c=='h') || (c=='H'))
								{
									do_help();
								}
								else if ((c=='v') || (c=='V'))
								{
									do_getVersion();
								}
								else if ((c=='s') || (c=='S'))
								{
									do_getSchema();
								}
								else if ((c=='c') || (c=='C'))
								{
									do_getCapablity();
								}
								else if ((c=='w') || (c=='W'))
								{
									read_write_flag = 1;
								}
								else if ((c=='r') || (c=='R'))
								{
									read_write_flag = 0;
								}
								else if ((c=='u') || (c=='U'))
								{
									udx_filename=argv[i+1];
									printf("UDX file name: %s\n", udx_filename.c_str());
								}
								else if ((c=='f') || (c=='F'))
								{
									format_filename=argv[i+1];
									printf("raw file name: %s\n", format_filename.c_str());
								}
								else if ((c=='i') || (c=='I'))
								{
									node_idx_array=argv[i+1];
									std::string node_idx_array_str = node_idx_array;
									node_idx_array_str.find(',');
									printf("node index: %s\n", node_idx_array_str.c_str());
								}
								else if ((c=='n') || (c=='N'))
								{
									read_node_name=argv[i+1];
									printf("read node name: %s\n", read_node_name.c_str());
								}
								else
								{
									do_help();
									return;
								}
							}
						}
						else
						{
						}
					}
				}
			}

			void do_help()
			{
				printf("Usage : DataMappingTemplate.exe [-h] [-v] [-c] [-u] udx.xml [-f] format.* [-i] [1,1,1]\n\n" \
					"  -h\tShow help information\n" \
					"  -v\tShow version\n" \
					"  -c\tList Capabilitier\n" \
					"  -u\tUDX file path\n" \
					"  -r\tRead UDX node from raw data\n" \
					"  -w\tWrite raw data from UDX node\n" \
					"  -f\tOriginal format file path\n" \
					"  -n\tName of try read node\n" \
					"  -i\tSet read index for each related node\n\n");
			}

			void do_getVersion()
			{
				printf("name: %s \n", mHandle->GetHandleVersion());
			}

			void do_getName()
			{
				printf("version: %s \n", mHandle->GetHandleName());
			}

			void do_getSchema()
			{
				IUdxDatasetSchema* dataSchema = mHandle->GetUdxSchema();
				std::string xml_str = "";
				dataSchema->FormatToXmlStream(xml_str);
				printf("Supported Schema: \n%s\n", xml_str.c_str());
			}

			void do_getCapablity()
			{
				const char* name = mHandle->GetHandleName();
				int capabilityCount = mHandle->GetCapbilityCount();
				printf("%s: Capability Count = %d\n", name, capabilityCount);
				for (int iC=0; iC<capabilityCount; iC++)
				{
					IUdxNodeSchema* temp_node = mHandle->getCapbilityNode(iC);
					printf("name = %s, kernel = %s\n", temp_node->getName(), 
						SchemaNodeType2String(temp_node->getDescription()->getKernelType()));
				}
			}

			void do_dataMapping()
			{
				if (read_write_flag==0 && format_filename!="" && udx_filename!="")
				{
					printf("executing read udx node from raw file\n");
					mHandle->InitHandle(format_filename.c_str(),"");

					IUdxDatasetSchema* dataSchema = mHandle->GetUdxSchema();
					IUdxDataset* udxDataset = createUdxDataset("dataset");
					ConstructDataFrame(dataSchema, udxDataset);

					IUdxNodeSchema* layerCount_S = dataSchema->getChildNode(0);
					IUdxNode* layerCount_D = udxDataset->getChildNode(0);
					mHandle->ReadToNode(layerCount_S, layerCount_D);

					IUdxKernelIntValue* layerCountKernel = (IUdxKernelIntValue*)layerCount_D->getKernel();
					int layerCount = layerCountKernel->getTypedValue();

					for (int iLayer=0; iLayer<layerCount; iLayer++)
					{
						IUdxNode* item_node = udxDataset->getChildNode(1)->getChildNode(0);
						if (iLayer > 0)
						{
							item_node = udxDataset->getChildNode(1)->addChildNode("Layer_Item", EKernelType::EKT_NODE);
						}

						dataSchema->getChildNode(1)->setExtension(iLayer);

						IUdxNodeSchema* layerName_S = dataSchema->getChildNode(1)->getChildNode(0)->getChildNode(0);
						IUdxNode* layerName_D = item_node->getChildNode(0);
						mHandle->ReadToNode(layerName_S, layerName_D);

						IUdxNodeSchema* shapeType_S = dataSchema->getChildNode(1)->getChildNode(0)->getChildNode(1);
						IUdxNode* shapeType_D = item_node->getChildNode(1);
						mHandle->ReadToNode(shapeType_S, shapeType_D);

						IUdxNodeSchema* coordinateType_S = dataSchema->getChildNode(1)->getChildNode(0)->getChildNode(2);
						IUdxNode* coordinateType_D = item_node->getChildNode(2);
						mHandle->ReadToNode(coordinateType_S, coordinateType_D);

						IUdxNodeSchema* featureCollection_S = dataSchema->getChildNode(1)->getChildNode(0)->getChildNode(3);
						IUdxNode* featureCollection_D = item_node->getChildNode(3);
						mHandle->ReadToNode(featureCollection_S, featureCollection_D);

						IUdxNodeSchema* attributeTable_S = dataSchema->getChildNode(1)->getChildNode(0)->getChildNode(4);
						IUdxNode* attributeTable_D = item_node->getChildNode(4);
						mHandle->ReadToNode(attributeTable_S, attributeTable_D);

						IUdxNodeSchema* spatialRef_S = dataSchema->getChildNode(1)->getChildNode(0)->getChildNode(5);
						IUdxNode* spatialRef_D = item_node->getChildNode(5);
						mHandle->ReadToNode(spatialRef_S, spatialRef_D);
					}

					//////////////////////////////////////////////////////////////////////////
					udxDataset->FormatToXmlFile(udx_filename.c_str());
					udxDataset->release();
				}
				//////////////////////////////////////////////////////////////////////////
				//
				//
				//
				//////////////////////////////////////////////////////////////////////////
				if (read_write_flag==1 && udx_filename!="" && format_filename!="")
				{
					printf("executing write raw file from udx node\n");
					mHandle->InitHandle("",format_filename.c_str());

					IUdxDataset* pExistData = createUdxDataset("dataset");
					bool loadFlag = pExistData->LoadFromXmlFile(udx_filename.c_str());
					if (loadFlag==true)
					{
						mHandle->BeginWriteToFile();
						mHandle->WriteFromNode(pExistData);
						mHandle->EndWriteToFile();
					}
					pExistData->release();
				}

			}
		};
	}
}

void main(int argc, char *argv[])
{
	OGRVectorMapping* dataMappingShell = new OGRVectorMapping(argc, argv);
	dataMappingShell->do_dataMapping();
	
	return;
}
