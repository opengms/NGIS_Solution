# NGIS_Solution

[![Travis (.org)](doc/OpenGMS.svg)](http://geomodeling.njnu.edu.cn/)

#### Introdution
`NGIS_Solution` is a project for Universal Data eXchange Model (UDX model), Model Encapsualtion, Model Description Language (MDL) in OpenGMS Platform. 

#### Framework

##### Module Introduction

###### Universal Data eXchange Model

###### Model Encapsualtion 

###### Model Description Language Model

##### Language Support
|    |     UDX    |  Model Encapsualtion  |  MDL  |
|:----------:|:-------------:|:------:|:------:|
| C++ | `Done √` | `Done √` | `Done √` |
| C#  | `Done √` |  `Done √`  | `Done √` |
| Java | `Done √` | `Done √` | `Done √` |
| Javescript | `Done √` | `No ×` | `No ×` |
| Python | `No ×` | `Done √` | `Done √` |

#### Install



#### Usage



#### Contributors
##### Founder
Min Chen (<chenmin0902@163.com>) 

Yongning Wen (<wenyn@msn.com>)

Songshan Yue (<yss123yss@126.com>)

Fengyuan Zhang (<franklinzhang@foxmail.com>) 
##### Coder
Yongning Wen (<wenyn@msn.com>)

Fengyuan Zhang (<franklinzhang@foxmail.com>) 

All codes are used in [OpenGMS](http://geomodeling.njnu.edu.cn/) platform