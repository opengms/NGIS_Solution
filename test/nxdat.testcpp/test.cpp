#include <stdio.h>
#include <conio.h>
#include <string>
#include "UdxDataApi.h"
#include "UdxDataSchemaApi.h"


void IterateNode(NGIS::Data::IUdxNode* pNode)
{
	const char* name =  pNode->getName();
	NGIS::Data::EKernelType kernelType = pNode->getKernel()->getType();
	NGIS::Data::IUdxKernel* kernel = pNode->getKernel();
	printf("\n");
	printf("name=%s\n", name);
	printf("kernelType=%s\n", NGIS::Data::KernelType2String(kernelType).c_str());
	if (kernelType == NGIS::Data::EKernelType::EKT_INT)
	{
		NGIS::Data::IUdxKernelIntValue* realKernel = (NGIS::Data::IUdxKernelIntValue*)kernel;
		int val = realKernel->getTypedValue();
		printf("value = %d \n", val);
	}
	else if (kernelType == NGIS::Data::EKernelType::EKT_REAL)
	{
		NGIS::Data::IUdxKernelRealValue* realKernel = (NGIS::Data::IUdxKernelRealValue*)kernel;
		double val = realKernel->getTypedValue();
		printf("value = %lf \n", val);
	}
	else if (kernelType == NGIS::Data::EKernelType::EKT_STRING)
	{
		NGIS::Data::IUdxKernelStringValue* realKernel = (NGIS::Data::IUdxKernelStringValue*)kernel;
		std::string val = realKernel->getTypedValue();
		printf("value = %s \n", val.c_str());
	}
	else if (kernelType == NGIS::Data::EKernelType::EKT_VECTOR2)
	{
		NGIS::Data::IUdxKernelVector2dValue* realKernel = (NGIS::Data::IUdxKernelVector2dValue*)kernel;
		NGIS::Data::Vector2d val;
		val = realKernel->getTypedValue();
		printf("value = %lf, %lf \n", val.x, val.y);
	}
	else if (kernelType == NGIS::Data::EKernelType::EKT_VECTOR3)
	{
		NGIS::Data::IUdxKernelVector3dValue* realKernel = (NGIS::Data::IUdxKernelVector3dValue*)kernel;
		double x, y, z;
		realKernel->getTypedValue(x, y, z);
		printf("value = %lf, %lf, %lf \n", x, y, z);
	}
	else if (kernelType == NGIS::Data::EKernelType::EKT_VECTOR4)
	{
		NGIS::Data::IUdxKernelVector4dValue* realKernel = (NGIS::Data::IUdxKernelVector4dValue*)kernel;
		double x, y, z, m;
		realKernel->getTypedValue(x, y, z, m);
		printf("value = %lf, %lf, %lf, %lf \n", x, y, z, m);
	}
	else if (kernelType == (NGIS::Data::EKernelType)(NGIS::Data::EKernelType::EKT_INT|NGIS::Data::EKernelType::EKT_LIST))
	{
		NGIS::Data::IUdxKernelIntArray* realKernel = (NGIS::Data::IUdxKernelIntArray*)kernel;
		int valueLen = realKernel->getCount();
		for (int iVal=0; iVal<valueLen; iVal++)
		{
			int val = 0;
			if (realKernel->getTypedValueByIndex(iVal, val))
				printf("value = %d \n", val);
		}
		printf("\n");
	}
	else if (kernelType == (NGIS::Data::EKernelType)(NGIS::Data::EKernelType::EKT_REAL|NGIS::Data::EKernelType::EKT_LIST))
	{
		NGIS::Data::IUdxKernelRealArray* realKernel = (NGIS::Data::IUdxKernelRealArray*)kernel;
		int valueLen = realKernel->getCount();
		for (int iVal=0; iVal<valueLen; iVal++)
		{
			double val = 0.0;
			if (realKernel->getTypedValueByIndex(iVal, val))
				printf("value = %lf \n", val);
		}
		printf("\n");
	}
	else if (kernelType == (NGIS::Data::EKernelType)(NGIS::Data::EKernelType::EKT_STRING|NGIS::Data::EKernelType::EKT_LIST))
	{
		NGIS::Data::IUdxKernelStringArray* realKernel = (NGIS::Data::IUdxKernelStringArray*)kernel;
		int valueLen = realKernel->getCount();
		for (int iVal=0; iVal<valueLen; iVal++)
		{
			std::string val = "";
			if (realKernel->getTypedValueByIndex(iVal, val))
				printf("value = %s \n", val);
		}
		printf("\n");
	}
	else if (kernelType == (NGIS::Data::EKernelType)(NGIS::Data::EKernelType::EKT_VECTOR2|NGIS::Data::EKernelType::EKT_LIST))
	{
		NGIS::Data::IUdxKernelVector2dArray* realKernel = (NGIS::Data::IUdxKernelVector2dArray*)kernel;
		int valueLen = realKernel->getCount();
		for (int iVal=0; iVal<valueLen; iVal++)
		{
			NGIS::Data::Vector2d val;
			if (realKernel->getTypedValueByIndex(iVal, val))
				printf("value = %lf, %lf; \n", val.x, val.y);
		}
		printf("\n");
	}
	else if (kernelType == (NGIS::Data::EKernelType)(NGIS::Data::EKernelType::EKT_VECTOR3|NGIS::Data::EKernelType::EKT_LIST))
	{
		NGIS::Data::IUdxKernelVector3dArray* realKernel = (NGIS::Data::IUdxKernelVector3dArray*)kernel;
		int valueLen = realKernel->getCount();
		for (int iVal=0; iVal<valueLen; iVal++)
		{
			NGIS::Data::Vector3d val;
			if (realKernel->getTypedValueByIndex(iVal, val))
				printf("value = %lf, %lf, %lf; \n", val.x, val.y, val.z);
		}
		printf("\n");
	}
	else if (kernelType == (NGIS::Data::EKernelType)(NGIS::Data::EKernelType::EKT_VECTOR4|NGIS::Data::EKernelType::EKT_LIST))
	{
		NGIS::Data::IUdxKernelVector4dArray* realKernel = (NGIS::Data::IUdxKernelVector4dArray*)kernel;
		int valueLen = realKernel->getCount();
		for (int iVal=0; iVal<valueLen; iVal++)
		{
			NGIS::Data::Vector4d val;
			if (realKernel->getTypedValueByIndex(iVal, val))
				printf("value = %lf, %lf, %lf, %lf; \n", val.x, val.y, val.z, val.m);
		}
		printf("\n");
	}
	else if (kernelType == NGIS::Data::EKernelType::EKT_NODE || 
		kernelType == NGIS::Data::EKernelType::EKT_LIST ||
		kernelType == NGIS::Data::EKernelType::EKT_MAP ||
		kernelType == NGIS::Data::EKernelType::EKT_TABLE)
	{
		int nodeCount = pNode->getChildNodeCount();
		for (int iNode=0; iNode<nodeCount; iNode++)
		{
			IterateNode(pNode->getChildNode(iNode));
		}
	}
}

void testLoad()
{
	NGIS::Data::IUdxDataset* udxDataset1 = NGIS::Data::createUdxDataset("dataset");
	//udxDataset1->LoadFromXmlFile("D:\\testUdx.xml");
	udxDataset1->LoadFromXmlFile("E:\\test_udx\\demo_data_nj\\aspect_result.xml");

	for(int iNode=0; iNode<udxDataset1->getChildNodeCount(); iNode++)
	{
		IterateNode(udxDataset1->getChildNode(iNode));
	}

}

void test_loadFromString()
{
	NGIS::Data::IUdxDataset* udxDataset1 = NGIS::Data::createUdxDataset("dataset");
	std::string xml_str = "<dataset><XDO name=\"node0\" kernelType=\"any\"><XDO name=\"node0_node0\" kernelType=\"int\" value=\"12\"/><XDO name=\"node0_node1\" kernelType=\"real\" value=\"12.23\"/><XDO name=\"node0_node2\" kernelType=\"string\" value=\"woshiwho\"/><XDO name=\"node0_node3\" kernelType=\"vector2d\" value=\"89.000000,98.000000\"/><XDO name=\"node0_node4\" kernelType=\"vector3d\" value=\"89.000000,98.000000,78.000000\"/><XDO name=\"node0_node5\" kernelType=\"vector4d\" value=\"89.000000,98.000000,78.000000,67.000000\"/></XDO><XDO name=\"node1\" kernelType=\"int\" value=\"12\"/><XDO name=\"node2\" kernelType=\"real\" value=\"0\"/></dataset>";

	udxDataset1->LoadFromXmlStream(xml_str.c_str());

	for(int iNode=0; iNode<udxDataset1->getChildNodeCount(); iNode++)
	{
		IterateNode(udxDataset1->getChildNode(iNode));
	}
}

void createTestUdxData()
{
	NGIS::Data::IUdxDataset* udxDataset = NGIS::Data::createUdxDataset("dataset");
	udxDataset->addChildNode("Node1", NGIS::Data::EKT_INT);
	udxDataset->addChildNode("Node2", NGIS::Data::EKT_NODE);

	NGIS::Data::IUdxNode* firstNode = udxDataset->getChildNode(0);
	{
		((NGIS::Data::IUdxKernelIntValue*)firstNode->getKernel())->setTypedValue(1234);
	}

	NGIS::Data::IUdxNode* secondNode = udxDataset->getChildNode(1);
	{
		NGIS::Data::IUdxNode* realValueNode = secondNode->addChildNode("Node2_node1", NGIS::Data::EKT_REAL);
		((NGIS::Data::IUdxKernelRealValue*)realValueNode->getKernel())->setTypedValue(10.234);

		//////////////////////////////////////////////////////////////////////////
		NGIS::Data::IUdxNode* stringValueNode = secondNode->addChildNode("Node2_node2", NGIS::Data::EKT_STRING);
		((NGIS::Data::IUdxKernelStringValue*)stringValueNode->getKernel())->setTypedValue("woshishui我是谁");

		//////////////////////////////////////////////////////////////////////////
		NGIS::Data::IUdxNode* vector4dArrayNode = secondNode->addChildNode("Node2_node3", NGIS::Data::EKT_VECTOR4_LIST);
		NGIS::Data::IUdxKernelVector4dArray* realKernel = (NGIS::Data::IUdxKernelVector4dArray*)vector4dArrayNode->getKernel();
		realKernel->addTypedValue(1.0, 2.0, 3.0, 4.0);
		realKernel->addTypedValue(5.0, 6.0, 7.0, 8.0);
		realKernel->addTypedValue(9.0, 10.0, 11.0, 12.0);
		realKernel->addTypedValue(13.0, 14.0, 15.0, 16.0);

		realKernel->clearValue();
		realKernel->addTypedValue(9.8, 8.7, 7.6, 6.5);
		realKernel->addTypedValue(4.3, 3.2, 2.1, 1.0);
		realKernel->addTypedValue(0.1, 1.2, 2.3, 3.4);
		realKernel->addTypedValue(4.5, 5.6, 6.7, 7.8);

		realKernel->removeValueByIndex(1);
	}

	//////////////////////////////////////////////////////////////////////////
	//
	//
	//
	//////////////////////////////////////////////////////////////////////////

	for(int iNode=0; iNode<udxDataset->getChildNodeCount(); iNode++)
	{
		IterateNode(udxDataset->getChildNode(iNode));
	}

	udxDataset->FormatToXmlFile("D:\\testUdxInCPP.xml");

	std::string xmlStr = "";
	udxDataset->FormatToXmlStream(xmlStr);
	printf("%s", xmlStr.c_str());

	printf("\n******************\n");

	NGIS::Data::IUdxNode* temp_node = udxDataset->getChildNode(1);
	temp_node->removeChildNode(1);
	udxDataset->FormatToXmlStream(xmlStr);
	printf("%s", xmlStr.c_str());

	udxDataset->release();
};

void createTestUdxSchema()
{

	NGIS::Data::Schema::IUdxDatasetSchema* pDatasetSchema = NGIS::Data::Schema::createUdxDatasetSchema("UdxDescription");
	NGIS::Data::Schema::INodeDescription* pNodeDescription1 = NGIS::Data::Schema::createUdxNodeDescription(NGIS::Data::Schema::ESchemaNodeType::EDTKT_INT);
	NGIS::Data::Schema::IUdxNodeSchema* pNodeSchema1 = pDatasetSchema->addChildNode("node1", pNodeDescription1);
	pNodeDescription1->release();

	NGIS::Data::Schema::INodeDescription* pNodeDescription2 = NGIS::Data::Schema::createUdxNodeDescription(NGIS::Data::Schema::ESchemaNodeType::EDTKT_REAL);
	NGIS::Data::Schema::IUdxNodeSchema* pNodeSchema2 = pDatasetSchema->addChildNode("node2", pNodeDescription2);
	pNodeDescription2->release();

	NGIS::Data::Schema::IUdxNodeSchema* pNode3 = pDatasetSchema->addChildNode("node3", NGIS::Data::Schema::ESchemaNodeType::EDTKT_LIST);
	NGIS::Data::Schema::IUdxNodeSchema* pNode4 = pDatasetSchema->addChildNode("node4", NGIS::Data::Schema::ESchemaNodeType::EDTKT_NODE);

	NGIS::Data::Schema::IUdxNodeSchema* pNode5 = pNode4->addChildNode("node5", NGIS::Data::Schema::ESchemaNodeType::EDTKT_TABLE, "我也不知道");
	NGIS::Data::Schema::IUdxNodeSchema* pNode5_1 = pNode5->addChildNode("node5_1", NGIS::Data::Schema::ESchemaNodeType::EDTKT_INT|NGIS::Data::Schema::ESchemaNodeType::EDTKT_LIST);
	NGIS::Data::Schema::IUdxNodeSchema* pNode5_2 = pNode5->addChildNode("node5_2", NGIS::Data::Schema::ESchemaNodeType::EDTKT_REAL|NGIS::Data::Schema::ESchemaNodeType::EDTKT_LIST);
	NGIS::Data::Schema::IUdxNodeSchema* pNode5_3 = pNode5->addChildNode("node5_3", NGIS::Data::Schema::ESchemaNodeType::EDTKT_VECTOR2|NGIS::Data::Schema::ESchemaNodeType::EDTKT_LIST);

	pNode5_1->getDescription()->modifyConceptTag("{941671BD-9ED8-4146-979D-10F7C856DF2A}");
	pNode5_1->getDescription()->modifySpatialReferenceTag("{941671BD-9ED8-4146-979D-10F7C856DF2A}");
	pNode5_1->getDescription()->modifyUnitTag("{941671BD-9ED8-4146-979D-10F7C856DF2A}");
	pNode5_1->getDescription()->modifyDataTemplateTag("{941671BD-9ED8-4146-979D-10F7C856DF2A}");

	std::string xmlStr = "";
	pDatasetSchema->FormatToXmlStream(xmlStr);
	pDatasetSchema->release();
	printf(xmlStr.c_str());
	printf("\n***********************************************\n");

	//////////////////////////////////////////////////////////////////////////
	NGIS::Data::Schema::IUdxDatasetSchema* pDatasetSchema1 = NGIS::Data::Schema::createUdxDatasetSchema("UdxDescription");
	pDatasetSchema1->LoadFromXmlStream(xmlStr.c_str());

	std::string xmlStr1="";
	pDatasetSchema1->FormatToXmlStream(xmlStr1);
	printf(xmlStr1.c_str());
	printf("\n");

	pDatasetSchema1->FormatToXmlFile("D:\\testUdxSchemaInCPP.xml");
	pDatasetSchema1->release();
}

void main()
{
	testLoad();
	return;


	createTestUdxData();

	createTestUdxSchema();

	//test_loadFromString();
	//getch();
	//return;

	//testLoad();
	getch();
	return;


	getch();
}