#include <stdio.h>
#include <conio.h>
#include "UdxDataApi.h"
#include "UdxDataSchemaApi.h"
#include "XgeModelClassApi.h"
#include "XgeModelServiceApi.h"

using namespace NGIS;
using namespace NGIS::Data;
using namespace NGIS::Data::Schema;
using namespace NGIS::Model;

void main(int argc, char *argv[])
{
	printf("Hello World\n");

	IModelServiceContext* pModelServiceContext = createModelServiceContext();
	IModelDataHandler* nativeDataHandler = createNativeRequestDataHandler(pModelServiceContext);

	if (argc<4) return;

	nxUID pInstanceID = pModelServiceContext->onInitialize(argv[1], argv[2], argv[3]);
	if (pInstanceID < 0)
		printf("Init Failed");

	std::string pStateId1 = "851a1985-a3f6-4c13-be53-f1c765022929";
	pModelServiceContext->onEnterState(pInstanceID, pStateId1.c_str());

	std::string  pEventId1 = "event1";
	pModelServiceContext->onFireEvent(pInstanceID, pStateId1.c_str(), pEventId1.c_str());

	pModelServiceContext->onRequestData(pInstanceID, pStateId1.c_str(), pEventId1.c_str());
	
	if (pModelServiceContext->getRequestDataFlag()==ERequestResponseDataFlag::ERDF_OK)
	{
		//! 两种用法，一种是文件的，一种是直接加载UDX XML的
		if (pModelServiceContext->getRequestDataMIME() == ERDM_ZIP_FILE)
		{
			nativeDataHandler->connectDataMappingMethod("DemoMappingMethod.exe");
			nativeDataHandler->conductUDXMapping("test.tif");
			std::string name = nativeDataHandler->getRealResultSaveName();
		}
		else if (pModelServiceContext->getRequestDataMIME() == ERDM_XML_STREAM)
		{
			std::string xml_str = pModelServiceContext->getRequestDataBody();

			IUdxDataset* pDataset = createUdxDataset("dataset");
			pDataset->LoadFromXmlStream(xml_str.c_str());
			IUdxKernel* pKernel1 = pDataset->getChildNode(0)->getKernel();
			IUdxKernel* pKernel2 = pDataset->getChildNode(1)->getKernel();
			int k1 = ((IUdxKernelIntValue*)pKernel1)->getTypedValue();
			int k2 = ((IUdxKernelIntValue*)pKernel2)->getTypedValue();
		}
	}

	//////////////////////////////////////////////////////////////////////////
	std::string file_save_path = pModelServiceContext->getCurrentDataDirectory();
	int a = 12;
	int b = 13;
	int c = a * b;
	FILE* fp = fopen("E:\\test.txt", "w");
	fprintf(fp, "***********For TEST**********\n");
	fprintf(fp, "C = %d", c);
	fclose(fp);
	//////////////////////////////////////////////////////////////////////////

	std::string  pEventId2 = "event2";
	pModelServiceContext->onFireEvent(pInstanceID, pStateId1.c_str(), pEventId2.c_str());

	pModelServiceContext->setResponseDataFlag(ERDF_OK);
	pModelServiceContext->setResponseDataMIME(ERDM_ZIP_FILE);

	std::vector<std::string> rawFiles;
	rawFiles.push_back("E:\\test.txt");
	nativeDataHandler->connectDataMappingMethod("DemoMappingMethod.exe");
	nativeDataHandler->conductFileMapping("test.zip", rawFiles.data(), 1);
	std::string name = nativeDataHandler->getRealResultSaveName();

	pModelServiceContext->setResponseDataBody(name.c_str());

	pModelServiceContext->onResponseData(pInstanceID, pStateId1.c_str(), pEventId1.c_str());

	pModelServiceContext->onLeaveState(pInstanceID, pStateId1.c_str());

	std::string pStateId2 = "B62ACC53-0DDF-4553-B71E-132753AD160F";
	pModelServiceContext->onEnterState(pInstanceID, pStateId2.c_str());

	std::string  pEventId3 = "event1";
	pModelServiceContext->onFireEvent(pInstanceID, pStateId2.c_str(), pEventId3.c_str());

	pModelServiceContext->onLeaveState(pInstanceID, pStateId2.c_str());

	pModelServiceContext->onFinalize(pInstanceID);

	//getch();
}