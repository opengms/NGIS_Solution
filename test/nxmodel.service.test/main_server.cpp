#include <stdio.h>
#include <conio.h>
#include <string>
#include <io.h>
#include <vector>
#include <winsock2.h>
#include <Windows.h>

#include "tinyxml2.h"
#include <fstream>
#include <iostream>

#pragma comment(lib,"ws2_32.lib")
#define MAX_LENGTH 2048

struct EventData
{
	std::string stateName;
	std::string eventName;
	std::string dataMIME;
	std::string dataFile;
};
std::vector<EventData> Debug_Data;

bool getDebugData(std::string stateId, std::string envetId, EventData& pEventData)
{
	for (int i=0; i<Debug_Data.size(); i++)
	{
		if (stateId	== Debug_Data[i].stateName && envetId == Debug_Data[i].eventName)
		{
			pEventData = Debug_Data[i];
			return true;
		}
	}
	return false;
}

void main()
{
	printf("Model Service Container Starting\n");

	WSADATA wsaData;
	SOCKET sockServer;
	SOCKADDR_IN addrServer;
	SOCKET sockClient;
	SOCKADDR_IN addrClient;
	WSAStartup(MAKEWORD(2,2),&wsaData);
	sockServer=socket(AF_INET,SOCK_STREAM,0);
	addrServer.sin_addr.S_un.S_addr=htonl(INADDR_ANY);//INADDR_ANY表示任何IP
	addrServer.sin_family=AF_INET;
	addrServer.sin_port=htons(6000);//绑定端口6000
	bind(sockServer,(SOCKADDR*)&addrServer,sizeof(SOCKADDR));

	//Listen监听端
	listen(sockServer,5);//5为等待连接数目
	printf("Listening: 127.0.0.1:6000 ...\n");
	int len=sizeof(SOCKADDR);

	//会阻塞进程，直到有客户端连接上来为止
	sockClient=accept(sockServer,(SOCKADDR*)&addrClient,&len);

	char recvBuf[MAX_LENGTH];//接受客户端返回的字符串
	char sendBuf[MAX_LENGTH];//发送至客户端的字符串

	strcpy(sendBuf, "{Connected}");
	//send(sockClient, sendBuf, strlen(sendBuf), 0);
	std::string file_save_dir = "";
	while(true)
	{
		//接收并打印客户端数据
		int retVal = recv(sockClient,recvBuf,MAX_LENGTH,0);
		if (SOCKET_ERROR == retVal)  
		{  
			printf("Model Instance Disconnected!\n");
			break;
		}

		std::string receiveStr(recvBuf);
		int idx1 = receiveStr.find_first_of('{');
		int idx2 = receiveStr.find_first_of('}');
		std::string op = receiveStr.substr(idx1+1, idx2-idx1-1);
		std::string cmd = receiveStr.substr(idx2+1, receiveStr.size()-idx2);

		//printf("**********\n");
		//printf(receiveStr.c_str()); printf("\n");
		//printf("**********\n");

		if (op == "init")
		{
			idx1 = cmd.find_first_of('&');
			std::string instanceId = cmd.substr(0, idx1);
			std::string debugFile = cmd.substr(idx1+1);
			printf("\n******Load Debug File*******\n");
			printf(debugFile.c_str());
			printf("\n*************\n\n");
			FILE* fp = fopen(debugFile.c_str(), "r");
			if (fp != NULL)
			{
				Debug_Data.clear();
				fclose(fp);

				tinyxml2::XMLDocument doc;
				doc.LoadFile(debugFile.c_str());
				std::string debugFile_Dir = debugFile.substr(0, debugFile.find_last_of('\\'));
				tinyxml2::XMLElement* rootEle = doc.RootElement();
				for (tinyxml2::XMLElement* ele=rootEle->FirstChildElement(); ele; ele=ele->NextSiblingElement())
				{
					std::string stateName = ele->Attribute("State");
					std::string eventName = ele->Attribute("Event");
					std::string dataMIME = ele->Attribute("Type");
					std::string path = ele->Attribute("File");
					EventData temp_data;
					temp_data.stateName = stateName;
					temp_data.eventName = eventName;
					temp_data.dataMIME = dataMIME;

					if (path.find_first_of(':')<0)
					{
						path = debugFile_Dir + path;
					}
					temp_data.dataFile = path;

					Debug_Data.push_back(temp_data);
					if (file_save_dir=="")
					{
						int temp_idx = path.find_last_of('\\');
						file_save_dir = path.substr(0, temp_idx+1);
					}
				}
			}

			std::string sendStr = "{Initialized}";
			sendStr += cmd;
			sendStr += "[F:\\XgeDataMapping\\CommonShell\\x64]";
			sendStr += "[F:\\XgeModelService\\ModelName1\\instance\\B5E63F73-9A72-479D-A1FC-60E04304B75B]";
			strcpy(sendBuf, sendStr.c_str());
			send(sockClient, sendBuf, strlen(sendBuf), 0);

			printf("Initialize\n");
		}
		else if (op == "onEnterState")
		{
			idx1 = cmd.find_first_of('&');
			std::string instanceID = cmd.substr(0, idx1);
			std::string sub_str = cmd.substr(idx1+1, cmd.size()-1);

			idx1 = sub_str.find_first_of('&');
			std::string stateID = sub_str.substr(0, idx1);
			std::string eventID = sub_str.substr(idx1+1);

			strcpy(sendBuf, "{Enter State Notified}");
			send(sockClient, sendBuf, strlen(sendBuf), 0);

			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),FOREGROUND_GREEN); 
			printf("Enter State: "); printf(stateID.c_str()); printf(":\t"); printf(eventID.c_str()); printf("\n");
			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),FOREGROUND_INTENSITY); 
		}
		else if(op == "onFireEvent")
		{
			idx1 = cmd.find_first_of('&');
			std::string instanceID = cmd.substr(0, idx1);
			std::string sub_str = cmd.substr(idx1+1, cmd.size()-1);

			idx1 = sub_str.find_first_of('&');
			std::string stateID = sub_str.substr(0, idx1);
			std::string eventID = sub_str.substr(idx1+1);

			strcpy(sendBuf, "{Fire Event Notified}");
			send(sockClient, sendBuf, strlen(sendBuf), 0);

			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),FOREGROUND_GREEN); 
			printf("Fire Event: "); printf(stateID.c_str()); printf(":\t"); printf(eventID.c_str()); printf("\n");
			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),FOREGROUND_INTENSITY); 
		}
		else if(op == "onRequestData")
		{
			idx1 = cmd.find_first_of('&');
			std::string instanceID = cmd.substr(0, idx1);
			std::string sub_str = cmd.substr(idx1+1, cmd.size()-1);

			idx1 = sub_str.find_first_of('&');
			std::string stateID = sub_str.substr(0, idx1);
			std::string eventID = sub_str.substr(idx1+1);

			std::string header_ret = "{Request Data Notified}";

			std::string data_ret_flag = "[OK]";
			std::string data_ret_mime = "[ZIP|FILE]";
			std::string data_path = "";
			EventData temp_data;
			if (getDebugData(stateID, eventID, temp_data))
			{
				if (temp_data.dataMIME=="Zip" || temp_data.dataMIME=="zip" || 
					temp_data.dataMIME=="Zip|File" || temp_data.dataMIME=="ZIP")
					data_ret_mime = "[ZIP|FILE]";
				else if (temp_data.dataMIME=="Xml" || temp_data.dataMIME=="xml" || 
					temp_data.dataMIME=="Xml|File" || temp_data.dataMIME=="XML")
					data_ret_mime = "[XML|FILE]";
				else if (temp_data.dataMIME=="Raw" || temp_data.dataMIME=="raw" || 
					temp_data.dataMIME=="Raw|File" || temp_data.dataMIME=="RAW")
					data_ret_mime = "[RAW|FILE]";

				data_path = temp_data.dataFile;
			}
			else
			{
				data_ret_flag = "[ERROR]";
			}
			std::string ret_str = header_ret + data_ret_flag + data_ret_mime + data_path;
			strcpy(sendBuf, ret_str.c_str());
			send(sockClient, sendBuf, strlen(sendBuf), 0);

			if (data_ret_flag == "[OK]")
			{
				SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),FOREGROUND_GREEN); 
				printf("Request Data: "); printf(stateID.c_str()); printf(":\t"); printf(eventID.c_str()); printf("\n");
				SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),FOREGROUND_INTENSITY); 
			}
			else
			{
				SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),FOREGROUND_RED); 
				printf("Request Data ERROR: "); printf(stateID.c_str()); printf(":\t"); printf(eventID.c_str()); printf("\n");
				SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),FOREGROUND_INTENSITY); 
			}
		}
		else if(op == "onResponseData")
		{
			idx1 = cmd.find_first_of('[');
			idx2 = cmd.find_first_of(']');
			std::string flag = cmd.substr(idx1+1, idx2-idx1-1);
			idx1 = cmd.find_last_of('[');
			idx2 = cmd.find_last_of(']');
			std::string mime = cmd.substr(idx1+1, idx2-idx1-1);
			idx1 = 0;
			idx2 = cmd.find_first_of('[');
			std::string queryStr = cmd.substr(idx1, idx2);
			
			idx1 = queryStr.find_last_of('&');
			std::string sizeStr = queryStr.substr(idx1+1, queryStr.size()-1);
			int dataSize = atoi(sizeStr.c_str());

			strcpy(sendBuf, "{Response Data Notified}");
			send(sockClient, sendBuf, strlen(sendBuf), 0);

			printf("Response Data Content: %s\n", cmd.c_str());
			if (flag == "OK")
			{
				idx2 = cmd.find_last_of(']');
				std::string file_name = cmd.substr(idx2+1);
				std::fstream in(file_name.c_str(), std::ios::in|std::ios::binary);
				std::string file_name_without_path = file_name.substr(file_name.find_last_of('\\')+1);
				std::string copy_file_name = file_save_dir + file_name_without_path;
				std::fstream out(copy_file_name.c_str(),std::ios::out|std::ios::binary);
				char temp;
				while( in.get(temp) )
				{
					out<<temp;
				}
				out<<std::endl;
				in.close();
				out.close();

				SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),FOREGROUND_GREEN); 
				printf("Response Data OK\n");
				printf("Copy Data To: %s\n", file_name_without_path.c_str());
				printf("Copy Data To: %s\n", copy_file_name.c_str());
				SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),FOREGROUND_INTENSITY); 
			}
			else
			{
				SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),FOREGROUND_RED); 
				printf("Response Data Error\n");
				SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),FOREGROUND_INTENSITY); 
			}
		}
		else if(op == "onPostErrorInfo")
		{
			idx1 = cmd.find_first_of('&');
			std::string instanceID = cmd.substr(0, idx1);
			std::string sub_str = cmd.substr(idx1+1, cmd.size()-1);

			idx1 = sub_str.find_first_of('&');
			std::string stateID = sub_str.substr(0, idx1);
			std::string eventID = sub_str.substr(idx1+1);

			strcpy(sendBuf, "{Post Error Info Notified}");
			send(sockClient, sendBuf, strlen(sendBuf), 0);

			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),FOREGROUND_RED); 
			printf("Post Error Info: "); printf(stateID.c_str()); printf(":\t"); printf(eventID.c_str()); printf("\n");
			printf("Error Info Content: %s\n", cmd.c_str());
			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),FOREGROUND_INTENSITY); 
		}
		else if(op == "onPostWarningInfo")
		{
			idx1 = cmd.find_first_of('&');
			std::string instanceID = cmd.substr(0, idx1);
			std::string sub_str = cmd.substr(idx1+1, cmd.size()-1);

			idx1 = sub_str.find_first_of('&');
			std::string stateID = sub_str.substr(0, idx1);
			std::string eventID = sub_str.substr(idx1+1);

			strcpy(sendBuf, "{Post Warning Info Notified}");
			send(sockClient, sendBuf, strlen(sendBuf), 0);

			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),FOREGROUND_BLUE); 
			printf("Post Warning Info: "); printf(stateID.c_str()); printf(":\t"); printf(eventID.c_str()); printf("\n");
			printf("Warning Info Content: %s\n", cmd.c_str());
			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),FOREGROUND_INTENSITY); 
		}
		else if(op == "onPostMessageInfo")
		{
			idx1 = cmd.find_first_of('&');
			std::string instanceID = cmd.substr(0, idx1);
			std::string sub_str = cmd.substr(idx1+1, cmd.size()-1);

			idx1 = sub_str.find_first_of('&');
			std::string stateID = sub_str.substr(0, idx1);
			std::string eventID = sub_str.substr(idx1+1);

			strcpy(sendBuf, "{Post Message Info Notified}");
			send(sockClient, sendBuf, strlen(sendBuf), 0);

			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),FOREGROUND_BLUE); 
			printf("Post Message Info: "); printf(stateID.c_str()); printf(":\t"); printf(eventID.c_str()); printf("\n");
			printf("Message Info Content: %s\n", cmd.c_str());
			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),FOREGROUND_INTENSITY); 
		}
		else if(op == "onLeaveState")
		{
			idx1 = cmd.find_first_of('&');
			std::string instanceID = cmd.substr(0, idx1);
			std::string sub_str = cmd.substr(idx1+1, cmd.size()-1);

			idx1 = sub_str.find_first_of('&');
			std::string stateID = sub_str.substr(0, idx1);
			std::string eventID = sub_str.substr(idx1+1);

			strcpy(sendBuf, "{Leave State Notified}");
			send(sockClient, sendBuf, strlen(sendBuf), 0);

			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),FOREGROUND_GREEN); 
			printf("Leave State: "); printf(stateID.c_str()); printf(":\t"); printf(eventID.c_str()); printf("\n");
			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),FOREGROUND_INTENSITY); 
		}
		else if(op == "onFinalize")
		{
			strcpy(sendBuf, "{Finalize Notified}");
			send(sockClient, sendBuf, strlen(sendBuf), 0);

			printf("Finalize\n");
		}
		else if(op == "onGetModelAssembly")
		{
			//TODO 从MDL里面读取Assembly中相应条目的路径
			idx1 = cmd.find_first_of('&');
			std::string assembly_name = cmd.substr(idx1+1, cmd.size()-1);

			if (assembly_name == "GDALRasterMapping.exe")
				strcpy(sendBuf, "{GetModelAssembly Notified}F:\\XgeDataMapping\\GDALRasterMapping\\");
			else if (assembly_name == "OGRVectorMapping.exe")
				strcpy(sendBuf, "{GetModelAssembly Notified}F:\\XgeDataMapping\\OGRVectorMapping\\");
			else if (assembly_name == "AI3dModelMapping.exe")
				strcpy(sendBuf, "{GetModelAssembly Notified}F:\\XgeDataMapping\\AI3dModelMapping\\");
			else if (assembly_name == "TauDEM_Path")
				strcpy(sendBuf, "{GetModelAssembly Notified}D:\\OGMS建模环境\\TauDEM5Exe\\");
			else
				strcpy(sendBuf, "{GetModelAssembly Notified}");
			send(sockClient, sendBuf, strlen(sendBuf), 0);

			printf("Get Model Assembly: %s\n", assembly_name.c_str());
		}
	}

	//关闭socket
	closesocket(sockServer);
	closesocket(sockClient);
	WSACleanup();

	getch();
}