#include <stdio.h>
#include <conio.h>
#include <string.h>

#include "gdal.h"
#include "gdal_vrt.h"
#include "ogr_srs_api.h"
#include "cpl_string.h"
#include "cpl_error.h"

void main()
{
	GDALAllRegister();
	CPLSetConfigOption("GDAL_FILENAME_IS_UTF8", "NO");
	std::string version = GDALVersionInfo("RELEASE_NAME");
	printf("version: %s\n",version.c_str());

	int count = GDALGetDriverCount();
	printf("count: %d",count);

	getch();
}