#include "nxdat.h"
#include "nxid.h"
#include "nxdat.mini.api.h"
#include <vector>

class c_byte_stream_write_t:public dx_stream_write_t
{
public:
	c_byte_stream_write_t(std::vector<char>& buf):_buf(buf){
		this->handle = this;
		this->write = write_;
	}
	unsigned write_(void* pbuf,unsigned len)
	{
		char* ch = (char*)pbuf;
		for(int i=0;i<len;i++)
			_buf.push_back(ch[i]);
		return len;
	}
	static unsigned write_(void* pthis,void* pbuf,unsigned le)
	{
		return static_cast<c_byte_stream_write_t*>(pthis)->write_(pbuf,le);
	}
public:
	std::vector<char>& _buf;
};

class c_byte_stream_read_t:public dx_stream_read_t
{
public:
	c_byte_stream_read_t(std::vector<char>& buf):_buf(buf),_it(0){
		this->handle = this;
		this->read = read_;
	}
	unsigned read_(void* pbuf,unsigned len)
	{
		char* ch = (char*)pbuf;
		for(int i=0;i<len;i++,_it++)
			ch[i] = _buf[_it];
		return len;
	}
	static unsigned read_(void* pthis,void* pbuf,unsigned le)
	{
		return static_cast<c_byte_stream_read_t*>(pthis)->read_(pbuf,le);
	}
public:
	int				   _it;
	std::vector<char>& _buf;
};

void dump_dx_node(dx_node_t* node)
{
	dx_object_t* dx = node->dxobject;
	dx_node_t*	 nx = node;
	e_dx_type_t  ty = dx->dxftable->fn_node_get_type(nx);
	nxid_t       name =dx->dxftable->fn_node_get_name(nx);
	printf("\n-------------------\nname=");
	printf(name);printf("\n");
	if(ty==dxint)
	{
		dx_cell_t ce = dx->dxftable->fn_node_get_data_cell(nx);
		printf("int[%d]",ce.length);
		for(int k=0;k<ce.length;k++)
		{
			printf("%d ",ce.iVal[k]);
		}
		printf("\n");
	}
	else if(ty==dxreal)
	{
		dx_cell_t ce = dx->dxftable->fn_node_get_data_cell(nx);
		printf("real[%d]",ce.length);
		for(int k=0;k<ce.length;k++)
		{
			printf("%lf ",ce.rVal[k]);
		}
		printf("\n");
	}

	else if(ty==dxvector2)
	{
		dx_cell_t ce = dx->dxftable->fn_node_get_data_cell(nx);
		printf("vec2[%d]",ce.length);
		for(int k=0;k<ce.length;k++)
		{
			printf("%lf %lf",ce.vec2[k].x,ce.vec2[k].y);
		}
		printf("\n");
	}	
	else if(ty==dxvector3)
	{
		dx_cell_t ce = dx->dxftable->fn_node_get_data_cell(nx);
		printf("vec3[%d]",ce.length);
		for(int k=0;k<ce.length;k++)
		{
			printf("%lf %lf %lf ",ce.vec3[k].x,ce.vec3[k].y,ce.vec3[k].z);
		}
		printf("\n");
	}	
	else if(ty==dxvector4)
	{
		dx_cell_t ce = dx->dxftable->fn_node_get_data_cell(nx);
		printf("vec4[%d]",ce.length);
		for(int k=0;k<ce.length;k++)
		{
			printf("%lf %lf %lf %lf",ce.vec4[k].x,ce.vec4[k].y,ce.vec4[k].z,ce.vec4[k].w);
		}
		printf("\n");
	}
	else if(ty==dxstring)
	{	
		dx_cell_t ce = dx->dxftable->fn_node_get_data_cell(nx);
		printf("string[%d]",ce.length);
		if(ce.length==1)
		{		
			for(int k=0;k<ce.length;k++)
			{
				printf(ce.str);
			}
		}
		else
		{
			for(int k=0;k<ce.length;k++)
			{
				printf(ce.pstr[k]);
			}
		}
	}		
	else if(ty==dxwstring)
	{	
		dx_cell_t ce = dx->dxftable->fn_node_get_data_cell(nx);
		printf("string[%d]",ce.length);
		if(ce.length==1)
		{		
			for(int k=0;k<ce.length;k++)
			{
				wprintf(ce.wstr);
			}
		}
		else
		{
			for(int k=0;k<ce.length;k++)
			{
				wprintf(ce.pwstr[k]);
			}
		}
	}
	else if(ty==dxnode || ty==dxlist || ty==dxmap ||ty==dxtable)
	{
		if(ty==dxnode )
		{
			printf("dxnode....\n");
		}
		else  if(ty==dxlist)
		{
			printf("dxlist....\n");
		}
		else if(ty==dxmap)
		{
			printf("dxmap....\n");
		}
		else if(ty==dxtable)
		{
			printf("dxtable....\n");
		}

		for(int i=0,n=dx->dxftable->fn_node_get_length(nx);i<n;i++)
		{
			dump_dx_node(dx->dxftable->fn_node_get_child(nx,i));
		}
	}
}

int main(int argc ,const char **argv)
{
	const dxftable_t* dxftable = //获取一个dx_datset的实现
		nx_get_dxftable_mini(0);

	//创建数据集
	dx_object_t* dx=dxftable->fn_dx_object_create("",nxid("datset"),dxnode,0);
	{
		dx_node_t*		nxInt=dx->dxftable->fn_node_add_child(dx->node,nxid("int1"),dxint,1);
		dx_cell_t cell= dx->dxftable->fn_node_get_data_cell(nxInt);
		*cell.iVal = 1234;//整数1
	}

	{
		dx_node_t*		nx=dx->dxftable->fn_node_add_child(dx->node,nxid("int2"),dxint,2);
		dx_cell_t cell= dx->dxftable->fn_node_get_data_cell(nx);
		cell.iVal[0] = 1234;//整数1
		cell.iVal[1] = 5678;
	}

	{
		dx_node_t*		nx=dx->dxftable->fn_node_add_child(dx->node,nxid("real1"),dxreal,1);
		dx_cell_t cell= dx->dxftable->fn_node_get_data_cell(nx);
		*cell.rVal = 1234.567;//real 1
	}

	{
		dx_node_t*		nx=dx->dxftable->fn_node_add_child(dx->node,nxid("real2"),dxreal,2);
		dx_cell_t cell= dx->dxftable->fn_node_get_data_cell(nx);
		cell.rVal[0] = 789.1234;//real 1
		cell.rVal[1] = 567.890;	//real 2
	}

	{
		dx_node_t*		nx=dx->dxftable->fn_node_add_child(dx->node,nxid("vector2"),dxvector2,1);
		dx_cell_t cell= dx->dxftable->fn_node_get_data_cell(nx);
		cell.vec2[0].x=1;
		cell.vec2[0].y=2;
	}	
	{
		dx_node_t*		nx=dx->dxftable->fn_node_add_child(dx->node,nxid("vector3"),dxvector3,2);
		dx_cell_t cell= dx->dxftable->fn_node_get_data_cell(nx);
		cell.vec3[0].x=5;
		cell.vec3[0].y=6;
		cell.vec3[0].z=7;

		cell.vec3[1].x=8;
		cell.vec3[1].y=9;
		cell.vec3[1].z=10;
	}

	{
		dx_node_t* nx=dx->dxftable->fn_node_add_child(dx->node,nxid("vector4"),dxvector4,3);
		dx_cell_t  cell= dx->dxftable->fn_node_get_data_cell(nx);
		cell.vec4[0].x=11;
		cell.vec4[0].y=12;
		cell.vec4[0].z=13;
		cell.vec4[0].w=14;

		cell.vec4[1].x=15;
		cell.vec4[1].y=16;
		cell.vec4[1].z=17;
		cell.vec4[1].w=18;

		cell.vec4[2].x=19;
		cell.vec4[2].y=20;
		cell.vec4[2].z=21;
		cell.vec4[2].w=22;
	}

	{
		dx_node_t* nx=dx->dxftable->fn_node_add_child(dx->node,nxid("string"),dxstring,1);
		dx->dxftable->fn_put_string(nx,"hello",0);
	}

	{
		dx_node_t* nx=dx->dxftable->fn_node_add_child(dx->node,nxid("wstring"),dxwstring,2);
		dx->dxftable->fn_put_wstring(nx,L"HELLO ",0);
		dx->dxftable->fn_put_wstring(nx,L"WORLD ",1);
	}
	{
		dx->dxftable->fn_node_add_child(dx->node,nxid("node"),dxnode,0);
		dx->dxftable->fn_node_add_child(dx->node,nxid("list"),dxlist,0);
		dx->dxftable->fn_node_add_child(dx->node,nxid("map"), dxmap,0);
		dx->dxftable->fn_node_add_child(dx->node,nxid("table"),dxtable,0);
	}

	//将数据写入流中
	std::vector<char> streamBuf;
	c_byte_stream_write_t stream(streamBuf);
	dx_write_node_to_xml(&stream,dx->node);
	//dx_write_node_to_json(&stream,dx->node);
	printf("%s", stream._buf.data());
	//FILE* fp = fopen("D:\\testUdxXml1.xml", "w");
	//fprintf(fp, "%s", stream._buf.data());

	//从流中读取
	dx_object_t* dx2 = dxftable->fn_dx_object_create("dx2","dx2",dxnode,0);
	c_byte_stream_read_t stream2(streamBuf);
	dx_read_node_from_xml(&stream2,dx2->node,NULL);


	dump_dx_node(dx2->node);

	dxftable->fn_dx_object_release(dx2);
	dxftable->fn_dx_object_release(dx);

	return 0;
}