#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>
#include "IDataMappingHandle.h"
#include "NativeDataMappingAPI.h"

void main3()
{
	NGIS::DataMapping::IDataMappingHandle* modelHandle = NGIS::DataMapping::createGeneral3dModelHandle();
	modelHandle->InitHandle("D:\\Work\\Plan3D\\lib_32\\dat\\模型\\行业实体\\铁路\\火车\\火车01\\火车01.x", "");

	NGIS::Data::Schema::IUdxDatasetSchema* dataSchema = modelHandle->GetUdxSchema();
	std::string xml_str =""; dataSchema->FormatToXmlStream(xml_str);
	printf("%s\n", xml_str.c_str());

	const char* name = modelHandle->GetHandleName();
	int capabilityCount = modelHandle->GetCapbilityCount();
	printf("%s: Capability Count = %d\n", name, capabilityCount);
	for (int iC=0; iC<capabilityCount; iC++)
	{
		IUdxNodeSchema* temp_node = modelHandle->getCapbilityNode(iC);
		printf("name = %s, kernel = %s\n", temp_node->getName(), 
			SchemaNodeType2String(temp_node->getDescription()->getKernelType()));
	}
	printf("**********************************************************\n");

	NGIS::Data::IUdxDataset* udxDataset = NGIS::Data::createUdxDataset("dataset");
	NGIS::DataMapping::ConstructDataFrame(dataSchema, udxDataset);

	int count = dataSchema->getChildNodeCount();
	for (int iNode=0; iNode<count; iNode++)
	{
		IUdxNodeSchema* tempNode_S = dataSchema->getChildNode(iNode);
		IUdxNode* tempNode_D = udxDataset->getChildNode(iNode);
		modelHandle->ReadToNode(tempNode_S, tempNode_D);
	}

	printf("**********************************************************\n");
	udxDataset->FormatToXmlFile("D:\\Work\\Plan3D\\lib_32\\dat\\模型\\行业实体\\铁路\\火车\\火车01\\test_udx.xml");
	udxDataset->release();

	getch();
}

void main2()
{
	getch();

	NGIS::DataMapping::IDataMappingHandle* vectorOGRHandle = NGIS::DataMapping::createGeneralVectorHandle();
	vectorOGRHandle->InitHandle("D:\\Data\\shapefile\\node.shp","D:\\Data\\shapefile\\node_by_yss.shp");

	NGIS::Data::Schema::IUdxDatasetSchema* dataSchema = vectorOGRHandle->GetUdxSchema();
	std::string xml_str =""; dataSchema->FormatToXmlStream(xml_str);
	printf("%s\n", xml_str.c_str());

	const char* name = vectorOGRHandle->GetHandleName();
	int capabilityCount = vectorOGRHandle->GetCapbilityCount();
	printf("%s: Capability Count = %d\n", name, capabilityCount);
	for (int iC=0; iC<capabilityCount; iC++)
	{
		IUdxNodeSchema* temp_node = vectorOGRHandle->getCapbilityNode(iC);
		printf("name = %s, kernel = %s\n", temp_node->getName(), 
			SchemaNodeType2String(temp_node->getDescription()->getKernelType()));
	}
	printf("**********************************************************\n");

	NGIS::Data::IUdxDataset* udxDataset = NGIS::Data::createUdxDataset("dataset");
	NGIS::DataMapping::ConstructDataFrame(dataSchema, udxDataset);

	IUdxNodeSchema* layerCount_S = dataSchema->getChildNode(0);
	IUdxNode* layerCount_D = udxDataset->getChildNode(0);
	vectorOGRHandle->ReadToNode(layerCount_S, layerCount_D);

	IUdxKernelIntValue* layerCountKernel = (IUdxKernelIntValue*)layerCount_D->getKernel();
	int layerCount = layerCountKernel->getTypedValue();

	for (int iLayer=0; iLayer<layerCount; iLayer++)
	{
		IUdxNode* item_node = udxDataset->getChildNode(1)->getChildNode(0);
		if (iLayer > 0)
		{
			item_node = udxDataset->getChildNode(1)->addChildNode("Layer_Item", EKernelType::EKT_NODE);
		}

		dataSchema->getChildNode(1)->setExtension(iLayer);

		IUdxNodeSchema* layerName_S = dataSchema->getChildNode(1)->getChildNode(0)->getChildNode(0);
		IUdxNode* layerName_D = item_node->getChildNode(0);
		vectorOGRHandle->ReadToNode(layerName_S, layerName_D);

		IUdxNodeSchema* shapeType_S = dataSchema->getChildNode(1)->getChildNode(0)->getChildNode(1);
		IUdxNode* shapeType_D = item_node->getChildNode(1);
		vectorOGRHandle->ReadToNode(shapeType_S, shapeType_D);

		IUdxNodeSchema* coordinateType_S = dataSchema->getChildNode(1)->getChildNode(0)->getChildNode(2);
		IUdxNode* coordinateType_D = item_node->getChildNode(2);
		vectorOGRHandle->ReadToNode(coordinateType_S, coordinateType_D);

		IUdxNodeSchema* featureCollection_S = dataSchema->getChildNode(1)->getChildNode(0)->getChildNode(3);
		IUdxNode* featureCollection_D = item_node->getChildNode(3);
		vectorOGRHandle->ReadToNode(featureCollection_S, featureCollection_D);

		IUdxNodeSchema* attributeTable_S = dataSchema->getChildNode(1)->getChildNode(0)->getChildNode(4);
		IUdxNode* attributeTable_D = item_node->getChildNode(4);
		vectorOGRHandle->ReadToNode(attributeTable_S, attributeTable_D);

		IUdxNodeSchema* spatialRef_S = dataSchema->getChildNode(1)->getChildNode(0)->getChildNode(5);
		IUdxNode* spatialRef_D = item_node->getChildNode(5);
		vectorOGRHandle->ReadToNode(spatialRef_S, spatialRef_D);
	}

	printf("**********************************************************\n");
	udxDataset->FormatToXmlFile("D:\\Data\\shapefile\\test_udx.xml");
	udxDataset->release();

	//////////////////////////////////////////////////////////////////////////
	//
	//
	//
	//////////////////////////////////////////////////////////////////////////

	IUdxDataset* pExistData = createUdxDataset("dataset");
	bool loadFlag = pExistData->LoadFromXmlFile("D:\\Data\\shapefile\\test_udx.xml");
	if (loadFlag==true)
	{
		vectorOGRHandle->BeginWriteToFile();
		int count = pExistData->getChildNodeCount();

		vectorOGRHandle->WriteFromNode(pExistData);

		vectorOGRHandle->EndWriteToFile();
	}
	pExistData->release();



	vectorOGRHandle->release();

	getch();
	return;
}

void main()
{
	printf("**************Test Data Mapping Method**************\n");

	NGIS::DataMapping::IDataMappingHandle* rasterGDALHandle = NGIS::DataMapping::createGeneralRasterHandle();
	rasterGDALHandle->InitHandle("D:\\Data\\elevation\\w001001.adf","D:\\Data\\elevation\\elevation.tif");

	NGIS::Data::Schema::IUdxDatasetSchema* dataSchema = rasterGDALHandle->GetUdxSchema();
	dataSchema->FormatToXmlFile("D:\\Data\\elevation\\test_udx_schema.xml");

	//////////////////////////////////////////////////////////////////////////
	const char* name = rasterGDALHandle->GetHandleName();
	int capabilityCount = rasterGDALHandle->GetCapbilityCount();
	printf("%s: Capability Count = %d\n", name, capabilityCount);
	for (int iC=0; iC<capabilityCount; iC++)
	{
		IUdxNodeSchema* temp_node = rasterGDALHandle->getCapbilityNode(iC);
		printf("name = %s, kernel = %s\n", temp_node->getName(), 
			SchemaNodeType2String(temp_node->getDescription()->getKernelType()));
	}
	printf("**********************************************************\n");

	//////////////////////////////////////////////////////////////////////////
	//
	//! 以下开始读取UDX
	//
	//////////////////////////////////////////////////////////////////////////

	NGIS::Data::IUdxDataset* udxDataset = NGIS::Data::createUdxDataset("dataset");
	NGIS::DataMapping::ConstructDataFrame(dataSchema, udxDataset);

	IUdxNodeSchema* headerNode_S = dataSchema->getChildNode(0);
	IUdxNode* headerNode_D = udxDataset->getChildNode(0);
	int count = headerNode_S->getChildNodeCount();
	for (int iNode=0; iNode<count; iNode++)
	{
		rasterGDALHandle->ReadToNode(headerNode_S->getChildNode(iNode), headerNode_D->getChildNode(iNode));
	}

	//////////////////////////////////////////////////////////////////////////
	IUdxNodeSchema* bandItemNode_S = dataSchema->getChildNode(1)->getChildNode(0);
	IUdxNodeSchema* nodataValueNode_S = bandItemNode_S->getChildNode(0);
	IUdxNodeSchema* scaleNode_S = bandItemNode_S->getChildNode(1);
	IUdxNodeSchema* offsetNode_S = bandItemNode_S->getChildNode(2);
	IUdxNodeSchema* unitTypeNode_S = bandItemNode_S->getChildNode(3);
	IUdxNodeSchema* dataTypeNode_S = bandItemNode_S->getChildNode(4);

	IUdxNode* bandItemNode_D = udxDataset->getChildNode(1)->getChildNode(0);
	IUdxNode* nodataValueNode_D = bandItemNode_D->getChildNode(0);
	IUdxNode* scaleNode_D = bandItemNode_D->getChildNode(1);
	IUdxNode* offsetNode_D = bandItemNode_D->getChildNode(2);
	IUdxNode* unitTypeNode_D = bandItemNode_D->getChildNode(3);
	IUdxNode* dataTypeNode_D = bandItemNode_D->getChildNode(4);

	int node_count = rasterGDALHandle->prepareNeedIndexSettingNode(nodataValueNode_S);
	rasterGDALHandle->getNeedIndexSettingNode(0)->setExtension(0);

	rasterGDALHandle->ReadToNode(nodataValueNode_S, nodataValueNode_D);
	rasterGDALHandle->ReadToNode(scaleNode_S, scaleNode_D);
	rasterGDALHandle->ReadToNode(offsetNode_S, offsetNode_D);
	rasterGDALHandle->ReadToNode(unitTypeNode_S, unitTypeNode_D);
	rasterGDALHandle->ReadToNode(dataTypeNode_S, dataTypeNode_D);

	//////////////////////////////////////////////////////////////////////////
	//for (int i=10; i<12; i++)
	int countflag = 0;
	IUdxNode* heightNode = udxDataset->getChildNode(0)->getChildNode(7);
	IUdxKernelIntValue* heightKernel = (IUdxKernelIntValue*)heightNode->getKernel();
	int rowCount = heightKernel->getTypedValue();
	while(true)
	{
		IUdxNodeSchema* rowItemNode_S = bandItemNode_S->getChildNode(5)->getChildNode(0);
		node_count = rasterGDALHandle->prepareNeedIndexSettingNode(rowItemNode_S);
		rasterGDALHandle->getNeedIndexSettingNode(0)->setExtension(countflag); //第几行
		rasterGDALHandle->getNeedIndexSettingNode(1)->setExtension(0); //第几个波段

		IUdxNode* rowItemNode_D = bandItemNode_D->getChildNode(5)->getChildNode(countflag);
		if (rowItemNode_D==NULL)
		{
			rowItemNode_D = bandItemNode_D->getChildNode(5)->addChildNode("Row_Item", 
				bandItemNode_D->getChildNode(5)->getChildNode(0)->getKernel()->getType());
		}
		bool ret = rasterGDALHandle->ReadToNode(rowItemNode_S, rowItemNode_D);
		//if (ret==false)
		//	break;
		countflag++;
		if (countflag>=rowCount)
			break;
	}

	IUdxNodeSchema* projNode_S = dataSchema->getChildNode(2);
	IUdxNode* projNode_D = udxDataset->getChildNode(2);
	rasterGDALHandle->ReadToNode(projNode_S, projNode_D);

	//////////////////////////////////////////////////////////////////////////
	//std::string xmlStr="";
	//udxDataset->FormatToXmlStream(xmlStr);
	udxDataset->FormatToXmlFile("D:\\Data\\elevation\\test_udx.xml");

	printf("*************************\n");
	//printf(xmlStr.c_str());
	printf("*************************\n");

	printf("\nEnd");


	//////////////////////////////////////////////////////////////////////////
	//
	//
	//
	//////////////////////////////////////////////////////////////////////////
	IUdxDataset* pExistData = createUdxDataset("dataset");
	bool loadFlag = pExistData->LoadFromXmlFile("D:\\Data\\elevation\\test_udx.xml");
	if (loadFlag==true)
	{
		rasterGDALHandle->BeginWriteToFile();
		int count = pExistData->getChildNodeCount();

		rasterGDALHandle->WriteFromNode(pExistData);

		rasterGDALHandle->EndWriteToFile();
	}

	getch();
}