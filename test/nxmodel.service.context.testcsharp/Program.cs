﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using NGIS.Model;
using NGIS.Data;
using NGIS.Data.Schema;

namespace nxmodel.service.context.testcsharp
{
    class Program
    {
        static void Main(string[] args)
        {
            ModelServiceContext pModelServiceContext = new ModelServiceContext();
            ModelDataHandler nativeDataHandler = new ModelDataHandler(pModelServiceContext);

            if (args.Length < 3) return;

            Guid pInstanceID = pModelServiceContext.onInitialize(args[0], args[1], args[2]);
            if (pInstanceID == Guid.Empty)
                Console.WriteLine("Init Failed");

            string pStateId1 = "State1";
            pModelServiceContext.onEnterState(pStateId1);

            string pEventId1 = "event1";
            pModelServiceContext.onFireEvent(pStateId1, pEventId1);

            pModelServiceContext.onRequestData(pStateId1, pEventId1);

            if (pModelServiceContext.getRequestDataFlag() == ERequestResponseDataFlag.ERDF_OK)
            {
                //! 两种用法，一种是文件的，一种是直接加载UDX XML的
                if (pModelServiceContext.getRequestDataMIME() == ERequestResponseDataMIME.ERDM_ZIP_FILE)
                {
                    nativeDataHandler.connectDataMappingMethod("DemoMappingMethod.exe");
                    nativeDataHandler.conductUDXMapping("");
                    string name = nativeDataHandler.getRealResultSaveName();
                }
                else if (pModelServiceContext.getRequestDataMIME() == ERequestResponseDataMIME.ERDM_XML_STREAM)
                {
                    string xml_str = pModelServiceContext.getRequestDataBody();

                    UdxDataset pDataset = new UdxDataset();
                    pDataset.LoadFromXmlStream(xml_str);
                    UdxKernel pKernel1 = pDataset.getChildNode(0).getKernel();
                    UdxKernel pKernel2 = pDataset.getChildNode(1).getKernel();
                    int k1 = ((UdxKernelIntValue)pKernel1).getTypedValue();
                    int k2 = ((UdxKernelIntValue)pKernel2).getTypedValue();
                }
            }

            //////////////////////////////////////////////////////////////////////////
            string file_save_path = pModelServiceContext.getCurrentDataDirectory();
            int a = 12;
            int b = 13;
            int c = a * b;
            string stream = "***********For TEST**********\n";
            stream += "C = " + c.ToString();
            FileStream fs = new FileStream("E:\\test.txt", FileMode.OpenOrCreate);
            StreamWriter sw = new StreamWriter(fs);
            sw.Write(stream);
            sw.Flush();
            sw.Close();
            fs.Close();
            //////////////////////////////////////////////////////////////////////////

            string pEventId2 = "event2";
            pModelServiceContext.onFireEvent(pStateId1, pEventId2);

            pModelServiceContext.setResponseDataFlag(ERequestResponseDataFlag.ERDF_OK);
            pModelServiceContext.setResponseDataMIME(ERequestResponseDataMIME.ERDM_ZIP_FILE);

            List<string> rawFiles = new List<string>();
            rawFiles.Add("E:\\test.txt");
            nativeDataHandler.connectDataMappingMethod("DemoMappingMethod.exe");
            nativeDataHandler.conductFileMapping("test.zip", rawFiles, 1);
            string name1 = nativeDataHandler.getRealResultSaveName();

            pModelServiceContext.setResponseDataBody(name1);

            pModelServiceContext.onResponseData(pStateId1, pEventId1);

            pModelServiceContext.onLeaveState(pStateId1);

            string pStateId2 = "State2";
            pModelServiceContext.onEnterState(pStateId2);

            string pEventId3 = "event1";
            pModelServiceContext.onFireEvent(pStateId2, pEventId3);

            pModelServiceContext.onLeaveState(pStateId2);

            pModelServiceContext.onFinalize();

            //Console.ReadKey();
        }
    }
}
