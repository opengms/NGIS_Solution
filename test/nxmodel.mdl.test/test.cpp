#include <stdio.h>
#include <conio.h>
#include "XgeModelClassApi.h"
#include "UdxDataSchemaApi.h"

void main()
{
	printf("****************************Simple Test***************************\n");

	NGIS::Model::IModelClass* modelClass = NGIS::Model::createModelClass();
	
	modelClass->setName("Test Model");
	modelClass->setUID("{B5031C06-2A44-46FA-B359-ABDB19F5198A}");
	modelClass->setExecutionStyle(NGIS::Model::EExecutionStyle::EES_STATE_SIMULATION);

	//////////////////////////////////////////////////////////////////////////
	//
	// ModelAttribute
	//
	//////////////////////////////////////////////////////////////////////////
	{
		NGIS::Model::IModelAttribute* modelAttribute = modelClass->getModelAttribute();
		NGIS::Model::ModelCategory modelCategory1;
		modelCategory1.principle = "VGE";
		modelCategory1.path = "DEM/TauDEM/BasicParameter";
		modelAttribute->addCategoryInfo(modelCategory1);

		NGIS::Model::LocalAttribute modelLocalAttribute1;
		modelLocalAttribute1.localType = NGIS::Model::ELT_EN_US;
		modelLocalAttribute1.localName = "English Name";
		modelLocalAttribute1.wikiUrl = "http://en.nnu.edu.cn/";
		modelLocalAttribute1.keywords.push_back("Keyword1");
		modelLocalAttribute1.keywords.push_back("Keyword2");
		modelLocalAttribute1.keywords.push_back("Keyword3");
		modelLocalAttribute1.abstractInfo = "Abstract Information";
		modelAttribute->addLocalAttributeInfo(modelLocalAttribute1);

		NGIS::Model::LocalAttribute modelLocalAttribute2;
		modelLocalAttribute2.localType = NGIS::Model::ELT_ZH_CN;
		modelLocalAttribute2.localName = "中文名称";
		modelLocalAttribute2.wikiUrl = "http://www.njnu.edu.cn/";
		modelLocalAttribute2.keywords.push_back("关键字1");
		modelLocalAttribute2.keywords.push_back("关键字2");
		modelLocalAttribute2.keywords.push_back("关键字3");
		modelLocalAttribute2.abstractInfo = "摘要";
		modelAttribute->addLocalAttributeInfo(modelLocalAttribute2);

		//////////////////////////////////////////////////////////////////////////
		modelCategory1.path = "New Path1";
		modelAttribute->updateCategory(0, modelCategory1);

		NGIS::Model::ModelCategory temp_model_cate("Principle", "New Path2");
		modelAttribute->updateCategory(0, temp_model_cate);
	}

	//////////////////////////////////////////////////////////////////////////
	//
	// ModelBehavior
	//
	//////////////////////////////////////////////////////////////////////////
	{
		NGIS::Model::IModelBehavior* modelBehavior = modelClass->getModelBehavior();
		{
			NGIS::Model::ModelDatasetItem modelDatasetItem;
			NGIS::Data::Schema::IUdxDatasetSchema* dataset1 = NGIS::Data::Schema::createUdxDatasetSchema("Init Parameter");
			{
				NGIS::Data::Schema::IUdxNodeSchema* pNode1 = dataset1->addChildNode("node1", NGIS::Data::Schema::ESchemaNodeType::EDTKT_INT, "int");
				NGIS::Data::Schema::IUdxNodeSchema* pNode2 = dataset1->addChildNode("node2", NGIS::Data::Schema::ESchemaNodeType::EDTKT_REAL, "real");
				NGIS::Data::Schema::IUdxNodeSchema* pNode3 = dataset1->addChildNode("node3", NGIS::Data::Schema::ESchemaNodeType::EDTKT_LIST);
				NGIS::Data::Schema::IUdxNodeSchema* pNode4 = dataset1->addChildNode("node4", NGIS::Data::Schema::ESchemaNodeType::EDTKT_NODE);
				NGIS::Data::Schema::IUdxNodeSchema* pNode5 = pNode4->addChildNode("node5", NGIS::Data::Schema::ESchemaNodeType::EDTKT_TABLE, "我也不知道");
				NGIS::Data::Schema::IUdxNodeSchema* pNode5_1 = pNode5->addChildNode("node5_1", NGIS::Data::Schema::ESchemaNodeType::EDTKT_INT|NGIS::Data::Schema::ESchemaNodeType::EDTKT_LIST);
				NGIS::Data::Schema::IUdxNodeSchema* pNode5_2 = pNode5->addChildNode("node5_2", NGIS::Data::Schema::ESchemaNodeType::EDTKT_REAL|NGIS::Data::Schema::ESchemaNodeType::EDTKT_LIST);
				NGIS::Data::Schema::IUdxNodeSchema* pNode5_3 = pNode5->addChildNode("node5_3", NGIS::Data::Schema::ESchemaNodeType::EDTKT_VECTOR2|NGIS::Data::Schema::ESchemaNodeType::EDTKT_LIST);

				pNode5_1->getDescription()->modifyConceptTag("{941671BD-9ED8-4146-979D-10F7C856DF2A}");
				pNode5_1->getDescription()->modifySpatialReferenceTag("{941671BD-9ED8-4146-979D-10F7C856DF2A}");
				pNode5_1->getDescription()->modifyUnitTag("{941671BD-9ED8-4146-979D-10F7C856DF2A}");
				pNode5_1->getDescription()->modifyDataTemplateTag("{941671BD-9ED8-4146-979D-10F7C856DF2A}");
			}
			modelDatasetItem.datasetName = "Related Data1";
			modelDatasetItem.datasetItemType = NGIS::Model::EMDIT_INTERNAL;
			modelDatasetItem.datasetItemDescription = "应该是什么描述";
			modelDatasetItem.externalId = "";
			modelDatasetItem.datasetItem = dataset1;

			modelBehavior->addModelDatasetItem(modelDatasetItem);
			
			//////////////////////////////////////////////////////////////////////////
			NGIS::Model::ModelDatasetItem modelDatasetItem1;
			NGIS::Data::Schema::IUdxDatasetSchema* dataset2 = NGIS::Data::Schema::createUdxDatasetSchema("Parameter1");
			{
				dataset2->addChildNode("Node1", NGIS::Data::Schema::EDTKT_INT, "Param1");
				dataset2->addChildNode("Node2", NGIS::Data::Schema::EDTKT_INT, "Param2");
				NGIS::Data::Schema::IUdxNodeSchema* data_node1 =  dataset2->addChildNode("Node3", NGIS::Data::Schema::EDTKT_TABLE, "ParamTable");
				data_node1->addChildNode("Node3_1", NGIS::Data::Schema::EDTKT_VECTOR4|NGIS::Data::Schema::EDTKT_LIST, "ppp");
				data_node1->addChildNode("Node3_1", NGIS::Data::Schema::EDTKT_VECTOR3|NGIS::Data::Schema::EDTKT_LIST, "ppp");
				data_node1->addChildNode("Node3_1", NGIS::Data::Schema::EDTKT_REAL|NGIS::Data::Schema::EDTKT_LIST, "ppp");
			}
			modelDatasetItem1.datasetName = "Related Data2";
			modelDatasetItem1.datasetItemType = NGIS::Model::EMDIT_INTERNAL;
			modelDatasetItem1.datasetItemDescription = "应该是什么描述1";
			modelDatasetItem1.externalId = "";
			modelDatasetItem1.datasetItem = dataset2;

			modelBehavior->addModelDatasetItem(modelDatasetItem1);

			//////////////////////////////////////////////////////////////////////////
			NGIS::Model::ModelDatasetItem modelDatasetItem2;
			modelDatasetItem2.datasetName = "Related Data3";
			modelDatasetItem2.datasetItemType = NGIS::Model::EMDIT_EXTERNAL;
			modelDatasetItem2.datasetItemDescription = "应该是什么描述1";
			modelDatasetItem2.externalId = "{3CC9478F-252E-4631-BA6B-2C5C5CF94E35}";
			modelDatasetItem2.datasetItem = NULL;

			modelBehavior->addModelDatasetItem(modelDatasetItem2);
		}// end model dataset

		// begin model state
		{
			NGIS::Model::ModelState modelState1;
			modelState1.stateId = "{3DCD4B5D-E097-4269-9C0F-6CDE362F33E7}";
			modelState1.stateName = "Step1";
			modelState1.stateType = NGIS::Model::EMST_BASIC;
			modelState1.stateDecription = "状态的Description";

			NGIS::Model::ModelState modelState2;
			modelState2.stateId = "{81D10275-F549-46D3-8741-48EEFEF45F63}";
			modelState2.stateName = "Step2";
			modelState2.stateType = NGIS::Model::EMST_BASIC;
			modelState2.stateDecription = "状态的Description";

			NGIS::Model::ModelState modelState3;
			modelState3.stateId = "{EF1CA40E-A02E-4EB8-A979-2A6C9B36ED54}";
			modelState3.stateName = "Step3";
			modelState3.stateType = NGIS::Model::EMST_BASIC;
			modelState3.stateDecription = "状态的Description";

			//////////////////////////////////////////////////////////////////////////
			NGIS::Model::ModelEvent event1;
			event1.eventName = "Init Event";
			event1.eventType = NGIS::Model::EMET_RESPONSE;
			event1.eventDescription = "初始化事件";
			event1.datasetReference = "Related Data1";
			event1.parameterDescription = "参数描述";
			modelState1.modelEvents.push_back(event1);

			//////////////////////////////////////////////////////////////////////////
			modelBehavior->addModelState(modelState1);
			modelBehavior->addModelState(modelState2);
			modelBehavior->addModelState(modelState3);

			modelBehavior->addModelStateTransition(modelState1, modelState2);
			modelBehavior->addModelStateTransition(modelState2, modelState3);

			//////////////////////////////////////////////////////////////////////////
			NGIS::Model::ModelState temp_state;
			modelBehavior->getModelState(1, temp_state);
			temp_state.stateName = "New Name";

			NGIS::Model::ModelEvent event2;
			event2.eventName = "Init Event";
			event2.eventType = NGIS::Model::EMET_NORESPONSE;
			event2.eventDescription = "初始化结束事件";
			event2.datasetReference = "Related Data2";
			event2.parameterDescription = "参数描述";
			temp_state.modelEvents.push_back(event2);

			modelBehavior->updateModelState(1, temp_state);

			//////////////////////////////////////////////////////////////////////////
			NGIS::Model::ModelState temp_state1;
			modelBehavior->getModelState(2, temp_state1);
			
			NGIS::Model::ModelEvent event3;
			event3.eventName = "Load Control Param";
			event3.eventType = NGIS::Model::EMET_CONTROL;
			event3.eventDescription = "控制参数";
			event3.parameterDescription = "参数的描述信息";
			NGIS::Model::ModelDatasetItem related_data;
			modelBehavior->getModelDatasetItem(2, related_data);
			event3.datasetReference = related_data.datasetName;
			temp_state1.modelEvents.push_back(event3);

			modelBehavior->updateModelState(2, temp_state1);
		}// end model state
	}

	//////////////////////////////////////////////////////////////////////////
	//
	// ModelRuntime
	//
	//////////////////////////////////////////////////////////////////////////
	{
		NGIS::Model::IModelRuntime* modelRuntime = modelClass->getModelRuntime();
		modelRuntime->setName("model name");
		modelRuntime->setVersion("5.4");
		modelRuntime->setBaseDirectory("$installPath/taudem/");
		modelRuntime->setEntry("slope.exe");

		NGIS::Model::HardwareRequirement hardwareRequirement;
		hardwareRequirement.requirementKey = "Main Frequency";
		hardwareRequirement.requirementValue = "2.8";
		modelRuntime->addHardwareRequirement(hardwareRequirement);

		NGIS::Model::SoftwareRequirement softwareRequirement;
		softwareRequirement.requirementKey = "Memory Size";
		softwareRequirement.requirementValue = "500M";
		modelRuntime->addSoftwareRequirement(softwareRequirement);

		NGIS::Model::ModelAssembly modelAssembly;
		modelAssembly.assemblyName = "dll";
		modelAssembly.assemblyPath = "./mpi.dll";
		modelRuntime->addModelAssembly(modelAssembly);

		NGIS::Model::SupportiveResource modelSupportiveResource;
		modelSupportiveResource.resourceType = "data";
		modelSupportiveResource.resourceName = "./preload data.txt";
		modelRuntime->addSupportiveResource(modelSupportiveResource);
	}

	std::string xml_str = "";
	modelClass->FormatToXmlStream(xml_str);
	printf(xml_str.c_str());
	printf("\n******************************************\n\n");
	modelClass->release();

	//////////////////////////////////////////////////////////////////////////
	NGIS::Model::IModelClass* modelClass1 = NGIS::Model::createModelClass();
	modelClass1->LoadFromXmlStream(xml_str.c_str());

	std::string xml_str1 = "";
	modelClass1->FormatToXmlStream(xml_str1);
	printf(xml_str1.c_str());
	modelClass1->release();

	//////////////////////////////////////////////////////////////////////////
	{
		NGIS::Model::IModelClass* modelClass1 = NGIS::Model::createModelClass();
		modelClass1->LoadFromXmlFile("D:\\Src\\NGIS\\NGIS_Solution\\data\\AspectAnalysis_FrmDiff.mdl");

		NGIS::Model::IModelClass* modelClass2 = NGIS::Model::createModelClass();
		modelClass2->LoadFromXmlFile("D:\\Src\\NGIS\\NGIS_Solution\\data\\AspectAnalysis_FrmDiff1.mdl");

		std::string obj = "";
		std::string name = "";
		bool compareFlag = modelClass1->compareOther(modelClass2, true, obj, name);
		printf("\n%d\n", compareFlag);
		printf("%s\n", obj.c_str());
		printf("%s\n", name.c_str());

	}

	getch();
}