﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NGIS.Data;
using NGIS.Data.Schema;

namespace nxdat.testcs
{
    class Program
    {
        public static void IterateNode(UdxNode pNode)
        {
            String name = pNode.getName();
            EKernelType kernelType = pNode.getKernel().getType();
            UdxKernel kernel = pNode.getKernel();
            Console.Write("\n");
            Console.Write("name={0}\n", name);
            Console.Write("kernelType={0}\n", kernelType.ToString());
            if (kernelType == EKernelType.EKT_INT)
            {
                UdxKernelIntValue realKernel = (UdxKernelIntValue)kernel;
                int val = realKernel.getTypedValue();
                Console.Write("value = {0} \n", val);
            }
            else if (kernelType == EKernelType.EKT_REAL)
            {
                UdxKernelRealValue realKernel = (UdxKernelRealValue)kernel;
                double val = realKernel.getTypedValue();
                Console.Write("value = {0} \n", val);
            }
            else if (kernelType == EKernelType.EKT_STRING)
            {
                UdxKernelStringValue realKernel = (UdxKernelStringValue)kernel;
                String val = realKernel.getTypedValue();
                Console.Write("value = {0} \n", val);
            }
            else if (kernelType == EKernelType.EKT_VECTOR2)
            {
                UdxKernelVector2dValue realKernel = (UdxKernelVector2dValue)kernel;
                Vector2d vec = realKernel.getTypedValue();
                Console.Write("value = {0}, {1} \n", vec.x, vec.y);
            }
            else if (kernelType == EKernelType.EKT_VECTOR3)
            {
                UdxKernelVector3dValue realKernel = (UdxKernelVector3dValue)kernel;
                Vector3d vec = realKernel.getTypedValue();
                Console.Write("value = {0}, {1}, {2} \n", vec.x, vec.y, vec.z);
            }
            else if (kernelType == EKernelType.EKT_VECTOR4)
            {
                UdxKernelVector4dValue realKernel = (UdxKernelVector4dValue)kernel;
                Vector4d vec = realKernel.getTypedValue();
                Console.Write("value = {0}, {1}, {2}, {3} \n", vec.x, vec.y, vec.z, vec.m);
            }
            else if (kernelType == (EKernelType.EKT_INT | EKernelType.EKT_LIST))
            {
                UdxKernelIntArray realKernel = (UdxKernelIntArray)kernel;
                int valueLen = realKernel.getCount();
                for (int iVal = 0; iVal < valueLen; iVal++)
                {
                    int val = realKernel.getTypedValueByIndex(iVal);
                    Console.Write("value = {0} \n", val);
                }
                Console.Write("\n");
            }
            else if (kernelType == (EKernelType.EKT_REAL | EKernelType.EKT_LIST))
            {
                UdxKernelRealArray realKernel = (UdxKernelRealArray)kernel;
                int valueLen = realKernel.getCount();
                for (int iVal = 0; iVal < valueLen; iVal++)
                {
                    double val = realKernel.getTypedValueByIndex(iVal);
                    Console.Write("value = {0} \n", val);
                }
                Console.Write("\n");
            }
            else if (kernelType == (EKernelType.EKT_STRING | EKernelType.EKT_LIST))
            {
                UdxKernelStringArray realKernel = (UdxKernelStringArray)kernel;
                int valueLen = realKernel.getCount();
                for (int iVal = 0; iVal < valueLen; iVal++)
                {
                    String val = realKernel.getTypedValueByIndex(iVal);
                    Console.Write("value = {0} \n", val);
                }
                Console.Write("\n");
            }
            else if (kernelType == (EKernelType.EKT_VECTOR2 | EKernelType.EKT_LIST))
            {
                UdxKernelVector2dArray realKernel = (UdxKernelVector2dArray)kernel;
                int valueLen = realKernel.getCount();
                for (int iVal = 0; iVal < valueLen; iVal++)
                {
                    Vector2d vec = realKernel.getTypedValueByIndex(iVal);
                    if (vec != null)
                        Console.Write("value = {0}, {1}; \n", vec.x, vec.y);
                }
                Console.Write("\n");
            }
            else if (kernelType == (EKernelType.EKT_VECTOR3 | EKernelType.EKT_LIST))
            {
                UdxKernelVector3dArray realKernel = (UdxKernelVector3dArray)kernel;
                int valueLen = realKernel.getCount();
                for (int iVal = 0; iVal < valueLen; iVal++)
                {
                    Vector3d vec = realKernel.getTypedValueByIndex(iVal);
                    if (vec != null)
                        Console.Write("value = {0}, {1}, {2}; \n", vec.x, vec.y, vec.z);
                }
                Console.Write("\n");
            }
            else if (kernelType == (EKernelType.EKT_VECTOR4 | EKernelType.EKT_LIST))
            {
                UdxKernelVector4dArray realKernel = (UdxKernelVector4dArray)kernel;
                int valueLen = realKernel.getCount();
                for (int iVal = 0; iVal < valueLen; iVal++)
                {
                    Vector4d vec = realKernel.getTypedValueByIndex(iVal);
                    if (vec != null)
                        Console.Write("value ={0}, {1}, {2}, {3}; \n", vec.x, vec.y, vec.z, vec.m);
                }
                Console.Write("\n");
            }
            else if (kernelType == EKernelType.EKT_NODE ||
                       kernelType == EKernelType.EKT_LIST ||
                       kernelType == EKernelType.EKT_MAP ||
                       kernelType == EKernelType.EKT_TABLE)
            {
                int nodeCount = pNode.getChildNodeCount();
                for (int iNode = 0; iNode < nodeCount; iNode++)
                {
                    IterateNode(pNode.getChildNode(iNode));
                }
            }
            //////////////////////////////////////////////////////////////////////////
        }

        public static void genTestUdx()
        {
            //int version = UdxHandleApi.getVersion();
            //Console.WriteLine(version);

            UdxDataset udxDataset = new UdxDataset();
            udxDataset.addChildNode("Node1", EKernelType.EKT_INT);
            udxDataset.addChildNode("Node2", EKernelType.EKT_NODE);

            UdxNode firstNode = udxDataset.getChildNode(0);
            {
                ((UdxKernelIntValue)firstNode.getKernel()).setTypedValue(1234);
            }

            UdxNode secondNode = udxDataset.getChildNode(1);
            {
                UdxNode realValueNode = secondNode.addChildNode("我是Node2_node1", EKernelType.EKT_REAL);
                ((UdxKernelRealValue)realValueNode.getKernel()).setTypedValue(10.234);

                //////////////////////////////////////////////////////////////////////////
                UdxNode stringValueNode = secondNode.addChildNode("Node2_node2", EKernelType.EKT_STRING);
                ((UdxKernelStringValue)stringValueNode.getKernel()).setTypedValue("woshishui我是谁");

                //////////////////////////////////////////////////////////////////////////
                UdxNode vector4dArrayNode = secondNode.addChildNode("Node2_node3", EKernelType.EKT_VECTOR4|EKernelType.EKT_LIST);
                UdxKernelVector4dArray realKernel = (UdxKernelVector4dArray)vector4dArrayNode.getKernel();
                realKernel.addTypedValue(1.0, 2.0, 3.0, 4.0);
                realKernel.addTypedValue(5.0, 6.0, 7.0, 8.0);
                realKernel.addTypedValue(9.0, 10.0, 11.0, 12.0);
                realKernel.addTypedValue(13.0, 14.0, 15.0, 16.0);
            }

            /////////////////////////////////////////////////////////////

            for (int iNode = 0; iNode < udxDataset.getChildNodeCount(); iNode++)
            {
                IterateNode(udxDataset.getChildNode(iNode));
            }

            udxDataset.FormatToXmlFile("D:\\testUdxInCSharp.xml");
        }

        public static void LoadUdx()
        {
            //int version = UdxHandleApi.getVersion();
            //Console.WriteLine(version);

            UdxDataset udxDataset = new UdxDataset();
            udxDataset.LoadFromXmlFile("D:\\testUdxInCSharp.xml");

            for (int iNode = 0; iNode < udxDataset.getChildNodeCount(); iNode++)
            {
                IterateNode(udxDataset.getChildNode(iNode));
            }

            udxDataset.FormatToXmlFile("D:\\testUdxInCSharp1.xml");
            String formated_xml_str = udxDataset.FormatToXmlStream();
            Console.Write(formated_xml_str);
            int len = formated_xml_str.Length;
            Console.WriteLine(len);
        }

        public static void createUdxData() //创建UDX Data
        {
            {
                UdxDataset dataset = new UdxDataset();
                UdxNode node1 = dataset.addChildNode("node1", EKernelType.EKT_INT);
                ((UdxKernelIntValue)(node1.getKernel())).setTypedValue(1234);

                int count = dataset.getChildNodeCount();
                Console.WriteLine("rootnode childnode count: " + count.ToString());

                string xml_str = dataset.FormatToXmlStream();
                dataset.FormatToXmlFile("E:\\Demo.xml");
                Console.Write(xml_str);
            }

            {
                UdxDataset dataset = new UdxDataset();
                UdxNode node1 = dataset.addChildNode("node1", EKernelType.EKT_INT);
                ((UdxKernelIntValue)(node1.getKernel())).setTypedValue(1234);
                UdxNode node2 = dataset.addChildNode("node1", EKernelType.EKT_REAL);
                ((UdxKernelRealValue)(node2.getKernel())).setTypedValue(12.34);

                int count = dataset.getChildNodeCount();
                Console.WriteLine("rootnode childnode count: " + count.ToString());

                string xml_str = dataset.FormatToXmlStream();
                dataset.FormatToXmlFile("E:\\Demo.xml");
                Console.Write(xml_str);
            }
        }

        /////////////////////////////////////////////////////////////////////////////
        public static void createUdxDeclaration() //创建UDX Schema
        {
            UdxDatasetSchema pDataset = new UdxDatasetSchema(null, "UdxDeclaration");
            UdxNodeSchema pNode1 = pDataset.addChildNode("node1", ESchemaNodeType.EDTKT_INT, "ddd");
            UdxNodeSchema pNode2 = pDataset.addChildNode("node2", ESchemaNodeType.EDTKT_REAL, "fff");
            UdxNodeSchema pNode3 = pDataset.addChildNode("node3", ESchemaNodeType.EDTKT_NODE, "啥事");

            UdxNodeSchema pNode3_1 = pNode3.addChildNode("node3_1", ESchemaNodeType.EDTKT_TABLE, "aaa");
            UdxNodeSchema pNode3_1_1 = pNode3_1.addChildNode("node3_1_1", ESchemaNodeType.EDTKT_INT | ESchemaNodeType.EDTKT_LIST, "kkk");
            UdxNodeSchema pNode3_1_2 = pNode3_1.addChildNode("node3_1_2", ESchemaNodeType.EDTKT_REAL | ESchemaNodeType.EDTKT_LIST, "kkk");
            UdxNodeSchema pNode3_1_3 = pNode3_1.addChildNode("node3_1_3", ESchemaNodeType.EDTKT_STRING | ESchemaNodeType.EDTKT_LIST, "ttt");

            string xmlStr = "";
            pDataset.FormatToXmlStream(ref xmlStr);
            Console.Write(xmlStr);

            pDataset.FormatToXmlFile("D:\\TestUdxSchemaInCShape.xml");
        }

        static void Main(string[] args)
        {
            //genTestUdx();

            createUdxDeclaration();

            //createUdxData();

            //IterateNode()
            //LoadUdx();

            Console.ReadLine();
        }
    }
}
