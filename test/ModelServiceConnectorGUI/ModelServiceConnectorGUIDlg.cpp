
// ModelServiceConnectorGUIDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "ModelServiceConnectorGUI.h"
#include "ModelServiceConnectorGUIDlg.h"
#include "afxdialogex.h"

#include "XgeModelServiceApi.h"

#include <winsock2.h>

#pragma comment(lib,"ws2_32.lib")
#define MAX_LENGTH 2048

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CModelServiceConnectorGUIDlg 对话框




CModelServiceConnectorGUIDlg::CModelServiceConnectorGUIDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CModelServiceConnectorGUIDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CModelServiceConnectorGUIDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_MODELFULLNAME, mEdit_ModelPath);
	DDX_Control(pDX, IDC_EDIT2, mEdit_Message);
}

BEGIN_MESSAGE_MAP(CModelServiceConnectorGUIDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON_OPENMODEL, &CModelServiceConnectorGUIDlg::OnBnClickedButtonOpenmodel)
	ON_BN_CLICKED(IDC_BUTTON_START, &CModelServiceConnectorGUIDlg::OnBnClickedButtonStart)
	ON_BN_CLICKED(IDC_BUTTON_PAUSE, &CModelServiceConnectorGUIDlg::OnBnClickedButtonPause)
	ON_BN_CLICKED(IDC_BUTTON_STOP, &CModelServiceConnectorGUIDlg::OnBnClickedButtonStop)
END_MESSAGE_MAP()


// CModelServiceConnectorGUIDlg 消息处理程序

BOOL CModelServiceConnectorGUIDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CModelServiceConnectorGUIDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CModelServiceConnectorGUIDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CModelServiceConnectorGUIDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CModelServiceConnectorGUIDlg::OnBnClickedButtonOpenmodel()
{
	// TODO: 在此添加控件通知处理程序代码
	TCHAR szFilter[] = _T("可执行文件(*.exe)|*.exe|Jar文件(*.jar)|*.jar||");   
	// 构造打开文件对话框   
	CFileDialog fileDlg(TRUE, _T("exe"), NULL, 0, szFilter, this);   
	CString strFilePath;   

	// 显示打开文件对话框   
	if (IDOK == fileDlg.DoModal())   
	{   
		// 如果点击了文件对话框上的“打开”按钮，则将选择的文件路径显示到编辑框里   
		strFilePath = fileDlg.GetPathName();   
		mEdit_ModelPath.SetWindowText(strFilePath);
	}
}

std::string genGUID()
{
	static char buf[64] = {0};  
	GUID guid;  
	if (S_OK == ::CoCreateGuid(&guid))  
	{
		_snprintf(buf, sizeof(buf)  
			, "%08X-%04X-%04x-%02X%02X-%02X%02X%02X%02X%02X%02X"  
			, guid.Data1  
			, guid.Data2  
			, guid.Data3  
			, guid.Data4[0], guid.Data4[1]  
		, guid.Data4[2], guid.Data4[3], guid.Data4[4], guid.Data4[5]  
		, guid.Data4[6], guid.Data4[7]  
		);  
	}
	return buf;
}

bool startANewProcess(const char* cmd_str, const char* workingPath_str)
{
#ifdef _WINDOWS_
	std::string cmd = cmd_str;
	std::string workingPath = workingPath_str;

	char sSysDir[MAX_PATH] = {0};  
	GetSystemDirectory(sSysDir, MAX_PATH);  
	std::string strFullPath = sSysDir;  
	strFullPath += ":\\cmd.exe";  
	std::string strCmdLine = " /C "; 
	strCmdLine += cmd;

	STARTUPINFO StartInfo; 
	PROCESS_INFORMATION pinfo; 

	memset(&StartInfo,0,sizeof(STARTUPINFO)); //对程序的启动信息不作任何设定，全部清0 
	StartInfo.cb = sizeof(STARTUPINFO);//设定结构的大小 
	StartInfo.dwFlags = STARTF_USESHOWWINDOW;  
	StartInfo.wShowWindow = SW_SHOW;

	BOOL ret=CreateProcess(NULL, (LPSTR)cmd.c_str(), NULL,NULL,false,
		NORMAL_PRIORITY_CLASS,NULL, 
		(LPSTR)workingPath.c_str(),
		&StartInfo,&pinfo);

	DWORD dwExitCode;  
	if(ret)  
	{
		CloseHandle(pinfo.hThread);  
		WaitForSingleObject(pinfo.hProcess,INFINITE);  
		GetExitCodeProcess(pinfo.hProcess, &dwExitCode);  
		CloseHandle(pinfo.hProcess);  
		return true;
	}
	else
	{
		return false;
	}
#else

	return false;
#endif
}



void CModelServiceConnectorGUIDlg::OnBnClickedButtonStart()
{
	// TODO: 在此添加控件通知处理程序代码
	CString filePath("");
	mEdit_ModelPath.GetWindowText(filePath);

	std::string uid = genGUID();

	std::string model_file_path = filePath.GetBuffer();
	model_file_path += " 127.0.0.1";
	model_file_path += " 6000 ";
	model_file_path += uid;

	appendMessage("Start\n");
	startServer();

	startANewProcess(model_file_path.c_str(), "");
}


void CModelServiceConnectorGUIDlg::OnBnClickedButtonPause()
{
	// TODO: 在此添加控件通知处理程序代码
}


void CModelServiceConnectorGUIDlg::OnBnClickedButtonStop()
{
	// TODO: 在此添加控件通知处理程序代码
}

void CModelServiceConnectorGUIDlg::startServer()
{
	WSADATA wsaData;
	SOCKET sockServer;
	SOCKADDR_IN addrServer;
	SOCKET sockClient;
	SOCKADDR_IN addrClient;
	WSAStartup(MAKEWORD(2,2),&wsaData);
	sockServer=socket(AF_INET,SOCK_STREAM,0);
	addrServer.sin_addr.S_un.S_addr=htonl(INADDR_ANY);//INADDR_ANY表示任何IP
	addrServer.sin_family=AF_INET;
	addrServer.sin_port=htons(6000);//绑定端口6000
	bind(sockServer,(SOCKADDR*)&addrServer,sizeof(SOCKADDR));

	//Listen监听端
	listen(sockServer,5);//5为等待连接数目
	appendMessage("服务器已启动:\n监听中...\n");
	int len=sizeof(SOCKADDR);

	//会阻塞进程，直到有客户端连接上来为止
	sockClient=accept(sockServer,(SOCKADDR*)&addrClient,&len);

	char recvBuf[MAX_LENGTH];//接受客户端返回的字符串
	char sendBuf[MAX_LENGTH];//发送至客户端的字符串

	strcpy(sendBuf, "{Connected}");
	while(true)
	{
		//接收并打印客户端数据
		int retVal = recv(sockClient,recvBuf,MAX_LENGTH,0);
		if (SOCKET_ERROR == retVal)  
		{  
			appendMessage("recv failed!\n");
			break;
		}

		std::string receiveStr(recvBuf);
		int idx1 = receiveStr.find_first_of('{');
		int idx2 = receiveStr.find_first_of('}');
		std::string op = receiveStr.substr(idx1+1, idx2-idx1-1);
		std::string cmd = receiveStr.substr(idx2+1, receiveStr.size()-idx2);

		appendMessage("**********\n");
		appendMessage(receiveStr.c_str()); appendMessage("\n");
		appendMessage("**********\n");

		if (op == "init")
		{
			std::string sendStr = "{Initialized}";
			sendStr += cmd;
			sendStr += "[F:\\XgeDataMapping\\CommonShell\\x64]";
			sendStr += "[F:\\XgeModelService\\ModelName1\\instance\\B5E63F73-9A72-479D-A1FC-60E04304B75B]";
			strcpy(sendBuf, sendStr.c_str());
			send(sockClient, sendBuf, strlen(sendBuf), 0);
			appendMessage("Initialize\n");
		}
		else if (op == "onEnterState")
		{
			strcpy(sendBuf, "{Enter State Notified}");
			send(sockClient, sendBuf, strlen(sendBuf), 0);
			appendMessage("Enter State\n");
		}
		else if(op == "onFireEvent")
		{
			strcpy(sendBuf, "{Fire Event Notified}");
			send(sockClient, sendBuf, strlen(sendBuf), 0);
			appendMessage("Fire Event\n");
		}
		else if(op == "onRequestData")
		{
			strcpy(sendBuf, "{Request Data Notified}[OK][ZIP|FILE]E:\\test_udx\\data\\udx_zip_data.zip");
			send(sockClient, sendBuf, strlen(sendBuf), 0);
			appendMessage("Request Data\n");
		}
		else if(op == "onResponseData")
		{
			idx1 = cmd.find_first_of('[');
			idx2 = cmd.find_first_of(']');
			std::string flag = cmd.substr(idx1+1, idx2-idx1-1);
			idx1 = cmd.find_last_of('[');
			idx2 = cmd.find_last_of(']');
			std::string mime = cmd.substr(idx1+1, idx2-idx1-1);
			idx1 = 0;
			idx2 = cmd.find_first_of('[');
			std::string queryStr = cmd.substr(idx1, idx2);

			idx1 = queryStr.find_last_of('&');
			std::string sizeStr = queryStr.substr(idx1+1, queryStr.size()-1);
			int dataSize = atoi(sizeStr.c_str());

			strcpy(sendBuf, "{Response Data Notified}");
			send(sockClient, sendBuf, strlen(sendBuf), 0);

			appendMessage("Response Data Content: ");
			appendMessage(cmd);
			appendMessage("\n");
			appendMessage("Response Data End\n");
		}
		else if(op == "onPostErrorInfo")
		{
			strcpy(sendBuf, "{Post Error Info Notified}");
			send(sockClient, sendBuf, strlen(sendBuf), 0);
			appendMessage("Error Info Content: %s\n");
			appendMessage(cmd);
			appendMessage("\n");
			appendMessage("Post Error Info\n");
		}
		else if(op == "onLeaveState")
		{
			strcpy(sendBuf, "{Leave State Notified}");
			send(sockClient, sendBuf, strlen(sendBuf), 0);
			appendMessage("Leave State\n");
		}
		else if(op == "onFinalize")
		{
			strcpy(sendBuf, "{Finalize Notified}");
			send(sockClient, sendBuf, strlen(sendBuf), 0);
			appendMessage("Finalize\n");
		}
		else if(op == "onGetDataMappingMethod")
		{
			//TODO 从MDL里面读取Assembly中相应条目的路径
			idx1 = cmd.find_first_of('&');
			std::string method_name = cmd.substr(idx1+1, cmd.size()-1);

			if (method_name == "GDALRasterMapping.exe")
				strcpy(sendBuf, "{GetDataMappingMethod Notified}F:\\XgeDataMapping\\GDALRasterMapping\\");
			else if (method_name == "OGRVectorMapping.exe")
				strcpy(sendBuf, "{GetDataMappingMethod Notified}F:\\XgeDataMapping\\OGRVectorMapping\\");
			else if (method_name == "AI3dModelMapping.exe")
				strcpy(sendBuf, "{GetDataMappingMethod Notified}F:\\XgeDataMapping\\AI3dModelMapping\\");
			send(sockClient, sendBuf, strlen(sendBuf), 0);
			appendMessage("GetDataMappingMethod: ");
			appendMessage(method_name);
			appendMessage("\n");
		}
	}

	//关闭socket
	closesocket(sockServer);
	closesocket(sockClient);
	WSACleanup();
}

void CModelServiceConnectorGUIDlg::appendMessage( std::string str )
{
	CString info(str.c_str());
	mEdit_Message.SetWindowText(info);
}
