#ifndef MAPNIK_UTIL_CONVERSIONS_HPP
#define MAPNIK_UTIL_CONVERSIONS_HPP

#include "xml_color.hpp"

// stl
#include <string>
#include <algorithm>

namespace mp {

	namespace xml {

		bool string2bool(std::string const& value, bool & result);
		bool string2bool(const char * str, bool & result);

		bool string2color(std::string const& value, xml_color & result);
		bool string2color(const char * str, xml_color & result);

		bool string2uint(std::string const& value, unsigned & result);
		bool string2uint(const char * str, unsigned & result);

		bool string2int(std::string const& value, int & result);
		bool string2int(const char * str, int & result);

		bool string2float(std::string const& value, float & result);
		bool string2float(const char * str, float & result);

		bool string2double(std::string const& value, double & result);
		bool string2double(const char * str, double & result);

		bool to_string(std::string & str, int value);
		bool to_string(std::string & str, unsigned value);
		bool to_string(std::string & str, bool value);
		bool to_string(std::string & str, double value);

#ifdef _WINDOWS
		std::string utf16_to_utf8(std::wstring const& wstr);
		std::wstring utf8_to_utf16(std::string const& str);
#endif
	}
}

#endif // MAPNIK_UTIL_CONVERSIONS_HPP
