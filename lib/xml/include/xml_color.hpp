#ifndef MAPNIK_XML_COLOR_H
#define MAPNIK_XML_COLOR_H

namespace mp
{
	namespace xml
	{
		struct xml_color
		{
			unsigned c;
			xml_color() :c(0) { }
			xml_color(unsigned char r,unsigned char g,unsigned char b,unsigned char a=255)
			:c(((a & 0xff)<<24) | ((r & 0xff)<<16) | ((g & 0xff)<<8) | (b & 0xff))
			{ }
		};
	}
} 

#endif // MAPNIK_XML_TREE_H
