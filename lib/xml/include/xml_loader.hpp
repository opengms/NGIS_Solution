#ifndef XML_LOADER_HPP
#define XML_LOADER_HPP

// stl
#include <string>

namespace mp
{
	namespace xml
	{
		class xml_node;
		bool read_xml(std::string const & filename, xml_node &node);
		bool read_xml_string(std::string const & str, xml_node &node, std::string const & base_path="");
	}
}
#endif // MAPNIK_LIBXML2_LOADER_HPP
